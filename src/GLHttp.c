/*
 * HttpReqGL.c
 *
 *  Created on: Feb 12, 2018
 *      Author: Hartaji
 */

#include "GLHttp.h"
#include "uib_app_manager.h"
#include "dlog.h"
#include <app_preference.h>
struct MemoryStruct {
  char *memory;
  size_t size;
};struct MemoryStruct chunk;

uib_GLChoose_view_context *Vc;
char *url;
bool animate=true;

void generateGenListItem(){

}

size_t WriteGLCallback(char *contents, size_t size, size_t nmemb, void *userp)
{

	int resultJSON;

	animate=false;

	if(contents==""){
		elm_object_text_set(Vc->loadingLabel,"<br><br><font_size=30><align=0.5 0.5><color=#FA1414><b>FAILED</b></color><br><b>server returned 0</b></align></font_size>");
		return 0;
	}

	setList(contents,&resultJSON);

	if(resultJSON>0){

		//((std::string*)userp)->append((char*)contents, size * nmemb);
		evas_object_hide(Vc->progressbar1);
		evas_object_show(Vc->GLList);
		//evas_object_show(Vc->GLNext);
		evas_object_hide(Vc->loadingLabel);
	}else{
		elm_object_text_set(Vc->loadingLabel,"<br><br><font_size=30><align=0.5 0.5><color=#FAD014><b>FAILED</b></color><br><b>No GL Data In Server</b></align></font_size>");
	}
    return size * nmemb;
}

void initGLList(uib_GLChoose_view_context *vc,char *URL){
	url=URL;
	Vc=vc;

	evas_object_hide(vc->GLList);
	evas_object_show(vc->progressbar1);
	//evas_object_hide(vc->GLNext);

	elm_object_style_set((Elm_Progressbar*)vc->progressbar1, "pending_list");
	elm_object_text_set(vc->progressbar1, "Loading GL list..");

	elm_progressbar_pulse_set((Elm_Progressbar*)vc->progressbar1, EINA_TRUE);
	elm_progressbar_pulse((Elm_Progressbar*)vc->progressbar1, EINA_TRUE);

	eext_circle_object_value_min_max_set(vc->progressbar1,0.0,100.0);
	eext_circle_object_value_set(vc->progressbar1,-1);

}

void animatePB(){
	int i=0;
	while(animate==true){
		usleep(600);
		//eext_circle_object_value_set(Vc->progressbar1,i);

		if(i%3==0)
			eext_circle_object_color_set(Vc->progressbar1,237,169,21,1);
		if(i%3!=0)
			eext_circle_object_color_set(Vc->progressbar1,21,190,23,1);

		if(i==100){
			i=0;
		}else{
			i++;
		}
	}
}

Ecore_Thread_Cb getAndSetGLList(Ecore_Thread *thread){
	//ecore_thread_run(animatePB,NULL,NULL,NULL);
	sleep(2);
	//list=GLlist;
	bool *checkWIFI;//, check2;

		//system_info_get_platform_bool ("http://tizen.org/feature/network.wifi", &checkWIFI);
		//system_info_get_platform_bool ("http://tizen.org/feature/network.telephony", &check2);

		//if(checkWIFI==false)
			//return;

		CURL *curl;
		CURLcode curl_err;
		curl = curl_easy_init();
		char *readBuffer;

		if(!curl){
			dlog_print(DLOG_ERROR, "CURL INIT", "CURL INIT GAGAL");
			return thread;
		}

		connection_h connection;
		int conn_err;
		conn_err = connection_create(&connection);
		if (conn_err != CONNECTION_ERROR_NONE) {
		    /* Error handling */

		    return thread;
		}

		curl_easy_setopt(curl, CURLOPT_URL, url);

		curl_easy_setopt(curl, CURLOPT_HTTPGET, true);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteGLCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

		curl_err = curl_easy_perform(curl);
		if (curl_err != CURLE_OK){
			endCurlGLConnection(connection,curl);
			return thread;
		}

		endCurlGLConnection(connection,curl);

		return thread;
}


void endCurlGLConnection(connection_h connection,CURL *curl){

	if(curl!=NULL){
		curl_easy_cleanup(curl);
	}

	if(connection!=NULL){
		connection_unset_proxy_address_changed_cb(connection);
		connection_destroy(connection);
	}

	evas_object_hide(Vc->progressbar1);
	animate=false;
}

void setList(char *data, int *resultCount){

	JsonParser *parser;
	JsonNode *root;
	GError *error;

    int bufSize = 100;
	char *Content=malloc(bufSize);
	char *StrbodiesNumber=malloc(bufSize);
	char *Strhour=malloc(bufSize);
	char *StrJobsName=malloc(bufSize);

	gsize size =strlen(data);
	parser = json_parser_new();
	gboolean Bresult = json_parser_load_from_data(parser,(gchar *)data,size,&error);//file(parser, get_write_filepath("notifJSON"), &error);

	if(Bresult){

		 int mallocatedID=0;
		 int mallocatedCaption=0;
		 int mallocatedName=0;

	     root = json_parser_get_root(parser);

	     //GET BODIES JSON ARRAY
	     JsonArray *bodies = json_node_get_array(root);
	     int leng_bodies=json_array_get_length(bodies);

	     if(leng_bodies>0){
	    	 create_genlist_item(Vc->GLList, "title" , _UIB_LOCALE("Group Leaders"), _UIB_LOCALE("Sub Text"), " "," ", " "," ", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

			 for(int i=0;i<leng_bodies;i++){

				 JsonObject *body=json_array_get_object_element(bodies,i);
				 const int ID = json_object_get_int_member(body, "ID");
				 const gchar *Caption = json_object_get_string_member(body, "Caption");
				 const gchar *Name = json_object_get_string_member(body, "Name");

				 char *caption=malloc(1+strlen(Caption));
				 sprintf(caption,"%s",Caption);

				 char *name=malloc(24+strlen(Name));
				 sprintf(name,"<color=#5494f9>%s</color>",Name);

				 create_genlist_item(Vc->GLList, "1text" , _UIB_LOCALE(caption), _UIB_LOCALE(name), "","", "","", NULL, ELM_GENLIST_ITEM_NONE, _selectedGL, Caption);

			 }

	     //if(leng_bodies>0)
	    	 create_genlist_item(Vc->GLList, "1text" , _UIB_LOCALE(" "), _UIB_LOCALE(" "), "","", "","", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
	     }
	     else
	     {
	    	 create_genlist_item(Vc->GLList, "title" , _UIB_LOCALE("No line code"), _UIB_LOCALE("Sub Text"), " "," ", " "," ", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
	     }
	 	*resultCount=leng_bodies;

	}

}

static void _selectedGL(char *data, Evas_Object *obj, void *event_info)
{
    //printf("item(%d) is selected", (int)data);
    dlog_print(DLOG_ERROR,"GL SELECTED","ID GL : %s",data);
    preference_set_string("LINE_CD", data);
}



