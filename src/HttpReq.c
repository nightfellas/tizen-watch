/*
 * HttpReq.cpp
 *
 *  Created on: Feb 8, 2018
 *      Author: Hartaji
 *
 *  Modifided on : Apr 19, 2018
 *  	Author: yusuf
 *  	function: jsonParsing
 */

#include <HttpReq.h>
#include "app.h"
#include "app_preference.h"

char *URL="";

char *URLResponseRead="";
int sendResponseRead=0;
NotifData *notifData;

char *NotifCaption[250];
char *NotifDescriptions;
int countDeffected=0;

struct MemoryStruct {
  char *memory;
  size_t size;
};struct MemoryStruct chunk;

void setURL(char *url){
	URL=url;
}

void setURLResponseRead(char *url){
	URLResponseRead=url;
}

Ecore_Thread_Cb threadMain(Ecore_Thread *thread){

	//notifData = (NotifData*) calloc (1, sizeof(NotifData));

	NotifCaption==malloc(30);
	NotifDescriptions=malloc(90);
	initdb();
	curl_global_init (CURL_GLOBAL_ALL);
	//setURI(URL);

	chunk.memory = malloc(1);
	chunk.size = 0;

	while(true){
		countDeffected=0;
		sleep(2);
		CurlRequestHTTP();
		//request();
		//dlog_print(DLOG_ERROR, "HTTP REQ", "threadMain");

		sleep(3);
	}

}

size_t WriteCallback(char *contents, size_t size, size_t nmemb, void *userp)
{

	if(sendResponseRead==0 && contents!=NULL){
		char *resultJSON;
		jsonParsing(contents,&resultJSON);

		if(resultJSON!=NULL && countDeffected>0){
			//write_file("notifJS",resultJSON);
			//Insert(resultJSON);
			sendResponseRead=1;
			ShowNotif(NotifCaption,"",false);
			CurlRequestHTTP();
		}

	}else{
		sendResponseRead=0;
	}

    //((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

void HTTPReq(){
	//request();
}

void CurlRequestHTTP()
{
	bool checkWIFI;//, check2;

	system_info_get_platform_bool ("http://tizen.org/feature/network.wifi", &checkWIFI);
	//system_info_get_platform_bool ("http://tizen.org/feature/network.telephony", &check2);

	//if(checkWIFI==false)
		//return;
	preference_get_string("URL", &URL);
	CURL *curl;
	CURLcode curl_err;
	curl = curl_easy_init();
	char *readBuffer;

	if(!curl){
		dlog_print(DLOG_ERROR, "CURL INIT", "CURL INIT GAGAL");
		return;
	}

	connection_h connection;
	int conn_err;
	conn_err = connection_create(&connection);
	if (conn_err != CONNECTION_ERROR_NONE) {
	    /* Error handling */

	    return;
	}

	if(sendResponseRead==0){
		curl_easy_setopt(curl, CURLOPT_URL, URL);
	}else{
		curl_easy_setopt(curl, CURLOPT_URL, URLResponseRead);
	}

	curl_easy_setopt(curl, CURLOPT_HTTPGET, true);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

	curl_err = curl_easy_perform(curl);
	if (curl_err != CURLE_OK){
		endCurlConnection(connection,curl);
		return;
	}

	endCurlConnection(connection,curl);

}


void jsonParsing(char *data, char *result[100000]){

	JsonParser *parser;
	JsonNode *root;
	GError *error;

    int bufSize = 100;

	char *StrbodiesNumber=malloc(bufSize);
	char *Strhour=malloc(bufSize);
	char *StrJobsName=malloc(bufSize);

	gsize size =strlen(data);
	parser = json_parser_new();
	gboolean Bresult = json_parser_load_from_data(parser,(gchar *)data,size,&error);//file(parser, get_write_filepath("notifJSON"), &error);

	if(Bresult){

		 int mallocated=0;
		 int mallocated2=0;

	     root = json_parser_get_root(parser);

	     //GET BODIES JSON ARRAY

	     JsonArray *bodies = json_node_get_array(root);
	     //int leng_bodies=json_array_get_length(bodies);

	     //countDeffected=leng_bodies;
	     JsonObject *obj = json_node_get_object(root);
	     gchar *mess = json_object_get_string_member(obj, "message");
	     gboolean *datares = json_object_get_boolean_member(obj, "result");
	     char *Content=malloc(bufSize);
	     Content=realloc(Content,1000000);
	     sprintf(Content, "%s","");
	     int cnt;
	     if(datares)
	     {
	    	 JsonObject *objDefect = json_object_get_object_member(obj, "data");
	    	 int totalDefect = json_object_get_int_member(objDefect,"totalDefect");

	    	 JsonArray *arrDetail = json_object_get_array_member(objDefect,"detail");
	    	 int length_detail = json_array_get_length(arrDetail);
	    	 char *conts = malloc(100000);
	    	 int get = getAllNotifCount(&cnt);
	    	 if(length_detail>1)
	    	 {

	    		 if(cnt==0)
	    		 {
	    			 sprintf(NotifCaption, "Total Defect %d", totalDefect);
	    		 }
	    		 else
	    		 {
	    			 sprintf(NotifCaption, "Total Defect %d", totalDefect+cnt);
	    		 }
	    		 dlog_print(DLOG_ERROR,"NOTI",NotifCaption);
	    		 //create_genlist_item(NVvc->genlist4, "2text" , _UIB_LOCALE(NotifCaption), _UIB_LOCALE(" "), " "," ", " "," ", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
	    		 //NotifCaption=realloc(NotifCaption,strlen(mess)+1);
	    		 //snprintf(NotifCaption,strlen(mess)+1 , "%s", mess);

	    		 //sprintf(Content, "<br><align=center>%s%s%s</align>",Strhour,StrbodiesNumber,StrJobsName);
	    		 //sprintf(Content, "<br><align=center><font color=#ff9900><b>%s</b></font></align>", NotifCaption);
	    		 //Insert(Content);
	    	 }
	    	 else
	    	 {
	    		 JsonObject *defects = json_array_get_object_element(arrDetail,0);
	    		 gchar *body_no = json_object_get_string_member(defects,"bodyNo");
	    		 if(cnt==0)
	    		 {
					 sprintf(NotifCaption, "%s", body_no);
					 sprintf(NotifDescriptions, "Total Defect %d", totalDefect);
	    		 }
	    		 else
	    		 {
	    			 sprintf(NotifCaption, "Total Defect %d", totalDefect+cnt);
	    		 }
	    		 //Content=realloc(Content,100000);
	    		 //sprintf(Content, "<br><align=center>%s%s%s</align>",Strhour,StrbodiesNumber,StrJobsName);
	    		 //sprintf(conts, "<br><align=center><font color=#ff9900><b>%s</b></font></align>", NotifCaption);
	    		 //sprintf(conts, "%s<br><align=center>%s</align>",Content, NotifDescriptions);
	    		 //Insert(Content);
	    		 //create_genlist_item(NVvc->genlist4, "2text" , _UIB_LOCALE(NotifCaption), _UIB_LOCALE(NotifDescriptions), " "," ", " "," ", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
	    		 dlog_print(DLOG_ERROR,"NOTI",NotifCaption);
	    	 }
	    	 countDeffected = totalDefect;
	    	 for(int i = 0; i< length_detail; i++)
	    	 {

	    		 JsonObject *defects = json_array_get_object_element(arrDetail,i);
	    		 gchar *bodyno = json_object_get_string_member(defects,"bodyNo");
	    		 int *subTotalDefect = json_object_get_int_member(defects, "count");
	    		 JsonArray *notifDefect = json_object_get_array_member(defects,"defect");
	    		 int *length_defect = json_array_get_length(notifDefect);

	    		 //Content=realloc(Content,100000);
	    		 //sprintf(Content, "<br><align=center>%s%s%s</align>",Strhour,StrbodiesNumber,StrJobsName);
	    		 sprintf(Content, "%s<br><align=center><font color=#ff9900>%s</font></align>",Content,bodyno);


	    		 //sprintf(Content, "%s<br><align=left>Defects : %d</align>",Content, subTotalDefect);
	    		 //Insert(Content);

	    		 if(subTotalDefect==length_defect)
	    		 {
	    			 for(int x=0;x<length_defect;x++)
	    			 {
	    				 JsonObject *defect = json_array_get_object_element(notifDefect,x);
	    				 gchar *notifId = json_object_get_string_member(defect,"notifId");
	    				 gchar *problem = json_object_get_string_member(defect,"defect");
	    				 gchar *problem_description = json_object_get_string_member(defect,"problem");
	    				 dlog_print(DLOG_ERROR,"NOTIF ID",notifId);
	    				 dlog_print(DLOG_ERROR,"problem",problem);
	    				 dlog_print(DLOG_ERROR,"problem_desc",problem_description);

	    				 //Content=realloc(Content,100000);
	    				 //sprintf(Content, "<br><align=center>%s%s%s</align>",Strhour,StrbodiesNumber,StrJobsName);
	    				 //sprintf(Content, "%s<br><align=left>Problem : %s</align>",Content, problem);
	    				 //sprintf(Content, "%s<br><align=left>Problem Description : %s</align>",Content, problem_description);
	    				 //sprintf(Content, "%s<br><hr></hr>",Content);
	    				 //Insert(Content);
	    				 //Content=realloc(Content,100000);
	    				 sprintf(Content, "%s<br><align=center>%s</align>",Content, problem);
	    			     sprintf(Content, "%s<br><align=center><font color=#ff0000>%s</font></align>",Content, "ABC DEF GHI JKL MNO PQR STU VWX YZ");
	    			     sprintf(Content, "%s<br>%s<hr></hr>",Content,"______________________________");
	    			     Insert(bodyno, notifId, problem, problem_description);
	    			 }
	    		 }

	    		 //Insert(Content);
	    		 //sprintf(Content, "","");
	    		 dlog_print(DLOG_ERROR,"BODY NO",bodyno);
	    	 }

	     }
	     else
	     {
	    	 dlog_print(DLOG_ERROR,"JSON PARSING","No data could be display");
	     }
	    //*result="oi";
	 	*result=Content;

	 	dlog_print(DLOG_ERROR,"JSON PARSING",result);

	}

}

void endCurlConnection(connection_h connection,CURL *curl){

	if(curl!=NULL){
		curl_easy_cleanup(curl);
	}

	if(connection!=NULL){
		connection_unset_proxy_address_changed_cb(connection);
		connection_destroy(connection);
	}
}

void endReq(void *data, Ecore_Thread *thread)
{

	dlog_print(DLOG_INFO, "endReq", "END REQ");
}

void cancelReq(void *data, Ecore_Thread *thread)
{
	dlog_print(DLOG_INFO, "CANCELReq", "CANCEL REQ");
}


/*
 * HttpReq.cpp
 *
 *  Created on: Feb 8, 2018
 *      Author: Hartaji


#include <HttpReq.h>


char *URL="";

char *URLResponseRead="";
int sendResponseRead=0;
NotifData *notifData;

char *NotifCaption;
char *NotifDescriptions;
int countDeffected=0;

struct MemoryStruct {
  char *memory;
  size_t size;
};struct MemoryStruct chunk;

void setURL(char *url){
	URL=url;
}

void setURLResponseRead(char *url){
	URLResponseRead=url;
}

Ecore_Thread_Cb threadMain(Ecore_Thread *thread){

	//notifData = (NotifData*) calloc (1, sizeof(NotifData));

	NotifCaption==malloc(30);
	NotifDescriptions=malloc(90);
	initdb();
	curl_global_init (CURL_GLOBAL_ALL);
	//setURI(URL);

	chunk.memory = malloc(1);
	chunk.size = 0;

	while(true){
		countDeffected=0;
		sleep(2);
		CurlRequestHTTP();
		//request();
		//dlog_print(DLOG_ERROR, "HTTP REQ", "threadMain");

		sleep(3);
	}

}

size_t WriteCallback(char *contents, size_t size, size_t nmemb, void *userp)
{

	if(sendResponseRead==0 && contents!=NULL){
		char *resultJSON;
		jsonParsing(contents,&resultJSON);

		if(resultJSON!=NULL && countDeffected>0){
			//write_file("notifJS",resultJSON);
			Insert(resultJSON);
			sendResponseRead=1;
			ShowNotif(NotifCaption,"",false);
			CurlRequestHTTP();
		}

	}else{
		sendResponseRead=0;
	}

    //((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

void HTTPReq(){
	//request();
}

void CurlRequestHTTP()
{
	bool checkWIFI;//, check2;

	system_info_get_platform_bool ("http://tizen.org/feature/network.wifi", &checkWIFI);
	//system_info_get_platform_bool ("http://tizen.org/feature/network.telephony", &check2);

	//if(checkWIFI==false)
		//return;

	CURL *curl;
	CURLcode curl_err;
	curl = curl_easy_init();
	char *readBuffer;

	if(!curl){
		dlog_print(DLOG_ERROR, "CURL INIT", "CURL INIT GAGAL");
		return;
	}

	connection_h connection;
	int conn_err;
	conn_err = connection_create(&connection);
	if (conn_err != CONNECTION_ERROR_NONE) {


	    return;
	}

	if(sendResponseRead==0){
		curl_easy_setopt(curl, CURLOPT_URL, URL);
	}else{
		curl_easy_setopt(curl, CURLOPT_URL, URLResponseRead);
	}

	curl_easy_setopt(curl, CURLOPT_HTTPGET, true);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

	curl_err = curl_easy_perform(curl);
	if (curl_err != CURLE_OK){
		endCurlConnection(connection,curl);
		return;
	}

	endCurlConnection(connection,curl);

}


void jsonParsing(char *data, char *result[1024]){

	JsonParser *parser;
	JsonNode *root;
	GError *error;

    int bufSize = 100;
	char *Content=malloc(bufSize);
	char *StrbodiesNumber=malloc(bufSize);
	char *Strhour=malloc(bufSize);
	char *StrJobsName=malloc(bufSize);

	gsize size =strlen(data);
	parser = json_parser_new();
	gboolean Bresult = json_parser_load_from_data(parser,(gchar *)data,size,&error);//file(parser, get_write_filepath("notifJSON"), &error);

	if(Bresult){

		 int mallocated=0;
		 int mallocated2=0;

	     root = json_parser_get_root(parser);

	     //GET BODIES JSON ARRAY
	     JsonArray *bodies = json_node_get_array(root);
	     int leng_bodies=json_array_get_length(bodies);

	     countDeffected=leng_bodies;

	     for(int i=0;i<leng_bodies;i++){

	    	 JsonObject *body=json_array_get_object_element(bodies,i);
	    	 gchar *jam = json_object_get_string_member(body, "hour");
	    	 gchar *body_no = json_object_get_string_member(body, "body_no");

	    	 dlog_print(DLOG_ERROR,"JSON PARSING","body no:%s  hour:%s  leng:%d ke:%d",body_no,jam,leng_bodies,i);

	    	 if(leng_bodies==1){
	    		 NotifCaption=realloc(NotifCaption,strlen(body_no)+1);
	    		 snprintf(NotifCaption,strlen(body_no)+1 , "%s", body_no);
	    	 }else{
	    		 NotifCaption=realloc(NotifCaption,strlen(leng_bodies)+12);
	    		 snprintf(NotifCaption,strlen(leng_bodies)+12 , "TOTAL DFCT %d", leng_bodies);
	    	 }

	    	 if(i==0){
	    		 mallocated=strlen(jam)+1+61;
	    		 realloc(Strhour,mallocated);

		    	 mallocated2=strlen(body_no)+1+30;
		    	 realloc(StrbodiesNumber,mallocated2);
	    		 snprintf(Strhour,mallocated , "<color=#999999><font_size=27><b>%s</b></font_size></color><br>", jam);
	    		 snprintf(StrbodiesNumber,mallocated2, "<color=#F27916><b>%s</b></color>", body_no);
	    	 }else{

	    		 mallocated=strlen(jam)+strlen(Strhour)+1+61;
	    		 mallocated2=strlen(StrbodiesNumber)+strlen(body_no)+1+30;
	    		 char *Strhours =malloc(mallocated);
	    		 char *StrbodiesNumbers=malloc(mallocated2);
	    		 //Strhour=realloc(mallocated);
	    		 //StrbodiesNumber=realloc(StrbodiesNumber,mallocated2);

	    		 //free(StrbodiesNumber);
		    	 snprintf(Strhours,mallocated, "%s<br><color=#999999><font_size=27><b>%s</b></font_size></color><br>",Strhour,jam);
	    		 snprintf(StrbodiesNumbers,mallocated2, "%s<br><color=#F27916><b>%s</b></color>",StrbodiesNumber, body_no);

	    		 Strhour=Strhours;
	    		 StrbodiesNumber=StrbodiesNumbers;
	    	 }


	    	 //get JSON ARRAY JOBCODES,SHOPCODES
	    	 JsonArray *jobCodes = json_object_get_array_member(body, "data");
		     int leng_jobCodes=json_array_get_length(jobCodes);
		     for(int u=0;u<leng_jobCodes;u++){

		    	 JsonObject *JSjobCode=json_array_get_object_element(jobCodes,u);
		    	 gchar *jobCode = json_object_get_string_member(JSjobCode, "job_code");
		    	 gchar *jobName = json_object_get_string_member(JSjobCode, "job_name");
		    	 gchar *shopCode = json_object_get_string_member(JSjobCode, "shop_code");
		    	 gchar *shopName = json_object_get_string_member(JSjobCode, "shop_name");

		    	 dlog_print(DLOG_ERROR,"JSON PARSING","jobCode:%s  shopCode:%s leng:%d ke%d",jobCode,shopCode,leng_jobCodes,u);

		    	 //get JSON ARRAY DEFFECTS
		    	 JsonArray *deffects = json_object_get_array_member(JSjobCode, "defects");
			     int leng_deffects=json_array_get_length(deffects);
			     char *listDefect=malloc(bufSize);
		    	 for(int a=0;a<leng_deffects;a++){

		    		 JsonObject *JSDeffect=json_array_get_object_element(deffects,a);
		    		 gchar *DeffectCode = json_object_get_string_member(JSDeffect, "defect_code");
		    		 gchar *DeffectName = json_object_get_string_member(JSDeffect, "defect_name");

			    	 dlog_print(DLOG_ERROR,"JSON PARSING","DeffectCode:%s  DeffectName:%s leng:%d ke:%d",DeffectCode,DeffectName,leng_deffects,a);

		    		 if(a==0){
		    			 mallocated=strlen(DeffectCode)+strlen(DeffectName)+1+35;
		    			 realloc(listDefect,mallocated);
						 snprintf(listDefect, mallocated ,"<br><font_size=30>- %s [%s]</font_size>", DeffectName,DeffectCode);
		    		 }else{
		    			 mallocated=strlen(listDefect)+strlen(DeffectName)+strlen(DeffectCode)+1+35;
		    			 char *listDefects=malloc(mallocated);
		    			 //listDefects= realloc(listDefect,mallocated);
		    			 snprintf(listDefects,mallocated ,"%s<br><font_size=30>- %s [%s]</font_size>", listDefect,DeffectName,DeffectCode);
		    			 listDefect=listDefects;
		    		 }

		    	}

		    	if(listDefect!=NULL){
					if(u==0){
						mallocated=strlen(jobName)+strlen(shopName)+strlen(listDefect)+1+70;
						StrJobsName=realloc(StrJobsName,mallocated);
						snprintf(StrJobsName, mallocated ,"<br><font_size=28><color=#3AE6F2>%s | %s</color></font_size>%s<br>",jobName ,shopName,listDefect);
					}else{
						mallocated=strlen(StrJobsName)+strlen(jobName)+strlen(shopName)+strlen(listDefect)+1+70 ;
						char *StrJobsNames=malloc(mallocated);
						snprintf(StrJobsNames,mallocated ,"%s<br><font_size=28><color=#3AE6F2>%s | %s</color></font_size>%s<br>",StrJobsName, jobName ,shopName,listDefect);
						StrJobsName=StrJobsNames;
					}
		    	}

		     }

	     }


	    mallocated=strlen(Strhour)+strlen(StrbodiesNumber)+strlen(StrJobsName)+1+26;
	    Content=realloc(Content,mallocated);

	 	sprintf(Content, "<br><align=center>%s%s%s</align>",Strhour,StrbodiesNumber,StrJobsName);
	 	*result=Content;

	 	dlog_print(DLOG_ERROR,"JSON PARSING",result);
	}

}

void endCurlConnection(connection_h connection,CURL *curl){

	if(curl!=NULL){
		curl_easy_cleanup(curl);
	}

	if(connection!=NULL){
		connection_unset_proxy_address_changed_cb(connection);
		connection_destroy(connection);
	}
}

void endReq(void *data, Ecore_Thread *thread)
{

	dlog_print(DLOG_INFO, "endReq", "END REQ");
}

void cancelReq(void *data, Ecore_Thread *thread)
{
	dlog_print(DLOG_INFO, "CANCELReq", "CANCEL REQ");
}
*/
