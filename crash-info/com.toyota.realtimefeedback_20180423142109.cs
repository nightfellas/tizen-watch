S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2652
Date: 2018-04-23 14:21:09+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2652, uid 5000)

Register Information
r0   = 0xb6f4ffef, r1   = 0x00000000
r2   = 0x00000000, r3   = 0x00000064
r4   = 0x00000000, r5   = 0x00000000
r6   = 0xb6f4fbd1, r7   = 0xbe89df60
r8   = 0xb6f4fbd1, r9   = 0xb6f4fbd1
r10  = 0xb6f4fbd1, fp   = 0xb6f4fbd1
ip   = 0xb6f613d0, sp   = 0xbe89df50
lr   = 0xb6f4f671, pc   = 0xb6a3acb8
cpsr = 0x60000010

Memory Information
MemTotal:   405512 KB
MemFree:      7964 KB
Buffers:      7200 KB
Cached:     108488 KB
VmPeak:     115384 KB
VmSize:     108492 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       26888 KB
VmRSS:       26888 KB
VmData:      36208 KB
VmStk:         136 KB
VmExe:          36 KB
VmLib:       35904 KB
VmPTE:          76 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2652 TID = 2652
2652 2657 2661 2667 

Maps Information
b04fc000 b0500000 r-xp /usr/lib/libogg.so.0.7.1
b0508000 b052a000 r-xp /usr/lib/libvorbis.so.0.4.3
b0532000 b0579000 r-xp /usr/lib/libsndfile.so.1.0.26
b0585000 b05ce000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b05d7000 b05dc000 r-xp /usr/lib/libjson.so.0.0.1
b1e7d000 b1f83000 r-xp /usr/lib/libicuuc.so.57.1
b1f99000 b2121000 r-xp /usr/lib/libicui18n.so.57.1
b2131000 b213e000 r-xp /usr/lib/libail.so.0.1.0
b2147000 b214a000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b2152000 b218a000 r-xp /usr/lib/libpulse.so.0.16.2
b218b000 b218e000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b2196000 b21f7000 r-xp /usr/lib/libasound.so.2.0.0
b2201000 b221a000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b2223000 b2227000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b222f000 b223a000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b2247000 b225f000 r-xp /usr/lib/libmmfsound.so.0.1.0
b2270000 b2277000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b229f000 b2326000 rw-s anon_inode:dmabuf
b2326000 b232a000 r-xp /usr/lib/libmmfsession.so.0.0.0
b2333000 b233e000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b2346000 b2348000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b2350000 b23d7000 rw-s anon_inode:dmabuf
b240f000 b2410000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b2418000 b2420000 r-xp /usr/lib/libfeedback.so.0.1.4
b24d8000 b255f000 rw-s anon_inode:dmabuf
b265e000 b265f000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b27ee000 b2875000 rw-s anon_inode:dmabuf
b2876000 b3075000 rw-p [stack:2667]
b3308000 b330b000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3361000 b3362000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b336c000 b336d000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b346a000 b3c69000 rw-p [stack:2661]
b3c69000 b3c6b000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3c73000 b3c8a000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3e9b000 b469a000 rw-p [stack:2657]
b46b1000 b4fdd000 r-xp /usr/lib/libsc-a3xx.so
b5241000 b5243000 r-xp /usr/lib/libdri2.so.0.0.0
b524b000 b5253000 r-xp /usr/lib/libdrm.so.2.4.0
b525b000 b525f000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b5267000 b526a000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b5272000 b5273000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b527b000 b5286000 r-xp /usr/lib/libtbm.so.1.0.0
b528e000 b5291000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b5299000 b529b000 r-xp /usr/lib/libgenlock.so
b52a3000 b52a8000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b52b0000 b53eb000 r-xp /usr/lib/egl/libGLESv2.so
b5427000 b5429000 r-xp /usr/lib/libadreno_utils.so
b5433000 b545a000 r-xp /usr/lib/libgsl.so
b5469000 b5470000 r-xp /usr/lib/egl/eglsubX11.so
b547a000 b549c000 r-xp /usr/lib/egl/libEGL.so
b54a5000 b551a000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b572a000 b5734000 r-xp /lib/libnss_files-2.13.so
b573d000 b5740000 r-xp /lib/libattr.so.1.1.0
b5748000 b574f000 r-xp /lib/libcrypt-2.13.so
b577f000 b5782000 r-xp /lib/libcap.so.2.21
b578a000 b578c000 r-xp /usr/lib/libiri.so
b5794000 b57b1000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b57ba000 b57be000 r-xp /usr/lib/libsmack.so.1.0.0
b57c7000 b57f6000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b57fe000 b5892000 r-xp /usr/lib/libstdc++.so.6.0.16
b58a5000 b5974000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b598a000 b59ae000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b59b7000 b5a81000 r-xp /usr/lib/libCOREGL.so.4.0
b5a98000 b5a9a000 r-xp /usr/lib/libXau.so.6.0.0
b5aa3000 b5ab3000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5abb000 b5abe000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ac6000 b5ade000 r-xp /usr/lib/liblzma.so.5.0.3
b5ae7000 b5ae9000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5af1000 b5af4000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5afc000 b5b00000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5b09000 b5b0e000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5b18000 b5b3b000 r-xp /usr/lib/libjpeg.so.8.0.2
b5b53000 b5b69000 r-xp /lib/libexpat.so.1.6.0
b5b73000 b5b86000 r-xp /usr/lib/libxcb.so.1.1.0
b5b8f000 b5b95000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5b9d000 b5b9e000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5ba8000 b5bc0000 r-xp /usr/lib/libpng12.so.0.50.0
b5bc8000 b5bcb000 r-xp /usr/lib/libEGL.so.1.4
b5bd3000 b5be1000 r-xp /usr/lib/libGLESv2.so.2.0
b5bea000 b5beb000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5bf3000 b5c0a000 r-xp /usr/lib/liblua-5.1.so
b5c14000 b5c1b000 r-xp /usr/lib/libembryo.so.1.7.99
b5c23000 b5c2d000 r-xp /usr/lib/libXext.so.6.4.0
b5c36000 b5c3a000 r-xp /usr/lib/libXtst.so.6.1.0
b5c42000 b5c48000 r-xp /usr/lib/libXrender.so.1.3.0
b5c50000 b5c56000 r-xp /usr/lib/libXrandr.so.2.2.0
b5c5e000 b5c5f000 r-xp /usr/lib/libXinerama.so.1.0.0
b5c69000 b5c6c000 r-xp /usr/lib/libXfixes.so.3.1.0
b5c74000 b5c76000 r-xp /usr/lib/libXgesture.so.7.0.0
b5c7e000 b5c80000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5c88000 b5c8a000 r-xp /usr/lib/libXdamage.so.1.1.0
b5c92000 b5c99000 r-xp /usr/lib/libXcursor.so.1.0.2
b5ca2000 b5cb2000 r-xp /lib/libresolv-2.13.so
b5cb6000 b5cb8000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5cc0000 b5cc5000 r-xp /usr/lib/libffi.so.5.0.10
b5ccd000 b5cce000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5cd6000 b5d1f000 r-xp /usr/lib/libmdm.so.1.2.70
b5d29000 b5d2f000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5d37000 b5d3d000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5d45000 b5d5f000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5d67000 b5d85000 r-xp /usr/lib/libsystemd.so.0.4.0
b5d90000 b5d91000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5d99000 b5d9e000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5da6000 b5dbd000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5dc5000 b5dcb000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5dd4000 b5ddd000 r-xp /usr/lib/libcom-core.so.0.0.1
b5de7000 b5de9000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5df2000 b5e48000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5e55000 b5eab000 r-xp /usr/lib/libfreetype.so.6.11.3
b5eb7000 b5efc000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5f05000 b5f18000 r-xp /usr/lib/libfribidi.so.0.3.1
b5f21000 b5f3b000 r-xp /usr/lib/libecore_con.so.1.7.99
b5f44000 b5f6e000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5f77000 b5f80000 r-xp /usr/lib/libedbus.so.1.7.99
b5f88000 b5f99000 r-xp /usr/lib/libecore_input.so.1.7.99
b5fa1000 b5fa6000 r-xp /usr/lib/libecore_file.so.1.7.99
b5faf000 b5fd1000 r-xp /usr/lib/libecore_evas.so.1.7.99
b5fda000 b5ff3000 r-xp /usr/lib/libeet.so.1.7.99
b6004000 b602c000 r-xp /usr/lib/libfontconfig.so.1.8.0
b602d000 b6036000 r-xp /usr/lib/libXi.so.6.1.0
b603e000 b611f000 r-xp /usr/lib/libX11.so.6.3.0
b612b000 b61e3000 r-xp /usr/lib/libcairo.so.2.11200.14
b61ee000 b624c000 r-xp /usr/lib/libedje.so.1.7.99
b6256000 b62a6000 r-xp /usr/lib/libecore_x.so.1.7.99
b62a8000 b6311000 r-xp /lib/libm-2.13.so
b631a000 b6320000 r-xp /lib/librt-2.13.so
b6329000 b633f000 r-xp /lib/libz.so.1.2.5
b6348000 b64da000 r-xp /usr/lib/libcrypto.so.1.0.0
b64fb000 b6542000 r-xp /usr/lib/libssl.so.1.0.0
b654e000 b657c000 r-xp /usr/lib/libidn.so.11.5.44
b6584000 b658d000 r-xp /usr/lib/libcares.so.2.1.0
b6596000 b6662000 r-xp /usr/lib/libxml2.so.2.7.8
b6670000 b6672000 r-xp /usr/lib/libiniparser.so.0
b667b000 b66af000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b66b8000 b678b000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6796000 b67af000 r-xp /usr/lib/libnetwork.so.0.0.0
b67b7000 b67c0000 r-xp /usr/lib/libvconf.so.0.2.45
b67c9000 b6899000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b689a000 b68db000 r-xp /usr/lib/libeina.so.1.7.99
b68e4000 b68e9000 r-xp /usr/lib/libappcore-common.so.1.1
b68f1000 b68f7000 r-xp /usr/lib/libappcore-efl.so.1.1
b68ff000 b6902000 r-xp /usr/lib/libbundle.so.0.1.22
b690a000 b6910000 r-xp /usr/lib/libappsvc.so.0.1.0
b6918000 b692c000 r-xp /lib/libpthread-2.13.so
b6937000 b695a000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6962000 b696f000 r-xp /usr/lib/libaul.so.0.1.0
b6979000 b697b000 r-xp /lib/libdl-2.13.so
b6984000 b698f000 r-xp /lib/libunwind.so.8.0.1
b69bc000 b69c4000 r-xp /lib/libgcc_s-4.6.so.1
b69c5000 b6ae9000 r-xp /lib/libc-2.13.so
b6af7000 b6b6c000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6b76000 b6b82000 r-xp /usr/lib/libnotification.so.0.1.0
b6b8b000 b6b9a000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6ba3000 b6c71000 r-xp /usr/lib/libevas.so.1.7.99
b6c97000 b6dd3000 r-xp /usr/lib/libelementary.so.1.7.99
b6dea000 b6e0b000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6e13000 b6e2a000 r-xp /usr/lib/libecore.so.1.7.99
b6e41000 b6e43000 r-xp /usr/lib/libdlog.so.0.0.0
b6e4b000 b6e8f000 r-xp /usr/lib/libcurl.so.4.3.0
b6e98000 b6e9d000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6ea5000 b6eaa000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6eb2000 b6ec2000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6eca000 b6ed2000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6eda000 b6ede000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6ee6000 b6eea000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6ef3000 b6ef5000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6f00000 b6f0b000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6f15000 b6f19000 r-xp /usr/lib/libsys-assert.so
b6f22000 b6f3f000 r-xp /lib/ld-2.13.so
b6f48000 b6f51000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b6fd5000 b7472000 rw-p [heap]
be87e000 be89f000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2652)
Call Stack Count: 1
 0: strcmp + 0x4 (0xb6a3acb8) [/lib/libc.so.6] + 0x75cb8
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
anager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-23 14:21:06.499+0700 E/ALARM_MANAGER(  969): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-23 14:21:06.499+0700 E/ALARM_MANAGER(  969): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 23-4-2018, 07:23:41 (UTC).
04-23 14:21:06.499+0700 E/ALARM_MANAGER(  969): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-23 14:21:06.499+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.599121)
04-23 14:21:06.499+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(0)
04-23 14:21:06.499+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.599121)
04-23 14:21:06.509+0700 W/W_HOME  ( 1254): gesture.c: _manual_render(182) > 
04-23 14:21:06.509+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(77)
04-23 14:21:06.509+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.509+0700 E/ALARM_MANAGER(  969): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-23 14:21:06.509+0700 E/ALARM_MANAGER(  969): alarm-manager.c: alarm_manager_alarm_delete(2462) > alarm_id[1445072242] is removed.
04-23 14:21:06.519+0700 W/STARTER ( 1179): clock-mgr.c: _on_lcd_signal_receive_cb(1271) > [_on_lcd_signal_receive_cb:1271] _on_lcd_signal_receive_cb, 1271, Post LCD on by [unknown]
04-23 14:21:06.519+0700 W/STARTER ( 1179): clock-mgr.c: _post_lcd_on(1059) > [_post_lcd_on:1059] LCD ON as reserved app[(null)] alpm mode[0]
04-23 14:21:06.539+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.720442)
04-23 14:21:06.539+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(77)
04-23 14:21:06.539+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.720442)
04-23 14:21:06.539+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(92)
04-23 14:21:06.539+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.559+0700 I/efl-extension( 2652): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-23 14:21:06.559+0700 I/efl-extension( 2652): efl_extension_rotary.c: _rotary_rotate_handler_cb(537) > Deliver clockwise rotary event to object: 0xb72b3ac8, elm_image, time_stamp : 648776
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.808487)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(92)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.808487)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(104)
04-23 14:21:06.569+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb729e400 : elm_genlist] mx(0), my(272), minx(0), miny(0), px(0), py(104)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb729e400 : elm_genlist] cw(360), ch(632), pw(360), ph(360)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:1910 _elm_scroll_content_region_show_internal() [0xb729e400 : elm_genlist] scrollto.x.animator(0xb73482d8)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:1916 _elm_scroll_content_region_show_internal() [0xb729e400 : elm_genlist] scrollto.y.animator(0xb7348418)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb729e400 : elm_genlist] x(0), y(258)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb729e400 : elm_genlist] t_in(0.320000), pos_x(0)
04-23 14:21:06.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb729e400 : elm_genlist] t_in(0.320000), pos_y(258)
04-23 14:21:06.589+0700 W/W_HOME  ( 1254): gesture.c: _manual_render_enable(138) > 0
04-23 14:21:06.589+0700 I/efl-extension( 1254): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-23 14:21:06.589+0700 I/efl-extension( 1254): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-23 14:21:06.599+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.222652)
04-23 14:21:06.599+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(104)
04-23 14:21:06.599+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.222652)
04-23 14:21:06.599+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(138)
04-23 14:21:06.599+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.629+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.359137)
04-23 14:21:06.629+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(138)
04-23 14:21:06.439+0700 W/WAKEUP-SERVICE( 1685): WakeupService.cpp: OnReceiveDisplayChanged(970) > [0;32mINFO: LCDOn receive data : -1226015988[0;m
04-23 14:21:06.629+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.359137)
04-23 14:21:06.639+0700 W/WAKEUP-SERVICE( 1685): WakeupService.cpp: OnReceiveDisplayChanged(971) > [0;32mINFO: WakeupServiceStart[0;m
04-23 14:21:06.639+0700 W/WAKEUP-SERVICE( 1685): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
04-23 14:21:06.639+0700 W/WAKEUP-SERVICE( 1685): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
04-23 14:21:06.639+0700 I/TIZEN_N_SOUND_MANAGER( 1685): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
04-23 14:21:06.639+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(159)
04-23 14:21:06.639+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.659+0700 I/efl-extension( 1180): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-23 14:21:06.659+0700 W/W_INDICATOR( 1180): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(538) > [_windicator_dbus_lcd_changed_cb:538] LCD ON signal is received
04-23 14:21:06.659+0700 W/W_INDICATOR( 1180): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(559) > [_windicator_dbus_lcd_changed_cb:559] 559, str=[unknown],charge : 0, connected : 0
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [starter:org.tizen.idled.ReservedApp]
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [starter:org.tizen.idled.ReservedApp]
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-23 14:21:06.659+0700 W/STARTER ( 1179): clock-mgr.c: __reserved_apps_message_received_cb(586) > [__reserved_apps_message_received_cb:586] appid[com.samsung.windicator]
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.windicator:org.tizen.idled.ReservedApp]
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-23 14:21:06.659+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-23 14:21:06.659+0700 W/W_INDICATOR( 1180): windicator_dbus.c: _msg_reserved_app_cb(341) > [_msg_reserved_app_cb:341] Moment view is already shown or call is enabled. moment view [0]
04-23 14:21:06.659+0700 W/TIZEN_N_SOUND_MANAGER( 1685): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
04-23 14:21:06.679+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.589550)
04-23 14:21:06.679+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(159)
04-23 14:21:06.679+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.589550)
04-23 14:21:06.679+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(194)
04-23 14:21:06.679+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.699+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.655062)
04-23 14:21:06.699+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(194)
04-23 14:21:06.699+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.655062)
04-23 14:21:06.699+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(204)
04-23 14:21:06.699+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.709+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.713471)
04-23 14:21:06.709+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(204)
04-23 14:21:06.439+0700 W/SHealthCommon( 1750): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
04-23 14:21:06.709+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.713471)
04-23 14:21:06.719+0700 W/SHealthServiceCommon( 1750): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
04-23 14:21:06.719+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(213)
04-23 14:21:06.719+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.719+0700 W/SHealthServiceCommon( 1750): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1524416400000.000000, end 1524468066729.000000, calories 965.882399[0;m
04-23 14:21:06.719+0700 W/SHealthCommon( 1750): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
04-23 14:21:06.729+0700 E/WAKEUP-SERVICE( 1685): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-23 14:21:06.739+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.807186)
04-23 14:21:06.739+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(213)
04-23 14:21:06.739+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.807186)
04-23 14:21:06.739+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(228)
04-23 14:21:06.739+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.759+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.845434)
04-23 14:21:06.759+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(228)
04-23 14:21:06.759+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.845434)
04-23 14:21:06.759+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(234)
04-23 14:21:06.759+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.769+0700 E/WAKEUP-SERVICE( 1685): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-23 14:21:06.769+0700 I/TIZEN_N_SOUND_MANAGER( 1685): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
04-23 14:21:06.779+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.886433)
04-23 14:21:06.779+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(234)
04-23 14:21:06.779+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.886433)
04-23 14:21:06.779+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(240)
04-23 14:21:06.779+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.799+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.925230)
04-23 14:21:06.799+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(240)
04-23 14:21:06.799+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.925230)
04-23 14:21:06.799+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(246)
04-23 14:21:06.799+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.809+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.951001)
04-23 14:21:06.809+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(246)
04-23 14:21:06.809+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.951001)
04-23 14:21:06.819+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(250)
04-23 14:21:06.819+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.829+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.974158)
04-23 14:21:06.829+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(250)
04-23 14:21:06.829+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.974158)
04-23 14:21:06.839+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(254)
04-23 14:21:06.839+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.849+0700 I/TIZEN_N_SOUND_MANAGER( 1685): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
04-23 14:21:06.849+0700 W/TIZEN_N_SOUND_MANAGER( 1685): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-23 14:21:06.849+0700 W/WAKEUP-SERVICE( 1685): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
04-23 14:21:06.849+0700 I/HIGEAR  ( 1685): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
04-23 14:21:06.849+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.988585)
04-23 14:21:06.849+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(254)
04-23 14:21:06.849+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.988585)
04-23 14:21:06.849+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(256)
04-23 14:21:06.849+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.859+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.995990)
04-23 14:21:06.859+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(256)
04-23 14:21:06.859+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.995990)
04-23 14:21:06.869+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(257)
04-23 14:21:06.869+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:06.879+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.999862)
04-23 14:21:06.879+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(257)
04-23 14:21:06.879+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.999862)
04-23 14:21:06.879+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(257)
04-23 14:21:06.889+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.999393)
04-23 14:21:06.889+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2559 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(257)
04-23 14:21:06.889+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.999393)
04-23 14:21:06.899+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] animation stop!!
04-23 14:21:06.899+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(258)
04-23 14:21:06.899+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.119+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.139+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.149+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.169+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.189+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.199+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.219+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.239+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.249+0700 I/efl-extension( 1254): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-23 14:21:07.249+0700 I/efl-extension( 1180): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-23 14:21:07.249+0700 I/efl-extension( 2652): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-23 14:21:07.249+0700 I/efl-extension( 2652): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb72b3ac8, elm_image, time_stamp : 649466
04-23 14:21:07.249+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.259+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb729e400 : elm_genlist] mx(0), my(272), minx(0), miny(0), px(0), py(258)
04-23 14:21:07.259+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb729e400 : elm_genlist] cw(360), ch(632), pw(360), ph(360)
04-23 14:21:07.259+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb729e400 : elm_genlist] x(0), y(129)
04-23 14:21:07.259+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb729e400 : elm_genlist] t_in(0.320000), pos_x(0)
04-23 14:21:07.259+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb729e400 : elm_genlist] t_in(0.320000), pos_y(129)
04-23 14:21:07.269+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.114909)
04-23 14:21:07.269+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(258)
04-23 14:21:07.269+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.114909)
04-23 14:21:07.269+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(243)
04-23 14:21:07.269+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.279+0700 E/EFL     ( 2652): evas_main<2652> evas_object_image.c:3445 evas_object_image_render_pre() 0xb7362ce0 has invalid fill size: 0x0. Ignored
04-23 14:21:07.289+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.223481)
04-23 14:21:07.289+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(243)
04-23 14:21:07.289+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.223481)
04-23 14:21:07.299+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(229)
04-23 14:21:07.299+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.309+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.345142)
04-23 14:21:07.319+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(229)
04-23 14:21:07.319+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.345142)
04-23 14:21:07.319+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(213)
04-23 14:21:07.319+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.329+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.423057)
04-23 14:21:07.329+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(213)
04-23 14:21:07.329+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.423057)
04-23 14:21:07.329+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(203)
04-23 14:21:07.329+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.349+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.495938)
04-23 14:21:07.349+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(203)
04-23 14:21:07.349+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.495938)
04-23 14:21:07.349+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(194)
04-23 14:21:07.349+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.359+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.575955)
04-23 14:21:07.369+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(194)
04-23 14:21:07.369+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.575955)
04-23 14:21:07.369+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(183)
04-23 14:21:07.369+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.379+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.636725)
04-23 14:21:07.379+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(183)
04-23 14:21:07.379+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.636725)
04-23 14:21:07.379+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(175)
04-23 14:21:07.379+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.399+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.703786)
04-23 14:21:07.399+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(175)
04-23 14:21:07.399+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.703786)
04-23 14:21:07.399+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(167)
04-23 14:21:07.399+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.419+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.758992)
04-23 14:21:07.419+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(167)
04-23 14:21:07.419+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.758992)
04-23 14:21:07.419+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(160)
04-23 14:21:07.419+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.429+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.800438)
04-23 14:21:07.429+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(160)
04-23 14:21:07.429+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.800438)
04-23 14:21:07.429+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(154)
04-23 14:21:07.429+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.439+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.838432)
04-23 14:21:07.449+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(154)
04-23 14:21:07.449+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.838432)
04-23 14:21:07.449+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(149)
04-23 14:21:07.449+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.459+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.881664)
04-23 14:21:07.459+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(149)
04-23 14:21:07.459+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.881664)
04-23 14:21:07.469+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(144)
04-23 14:21:07.469+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.479+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.907877)
04-23 14:21:07.479+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(144)
04-23 14:21:07.479+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.907877)
04-23 14:21:07.479+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(140)
04-23 14:21:07.479+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.489+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.932250)
04-23 14:21:07.489+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(140)
04-23 14:21:07.489+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.932250)
04-23 14:21:07.489+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(137)
04-23 14:21:07.489+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.509+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.958611)
04-23 14:21:07.509+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(137)
04-23 14:21:07.509+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.958611)
04-23 14:21:07.509+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(134)
04-23 14:21:07.509+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.519+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.975402)
04-23 14:21:07.519+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(134)
04-23 14:21:07.519+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.975402)
04-23 14:21:07.529+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(132)
04-23 14:21:07.529+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.539+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.986146)
04-23 14:21:07.539+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(132)
04-23 14:21:07.539+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.986146)
04-23 14:21:07.539+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(130)
04-23 14:21:07.539+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.549+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.995608)
04-23 14:21:07.549+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(130)
04-23 14:21:07.549+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.995608)
04-23 14:21:07.549+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(129)
04-23 14:21:07.549+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.999818)
04-23 14:21:07.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(129)
04-23 14:21:07.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.999818)
04-23 14:21:07.569+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(129)
04-23 14:21:07.589+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] time(0.998564)
04-23 14:21:07.589+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2559 _elm_scroll_scroll_to_x_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(129)
04-23 14:21:07.589+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] time(0.998564)
04-23 14:21:07.589+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] animation stop!!
04-23 14:21:07.589+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb729e400 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(129)
04-23 14:21:07.769+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.789+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.799+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.819+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.839+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.849+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.869+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.889+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.899+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.919+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.939+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.949+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:07.969+0700 E/EFL     ( 2652): <2652> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:21:08.799+0700 E/EFL     ( 2652): ecore_x<2652> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=651015 button=1
04-23 14:21:08.819+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb729e400 : elm_genlist] mouse move
04-23 14:21:08.819+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb729e400 : elm_genlist] hold(0), freeze(0)
04-23 14:21:08.829+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb729e400 : elm_genlist] mouse move
04-23 14:21:08.829+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb729e400 : elm_genlist] hold(0), freeze(0)
04-23 14:21:08.909+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb729e400 : elm_genlist] mouse move
04-23 14:21:08.909+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb729e400 : elm_genlist] hold(0), freeze(0)
04-23 14:21:08.919+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb729e400 : elm_genlist] mouse move
04-23 14:21:08.919+0700 E/EFL     ( 2652): elementary<2652> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb729e400 : elm_genlist] hold(0), freeze(0)
04-23 14:21:08.929+0700 E/EFL     ( 2652): ecore_x<2652> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=651146 button=1
04-23 14:21:09.229+0700 E/GL SELECTED( 2652): ID GL : TRM2
04-23 14:21:09.469+0700 E/EFL     ( 2652): ecore_x<2652> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=651680 button=1
04-23 14:21:09.589+0700 E/EFL     ( 2652): ecore_x<2652> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=651799 button=1
04-23 14:21:09.589+0700 I/efl-extension( 2652): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-23 14:21:09.599+0700 I/efl-extension( 2652): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-23 14:21:09.599+0700 I/efl-extension( 2652): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7357fe0, elm_image, _activated_obj : 0xb72b3ac8, activated : 1
04-23 14:21:09.599+0700 I/efl-extension( 2652): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-23 14:21:10.399+0700 W/CRASH_MANAGER( 2668): worker.c: worker_job(1205) > 1102652726561152446806
