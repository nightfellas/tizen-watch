S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2181
Date: 2018-04-27 11:06:37+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2181, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb58ac443, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb8597078, r7   = 0xbeb381d0
r8   = 0x000186a0, r9   = 0xb834f690
r10  = 0xb6c83b10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbeb38148
lr   = 0xb58ac443, pc   = 0xb6a396f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      3176 KB
Buffers:      9852 KB
Cached:     112860 KB
VmPeak:      90036 KB
VmSize:      89492 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23524 KB
VmRSS:       23524 KB
VmData:      29260 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24516 KB
VmPTE:          60 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2181 TID = 2181
2181 2206 2213 

Maps Information
b1e51000 b1e55000 r-xp /usr/lib/libogg.so.0.7.1
b1e5d000 b1e7f000 r-xp /usr/lib/libvorbis.so.0.4.3
b1e87000 b1ece000 r-xp /usr/lib/libsndfile.so.1.0.26
b1eda000 b1f23000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1f2c000 b1f31000 r-xp /usr/lib/libjson.so.0.0.1
b37d2000 b38d8000 r-xp /usr/lib/libicuuc.so.57.1
b38ee000 b3a76000 r-xp /usr/lib/libicui18n.so.57.1
b3a86000 b3a93000 r-xp /usr/lib/libail.so.0.1.0
b3a9c000 b3a9f000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3aa7000 b3adf000 r-xp /usr/lib/libpulse.so.0.16.2
b3ae0000 b3ae3000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3aeb000 b3b4c000 r-xp /usr/lib/libasound.so.2.0.0
b3b56000 b3b6f000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3b78000 b3b7c000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3b84000 b3b8f000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3b9c000 b3ba0000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3ba9000 b3bc1000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3bd2000 b3bd9000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3be1000 b3bec000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3c7b000 b3c7e000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3e6f000 b3ef6000 rw-s anon_inode:dmabuf
b3ef7000 b46f6000 rw-p [stack:2213]
b4a0a000 b4a0c000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b4a14000 b4a15000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4a1d000 b4a25000 r-xp /usr/lib/libfeedback.so.0.1.4
b4a3e000 b4a3f000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b4a47000 b4a48000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b4a7e000 b4a7f000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b07000 b5306000 rw-p [stack:2206]
b5306000 b5308000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b5310000 b5327000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b5334000 b5336000 r-xp /usr/lib/libdri2.so.0.0.0
b533e000 b5349000 r-xp /usr/lib/libtbm.so.1.0.0
b5351000 b5359000 r-xp /usr/lib/libdrm.so.2.4.0
b5361000 b5363000 r-xp /usr/lib/libgenlock.so
b536b000 b5370000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5378000 b5383000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b558c000 b5656000 r-xp /usr/lib/libCOREGL.so.4.0
b5667000 b5677000 r-xp /usr/lib/libmdm-common.so.1.1.25
b567f000 b5685000 r-xp /usr/lib/libxcb-render.so.0.0.0
b568d000 b568e000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5697000 b569a000 r-xp /usr/lib/libEGL.so.1.4
b56a2000 b56b0000 r-xp /usr/lib/libGLESv2.so.2.0
b56b9000 b5702000 r-xp /usr/lib/libmdm.so.1.2.70
b570b000 b5711000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5719000 b5722000 r-xp /usr/lib/libcom-core.so.0.0.1
b572b000 b57e3000 r-xp /usr/lib/libcairo.so.2.11200.14
b57ee000 b5807000 r-xp /usr/lib/libnetwork.so.0.0.0
b580f000 b581b000 r-xp /usr/lib/libnotification.so.0.1.0
b5824000 b5833000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b583c000 b585d000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5865000 b586a000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5872000 b5877000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b587f000 b588f000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5897000 b589f000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b58a7000 b58b0000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a56000 b5a60000 r-xp /lib/libnss_files-2.13.so
b5a69000 b5b38000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b4e000 b5b72000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b7b000 b5b81000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b89000 b5b8d000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b9a000 b5ba5000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5bad000 b5baf000 r-xp /usr/lib/libiniparser.so.0
b5bb8000 b5bbd000 r-xp /usr/lib/libappcore-common.so.1.1
b5bc5000 b5bc7000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5bd0000 b5bd4000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5be1000 b5be3000 r-xp /usr/lib/libXau.so.6.0.0
b5beb000 b5bf2000 r-xp /lib/libcrypt-2.13.so
b5c22000 b5c24000 r-xp /usr/lib/libiri.so
b5c2d000 b5dbf000 r-xp /usr/lib/libcrypto.so.1.0.0
b5de0000 b5e27000 r-xp /usr/lib/libssl.so.1.0.0
b5e33000 b5e61000 r-xp /usr/lib/libidn.so.11.5.44
b5e69000 b5e72000 r-xp /usr/lib/libcares.so.2.1.0
b5e7c000 b5e8f000 r-xp /usr/lib/libxcb.so.1.1.0
b5e98000 b5e9b000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ea3000 b5ea5000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5eae000 b5f7a000 r-xp /usr/lib/libxml2.so.2.7.8
b5f87000 b5f89000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f92000 b5f97000 r-xp /usr/lib/libffi.so.5.0.10
b5f9f000 b5fa0000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5fa8000 b5fab000 r-xp /lib/libattr.so.1.1.0
b5fb3000 b6047000 r-xp /usr/lib/libstdc++.so.6.0.16
b605a000 b6077000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6081000 b6099000 r-xp /usr/lib/libpng12.so.0.50.0
b60a1000 b60b7000 r-xp /lib/libexpat.so.1.6.0
b60c1000 b6105000 r-xp /usr/lib/libcurl.so.4.3.0
b610e000 b6118000 r-xp /usr/lib/libXext.so.6.4.0
b6122000 b6126000 r-xp /usr/lib/libXtst.so.6.1.0
b612e000 b6134000 r-xp /usr/lib/libXrender.so.1.3.0
b613c000 b6142000 r-xp /usr/lib/libXrandr.so.2.2.0
b614a000 b614b000 r-xp /usr/lib/libXinerama.so.1.0.0
b6154000 b615d000 r-xp /usr/lib/libXi.so.6.1.0
b6165000 b6168000 r-xp /usr/lib/libXfixes.so.3.1.0
b6171000 b6173000 r-xp /usr/lib/libXgesture.so.7.0.0
b617b000 b617d000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6185000 b6187000 r-xp /usr/lib/libXdamage.so.1.1.0
b618f000 b6196000 r-xp /usr/lib/libXcursor.so.1.0.2
b619e000 b61a1000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b61aa000 b61ae000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b61b7000 b61bc000 r-xp /usr/lib/libecore_fb.so.1.7.99
b61c5000 b62a6000 r-xp /usr/lib/libX11.so.6.3.0
b62b1000 b62d4000 r-xp /usr/lib/libjpeg.so.8.0.2
b62ec000 b6302000 r-xp /lib/libz.so.1.2.5
b630b000 b630d000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6315000 b638a000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6394000 b63ae000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b63b6000 b63ea000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63f3000 b64c6000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b64d2000 b64e2000 r-xp /lib/libresolv-2.13.so
b64e6000 b64fe000 r-xp /usr/lib/liblzma.so.5.0.3
b6506000 b6509000 r-xp /lib/libcap.so.2.21
b6511000 b6540000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6548000 b6549000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6552000 b6558000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6560000 b6577000 r-xp /usr/lib/liblua-5.1.so
b6580000 b6587000 r-xp /usr/lib/libembryo.so.1.7.99
b658f000 b6595000 r-xp /lib/librt-2.13.so
b659e000 b65f4000 r-xp /usr/lib/libpixman-1.so.0.28.2
b6602000 b6658000 r-xp /usr/lib/libfreetype.so.6.11.3
b6664000 b668c000 r-xp /usr/lib/libfontconfig.so.1.8.0
b668d000 b66d2000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b66db000 b66ee000 r-xp /usr/lib/libfribidi.so.0.3.1
b66f6000 b6710000 r-xp /usr/lib/libecore_con.so.1.7.99
b671a000 b6723000 r-xp /usr/lib/libedbus.so.1.7.99
b672b000 b677b000 r-xp /usr/lib/libecore_x.so.1.7.99
b677d000 b6786000 r-xp /usr/lib/libvconf.so.0.2.45
b678e000 b679f000 r-xp /usr/lib/libecore_input.so.1.7.99
b67a7000 b67ac000 r-xp /usr/lib/libecore_file.so.1.7.99
b67b4000 b67d6000 r-xp /usr/lib/libecore_evas.so.1.7.99
b67df000 b6820000 r-xp /usr/lib/libeina.so.1.7.99
b6829000 b6842000 r-xp /usr/lib/libeet.so.1.7.99
b6853000 b68bc000 r-xp /lib/libm-2.13.so
b68c5000 b68cb000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b68d4000 b68d5000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b68dd000 b6900000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6908000 b690d000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6915000 b693f000 r-xp /usr/lib/libdbus-1.so.3.8.12
b6948000 b695f000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6967000 b6972000 r-xp /lib/libunwind.so.8.0.1
b699f000 b69bd000 r-xp /usr/lib/libsystemd.so.0.4.0
b69c7000 b6aeb000 r-xp /lib/libc-2.13.so
b6af9000 b6b01000 r-xp /lib/libgcc_s-4.6.so.1
b6b02000 b6b06000 r-xp /usr/lib/libsmack.so.1.0.0
b6b0f000 b6b15000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b1d000 b6bed000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6bee000 b6c4c000 r-xp /usr/lib/libedje.so.1.7.99
b6c56000 b6c6d000 r-xp /usr/lib/libecore.so.1.7.99
b6c84000 b6d52000 r-xp /usr/lib/libevas.so.1.7.99
b6d78000 b6eb4000 r-xp /usr/lib/libelementary.so.1.7.99
b6ecb000 b6edf000 r-xp /lib/libpthread-2.13.so
b6eea000 b6eec000 r-xp /usr/lib/libdlog.so.0.0.0
b6ef4000 b6ef7000 r-xp /usr/lib/libbundle.so.0.1.22
b6eff000 b6f01000 r-xp /lib/libdl-2.13.so
b6f0a000 b6f17000 r-xp /usr/lib/libaul.so.0.1.0
b6f29000 b6f2f000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f38000 b6f3c000 r-xp /usr/lib/libsys-assert.so
b6f45000 b6f62000 r-xp /lib/ld-2.13.so
b6f6b000 b6f70000 r-xp /usr/bin/launchpad-loader
b8317000 b87e5000 rw-p [heap]
beb18000 beb39000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2181)
Call Stack Count: 1
 0: realloc + 0x4c (0xb6a396f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
:06:29.269+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] move content x(-9), y(10)
04-27 11:06:29.269+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] direction_x(1), direction_y(1)
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] drag_child_locked_x(0)
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] drag_child_locked_y(0)
04-27 11:06:29.309+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] move content x(-9), y(4)
04-27 11:06:29.309+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] direction_x(1), direction_y(1)
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] drag_child_locked_x(0)
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] drag_child_locked_y(0)
04-27 11:06:29.339+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8445818 : elm_genlist] move content x(-8), y(-2)
04-27 11:06:29.389+0700 I/APP_CORE( 1251): appcore-efl.c: __do_app(453) > [APP 1251] Event: MEM_FLUSH State: PAUSED
04-27 11:06:29.549+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.579+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=102267 button=1
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:29.579+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:29.619+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.639+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.659+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.669+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.689+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.699+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.719+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.729+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.739+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.759+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.769+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:29.789+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:30.089+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=102844 button=1
04-27 11:06:30.089+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:30.099+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:30.099+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:30.109+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:30.109+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:30.179+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:30.179+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:30.189+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:30.189+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:30.199+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=102951 button=1
04-27 11:06:30.229+0700 E/GL SELECTED( 2181): ID GL : TRM1
04-27 11:06:31.199+0700 E/EFL     (  893): ecore_x<893> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3200002 time=102951
04-27 11:06:31.199+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=102951
04-27 11:06:31.199+0700 E/EFL     (  893): ecore_x<893> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=102951
04-27 11:06:31.639+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=104391 button=1
04-27 11:06:31.639+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:31.649+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:31.649+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:31.659+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:31.659+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:31.679+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:31.679+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:31.699+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:31.699+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:31.709+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:31.709+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:31.719+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:31.719+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:31.729+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=104489 button=1
04-27 11:06:32.509+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=105264 button=1
04-27 11:06:32.509+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:32.519+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:32.519+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:32.529+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:32.529+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:32.539+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:32.539+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:32.549+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:32.549+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:32.599+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:32.599+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:32.609+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] mouse move
04-27 11:06:32.609+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8445818 : elm_genlist] hold(0), freeze(0)
04-27 11:06:32.619+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=105372 button=1
04-27 11:06:32.639+0700 E/GL SELECTED( 2181): ID GL : TRM1
04-27 11:06:33.279+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=106039 button=1
04-27 11:06:33.439+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=106191 button=1
04-27 11:06:33.439+0700 I/efl-extension( 2181): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:06:33.439+0700 I/efl-extension( 2181): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:06:33.439+0700 I/efl-extension( 2181): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8595330, elm_image, _activated_obj : 0xb8454658, activated : 1
04-27 11:06:33.439+0700 I/efl-extension( 2181): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:06:33.449+0700 E/EFL     ( 2181): elementary<2181> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:06:33.449+0700 E/EFL     ( 2181): elementary<2181> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb8595798) will be pushed
04-27 11:06:33.459+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:33.459+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:33.459+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8514d28 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:06:33.459+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8514d28 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:06:33.469+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8514d28 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:06:33.469+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8514d28 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-27 11:06:33.489+0700 E/EFL     ( 2181): elementary<2181> elc_naviframe.c:2796 _push_transition_cb() item(0xb8595798) will transition
04-27 11:06:33.909+0700 E/EFL     ( 2181): elementary<2181> elc_naviframe.c:1193 _on_item_push_finished() item(0xb84940d0) transition finished
04-27 11:06:33.909+0700 E/EFL     ( 2181): elementary<2181> elc_naviframe.c:1218 _on_item_show_finished() item(0xb8595798) transition finished
04-27 11:06:33.939+0700 E/EFL     ( 2181): <2181> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:06:35.509+0700 I/CAPI_NETWORK_CONNECTION( 2181): connection.c: connection_create(453) > New handle created[0xb4917258]
04-27 11:06:35.529+0700 E/NOTI    ( 2181): 67026
04-27 11:06:35.529+0700 E/NOTIF ID( 2181): 40198
04-27 11:06:35.529+0700 E/problem ( 2181): Dirty
04-27 11:06:35.529+0700 E/problem_desc( 2181): 19A-Fender Lh Fender Dirty & Stain
04-27 11:06:35.539+0700 E/NOTIF ID( 2181): 40199
04-27 11:06:35.539+0700 E/problem ( 2181): Dirty
04-27 11:06:35.539+0700 E/problem_desc( 2181): 19A-Fender Rh Fender Dirty & Stain
04-27 11:06:35.549+0700 E/BODY NO ( 2181): 67026
04-27 11:06:35.549+0700 E/JSON PARSING( 2181): �ձ���
04-27 11:06:35.569+0700 I/AUL     ( 1146): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 11:06:35.579+0700 I/AUL     ( 1146): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 11:06:35.629+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:06:35.639+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:06:35.639+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1079, op_type : 1  //insert = 1, update = 2, delete = 3
04-27 11:06:35.649+0700 I/CAPI_NETWORK_CONNECTION( 2181): connection.c: connection_create(453) > New handle created[0xb859feb0]
04-27 11:06:35.649+0700 E/wnoti-service( 1443): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-27 11:06:35.649+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-27 11:06:35.649+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-27 11:06:35.649+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-27 11:06:35.649+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: (null), err: 0
04-27 11:06:35.669+0700 E/wnoti-service( 1443): wnoti-native-client.c: _insert_notification(1208) > fail to get metadata_value, ret : -2
04-27 11:06:35.669+0700 E/wnoti-service( 1443): wnoti-db-server.c: wnoti_update_panel(2712) > id : -1016, source : 1
04-27 11:06:35.669+0700 E/wnoti-service( 1443): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-27 11:06:35.669+0700 E/wnoti-service( 1443): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1079, application_id: -1016
04-27 11:06:35.689+0700 E/wnoti-service( 1443): wnoti-db-server.c: _wnoti_update_category(908) > re_table_id : 0
04-27 11:06:35.689+0700 E/wnoti-service( 1443): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 0, count : 1
04-27 11:06:35.699+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:06:35.709+0700 I/CAPI_NETWORK_CONNECTION( 2181): connection.c: connection_destroy(471) > Destroy handle: 0xb859feb0
04-27 11:06:35.709+0700 I/CAPI_NETWORK_CONNECTION( 2181): connection.c: connection_destroy(471) > Destroy handle: 0xb4917258
04-27 11:06:35.729+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:06:35.729+0700 E/APPS    ( 1251): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-27 11:06:35.739+0700 I/wnoti-service( 1443): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-27 11:06:35.739+0700 E/wnoti-service( 1443): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-27 11:06:35.739+0700 E/wnoti-service( 1443): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-27 11:06:35.749+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-27 11:06:35.749+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:06:35.749+0700 E/wnoti-proxy( 1877): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1877, caller_id : 0, listener_type : 0
04-27 11:06:35.749+0700 E/wnoti-proxy( 1251): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1251, caller_id : 0, listener_type : 0
04-27 11:06:35.759+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 1877
04-27 11:06:35.759+0700 E/wnoti-service( 1443): wnoti-sap-client.c: on_timer(275) > is_popup_running: 1, ret: 0
04-27 11:06:35.759+0700 E/wnoti-service( 1443): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524801995, g_use_aul_launch : 1524801995
04-27 11:06:35.899+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_service_changed_cb(243) > Handle this change type in idler.
04-27 11:06:35.899+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-27 11:06:35.899+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-27 11:06:35.899+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-27 11:06:35.899+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1139, is_duplicated : 0
04-27 11:06:35.899+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1311) > view_type : 1, turn_screen_on : 1, allow_gesture : 1, is_used_popup : 0, feedback : 2
04-27 11:06:35.919+0700 E/wnoti-proxy( 1877): wnoti-client.c: _wnoti_parse_extra_sub(373) > JSON_IS_NULL
04-27 11:06:35.919+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(226) > application_name: Real Feed Back, application_id: -1016, category_id: 114, time: 1524801995, launch_app_id: (null), bg_image: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, extracted_icon_color: -3139560, disble_block_app_action: 0, support_large_icon 0
04-27 11:06:35.919+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(236) > noti_type: 1
04-27 11:06:35.919+0700 I/wnotibp ( 1877): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1139, noti_type: 1
04-27 11:06:35.919+0700 I/wnotibp ( 1877): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 0, content_id: 0, notification_id: 0
04-27 11:06:35.919+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_convert_alert_data(67) > alert_type: 4, app_feedback_type: 2, popup_view_style: 0, feedback_pattern_app: -1
04-27 11:06:35.919+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 0, new_list count : 1
04-27 11:06:35.919+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_notification_change_cb(600) > [1,1139]
04-27 11:06:35.919+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:06:35.919+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-27 11:06:35.919+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: _common_app_id_from_win(152) > 2181
04-27 11:06:35.939+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_check_env_condition(437) > focus app is com.toyota.realtimefeedback, 0
04-27 11:06:35.939+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_add_notification(991) > Add noti_queue [1139, 0]
04-27 11:06:35.939+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1138
04-27 11:06:35.939+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 1
04-27 11:06:35.939+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1138
04-27 11:06:35.939+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 1
04-27 11:06:35.939+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(73) > ::APP:: CHECK STATE : 3, 0, (null)
04-27 11:06:35.939+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:06:35.939+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(79) > ::APP:: CHECK DATA : 1 1 0000
04-27 11:06:35.939+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:06:35.949+0700 W/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-27 11:06:35.949+0700 I/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_active_app_id(1010) > [2181]
04-27 11:06:35.959+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:06:35.959+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1178) > [1139, 1, 0, 2, 0000]
04-27 11:06:35.959+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1179) > [0, 1, 0]
04-27 11:06:35.959+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1180) > [1, 0, 0, 0]
04-27 11:06:35.959+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-27 11:06:35.959+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_detail_layout(3707) > wnotiboard_popup_vi_type: 2
04-27 11:06:35.959+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_detail_layout(3712) > (1139, 1139)
04-27 11:06:35.959+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-27 11:06:35.979+0700 E/WMS     ( 1008): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-27 11:06:35.989+0700 I/efl-extension( 1877): efl_extension_circle_surface.c: eext_circle_surface_layout_add(1290) > Put the surface[0xb0bb69e8]'s widget[0xb0bcabb8] to layout widget[0xaf88d610]
04-27 11:06:35.999+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_genlist(3639) > (1139, 1139)
04-27 11:06:36.009+0700 I/efl-extension( 1877): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:06:36.019+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_card_data(3073) > 0xb28f0bf8, 0xb28f0a80, 0xb28f0a80
04-27 11:06:36.019+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb0bca5d8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:06:36.019+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb0bca5d8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:06:36.039+0700 E/EFL     ( 1877): elementary<1877> elm_genlist.c:7236 elm_genlist_item_item_class_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-27 11:06:36.039+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:06:36.039+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:06:36.039+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:06:36.049+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:06:36.049+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: stack.separator
04-27 11:06:36.049+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:06:36.049+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.049+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.049+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.049+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 11:06:36.049+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.049+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.049+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.049+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.049+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:06:36.059+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:06:36.059+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:06:36.059+0700 E/EFL     ( 1877): elementary<1877> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 11:06:36.059+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(224) > can't get layout
04-27 11:06:36.059+0700 E/EFL     ( 1877): elementary<1877> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 11:06:36.059+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:06:36.059+0700 I/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1139, 114, -1016.
04-27 11:06:36.079+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:06:36.079+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:06:36.079+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:06:36.079+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:06:36.119+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:06:36.129+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:06:36.129+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:06:36.129+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:06:36.129+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.129+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.129+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.129+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 11:06:36.129+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.129+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.129+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:06:36.129+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:06:36.129+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:06:36.129+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:06:36.129+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:06:36.129+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 11:06:36.129+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:06:36.129+0700 I/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1139, 114, -1016.
04-27 11:06:36.139+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb0bca5d8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:06:36.139+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb0bca5d8 : elm_genlist] cw(360), ch(360), pw(360), ph(360)
04-27 11:06:36.149+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb0bca5d8 : elm_genlist] mx(0), my(52), minx(0), miny(0), px(0), py(0)
04-27 11:06:36.149+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb0bca5d8 : elm_genlist] cw(360), ch(412), pw(360), ph(360)
04-27 11:06:36.229+0700 W/APP_CORE( 1877): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3000009
04-27 11:06:36.249+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1877] goes to (3)
04-27 11:06:36.249+0700 W/AUL_AMD (  929): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 11:06:36.249+0700 W/AUL_AMD (  929): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-27 11:06:36.249+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.wnotiboard-popup) pid(1877) status(fg) type(uiapp)
04-27 11:06:36.259+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(263) > fully_obscured: 0, 0
04-27 11:06:36.259+0700 I/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x3000009 0x300000d 0x3000009]
04-27 11:06:36.259+0700 I/APP_CORE( 1877): appcore-efl.c: __do_app(453) > [APP 1877] Event: RESUME State: PAUSED
04-27 11:06:36.259+0700 I/CAPI_APPFW_APPLICATION( 1877): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-27 11:06:36.259+0700 I/wnotibp ( 1877): wnotiboard-popup.c: _popup_app_resume(229) > 
04-27 11:06:36.259+0700 I/GATE    ( 1877): <GATE-M>APP_FULLY_LOADED_wnotiboard-popup</GATE-M>
04-27 11:06:36.259+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2809) > ::UI:: VI TYPE : 2
04-27 11:06:36.259+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:06:36.259+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2816) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 11:06:36.259+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:06:36.259+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(284) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 11:06:36.259+0700 W/TIZEN_N_RECORDER( 1877): recorder_product.c: recorder_is_in_recording(82) > pid =/proc/0 , state =0, alive=0
04-27 11:06:36.259+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(298) > ::APP:: application_id: -1016, is_disaster: 0, disaster_info: (null), app_feedback_type: 2, feedback_pattern_app: -1, is_recording: 0
04-27 11:06:36.259+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(307) > Gear side feedback setting is_sound_on: 0, is_vibration_on: 1, is_vibrate_when_noti_on: 0
04-27 11:06:36.259+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(341) > ::APP:: Determined feedback: sound 0, vibration: 1
04-27 11:06:36.259+0700 I/wnotib  ( 1877): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 13, ret: -3, request_id: 0
04-27 11:06:36.259+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_set_smart_relay(501) > Set the smart relay for 0, 114, -1016, Real Feed Back
04-27 11:06:36.269+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:06:36.269+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_turn_on_lcd(534) > [4, 0, 1, 0]
04-27 11:06:36.299+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(585) > ::UI:: [[[ ===> already [small popup] is LOCK, 0010 ]]]
04-27 11:06:36.299+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_sub_popup_show_animator_pre_cb(1781) > ::UI:: start showing animation
04-27 11:06:36.339+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1016) > _query_step failed(NO ROW)
04-27 11:06:36.339+0700 E/wnoti-service( 1443): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 2
04-27 11:06:36.349+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-27 11:06:36.349+0700 E/wnoti-proxy( 1877): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-27 11:06:36.349+0700 E/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(311) > ::DATA:: No categories available.
04-27 11:06:36.349+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 1, new_list count : 0
04-27 11:06:36.609+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_sub_popup_show_animator_cb(1684) > ::UI:: end show animation
04-27 11:06:36.609+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-27 11:06:37.119+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=109874 button=1
04-27 11:06:37.119+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8514d28 : elm_genlist] mouse move
04-27 11:06:37.119+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8514d28 : elm_genlist] mouse move
04-27 11:06:37.119+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8514d28 : elm_genlist] hold(0), freeze(0)
04-27 11:06:37.149+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8514d28 : elm_genlist] mouse move
04-27 11:06:37.149+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8514d28 : elm_genlist] hold(0), freeze(0)
04-27 11:06:37.239+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8514d28 : elm_genlist] mouse move
04-27 11:06:37.239+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8514d28 : elm_genlist] hold(0), freeze(0)
04-27 11:06:37.239+0700 E/EFL     ( 2181): ecore_x<2181> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=109996 button=1
04-27 11:06:37.269+0700 I/efl-extension( 2181): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:06:37.269+0700 I/efl-extension( 2181): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:06:37.269+0700 I/efl-extension( 2181): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8535168, elm_image, _activated_obj : 0xb8595330, activated : 1
04-27 11:06:37.269+0700 I/efl-extension( 2181): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:06:37.269+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb864e580 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-27 11:06:37.269+0700 E/EFL     ( 2181): elementary<2181> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb864e580 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-27 11:06:37.549+0700 W/CRASH_MANAGER( 2220): worker.c: worker_job(1205) > 1102181726561152480199
