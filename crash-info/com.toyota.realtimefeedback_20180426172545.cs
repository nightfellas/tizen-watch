S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2073
Date: 2018-04-26 17:25:45+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2073, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb5936445, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb3b05268, r7   = 0xbecab1d0
r8   = 0x000186a0, r9   = 0xb8e43218
r10  = 0xb6d0db10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbecab148
lr   = 0xb5936445, pc   = 0xb6ac36f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      4516 KB
Buffers:     10444 KB
Cached:     115244 KB
VmPeak:      93736 KB
VmSize:      91572 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23492 KB
VmRSS:       23492 KB
VmData:      31420 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24504 KB
VmPTE:          64 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2073 TID = 2073
2073 2110 2116 

Maps Information
b1dbb000 b1dbf000 r-xp /usr/lib/libogg.so.0.7.1
b1dc7000 b1de9000 r-xp /usr/lib/libvorbis.so.0.4.3
b1df1000 b1e38000 r-xp /usr/lib/libsndfile.so.1.0.26
b1e44000 b1e8d000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1e96000 b1e9b000 r-xp /usr/lib/libjson.so.0.0.1
b373c000 b3842000 r-xp /usr/lib/libicuuc.so.57.1
b3858000 b39e0000 r-xp /usr/lib/libicui18n.so.57.1
b39f0000 b39fd000 r-xp /usr/lib/libail.so.0.1.0
b3a06000 b3a09000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3a11000 b3a49000 r-xp /usr/lib/libpulse.so.0.16.2
b3a4a000 b3aab000 r-xp /usr/lib/libasound.so.2.0.0
b3ab5000 b3ace000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3ad7000 b3aef000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3c09000 b3c14000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3c7e000 b3d05000 rw-s anon_inode:dmabuf
b3d05000 b3d8c000 rw-s anon_inode:dmabuf
b3d92000 b3d96000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3d9e000 b3da2000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3dab000 b3db2000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3e67000 b3eee000 rw-s anon_inode:dmabuf
b3eee000 b3f75000 rw-s anon_inode:dmabuf
b3f76000 b4775000 rw-p [stack:2116]
b4a00000 b4a03000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b4a0b000 b4a16000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b4a1e000 b4a20000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b4a28000 b4a29000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4a31000 b4a39000 r-xp /usr/lib/libfeedback.so.0.1.4
b4a52000 b4a53000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b4b08000 b4b09000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b91000 b5390000 rw-p [stack:2110]
b5390000 b5392000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b539a000 b53b1000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b53be000 b53c0000 r-xp /usr/lib/libdri2.so.0.0.0
b53c8000 b53d3000 r-xp /usr/lib/libtbm.so.1.0.0
b53db000 b53e3000 r-xp /usr/lib/libdrm.so.2.4.0
b53eb000 b53ed000 r-xp /usr/lib/libgenlock.so
b53f5000 b53fa000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5402000 b540d000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b5616000 b56e0000 r-xp /usr/lib/libCOREGL.so.4.0
b56f1000 b5701000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5709000 b570f000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5717000 b5718000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5721000 b5724000 r-xp /usr/lib/libEGL.so.1.4
b572c000 b573a000 r-xp /usr/lib/libGLESv2.so.2.0
b5743000 b578c000 r-xp /usr/lib/libmdm.so.1.2.70
b5795000 b579b000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b57a3000 b57ac000 r-xp /usr/lib/libcom-core.so.0.0.1
b57b5000 b586d000 r-xp /usr/lib/libcairo.so.2.11200.14
b5878000 b5891000 r-xp /usr/lib/libnetwork.so.0.0.0
b5899000 b58a5000 r-xp /usr/lib/libnotification.so.0.1.0
b58ae000 b58bd000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b58c6000 b58e7000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58ef000 b58f4000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58fc000 b5901000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b5909000 b5919000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5921000 b5929000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5931000 b593b000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5ae0000 b5aea000 r-xp /lib/libnss_files-2.13.so
b5af3000 b5bc2000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5bd8000 b5bfc000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5c05000 b5c0b000 r-xp /usr/lib/libappsvc.so.0.1.0
b5c13000 b5c17000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5c24000 b5c2f000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5c37000 b5c39000 r-xp /usr/lib/libiniparser.so.0
b5c42000 b5c47000 r-xp /usr/lib/libappcore-common.so.1.1
b5c4f000 b5c51000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c5a000 b5c5e000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c6b000 b5c6d000 r-xp /usr/lib/libXau.so.6.0.0
b5c75000 b5c7c000 r-xp /lib/libcrypt-2.13.so
b5cac000 b5cae000 r-xp /usr/lib/libiri.so
b5cb7000 b5e49000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e6a000 b5eb1000 r-xp /usr/lib/libssl.so.1.0.0
b5ebd000 b5eeb000 r-xp /usr/lib/libidn.so.11.5.44
b5ef3000 b5efc000 r-xp /usr/lib/libcares.so.2.1.0
b5f06000 b5f19000 r-xp /usr/lib/libxcb.so.1.1.0
b5f22000 b5f25000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5f2d000 b5f2f000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5f38000 b6004000 r-xp /usr/lib/libxml2.so.2.7.8
b6011000 b6013000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b601c000 b6021000 r-xp /usr/lib/libffi.so.5.0.10
b6029000 b602a000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b6032000 b6035000 r-xp /lib/libattr.so.1.1.0
b603d000 b60d1000 r-xp /usr/lib/libstdc++.so.6.0.16
b60e4000 b6101000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b610b000 b6123000 r-xp /usr/lib/libpng12.so.0.50.0
b612b000 b6141000 r-xp /lib/libexpat.so.1.6.0
b614b000 b618f000 r-xp /usr/lib/libcurl.so.4.3.0
b6198000 b61a2000 r-xp /usr/lib/libXext.so.6.4.0
b61ac000 b61b0000 r-xp /usr/lib/libXtst.so.6.1.0
b61b8000 b61be000 r-xp /usr/lib/libXrender.so.1.3.0
b61c6000 b61cc000 r-xp /usr/lib/libXrandr.so.2.2.0
b61d4000 b61d5000 r-xp /usr/lib/libXinerama.so.1.0.0
b61de000 b61e7000 r-xp /usr/lib/libXi.so.6.1.0
b61ef000 b61f2000 r-xp /usr/lib/libXfixes.so.3.1.0
b61fb000 b61fd000 r-xp /usr/lib/libXgesture.so.7.0.0
b6205000 b6207000 r-xp /usr/lib/libXcomposite.so.1.0.0
b620f000 b6211000 r-xp /usr/lib/libXdamage.so.1.1.0
b6219000 b6220000 r-xp /usr/lib/libXcursor.so.1.0.2
b6228000 b622b000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6234000 b6238000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6241000 b6246000 r-xp /usr/lib/libecore_fb.so.1.7.99
b624f000 b6330000 r-xp /usr/lib/libX11.so.6.3.0
b633b000 b635e000 r-xp /usr/lib/libjpeg.so.8.0.2
b6376000 b638c000 r-xp /lib/libz.so.1.2.5
b6395000 b6397000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b639f000 b6414000 r-xp /usr/lib/libsqlite3.so.0.8.6
b641e000 b6438000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6440000 b6474000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b647d000 b6550000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b655c000 b656c000 r-xp /lib/libresolv-2.13.so
b6570000 b6588000 r-xp /usr/lib/liblzma.so.5.0.3
b6590000 b6593000 r-xp /lib/libcap.so.2.21
b659b000 b65ca000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b65d2000 b65d3000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b65dc000 b65e2000 r-xp /usr/lib/libecore_imf.so.1.7.99
b65ea000 b6601000 r-xp /usr/lib/liblua-5.1.so
b660a000 b6611000 r-xp /usr/lib/libembryo.so.1.7.99
b6619000 b661f000 r-xp /lib/librt-2.13.so
b6628000 b667e000 r-xp /usr/lib/libpixman-1.so.0.28.2
b668c000 b66e2000 r-xp /usr/lib/libfreetype.so.6.11.3
b66ee000 b6716000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6717000 b675c000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6765000 b6778000 r-xp /usr/lib/libfribidi.so.0.3.1
b6780000 b679a000 r-xp /usr/lib/libecore_con.so.1.7.99
b67a4000 b67ad000 r-xp /usr/lib/libedbus.so.1.7.99
b67b5000 b6805000 r-xp /usr/lib/libecore_x.so.1.7.99
b6807000 b6810000 r-xp /usr/lib/libvconf.so.0.2.45
b6818000 b6829000 r-xp /usr/lib/libecore_input.so.1.7.99
b6831000 b6836000 r-xp /usr/lib/libecore_file.so.1.7.99
b683e000 b6860000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6869000 b68aa000 r-xp /usr/lib/libeina.so.1.7.99
b68b3000 b68cc000 r-xp /usr/lib/libeet.so.1.7.99
b68dd000 b6946000 r-xp /lib/libm-2.13.so
b694f000 b6955000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b695e000 b695f000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6967000 b698a000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6992000 b6997000 r-xp /usr/lib/libxdgmime.so.1.1.0
b699f000 b69c9000 r-xp /usr/lib/libdbus-1.so.3.8.12
b69d2000 b69e9000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69f1000 b69fc000 r-xp /lib/libunwind.so.8.0.1
b6a29000 b6a47000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a51000 b6b75000 r-xp /lib/libc-2.13.so
b6b83000 b6b8b000 r-xp /lib/libgcc_s-4.6.so.1
b6b8c000 b6b90000 r-xp /usr/lib/libsmack.so.1.0.0
b6b99000 b6b9f000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ba7000 b6c77000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c78000 b6cd6000 r-xp /usr/lib/libedje.so.1.7.99
b6ce0000 b6cf7000 r-xp /usr/lib/libecore.so.1.7.99
b6d0e000 b6ddc000 r-xp /usr/lib/libevas.so.1.7.99
b6e02000 b6f3e000 r-xp /usr/lib/libelementary.so.1.7.99
b6f55000 b6f69000 r-xp /lib/libpthread-2.13.so
b6f74000 b6f76000 r-xp /usr/lib/libdlog.so.0.0.0
b6f7e000 b6f81000 r-xp /usr/lib/libbundle.so.0.1.22
b6f89000 b6f8b000 r-xp /lib/libdl-2.13.so
b6f94000 b6fa1000 r-xp /usr/lib/libaul.so.0.1.0
b6fb3000 b6fb9000 r-xp /usr/lib/libappcore-efl.so.1.1
b6fc2000 b6fc6000 r-xp /usr/lib/libsys-assert.so
b6fcf000 b6fec000 r-xp /lib/ld-2.13.so
b6ff5000 b6ffa000 r-xp /usr/bin/launchpad-loader
b8e0a000 b9029000 rw-p [heap]
bec8b000 becac000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2073)
Call Stack Count: 1
 0: realloc + 0x4c (0xb6ac36f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
string_version: 1
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1030, noti_type: 1
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 1, content_id: 0, notification_id: 1419728903
04-26 17:25:41.159+0700 W/wnotib  ( 1203): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(417) > action type = 6, input type: 3
04-26 17:25:41.159+0700 W/wnotib  ( 1203): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(421) > 
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(471) > Activity fetch finished: -1
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(476) > activity action count: 2
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(481) > 2 activities retrieved.
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(544) > package_name jp.naver.line.android
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 79, application_id: 219, type: 10
04-26 17:25:41.159+0700 W/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_message_receive(5214) > notiboard panel creation count [2], notiboard card appending count [2].
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 81, application_id: -1016, type: 10
04-26 17:25:41.159+0700 W/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_message_receive(5214) > notiboard panel creation count [2], notiboard card appending count [2].
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(714) > This noti will be removed from panel: 81, Real Feed Back.
04-26 17:25:41.159+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_remove_card(419) > db_id: 1034, application_id: -1016, application_name: Real Feed Back
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 81, application_id: -1016, type: 2
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3278) > db_id: 1034, is_request_from_noti_service: 1
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3432) > We don't need to delete the item for second_depth_type: 0.
04-26 17:25:41.159+0700 E/EFL     ( 1203): elementary<1203> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bpanel_coverview_gl_item_del(4095) > card[0xb8c89998]
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3529) > Hide the drawer for the current panel.
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_destroy(5758) > Destory panel, panel application_id [-1016], panel category_id [81]
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_deinitialize(5671) > Deinit drawer.
04-26 17:25:41.159+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_terminate_input_selector(5550) > No need to close w-input-selector.
04-26 17:25:41.159+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 14
04-26 17:25:41.169+0700 W/AUL_AMD (  969): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-26 17:25:41.169+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_terminate_noti_composer(5585) > ret : 0, is_running : 0
04-26 17:25:41.169+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_sending_popup_del_cb(705) > 0xb8e1f368, g_sending_popup_state: 0
04-26 17:25:41.169+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8d84cb0 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:41.169+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8d84cb0 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_more_option.c: eext_more_option_items_clear(572) > called!!
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_items_clear(2473) > called!!
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_more_option.c: _more_option_del_cb(268) > called!!
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_more_option.c: _panel_del_cb(173) > called!!
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _rotary_selector_del_cb(826) > called!!
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb884b628, elm_layout, func : 0xb68168c9
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb884b628, obj: 0xb884b628
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-26 17:25:41.179+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _event_area_del_cb(494) > called!!
04-26 17:25:41.189+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_deinitialize(5721) > g_wnb_action_data is freed.
04-26 17:25:41.189+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_reset_scroll_config(528) > Reset thumbscroll_sensitivity_friction to 1.600000
04-26 17:25:41.189+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_reset_scroll_config(530) > After thumbscroll_sensitivity_friction: 1.000000
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb8c937a0
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb87933b8, elm_box, _activated_obj : 0x0, activated : 1
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8c937a0, elm_genlist, _activated_obj : 0xb87933b8, activated : 0
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8c937a0, elm_genlist, func : 0xaf216fa1
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-26 17:25:41.189+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_set_current_detail_noti(1441) > Set current_detail_noti_db_id to 0
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb8c937a0, obj: 0xb8c937a0
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-26 17:25:41.189+0700 E/EFL     ( 1203): elementary<1203> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-26 17:25:41.189+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8c937a0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:41.189+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8c937a0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-26 17:25:41.189+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8c937a0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:41.189+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8c937a0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8c937a0, elm_genlist, func : 0xb680cea1
04-26 17:25:41.189+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-26 17:25:41.189+0700 W/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_create_noti_detail_default_image(543) > Panel will be removed , skip make default image. stack list count [0].
04-26 17:25:41.199+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8e13b38 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:41.199+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8e13b38 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-26 17:25:41.199+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8e13b38 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:41.199+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8e13b38 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-26 17:25:41.199+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb876e8b8 : elm_scroller] mx(3600), my(0), minx(0), miny(0), px(720), py(0)
04-26 17:25:41.199+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb876e8b8 : elm_scroller] cw(3960), ch(360), pw(360), ph(360)
04-26 17:25:41.199+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb876e8b8 : elm_scroller] x(360), y(0), nx(360), px(720), ny(0) py(0)
04-26 17:25:41.199+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb876e8b8 : elm_scroller] x(360), y(0)
04-26 17:25:41.199+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_activate(3236) > page_index: 0.
04-26 17:25:41.199+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_select(586) > 0
04-26 17:25:41.199+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_select(596) > select index:1
04-26 17:25:41.199+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_show(539) > 
04-26 17:25:41.199+0700 W/W_HOME  ( 1203): index.c: index_show(300) > is_paused:1 show VI:1 visibility:0 vi:(nil)
04-26 17:25:41.209+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_destroy(5920) > Destory panel done.
04-26 17:25:41.209+0700 W/wnotib  ( 1203): w-notification-board-panel-manager.c: wnb_pm_destroy_panel(330) > page_instance 81, 0xb8e4c570 is destoryed!
04-26 17:25:41.209+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 79, application_id: 219, type: 9
04-26 17:25:41.209+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(724) > Num categories: 1, num total noti: 1
04-26 17:25:41.209+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_free_data(248) > Free noti manager data.
04-26 17:25:41.209+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_free_data(253) > Free previous notifications.
04-26 17:25:41.209+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_free_data(274) > Free previous categories.
04-26 17:25:41.209+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(747) > before_rpanel_count: 3, after_rpanel_count: 2.
04-26 17:25:41.209+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_update(560) > 0x2
04-26 17:25:41.229+0700 E/W_HOME  ( 1203): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
04-26 17:25:41.229+0700 E/W_HOME  ( 1203): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-26 17:25:41.229+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_update(560) > 0x2
04-26 17:25:41.279+0700 E/wnoti-proxy( 1888): wnoti.c: run_action_caller_cb(3933) > activity_type : 3
04-26 17:25:41.279+0700 E/W_HOME  ( 1203): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
04-26 17:25:41.279+0700 E/W_HOME  ( 1203): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-26 17:25:41.279+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_activate(3236) > page_index: 0.
04-26 17:25:41.279+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_select(586) > 0
04-26 17:25:41.279+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_select(596) > select index:1
04-26 17:25:41.279+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_show(539) > 
04-26 17:25:41.279+0700 W/W_HOME  ( 1203): index.c: index_show(300) > is_paused:1 show VI:1 visibility:0 vi:(nil)
04-26 17:25:41.299+0700 I/CAPI_NETWORK_CONNECTION( 2073): connection.c: connection_create(453) > New handle created[0xb8fc5f70]
04-26 17:25:41.359+0700 I/CAPI_NETWORK_CONNECTION( 2073): connection.c: connection_destroy(471) > Destroy handle: 0xb8fc5f70
04-26 17:25:41.369+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:41.379+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8f38c80 : elm_genlist] mx(0), my(272), minx(0), miny(0), px(0), py(0)
04-26 17:25:41.379+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8f38c80 : elm_genlist] cw(360), ch(632), pw(360), ph(360)
04-26 17:25:41.689+0700 I/AUL_PAD ( 2099): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-26 17:25:41.879+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_hide(550) > 
04-26 17:25:41.879+0700 W/W_HOME  ( 1203): index.c: index_hide(338) > hide VI:1 visibility:0 vi:(nil)
04-26 17:25:42.219+0700 E/AUL_AMD (  969): amd_main.c: __platform_ready_handler(371) > [Info]__platform_ready_handler
04-26 17:25:42.309+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=111570 button=1
04-26 17:25:42.319+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.319+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.339+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.339+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.349+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.349+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.359+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.359+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.369+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.369+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.379+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.379+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.389+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.389+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.399+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.399+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.419+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.419+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.429+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.429+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.439+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.439+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.449+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.449+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.459+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.459+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.469+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.469+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.469+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4292 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] add hold animator
04-26 17:25:42.469+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.469+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.469+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(13)
04-26 17:25:42.469+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(21)
04-26 17:25:42.499+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.529+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.529+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.529+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.529+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.529+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.529+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.539+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.539+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.539+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(40)
04-26 17:25:42.539+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(60)
04-26 17:25:42.579+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.609+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(77)
04-26 17:25:42.619+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.649+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(84)
04-26 17:25:42.649+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(98)
04-26 17:25:42.689+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.729+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(103)
04-26 17:25:42.729+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.769+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(108)
04-26 17:25:42.769+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.809+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(108)
04-26 17:25:42.819+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.819+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.829+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:42.829+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:42.839+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] direction_x(0), direction_y(1)
04-26 17:25:42.839+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] drag_child_locked_y(0)
04-26 17:25:42.839+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8f38c80 : elm_genlist] move content x(0), y(107)
04-26 17:25:42.839+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.869+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=112106 button=1
04-26 17:25:42.879+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.909+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.949+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:42.979+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.009+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.049+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.269+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.299+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.319+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.329+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.349+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.369+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.379+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.399+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.419+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.429+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.449+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.449+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=112714 button=1
04-26 17:25:43.449+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:43.449+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.459+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:43.459+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:43.469+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:43.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:43.499+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:43.549+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:43.549+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:43.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] mouse move
04-26 17:25:43.569+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8f38c80 : elm_genlist] hold(0), freeze(0)
04-26 17:25:43.569+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=112834 button=1
04-26 17:25:43.759+0700 E/GL SELECTED( 2073): ID GL : TRM2
04-26 17:25:44.289+0700 I/APP_CORE( 1203): appcore-efl.c: __do_app(453) > [APP 1203] Event: MEM_FLUSH State: PAUSED
04-26 17:25:44.299+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=113560 button=1
04-26 17:25:44.409+0700 I/GATE    ( 2079): <GATE-M>FORCE_CLOSED_realtimefeedback1</GATE-M>
04-26 17:25:44.439+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=113700 button=1
04-26 17:25:44.439+0700 I/efl-extension( 2073): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-26 17:25:44.449+0700 I/efl-extension( 2073): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-26 17:25:44.449+0700 I/efl-extension( 2073): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb3b01ab8, elm_image, _activated_obj : 0xb8f47888, activated : 1
04-26 17:25:44.449+0700 I/efl-extension( 2073): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-26 17:25:44.449+0700 E/EFL     ( 2073): elementary<2073> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-26 17:25:44.449+0700 E/EFL     ( 2073): elementary<2073> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb3b01f20) will be pushed
04-26 17:25:44.459+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:44.459+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:44.459+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb495e230 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:44.459+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb495e230 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-26 17:25:44.469+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb495e230 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:44.469+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb495e230 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-26 17:25:44.489+0700 E/EFL     ( 2073): elementary<2073> elc_naviframe.c:2796 _push_transition_cb() item(0xb3b01f20) will transition
04-26 17:25:44.919+0700 E/EFL     ( 2073): elementary<2073> elc_naviframe.c:1193 _on_item_push_finished() item(0xb8f525e0) transition finished
04-26 17:25:44.919+0700 E/EFL     ( 2073): elementary<2073> elc_naviframe.c:1218 _on_item_show_finished() item(0xb3b01f20) transition finished
04-26 17:25:44.949+0700 E/EFL     ( 2073): <2073> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:25:45.089+0700 E/WMS     ( 1041): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-26 17:25:45.439+0700 E/EFL     (  935): ecore_x<935> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3c00002 time=113739
04-26 17:25:45.439+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=113739
04-26 17:25:45.439+0700 E/EFL     (  935): ecore_x<935> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=113739
04-26 17:25:45.679+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=114947 button=1
04-26 17:25:45.679+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb495e230 : elm_genlist] mouse move
04-26 17:25:45.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb495e230 : elm_genlist] mouse move
04-26 17:25:45.689+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb495e230 : elm_genlist] hold(0), freeze(0)
04-26 17:25:45.699+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb495e230 : elm_genlist] mouse move
04-26 17:25:45.699+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb495e230 : elm_genlist] hold(0), freeze(0)
04-26 17:25:45.779+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb495e230 : elm_genlist] mouse move
04-26 17:25:45.779+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb495e230 : elm_genlist] hold(0), freeze(0)
04-26 17:25:45.799+0700 E/EFL     ( 2073): ecore_x<2073> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=115057 button=1
04-26 17:25:45.819+0700 I/efl-extension( 2073): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-26 17:25:45.819+0700 I/efl-extension( 2073): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-26 17:25:45.819+0700 I/efl-extension( 2073): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb494deb8, elm_image, _activated_obj : 0xb3b01ab8, activated : 1
04-26 17:25:45.819+0700 I/efl-extension( 2073): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-26 17:25:45.829+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8fbc7a8 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-26 17:25:45.829+0700 E/EFL     ( 2073): elementary<2073> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8fbc7a8 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-26 17:25:45.929+0700 W/CRASH_MANAGER( 2079): worker.c: worker_job(1205) > 1102073726561152473834
