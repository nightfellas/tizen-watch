S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2374
Date: 2018-04-27 13:19:18+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2374, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb5882248
r2   = 0xb5882248, r3   = 0xb586e28d
r4   = 0xbe873444, r5   = 0xb5b94800
r6   = 0x00000274, r7   = 0xbe873098
r8   = 0x00000000, r9   = 0xffffffff
r10  = 0xb6c378e4, fp   = 0xb6c378e4
ip   = 0xb5b9f084, sp   = 0xbe873028
lr   = 0xb586e2af, pc   = 0xb586e3a0
cpsr = 0xa0000030

Memory Information
MemTotal:   405512 KB
MemFree:     10856 KB
Buffers:      5384 KB
Cached:     111696 KB
VmPeak:     125440 KB
VmSize:     123768 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       24504 KB
VmRSS:       24108 KB
VmData:      63536 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          84 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2374 TID = 2374
2374 2404 2408 2443 

Maps Information
b0961000 b1255000 rw-p [stack:2443]
b12dc000 b12df000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b1f62000 b1f66000 r-xp /usr/lib/libogg.so.0.7.1
b1f6e000 b1f90000 r-xp /usr/lib/libvorbis.so.0.4.3
b1f98000 b1fdf000 r-xp /usr/lib/libsndfile.so.1.0.26
b1feb000 b2034000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b203d000 b2042000 r-xp /usr/lib/libjson.so.0.0.1
b38e3000 b39e9000 r-xp /usr/lib/libicuuc.so.57.1
b39ff000 b3b87000 r-xp /usr/lib/libicui18n.so.57.1
b3b97000 b3ba4000 r-xp /usr/lib/libail.so.0.1.0
b3bad000 b3bb0000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3bb8000 b3bf0000 r-xp /usr/lib/libpulse.so.0.16.2
b3bf1000 b3bf4000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3bfc000 b3c5d000 r-xp /usr/lib/libasound.so.2.0.0
b3c67000 b3c80000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c89000 b3c94000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3ca1000 b3ca5000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3cae000 b3cc6000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3cd7000 b3cde000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3e23000 b3e27000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3e2f000 b3e3a000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3e42000 b3e44000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3e4c000 b3e4d000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3e55000 b3e5d000 r-xp /usr/lib/libfeedback.so.0.1.4
b3e6d000 b3e6e000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3e76000 b3e77000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3f9a000 b4021000 rw-s anon_inode:dmabuf
b4022000 b4821000 rw-p [stack:2408]
b49c1000 b49c2000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4ac9000 b52c8000 rw-p [stack:2404]
b52c8000 b52ca000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52d2000 b52e9000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52f6000 b52f8000 r-xp /usr/lib/libdri2.so.0.0.0
b5300000 b530b000 r-xp /usr/lib/libtbm.so.1.0.0
b5313000 b531b000 r-xp /usr/lib/libdrm.so.2.4.0
b5323000 b5325000 r-xp /usr/lib/libgenlock.so
b532d000 b5332000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b533a000 b5345000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b554e000 b5618000 r-xp /usr/lib/libCOREGL.so.4.0
b5629000 b5639000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5641000 b5647000 r-xp /usr/lib/libxcb-render.so.0.0.0
b564f000 b5650000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5659000 b565c000 r-xp /usr/lib/libEGL.so.1.4
b5664000 b5672000 r-xp /usr/lib/libGLESv2.so.2.0
b567b000 b56c4000 r-xp /usr/lib/libmdm.so.1.2.70
b56cd000 b56d3000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56db000 b56e4000 r-xp /usr/lib/libcom-core.so.0.0.1
b56ed000 b57a5000 r-xp /usr/lib/libcairo.so.2.11200.14
b57b0000 b57c9000 r-xp /usr/lib/libnetwork.so.0.0.0
b57d1000 b57dd000 r-xp /usr/lib/libnotification.so.0.1.0
b57e6000 b57f5000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b57fe000 b581f000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5827000 b582c000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5834000 b5839000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b5841000 b5851000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5859000 b5861000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5869000 b5873000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a18000 b5a22000 r-xp /lib/libnss_files-2.13.so
b5a2b000 b5afa000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b10000 b5b34000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b3d000 b5b43000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b4b000 b5b4f000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b5c000 b5b67000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b6f000 b5b71000 r-xp /usr/lib/libiniparser.so.0
b5b7a000 b5b7f000 r-xp /usr/lib/libappcore-common.so.1.1
b5b87000 b5b89000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b92000 b5b96000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5ba3000 b5ba5000 r-xp /usr/lib/libXau.so.6.0.0
b5bad000 b5bb4000 r-xp /lib/libcrypt-2.13.so
b5be4000 b5be6000 r-xp /usr/lib/libiri.so
b5bef000 b5d81000 r-xp /usr/lib/libcrypto.so.1.0.0
b5da2000 b5de9000 r-xp /usr/lib/libssl.so.1.0.0
b5df5000 b5e23000 r-xp /usr/lib/libidn.so.11.5.44
b5e2b000 b5e34000 r-xp /usr/lib/libcares.so.2.1.0
b5e3e000 b5e51000 r-xp /usr/lib/libxcb.so.1.1.0
b5e5a000 b5e5d000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e65000 b5e67000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e70000 b5f3c000 r-xp /usr/lib/libxml2.so.2.7.8
b5f49000 b5f4b000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f54000 b5f59000 r-xp /usr/lib/libffi.so.5.0.10
b5f61000 b5f62000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f6a000 b5f6d000 r-xp /lib/libattr.so.1.1.0
b5f75000 b6009000 r-xp /usr/lib/libstdc++.so.6.0.16
b601c000 b6039000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6043000 b605b000 r-xp /usr/lib/libpng12.so.0.50.0
b6063000 b6079000 r-xp /lib/libexpat.so.1.6.0
b6083000 b60c7000 r-xp /usr/lib/libcurl.so.4.3.0
b60d0000 b60da000 r-xp /usr/lib/libXext.so.6.4.0
b60e4000 b60e8000 r-xp /usr/lib/libXtst.so.6.1.0
b60f0000 b60f6000 r-xp /usr/lib/libXrender.so.1.3.0
b60fe000 b6104000 r-xp /usr/lib/libXrandr.so.2.2.0
b610c000 b610d000 r-xp /usr/lib/libXinerama.so.1.0.0
b6116000 b611f000 r-xp /usr/lib/libXi.so.6.1.0
b6127000 b612a000 r-xp /usr/lib/libXfixes.so.3.1.0
b6133000 b6135000 r-xp /usr/lib/libXgesture.so.7.0.0
b613d000 b613f000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6147000 b6149000 r-xp /usr/lib/libXdamage.so.1.1.0
b6151000 b6158000 r-xp /usr/lib/libXcursor.so.1.0.2
b6160000 b6163000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b616c000 b6170000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6179000 b617e000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6187000 b6268000 r-xp /usr/lib/libX11.so.6.3.0
b6273000 b6296000 r-xp /usr/lib/libjpeg.so.8.0.2
b62ae000 b62c4000 r-xp /lib/libz.so.1.2.5
b62cd000 b62cf000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62d7000 b634c000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6356000 b6370000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6378000 b63ac000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63b5000 b6488000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6494000 b64a4000 r-xp /lib/libresolv-2.13.so
b64a8000 b64c0000 r-xp /usr/lib/liblzma.so.5.0.3
b64c8000 b64cb000 r-xp /lib/libcap.so.2.21
b64d3000 b6502000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b650a000 b650b000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6514000 b651a000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6522000 b6539000 r-xp /usr/lib/liblua-5.1.so
b6542000 b6549000 r-xp /usr/lib/libembryo.so.1.7.99
b6551000 b6557000 r-xp /lib/librt-2.13.so
b6560000 b65b6000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65c4000 b661a000 r-xp /usr/lib/libfreetype.so.6.11.3
b6626000 b664e000 r-xp /usr/lib/libfontconfig.so.1.8.0
b664f000 b6694000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b669d000 b66b0000 r-xp /usr/lib/libfribidi.so.0.3.1
b66b8000 b66d2000 r-xp /usr/lib/libecore_con.so.1.7.99
b66dc000 b66e5000 r-xp /usr/lib/libedbus.so.1.7.99
b66ed000 b673d000 r-xp /usr/lib/libecore_x.so.1.7.99
b673f000 b6748000 r-xp /usr/lib/libvconf.so.0.2.45
b6750000 b6761000 r-xp /usr/lib/libecore_input.so.1.7.99
b6769000 b676e000 r-xp /usr/lib/libecore_file.so.1.7.99
b6776000 b6798000 r-xp /usr/lib/libecore_evas.so.1.7.99
b67a1000 b67e2000 r-xp /usr/lib/libeina.so.1.7.99
b67eb000 b6804000 r-xp /usr/lib/libeet.so.1.7.99
b6815000 b687e000 r-xp /lib/libm-2.13.so
b6887000 b688d000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6896000 b6897000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b689f000 b68c2000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68ca000 b68cf000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68d7000 b6901000 r-xp /usr/lib/libdbus-1.so.3.8.12
b690a000 b6921000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6929000 b6934000 r-xp /lib/libunwind.so.8.0.1
b6961000 b697f000 r-xp /usr/lib/libsystemd.so.0.4.0
b6989000 b6aad000 r-xp /lib/libc-2.13.so
b6abb000 b6ac3000 r-xp /lib/libgcc_s-4.6.so.1
b6ac4000 b6ac8000 r-xp /usr/lib/libsmack.so.1.0.0
b6ad1000 b6ad7000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6adf000 b6baf000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6bb0000 b6c0e000 r-xp /usr/lib/libedje.so.1.7.99
b6c18000 b6c2f000 r-xp /usr/lib/libecore.so.1.7.99
b6c46000 b6d14000 r-xp /usr/lib/libevas.so.1.7.99
b6d3a000 b6e76000 r-xp /usr/lib/libelementary.so.1.7.99
b6e8d000 b6ea1000 r-xp /lib/libpthread-2.13.so
b6eac000 b6eae000 r-xp /usr/lib/libdlog.so.0.0.0
b6eb6000 b6eb9000 r-xp /usr/lib/libbundle.so.0.1.22
b6ec1000 b6ec3000 r-xp /lib/libdl-2.13.so
b6ecc000 b6ed9000 r-xp /usr/lib/libaul.so.0.1.0
b6eeb000 b6ef1000 r-xp /usr/lib/libappcore-efl.so.1.1
b6efa000 b6efe000 r-xp /usr/lib/libsys-assert.so
b6f07000 b6f24000 r-xp /lib/ld-2.13.so
b6f2d000 b6f32000 r-xp /usr/bin/launchpad-loader
b79f5000 b85a1000 rw-p [heap]
be853000 be874000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2374)
Call Stack Count: 15
 0: loadAllNotification + 0xf (0xb586e3a0) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53a0
 1: _on_resume_cb + 0x22 (0xb586e2af) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x52af
 2: (0xb5b936ad) [/usr/lib/libcapi-appfw-application.so.0] + 0x16ad
 3: (0xb6eee151) [/usr/lib/libappcore-efl.so.1] + 0x3151
 4: (0xb6eee5f5) [/usr/lib/libappcore-efl.so.1] + 0x35f5
 5: (0xb6c20e53) [/usr/lib/libecore.so.1] + 0x8e53
 6: (0xb6c2446b) [/usr/lib/libecore.so.1] + 0xc46b
 7: ecore_main_loop_begin + 0x30 (0xb6c24879) [/usr/lib/libecore.so.1] + 0xc879
 8: appcore_efl_main + 0x332 (0xb6eeeb47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
 9: ui_app_main + 0xb0 (0xb5b93ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
10: uib_app_run + 0xea (0xb586e227) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5227
11: main + 0x34 (0xb586e64d) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x564d
12:  + 0x0 (0xb6f2ea53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
13: __libc_start_main + 0x114 (0xb69a085c) [/lib/libc.so.6] + 0x1785c
14: (0xb6f2ee0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
ey_timer_cb(1165) > [_powerkey_timer_cb:1165] clock visibility[1] pressed lcd status[1], current lcd status[1] pressed pm state[1]
04-27 13:19:15.249+0700 E/STARTER ( 1178): dbus-util.c: dbus_request_cpu_boost(292) > [dbus_request_cpu_boost:292] failed to _invoke_dbus_method_sync
04-27 13:19:15.249+0700 W/AUL     ( 1178): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.samsung.w-home)
04-27 13:19:15.249+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 0
04-27 13:19:15.259+0700 W/AUL_AMD (  991): amd_launch.c: _start_app(1782) > caller pid : 1178
04-27 13:19:15.269+0700 W/AUL     (  991): app_signal.c: aul_send_app_resume_request_signal(567) > aul_send_app_resume_request_signal app(com.samsung.w-home) pid(1256) type(uiapp) bg(0)
04-27 13:19:15.269+0700 W/AUL_AMD (  991): amd_launch.c: __nofork_processing(1229) > __nofork_processing, cmd: 0, pid: 1256
04-27 13:19:15.269+0700 E/AUL     (  991): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 13:19:15.269+0700 I/APP_CORE( 1256): appcore-efl.c: __do_app(453) > [APP 1256] Event: RESET State: RUNNING
04-27 13:19:15.269+0700 W/AUL_AMD (  991): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(1256), cmd(0)
04-27 13:19:15.269+0700 W/AUL     ( 1178): launch.c: app_request_to_launchpad(298) > request cmd(0) result(1256)
04-27 13:19:15.269+0700 W/STARTER ( 1178): pkg-monitor.c: _app_mgr_status_cb(421) > [_app_mgr_status_cb:421] Resume request [1256]
04-27 13:19:15.279+0700 I/CAPI_APPFW_APPLICATION( 1256): app_main.c: app_appcore_reset(245) > app_appcore_reset
04-27 13:19:15.279+0700 W/CAPI_APPFW_APP_CONTROL( 1256): app_control.c: app_control_error(136) > [app_control_get_extra_data] KEY_NOT_FOUND(0xffffff82)
04-27 13:19:15.279+0700 E/W_HOME  ( 1256): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
04-27 13:19:15.279+0700 W/W_HOME  ( 1256): main.c: _app_control_progress(1606) > Service value : powerkey
04-27 13:19:15.279+0700 W/wnotib  ( 1256): w-notification-board-broker-main.c: _wnb_handle_view_event(1290) > Home view event: 0x40001
04-27 13:19:15.279+0700 E/wnotib  ( 1256): w-notification-board-action.c: wnb_action_is_popup_shown(5116) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-27 13:19:15.279+0700 E/wnotib  ( 1256): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-27 13:19:15.279+0700 W/wnotib  ( 1256): w-notification-board-broker-main.c: wnb_dismiss_confirmation_popup(1406) > 
04-27 13:19:15.279+0700 W/W_HOME  ( 1256): noti_broker.c: _noti_broker_home_cb(786) > continue the home key execution
04-27 13:19:15.279+0700 E/W_HOME  ( 1256): cs_broker.c: _cs_broker_home_cb(274) > (s_info.is_displayed == 0) -> _cs_broker_home_cb() return
04-27 13:19:15.279+0700 W/W_HOME  ( 1256): main.c: _home_key_cb(1504) > Home Key operation tutorial:0 window:1 clock:1 apps:0 is_app_resumed:1 is_tts:0
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): event_manager.c: home_event_manager_allowance_get(860) > editing:0 clocklist_state:0 addviewer:0 scrolling:0 apptray-state:1 apptray-visibility:0 apptray-edit_visibility:0
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): move.c: move_move_to_apps(219) > move to apps
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): rotary.c: rotary_attach(138) > rotary_attach:0xb163ce10
04-27 13:19:15.289+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb163ce10, elm_layout, _activated_obj : 0xb8ef24b0, activated : 1
04-27 13:19:15.289+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): event_manager.c: _move_module_anim_start_cb(660) > state: 0 -> 1
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:3, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:19:15.289+0700 W/W_INDICATOR( 1181): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 13:19:15.289+0700 W/W_INDICATOR( 1181): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 13:19:15.289+0700 W/APPS    ( 1256): AppsViewNecklace.cpp: setBubbleColor(2479) >  [249, 249, 249, 255]
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): event_manager.c: _apptray_visibility_cb(598) > apps,show,start
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): event_manager.c: _apptray_visibility_cb(618) > state: 1 -> 0
04-27 13:19:15.289+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:19:15.299+0700 W/W_INDICATOR( 1181): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 13:19:15.299+0700 W/W_INDICATOR( 1181): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 13:19:15.299+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:19:15.299+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:19:15.299+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:19:15.299+0700 W/APPS    ( 1256): AppsViewNecklace.cpp: onBubbleButtonEffect(2446) >  [249, 249, 249, 255]
04-27 13:19:15.319+0700 E/AUL     ( 1178): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 13:19:15.319+0700 I/APP_CORE( 1256): appcore-efl.c: __do_app(529) > Legacy lifecycle: 1
04-27 13:19:15.629+0700 W/W_HOME  ( 1256): event_manager.c: _move_module_anim_end_cb(674) > state: 1 -> 0
04-27 13:19:15.629+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:3, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:19:15.629+0700 W/W_HOME  ( 1256): rotary.c: rotary_deattach(156) > rotary_deattach:0xb163ce10
04-27 13:19:15.629+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 13:19:15.629+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb163ce10, elm_layout, func : 0xb6f41455
04-27 13:19:15.629+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-27 13:19:15.629+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-27 13:19:15.629+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 13:19:15.629+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8ef24b0, elm_box, _activated_obj : 0xb163ce10, activated : 1
04-27 13:19:15.629+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 13:19:15.629+0700 W/W_HOME  ( 1256): event_manager.c: _apptray_visibility_cb(598) > apps,show
04-27 13:19:15.629+0700 W/W_HOME  ( 1256): event_manager.c: _apptray_visibility_cb(618) > state: 1 -> 1
04-27 13:19:15.629+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:15.639+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:0(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:15.639+0700 W/W_HOME  ( 1256): main.c: home_pause(550) > clock/widget paused
04-27 13:19:15.639+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:0(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:15.639+0700 W/APPS    ( 1256): apps_main.c: _time_changed_cb(409) >  date : 27->27
04-27 13:19:15.639+0700 W/APPS    ( 1256): AppsViewNecklace.cpp: setBubbleColor(2479) >  [249, 249, 249, 255]
04-27 13:19:15.639+0700 W/W_INDICATOR( 1181): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 13:19:15.639+0700 W/W_HOME  ( 1256): rotary.c: rotary_attach(138) > rotary_attach:0xb8faee48
04-27 13:19:15.639+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8faee48, elm_layout, _activated_obj : 0xb8ef24b0, activated : 1
04-27 13:19:15.639+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 13:19:15.639+0700 I/GATE    ( 1256): <GATE-M>SCREEN_LOADED_APP_MENU_1</GATE-M>
04-27 13:19:15.639+0700 W/W_INDICATOR( 1181): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 13:19:15.639+0700 W/W_HOME  ( 1256): win.c: win_back_gesture_disable_set(170) > disable back gesture
04-27 13:19:15.639+0700 W/W_HOME  ( 1256): win.c: win_back_gesture_disable_set(170) > disable back gesture
04-27 13:19:15.639+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:15.639+0700 E/APPS    ( 1256): apps_main.c: apps_main_resume(1123) >  resumed already
04-27 13:19:15.669+0700 W/WATCH_CORE( 1358): appcore-watch.c: __widget_pause(1113) > widget_pause
04-27 13:19:15.669+0700 W/AUL     ( 1358): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1358) status(bg) type(watchapp)
04-27 13:19:15.669+0700 E/watchface-app( 1358): watchface.cpp: OnAppPause(1122) > 
04-27 13:19:16.209+0700 E/EFL     (  936): ecore_x<936> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=295171 button=1
04-27 13:19:16.209+0700 W/W_HOME  ( 1256): event_manager.c: home_event_manager_allowance_get(860) > editing:0 clocklist_state:0 addviewer:0 scrolling:0 apptray-state:0 apptray-visibility:1 apptray-edit_visibility:0
04-27 13:19:16.269+0700 W/AUL_AMD (  991): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 13:19:16.269+0700 W/AUL_AMD (  991): amd_launch.c: __grab_timeout_handler(1453) > back key ungrab error
04-27 13:19:16.339+0700 E/EFL     (  936): ecore_x<936> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=295302 button=1
04-27 13:19:16.339+0700 E/EFL     (  936): <936> e_mod_processmgr.c:499 _e_mod_processmgr_anr_ping() safety check failed: bd == NULL
04-27 13:19:16.339+0700 E/EFL     ( 1256): ecore_x<1256> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=295302 button=1
04-27 13:19:16.339+0700 W/W_HOME  ( 1256): event_manager.c: home_event_manager_allowance_get(860) > editing:0 clocklist_state:0 addviewer:0 scrolling:0 apptray-state:0 apptray-visibility:1 apptray-edit_visibility:0
04-27 13:19:16.339+0700 W/APPS    ( 1256): AppsViewNecklace.cpp: touchPressed(1663) >  TOUCH [228, 55]
04-27 13:19:16.339+0700 E/W_HOME  ( 1256): util.c: util_is_rdu_retailmode(1530) > Cannot get the vconf for retailmode
04-27 13:19:16.359+0700 I/CAPI_NETWORK_CONNECTION( 2374): connection.c: connection_create(453) > New handle created[0xb7c71d28]
04-27 13:19:16.379+0700 E/EFL     ( 1256): ecore_x<1256> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=295302 button=1
04-27 13:19:16.379+0700 W/APPS    ( 1256): AppsViewNecklace.cpp: touchReleased(1976) >  TOUCH [228, 55]->[228, 55]
04-27 13:19:16.379+0700 E/APPS    ( 1256): AppsViewNecklace.cpp: __GetAppsItemByTouchIndex(6941) >  Can't Find AppsItem at [-1]
04-27 13:19:16.379+0700 W/APPS    ( 1256): AppsViewNecklace.cpp: runFocusAni(3491) >  nNewFocus[0], anim[1], autoLaunch[1], FocusNext[0], FocusPrev[0], FocusRecent[1], HideNextPage[0], ItemListSize[27]
04-27 13:19:16.379+0700 E/APPS    ( 1256): effect.c: apps_effect_play_sound(86) >  effect_info.sound_status: [0]
04-27 13:19:16.379+0700 W/APPS    ( 1256): AppsViewNecklace.cpp: onClickedRecentApps(3256) >  item(Recent apps) launched, open(0)
04-27 13:19:16.389+0700 W/AUL     ( 1256): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.samsung.w-taskmanager)
04-27 13:19:16.389+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 0
04-27 13:19:16.389+0700 W/AUL_AMD (  991): amd_launch.c: _start_app(1782) > caller pid : 1256
04-27 13:19:16.389+0700 I/AUL_AMD (  991): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
04-27 13:19:16.409+0700 W/AUL_AMD (  991): amd_launch.c: _start_app(2218) > pad pid(-5)
04-27 13:19:16.409+0700 W/AUL_PAD ( 1934): launchpad.c: __launchpad_main_loop(611) > Launch on type-based process-pool
04-27 13:19:16.409+0700 W/AUL_PAD ( 1934): launchpad.c: __send_result_to_caller(272) > Check app launching
04-27 13:19:16.419+0700 W/AUL     ( 2437): smack_util.c: set_app_smack_label(242) > thr_cnt: 1, signal count: 1,  try count 1, launchpad type: 1
04-27 13:19:16.419+0700 W/AUL_PAD ( 2437): launchpad_loader.c: __candidate_process_prepare_exec(259) > [candidate] before __set_access
04-27 13:19:16.419+0700 W/AUL_PAD ( 2437): launchpad_loader.c: __candidate_process_prepare_exec(264) > [candidate] after __set_access
04-27 13:19:16.419+0700 W/AUL_PAD ( 2437): launchpad_loader.c: __candidate_process_launchpad_main_loop(414) > update argument
04-27 13:19:16.419+0700 W/AUL_PAD ( 2437): launchpad_loader.c: main(680) > [candidate] dlopen(com.samsung.w-taskmanager)
04-27 13:19:16.429+0700 E/EFL     (  936): elementary<936> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8b69688 in function: elm_layout_edje_get, of type: 'edje' when expecting type: 'elm_layout'
04-27 13:19:16.429+0700 E/UXT     (  936): uxt_theme_object.c: uxt_theme_object_set_changeable_ui_enabled(41) > failed to get edje from parent object.
04-27 13:19:16.439+0700 E/EFL     (  936): ecore_evas<936> ecore_evas_extn.c:2204 ecore_evas_extn_plug_connect() Extn plug failed to connect:ipctype=0, svcname=elm_indicator_portrait, svcnum=0, svcsys=0
04-27 13:19:16.459+0700 W/AUL_PAD ( 2437): launchpad_loader.c: main(690) > [candidate] dlsym
04-27 13:19:16.479+0700 W/AUL_PAD ( 2437): launchpad_loader.c: main(694) > [candidate] dl_main(com.samsung.w-taskmanager)
04-27 13:19:16.479+0700 I/CAPI_APPFW_APPLICATION( 2437): app_main.c: app_efl_main(129) > app_efl_main
04-27 13:19:16.499+0700 I/CAPI_APPFW_APPLICATION( 2437): app_main.c: app_appcore_create(152) > app_appcore_create
04-27 13:19:16.509+0700 W/AUL     (  991): app_signal.c: aul_send_app_launch_request_signal(521) > aul_send_app_launch_request_signal app(com.samsung.w-taskmanager) pid(2437) type(uiapp) bg(0)
04-27 13:19:16.519+0700 W/AUL_AMD (  991): amd_status.c: __socket_monitor_cb(1277) > (2437) was created
04-27 13:19:16.519+0700 E/AUL     (  991): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 13:19:16.519+0700 W/AUL     ( 1256): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2437)
04-27 13:19:16.519+0700 W/W_HOME  ( 1256): util.c: apps_util_launch_main_operation(785) > Launch app:[Recent apps] ret:[0]
04-27 13:19:16.519+0700 W/W_HOME  ( 1256): util.c: send_launch_appId(620) > launch appid[com.samsung.w-taskmanager]
04-27 13:19:16.519+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:19:16.519+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:19:16.519+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:19:16.519+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 13:19:16.519+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 13:19:16.519+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:19:16.519+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.app-shortcut-widget:Apptray.Message.Launch.AppId]
04-27 13:19:16.519+0700 E/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(235) > _MessagePortService::CheckRemotePort() Failed: MESSAGEPORT_ERROR_MESSAGEPORT_NOT_FOUND
04-27 13:19:16.519+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:19:16.519+0700 W/STARTER ( 1178): pkg-monitor.c: _app_mgr_status_cb(400) > [_app_mgr_status_cb:400] Launch request [2437]
04-27 13:19:16.529+0700 E/MESSAGE_PORT( 1256): MessagePortProxy.cpp: CheckRemotePort(379) > The remote message port (Apptray.Message.Launch.AppId) is not found.
04-27 13:19:16.529+0700 E/W_HOME  ( 1256): util.c: send_launch_appId(636) > There is no remote message port
04-27 13:19:16.839+0700 I/APP_CORE( 2437): appcore-efl.c: __do_app(453) > [APP 2437] Event: RESET State: CREATED
04-27 13:19:16.839+0700 I/CAPI_APPFW_APPLICATION( 2437): app_main.c: app_appcore_reset(245) > app_appcore_reset
04-27 13:19:16.959+0700 I/efl-extension( 2437): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 13:19:16.959+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 13:19:16.959+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_event_handler_add(77) > init_count: 0
04-27 13:19:16.959+0700 I/efl-extension( 2437): efl_extension_rotary.c: _init_Xi2_system(314) > In
04-27 13:19:16.969+0700 I/efl-extension( 2437): efl_extension_rotary.c: _init_Xi2_system(375) > Done
04-27 13:19:16.969+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb800a4b8, elm_image, _activated_obj : 0x0, activated : 1
04-27 13:19:17.009+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:19:17.009+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
04-27 13:19:17.009+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-27 13:19:17.009+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:19:17.009+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
04-27 13:19:17.019+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-27 13:19:17.029+0700 E/W_TASKMANAGER( 2437): util_wc1.c: close_button_disabled_set(375) > [close_button_disabled_set:375] (ad->ly_main == NULL) -> close_button_disabled_set() return
04-27 13:19:17.089+0700 E/W_TASKMANAGER( 2437): task.c: _app_list_cb(499) > [_app_list_cb:499] pkgmgrinfo_appinfo_get_label(com.samsung.skmsa) failed(0)
04-27 13:19:17.099+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 12
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-home)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (health-data-service)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.techgraphy.DigitalTick)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.weather-widget)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.shealth.widget.pedometer)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.remote-sensor-service)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.wusvc)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.shealth-service)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-music-player.music-control-service)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.watchface-service)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.message.consumer)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.weip.consumer)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.call.consumer)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.wnotiboard-popup)!!
04-27 13:19:17.099+0700 E/W_TASKMANAGER( 2437): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-taskmanager)!!
04-27 13:19:17.109+0700 E/RUA     ( 2437): rua.c: rua_history_load_db(278) > rua_history_load_db ok. nrows : 17, ncols : 5
04-27 13:19:17.139+0700 E/EFL     ( 2437): evas_main<2437> evas_stack.c:158 evas_object_stack_above() BITCH! evas_object_stack_above(), 0xb80a91b0 not inside same smart as 0xb8041528!
04-27 13:19:17.149+0700 E/EFL     ( 2437): elementary<2437> elm_layout.c:1021 _elm_layout_smart_content_set() could not swallow 0xb801bc70 into part 'elm.swallow.event.0'
04-27 13:19:17.179+0700 I/APP_CORE( 2437): appcore-efl.c: __do_app(522) > Legacy lifecycle: 0
04-27 13:19:17.179+0700 I/APP_CORE( 2437): appcore-efl.c: __do_app(524) > [APP 2437] Initial Launching, call the resume_cb
04-27 13:19:17.179+0700 I/CAPI_APPFW_APPLICATION( 2437): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-27 13:19:17.219+0700 W/W_HOME  ( 1256): event_manager.c: _ecore_x_message_cb(421) > state: 0 -> 1
04-27 13:19:17.219+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:17.219+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:17.219+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:17.219+0700 W/W_HOME  ( 1256): win.c: win_back_gesture_disable_set(170) > disable back gesture
04-27 13:19:17.219+0700 W/W_HOME  ( 1256): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 0
04-27 13:19:17.219+0700 W/W_INDICATOR( 1181): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 13:19:17.219+0700 W/W_INDICATOR( 1181): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 13:19:17.429+0700 W/APP_CORE( 2437): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3400003
04-27 13:19:17.439+0700 E/AUL     (  991): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 13:19:17.439+0700 W/W_HOME  ( 1256): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(1)
04-27 13:19:17.439+0700 W/W_HOME  ( 1256): event_manager.c: _window_visibility_cb(470) > state: 1 -> 0
04-27 13:19:17.439+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:17.439+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:6, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:17.439+0700 W/W_HOME  ( 1256): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(1)
04-27 13:19:17.439+0700 I/APP_CORE( 1256): appcore-efl.c: __do_app(453) > [APP 1256] Event: PAUSE State: RUNNING
04-27 13:19:17.439+0700 I/CAPI_APPFW_APPLICATION( 1256): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-27 13:19:17.439+0700 W/W_HOME  ( 1256): main.c: _appcore_pause_cb(489) > appcore pause
04-27 13:19:17.439+0700 W/W_HOME  ( 1256): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
04-27 13:19:17.439+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:17.449+0700 W/W_INDICATOR( 1181): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 13:19:17.449+0700 W/W_INDICATOR( 1181): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 13:19:17.449+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:17.449+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-27 13:19:17.449+0700 W/W_HOME  ( 1256): rotary.c: rotary_deattach(156) > rotary_deattach:0xb8faee48
04-27 13:19:17.449+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 13:19:17.449+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8faee48, elm_layout, func : 0xb6f41455
04-27 13:19:17.449+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-27 13:19:17.449+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-27 13:19:17.449+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 13:19:17.449+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8ef24b0, elm_box, _activated_obj : 0xb8faee48, activated : 1
04-27 13:19:17.449+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.459+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 E/APPS    ( 1256): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-27 13:19:17.469+0700 W/W_HOME  ( 1256): win.c: win_back_gesture_disable_set(170) > disable back gesture
04-27 13:19:17.469+0700 W/AUL     (  991): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1256) status(bg) type(uiapp)
04-27 13:19:17.469+0700 W/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1256] goes to (4)
04-27 13:19:17.469+0700 E/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1256)'s state(4)
04-27 13:19:17.469+0700 W/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2437] goes to (3)
04-27 13:19:17.479+0700 W/AUL     (  991): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-taskmanager) pid(2437) status(fg) type(uiapp)
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 13:19:17.489+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:19:17.489+0700 W/MUSIC_CONTROL_SERVICE( 1758): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1758]   [com.samsung.w-home]register msg port [false][0m
04-27 13:19:17.489+0700 I/wnotib  ( 1256): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 1
04-27 13:19:17.489+0700 E/wnotib  ( 1256): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-27 13:19:17.489+0700 W/wnotib  ( 1256): w-notification-board-noti-manager.c: wnb_nm_postpone_updating_job(997) > Set is_notiboard_update_postponed to true with is_for_VI 0, notiboard panel creation count [2], notiboard card appending count [2].
04-27 13:19:17.529+0700 I/APP_CORE( 2437): appcore-efl.c: __do_app(453) > [APP 2437] Event: RESUME State: RUNNING
04-27 13:19:17.529+0700 I/GATE    ( 2437): <GATE-M>APP_FULLY_LOADED_w-taskmanager</GATE-M>
04-27 13:19:17.579+0700 W/APPS    ( 1256): AppsViewNecklace.cpp: onPausedIdlerCb(5178) >  elm_cache_all_flush
04-27 13:19:17.679+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 13:19:17.699+0700 W/AUL_AMD (  991): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2437
04-27 13:19:17.699+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 12
04-27 13:19:17.719+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 13:19:17.739+0700 W/AUL_AMD (  991): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2437
04-27 13:19:17.739+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 12
04-27 13:19:17.749+0700 I/CAPI_NETWORK_CONNECTION( 2374): connection.c: connection_create(453) > New handle created[0xb7c72178]
04-27 13:19:17.959+0700 I/CAPI_NETWORK_CONNECTION( 2374): connection.c: connection_destroy(471) > Destroy handle: 0xb7c72178
04-27 13:19:17.989+0700 I/APP_CORE( 1256): appcore-efl.c: __do_app(453) > [APP 1256] Event: MEM_FLUSH State: PAUSED
04-27 13:19:18.319+0700 I/efl-extension( 2452): efl_extension.c: eext_mod_init(40) > Init
04-27 13:19:18.339+0700 E/EFL     ( 2437): ecore_x<2437> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=297297 button=1
04-27 13:19:18.339+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] mouse move
04-27 13:19:18.339+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] hold(0), freeze(0)
04-27 13:19:18.349+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] mouse move
04-27 13:19:18.349+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] hold(0), freeze(0)
04-27 13:19:18.439+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] mouse move
04-27 13:19:18.439+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] hold(0), freeze(0)
04-27 13:19:18.449+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] mouse move
04-27 13:19:18.449+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] hold(0), freeze(0)
04-27 13:19:18.459+0700 I/UXT     ( 2452): Uxt_ObjectManager.cpp: OnInitialized(753) > Initialized.
04-27 13:19:18.459+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] mouse move
04-27 13:19:18.459+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7fec530 : elm_scroller] hold(0), freeze(0)
04-27 13:19:18.479+0700 E/EFL     ( 2437): ecore_x<2437> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=297438 button=1
04-27 13:19:18.519+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 13:19:18.529+0700 W/AUL_AMD (  991): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2374
04-27 13:19:18.529+0700 W/AUL     ( 2437): launch.c: app_request_to_launchpad(284) > request cmd(1) to(com.toyota.realtimefeedback)
04-27 13:19:18.529+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 1
04-27 13:19:18.529+0700 W/AUL_AMD (  991): amd_launch.c: _start_app(1782) > caller pid : 2437
04-27 13:19:18.549+0700 W/AUL     (  991): app_signal.c: aul_send_app_resume_request_signal(567) > aul_send_app_resume_request_signal app(com.toyota.realtimefeedback) pid(2374) type(uiapp) bg(0)
04-27 13:19:18.549+0700 W/AUL_AMD (  991): amd_launch.c: __nofork_processing(1229) > __nofork_processing, cmd: 1, pid: 2374
04-27 13:19:18.549+0700 W/AUL_AMD (  991): amd_launch.c: _resume_app(867) > resume done
04-27 13:19:18.549+0700 W/AUL_AMD (  991): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(2374), cmd(3)
04-27 13:19:18.549+0700 W/AUL     ( 2437): launch.c: app_request_to_launchpad(298) > request cmd(1) result(2374)
04-27 13:19:18.549+0700 E/W_TASKMANAGER( 2437): task.c: taskmanager_launch_task_info(956) > [taskmanager_launch_task_info:956] _launch_app() failed(2374)
04-27 13:19:18.549+0700 W/STARTER ( 1178): pkg-monitor.c: _app_mgr_status_cb(421) > [_app_mgr_status_cb:421] Resume request [2374]
04-27 13:19:18.559+0700 E/AUL     (  991): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 13:19:18.579+0700 W/AUL     (  991): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.toyota.realtimefeedback) pid(2374) status(fg) type(uiapp)
04-27 13:19:18.599+0700 W/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2374] goes to (3)
04-27 13:19:18.709+0700 I/APP_CORE( 2374): appcore-efl.c: __do_app(453) > [APP 2374] Event: RESUME State: PAUSED
04-27 13:19:18.709+0700 I/CAPI_APPFW_APPLICATION( 2374): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-27 13:19:18.709+0700 I/APP_CORE( 2437): appcore-efl.c: __do_app(453) > [APP 2437] Event: PAUSE State: RUNNING
04-27 13:19:18.719+0700 I/CAPI_APPFW_APPLICATION( 2437): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-27 13:19:18.719+0700 W/AUL     (  991): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-taskmanager) pid(2437) status(bg) type(uiapp)
04-27 13:19:18.729+0700 W/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2437] goes to (4)
04-27 13:19:18.919+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 22
04-27 13:19:18.919+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(999) > app status : 4
04-27 13:19:18.919+0700 E/APP_CORE( 2437): appcore.c: __del_vconf(453) > [FAILED]vconfkey_ignore_key_changed
04-27 13:19:18.919+0700 I/APP_CORE( 2437): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-27 13:19:18.919+0700 I/CAPI_APPFW_APPLICATION( 2437): app_main.c: app_appcore_terminate(177) > app_appcore_terminate
04-27 13:19:18.919+0700 I/efl-extension( 2437): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb7fec530, obj: 0xb7fec530
04-27 13:19:18.919+0700 I/efl-extension( 2437): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-27 13:19:18.919+0700 I/efl-extension( 2437): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-27 13:19:18.919+0700 I/efl-extension( 2437): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-27 13:19:18.919+0700 I/efl-extension( 2437): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-27 13:19:18.929+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:19:18.929+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
04-27 13:19:18.929+0700 E/EFL     ( 2437): elementary<2437> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7fec530 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb800a4b8
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7ffa360 is freed
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7fec530, elm_scroller, func : 0xb374e379
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb800a4b8, elm_image, func : 0xb374e379
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 13:19:18.939+0700 I/efl-extension( 2437): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb7fec530 : elm_scroller] rotary callabck is deleted
04-27 13:19:18.999+0700 I/APP_CORE( 2437): appcore-efl.c: __after_loop(1243) > [APP 2437] After terminate() callback
04-27 13:19:19.129+0700 E/AUL     (  991): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 13:19:19.169+0700 I/UXT     ( 2437): uxt_theme_private.c: uxt_theme_delete_file_monitor(1655) > deleted color config file monitor
04-27 13:19:19.169+0700 I/UXT     ( 2437): uxt_theme_private.c: uxt_theme_delete_file_monitor(1662) > deleted font config file monitor
04-27 13:19:19.169+0700 I/UXT     ( 2437): Uxt_ObjectManager.cpp: OnTerminating(774) > Terminating.
04-27 13:19:19.279+0700 I/AUL_PAD ( 2452): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-27 13:19:19.319+0700 I/Adreno-EGL( 2452): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-27 13:19:19.319+0700 I/Adreno-EGL( 2452): OpenGL ES Shader Compiler Version: E031.24.00.16
04-27 13:19:19.319+0700 I/Adreno-EGL( 2452): Build Date: 09/02/15 Wed
04-27 13:19:19.319+0700 I/Adreno-EGL( 2452): Local Branch: 
04-27 13:19:19.319+0700 I/Adreno-EGL( 2452): Remote Branch: 
04-27 13:19:19.319+0700 I/Adreno-EGL( 2452): Local Patches: 
04-27 13:19:19.319+0700 I/Adreno-EGL( 2452): Reconstruct Branch: 
04-27 13:19:19.519+0700 W/CRASH_MANAGER( 2457): worker.c: worker_job(1205) > 1102374726561152480995
