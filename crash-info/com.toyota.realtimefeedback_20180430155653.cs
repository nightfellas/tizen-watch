S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2111
Date: 2018-04-30 15:56:53+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2111, uid 5000)

Register Information
r0   = 0x495f455f, r1   = 0xb6f31ef8
r2   = 0xb6f21efc, r3   = 0x00001c30
r4   = 0xb6f31ef8, r5   = 0x495f455f
r6   = 0xb6de5b88, r7   = 0xb6f223e4
r8   = 0x00000000, r9   = 0xb87f4bd8
r10  = 0xbeed02a8, fp   = 0x00000000
ip   = 0xb6f48c28, sp   = 0xbeed00d0
lr   = 0xb6efa60f, pc   = 0xb6d43e06
cpsr = 0x20000030

Memory Information
MemTotal:   405512 KB
MemFree:      4504 KB
Buffers:      7784 KB
Cached:     118292 KB
VmPeak:     104424 KB
VmSize:     102260 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23896 KB
VmRSS:       23896 KB
VmData:      42028 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          70 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2111 TID = 2111
2111 2137 2138 

Maps Information
b19a7000 b1a2e000 rw-s anon_inode:dmabuf
b1a2e000 b1ab5000 rw-s anon_inode:dmabuf
b1baa000 b1bad000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b1e1e000 b1e22000 r-xp /usr/lib/libogg.so.0.7.1
b1e2a000 b1e4c000 r-xp /usr/lib/libvorbis.so.0.4.3
b1e54000 b1e9b000 r-xp /usr/lib/libsndfile.so.1.0.26
b1ea7000 b1ef0000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b3792000 b3898000 r-xp /usr/lib/libicuuc.so.57.1
b38ae000 b3a36000 r-xp /usr/lib/libicui18n.so.57.1
b3a46000 b3a53000 r-xp /usr/lib/libail.so.0.1.0
b3a5c000 b3a94000 r-xp /usr/lib/libpulse.so.0.16.2
b3a95000 b3af6000 r-xp /usr/lib/libasound.so.2.0.0
b3c05000 b3c0a000 r-xp /usr/lib/libjson.so.0.0.1
b3c12000 b3c15000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3c1d000 b3c20000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3c28000 b3c41000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c4a000 b3c55000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3c62000 b3c66000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3c6f000 b3c87000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3c98000 b3c9f000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3de3000 b3de7000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3def000 b3dfa000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3e02000 b3e04000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3e0c000 b3e0d000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3e15000 b3e1d000 r-xp /usr/lib/libfeedback.so.0.1.4
b3e2d000 b3e2e000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3e36000 b3e37000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3ebe000 b3f45000 rw-s anon_inode:dmabuf
b3f5a000 b3fe1000 rw-s anon_inode:dmabuf
b3fe2000 b47e1000 rw-p [stack:2138]
b4b09000 b4b0a000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b92000 b5391000 rw-p [stack:2137]
b5391000 b5393000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b539b000 b53b2000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b53bf000 b53c1000 r-xp /usr/lib/libdri2.so.0.0.0
b53c9000 b53d4000 r-xp /usr/lib/libtbm.so.1.0.0
b53dc000 b53e4000 r-xp /usr/lib/libdrm.so.2.4.0
b53ec000 b53ee000 r-xp /usr/lib/libgenlock.so
b53f6000 b53fb000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5403000 b540e000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b5617000 b56e1000 r-xp /usr/lib/libCOREGL.so.4.0
b56f2000 b5702000 r-xp /usr/lib/libmdm-common.so.1.1.25
b570a000 b5710000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5718000 b5719000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5722000 b5725000 r-xp /usr/lib/libEGL.so.1.4
b572d000 b573b000 r-xp /usr/lib/libGLESv2.so.2.0
b5744000 b578d000 r-xp /usr/lib/libmdm.so.1.2.70
b5796000 b579c000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b57a4000 b57ad000 r-xp /usr/lib/libcom-core.so.0.0.1
b57b6000 b586e000 r-xp /usr/lib/libcairo.so.2.11200.14
b5879000 b5892000 r-xp /usr/lib/libnetwork.so.0.0.0
b589a000 b58a6000 r-xp /usr/lib/libnotification.so.0.1.0
b58af000 b58be000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b58c7000 b58e8000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58f0000 b58f5000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58fd000 b5902000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b590a000 b591a000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5922000 b592a000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5932000 b593c000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5ae1000 b5aeb000 r-xp /lib/libnss_files-2.13.so
b5af4000 b5bc3000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5bd9000 b5bfd000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5c06000 b5c0c000 r-xp /usr/lib/libappsvc.so.0.1.0
b5c14000 b5c18000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5c25000 b5c30000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5c38000 b5c3a000 r-xp /usr/lib/libiniparser.so.0
b5c43000 b5c48000 r-xp /usr/lib/libappcore-common.so.1.1
b5c50000 b5c52000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c5b000 b5c5f000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c6c000 b5c6e000 r-xp /usr/lib/libXau.so.6.0.0
b5c76000 b5c7d000 r-xp /lib/libcrypt-2.13.so
b5cad000 b5caf000 r-xp /usr/lib/libiri.so
b5cb8000 b5e4a000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e6b000 b5eb2000 r-xp /usr/lib/libssl.so.1.0.0
b5ebe000 b5eec000 r-xp /usr/lib/libidn.so.11.5.44
b5ef4000 b5efd000 r-xp /usr/lib/libcares.so.2.1.0
b5f07000 b5f1a000 r-xp /usr/lib/libxcb.so.1.1.0
b5f23000 b5f26000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5f2e000 b5f30000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5f39000 b6005000 r-xp /usr/lib/libxml2.so.2.7.8
b6012000 b6014000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b601d000 b6022000 r-xp /usr/lib/libffi.so.5.0.10
b602a000 b602b000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b6033000 b6036000 r-xp /lib/libattr.so.1.1.0
b603e000 b60d2000 r-xp /usr/lib/libstdc++.so.6.0.16
b60e5000 b6102000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b610c000 b6124000 r-xp /usr/lib/libpng12.so.0.50.0
b612c000 b6142000 r-xp /lib/libexpat.so.1.6.0
b614c000 b6190000 r-xp /usr/lib/libcurl.so.4.3.0
b6199000 b61a3000 r-xp /usr/lib/libXext.so.6.4.0
b61ad000 b61b1000 r-xp /usr/lib/libXtst.so.6.1.0
b61b9000 b61bf000 r-xp /usr/lib/libXrender.so.1.3.0
b61c7000 b61cd000 r-xp /usr/lib/libXrandr.so.2.2.0
b61d5000 b61d6000 r-xp /usr/lib/libXinerama.so.1.0.0
b61df000 b61e8000 r-xp /usr/lib/libXi.so.6.1.0
b61f0000 b61f3000 r-xp /usr/lib/libXfixes.so.3.1.0
b61fc000 b61fe000 r-xp /usr/lib/libXgesture.so.7.0.0
b6206000 b6208000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6210000 b6212000 r-xp /usr/lib/libXdamage.so.1.1.0
b621a000 b6221000 r-xp /usr/lib/libXcursor.so.1.0.2
b6229000 b622c000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6235000 b6239000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6242000 b6247000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6250000 b6331000 r-xp /usr/lib/libX11.so.6.3.0
b633c000 b635f000 r-xp /usr/lib/libjpeg.so.8.0.2
b6377000 b638d000 r-xp /lib/libz.so.1.2.5
b6396000 b6398000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b63a0000 b6415000 r-xp /usr/lib/libsqlite3.so.0.8.6
b641f000 b6439000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6441000 b6475000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b647e000 b6551000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b655d000 b656d000 r-xp /lib/libresolv-2.13.so
b6571000 b6589000 r-xp /usr/lib/liblzma.so.5.0.3
b6591000 b6594000 r-xp /lib/libcap.so.2.21
b659c000 b65cb000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b65d3000 b65d4000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b65dd000 b65e3000 r-xp /usr/lib/libecore_imf.so.1.7.99
b65eb000 b6602000 r-xp /usr/lib/liblua-5.1.so
b660b000 b6612000 r-xp /usr/lib/libembryo.so.1.7.99
b661a000 b6620000 r-xp /lib/librt-2.13.so
b6629000 b667f000 r-xp /usr/lib/libpixman-1.so.0.28.2
b668d000 b66e3000 r-xp /usr/lib/libfreetype.so.6.11.3
b66ef000 b6717000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6718000 b675d000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6766000 b6779000 r-xp /usr/lib/libfribidi.so.0.3.1
b6781000 b679b000 r-xp /usr/lib/libecore_con.so.1.7.99
b67a5000 b67ae000 r-xp /usr/lib/libedbus.so.1.7.99
b67b6000 b6806000 r-xp /usr/lib/libecore_x.so.1.7.99
b6808000 b6811000 r-xp /usr/lib/libvconf.so.0.2.45
b6819000 b682a000 r-xp /usr/lib/libecore_input.so.1.7.99
b6832000 b6837000 r-xp /usr/lib/libecore_file.so.1.7.99
b683f000 b6861000 r-xp /usr/lib/libecore_evas.so.1.7.99
b686a000 b68ab000 r-xp /usr/lib/libeina.so.1.7.99
b68b4000 b68cd000 r-xp /usr/lib/libeet.so.1.7.99
b68de000 b6947000 r-xp /lib/libm-2.13.so
b6950000 b6956000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b695f000 b6960000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6968000 b698b000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6993000 b6998000 r-xp /usr/lib/libxdgmime.so.1.1.0
b69a0000 b69ca000 r-xp /usr/lib/libdbus-1.so.3.8.12
b69d3000 b69ea000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69f2000 b69fd000 r-xp /lib/libunwind.so.8.0.1
b6a2a000 b6a48000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a52000 b6b76000 r-xp /lib/libc-2.13.so
b6b84000 b6b8c000 r-xp /lib/libgcc_s-4.6.so.1
b6b8d000 b6b91000 r-xp /usr/lib/libsmack.so.1.0.0
b6b9a000 b6ba0000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ba8000 b6c78000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c79000 b6cd7000 r-xp /usr/lib/libedje.so.1.7.99
b6ce1000 b6cf8000 r-xp /usr/lib/libecore.so.1.7.99
b6d0f000 b6ddd000 r-xp /usr/lib/libevas.so.1.7.99
b6e03000 b6f3f000 r-xp /usr/lib/libelementary.so.1.7.99
b6f56000 b6f6a000 r-xp /lib/libpthread-2.13.so
b6f75000 b6f77000 r-xp /usr/lib/libdlog.so.0.0.0
b6f7f000 b6f82000 r-xp /usr/lib/libbundle.so.0.1.22
b6f8a000 b6f8c000 r-xp /lib/libdl-2.13.so
b6f95000 b6fa2000 r-xp /usr/lib/libaul.so.0.1.0
b6fb4000 b6fba000 r-xp /usr/lib/libappcore-efl.so.1.1
b6fc3000 b6fc7000 r-xp /usr/lib/libsys-assert.so
b6fd0000 b6fed000 r-xp /lib/ld-2.13.so
b6ff6000 b6ffb000 r-xp /usr/bin/launchpad-loader
b8642000 b8d73000 rw-p [heap]
beeb0000 beed1000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2111)
Call Stack Count: 22
 0: evas_object_smart_type_check_ptr + 0xd (0xb6d43e06) [/usr/lib/libevas.so.1] + 0x34e06
 1: elm_widget_type_get + 0xe (0xb6efa60f) [/usr/lib/libelementary.so.1] + 0xf760f
 2: elm_widget_type_check + 0x10 (0xb6efb415) [/usr/lib/libelementary.so.1] + 0xf8415
 3: elm_genlist_clear + 0x1c (0xb6e9970d) [/usr/lib/libelementary.so.1] + 0x9670d
 4: loadAllBodyDefect + 0x16 (0xb593763b) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x563b
 5: loadServerNotification + 0xa (0xb593786f) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x586f
 6: set_NVvc + 0x28 (0xb5937181) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5181
 7: nf_hw_back_cb + 0x32 (0xb5937273) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5273
 8: (0xb58cef4d) [/usr/lib/libefl-extension.so.0] + 0x7f4d
 9: (0xb6d26af9) [/usr/lib/libevas.so.1] + 0x17af9
10: evas_event_feed_key_up + 0x4cc (0xb6d2ed81) [/usr/lib/libevas.so.1] + 0x1fd81
11: (0xb622ac9d) [/usr/lib/libecore_input_evas.so.1] + 0x1c9d
12: (0xb6ce9e53) [/usr/lib/libecore.so.1] + 0x8e53
13: (0xb6ced46b) [/usr/lib/libecore.so.1] + 0xc46b
14: ecore_main_loop_begin + 0x30 (0xb6ced879) [/usr/lib/libecore.so.1] + 0xc879
15: appcore_efl_main + 0x332 (0xb6fb7b47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
16: ui_app_main + 0xb0 (0xb5c5ced5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
17: uib_app_run + 0xea (0xb59373ff) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53ff
18: main + 0x34 (0xb59378e9) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x58e9
19:  + 0x0 (0xb6ff7a53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
20: __libc_start_main + 0x114 (0xb6a6985c) [/lib/libc.so.6] + 0x1785c
21: (0xb6ff7e0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x360000d 0x360000d 0x3600009]
04-30 15:56:47.509+0700 I/APP_CORE( 2131): appcore-efl.c: __do_app(453) > [APP 2131] Event: RESUME State: RUNNING
04-30 15:56:47.509+0700 I/GATE    ( 2131): <GATE-M>APP_FULLY_LOADED_wnotiboard-popup</GATE-M>
04-30 15:56:47.509+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: _item_update_animator_cb(1546) > called
04-30 15:56:47.519+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: _item_update_animator_cb(1568) > item_list(0x-1217515768), count(3)
04-30 15:56:47.519+0700 E/wnoti-service( 1378): wnoti-db-client.c: wnoti_get_alert_categories(1016) > _query_step failed(NO ROW)
04-30 15:56:47.519+0700 E/wnoti-service( 1378): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 2
04-30 15:56:47.529+0700 E/wnoti-service( 1378): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-30 15:56:47.529+0700 E/wnoti-proxy( 2131): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-30 15:56:47.529+0700 E/wnotibp ( 2131): wnotiboard-popup-data.c: _data_get_alert_list(311) > ::DATA:: No categories available.
04-30 15:56:47.529+0700 W/wnotibp ( 2131): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 1, new_list count : 0
04-30 15:56:47.529+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2809) > ::UI:: VI TYPE : 1
04-30 15:56:47.529+0700 I/wnotibp ( 2131): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-30 15:56:47.529+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2816) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-30 15:56:47.529+0700 I/wnotibp ( 2131): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-30 15:56:47.529+0700 I/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_play_feedback(284) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-30 15:56:47.529+0700 W/TIZEN_N_RECORDER( 2131): recorder_product.c: recorder_is_in_recording(82) > pid =/proc/0 , state =0, alive=0
04-30 15:56:47.529+0700 W/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_play_feedback(298) > ::APP:: application_id: -1016, is_disaster: 0, disaster_info: (null), app_feedback_type: 2, feedback_pattern_app: -1, is_recording: 0
04-30 15:56:47.529+0700 I/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_play_feedback(307) > Gear side feedback setting is_sound_on: 0, is_vibration_on: 1, is_vibrate_when_noti_on: 0
04-30 15:56:47.529+0700 W/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_play_feedback(341) > ::APP:: Determined feedback: sound 0, vibration: 1
04-30 15:56:47.529+0700 I/wnotib  ( 2131): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 13, ret: -3, request_id: 0
04-30 15:56:47.529+0700 W/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_set_smart_relay(501) > Set the smart relay for 0, 156, -1016, Real Feed Back
04-30 15:56:47.539+0700 I/wnotibp ( 2131): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-30 15:56:47.539+0700 W/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_turn_on_lcd(534) > [4, 0, 1, 0]
04-30 15:56:47.539+0700 I/efl-extension( 2131): efl_extension_more_option.c: _panel_inactive_cb(99) > more_option is closed!!
04-30 15:56:47.539+0700 I/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_closed(5424) > More option closed!
04-30 15:56:47.539+0700 I/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_closed(5427) > Set the rotary focus to the parent of drawer: 0xb75b7610
04-30 15:56:47.539+0700 W/wnotibp ( 2131): w-notification-board-common.c: wnb_common_pass_rotary_focus_to_top_object(6914) > _wnb_common_rotary_focus_owner_list cnt [0], you can't pass rotray focus.
04-30 15:56:47.539+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb768f588, elm_layout, _activated_obj : 0x0, activated : 0
04-30 15:56:47.539+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_get(3146) > called!!
04-30 15:56:47.539+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_set(3107) > called!!
04-30 15:56:47.539+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_set(3140) > selected item index(0)
04-30 15:56:47.539+0700 W/efl-extension( 2131): efl_extension_events.c: eext_object_event_callback_del(325) > This object(0xb7636950) hasn't been registered before
04-30 15:56:47.559+0700 E/EFL     ( 2131): ecore_x<2131> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=130227 button=1
04-30 15:56:47.709+0700 E/EFL     ( 2131): ecore_x<2131> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=130359 button=1
04-30 15:56:47.809+0700 W/W_HOME  ( 1234): noti_broker.c: _handler_indicator_hide(550) > 
04-30 15:56:47.809+0700 W/W_HOME  ( 1234): index.c: index_hide(338) > hide VI:1 visibility:1 vi:(nil)
04-30 15:56:47.939+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_simple_popup_show_finished_cb(1130) > ::UI:: popup effect is finished (1392, b74d75b8)
04-30 15:56:47.939+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [simple popup] is UNLOCK , 0100 <=== ]]]
04-30 15:56:47.939+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(655) > ::UI:: wating unlock.
04-30 15:56:47.989+0700 I/APP_CORE( 1234): appcore-efl.c: __do_app(453) > [APP 1234] Event: MEM_FLUSH State: PAUSED
04-30 15:56:48.089+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_detail_view_by_timer_cb(1321) > ::APP:: view state=2, b74d75b8
04-30 15:56:48.089+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_vi_start(968) > ::APP:: view_state=3
04-30 15:56:48.089+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(585) > ::UI:: [[[ ===> already [detail view in simple popup] is LOCK, 0100 ]]]
04-30 15:56:48.219+0700 I/AUL_PAD ( 2150): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-30 15:56:48.699+0700 E/EFL     (  891): ecore_x<891> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x360000d time=130713
04-30 15:56:48.699+0700 E/EFL     ( 2131): ecore_x<2131> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=130713
04-30 15:56:48.699+0700 E/EFL     (  891): ecore_x<891> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=130713
04-30 15:56:48.699+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [detail view in simple popup] is UNLOCK , 0000 <=== ]]]
04-30 15:56:48.699+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb75b7610, elm_genlist, _activated_obj : 0x0, activated : 1
04-30 15:56:48.699+0700 I/wnotibp ( 2131): wnotiboard-popup-view.c: _view_simple_vi_end_idler_cb(692) > Set the drawer 0xb7635f08 again.
04-30 15:56:48.699+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_simple_vi_end_idler_cb(696) > ::UI:: SIMPLE_POPUP_TAP (-1219660360)
04-30 15:56:48.699+0700 E/wnoti-service( 1378): wnoti-server-mgr-stub.c: __wnoti_change_new_flag_stub(3143) > change_type : 1, value : 1392
04-30 15:56:48.709+0700 E/wnoti-service( 1378): wnoti-utility.c: wnoti_get_new_flag_count(896) > >> count : 0
04-30 15:56:48.709+0700 E/wnoti-service( 1378): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 9, source : 0, application_id : 0, display_count : 0, 
04-30 15:56:48.709+0700 E/wnoti-proxy( 1234): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1234, caller_id : 0, listener_type : 9
04-30 15:56:48.709+0700 I/wnotibp ( 2131): wnotiboard-popup-common.c: wnbp_common_change_win_level(629) > current : 12, 1, 2, 1
04-30 15:56:48.719+0700 I/efl-extension( 2131): efl_extension_more_option.c: _cue_show_cb(295) > called!!
04-30 15:56:48.729+0700 E/wnoti-proxy( 2131): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 2131, caller_id : 0, listener_type : 9
04-30 15:56:48.859+0700 W/wnotib  ( 1234): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 9, op_type: 0, category_id: 0, display count: 0
04-30 15:56:49.229+0700 E/EFL     ( 2131): ecore_x<2131> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=131889 button=1
04-30 15:56:49.229+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7517900 : elm_scroller] mouse move
04-30 15:56:49.229+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb75b7610 : elm_genlist] mouse move
04-30 15:56:49.309+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7517900 : elm_scroller] mouse move
04-30 15:56:49.309+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7517900 : elm_scroller] hold(0), freeze(0)
04-30 15:56:49.309+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb75b7610 : elm_genlist] mouse move
04-30 15:56:49.309+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb75b7610 : elm_genlist] hold(0), freeze(0)
04-30 15:56:49.319+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7517900 : elm_scroller] mouse move
04-30 15:56:49.319+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7517900 : elm_scroller] hold(0), freeze(0)
04-30 15:56:49.319+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb75b7610 : elm_genlist] mouse move
04-30 15:56:49.319+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb75b7610 : elm_genlist] hold(0), freeze(0)
04-30 15:56:49.329+0700 E/EFL     ( 2131): ecore_x<2131> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=131998 button=1
04-30 15:56:49.339+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_page_inline_action_btn_click_cb(2721) > ACTION BUTTON IS CLICKED~~~~~~~~~~
04-30 15:56:49.339+0700 W/wnotibp ( 2131): w-notification-board-common.c: wnb_common_play_sound_effect(1307) > Feedback not initialized yet.
04-30 15:56:49.339+0700 I/wnotibp ( 2131): w-notification-board-common.c: wnb_common_init_effect(1272) > Feeedback initialized.
04-30 15:56:49.339+0700 I/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_open_app_cb(3281) > Action clicked for 156, -1016
04-30 15:56:49.339+0700 W/AUL     ( 2131): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.toyota.realtimefeedback)
04-30 15:56:49.339+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 0
04-30 15:56:49.339+0700 W/AUL_AMD (  923): amd_launch.c: _start_app(1782) > caller pid : 2131
04-30 15:56:49.339+0700 I/AUL_AMD (  923): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
04-30 15:56:49.349+0700 W/AUL     (  923): app_signal.c: aul_send_app_resume_request_signal(567) > aul_send_app_resume_request_signal app(com.toyota.realtimefeedback) pid(2111) type(uiapp) bg(0)
04-30 15:56:49.349+0700 W/AUL_AMD (  923): amd_launch.c: __nofork_processing(1229) > __nofork_processing, cmd: 0, pid: 2111
04-30 15:56:49.359+0700 W/STARTER ( 1143): pkg-monitor.c: _app_mgr_status_cb(421) > [_app_mgr_status_cb:421] Resume request [2111]
04-30 15:56:49.369+0700 W/AUL_AMD (  923): amd_launch.c: __reply_handler(999) > listen fd(22) , send fd(15), pid(2111), cmd(0)
04-30 15:56:49.369+0700 W/AUL     ( 2131): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2111)
04-30 15:56:49.369+0700 I/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_trigger_action_timer(5096) > Remove previous action timer for 1392, -1016.
04-30 15:56:49.369+0700 W/wnotibp ( 2131): w-notification-board-common.c: wnb_common_perform_auto_deletion(3824) > auto_remove is set
04-30 15:56:49.369+0700 W/wnotibp ( 2131): w-notification-board-common.c: _wnb_auto_remove_timer_cb(3801) > 
04-30 15:56:49.369+0700 I/APP_CORE( 2111): appcore-efl.c: __do_app(453) > [APP 2111] Event: RESET State: PAUSED
04-30 15:56:49.369+0700 I/CAPI_APPFW_APPLICATION( 2111): app_main.c: _ui_app_appcore_reset(645) > app_appcore_reset
04-30 15:56:49.369+0700 I/APP_CORE( 2111): appcore-efl.c: __do_app(529) > Legacy lifecycle: 0
04-30 15:56:49.369+0700 I/APP_CORE( 2111): appcore-efl.c: __do_app(531) > [APP 2111] App already running, raise the window
04-30 15:56:49.369+0700 E/wnoti-service( 1378): wnoti-db-server.c: __delete_mini_app_image(29) > Gear application
04-30 15:56:49.369+0700 E/wnoti-service( 1378): wnoti-native-client.c: notify_to_native_noti(1849) > tizen_noti_id : 1224
04-30 15:56:49.379+0700 I/APP_CORE( 2111): appcore-efl.c: __do_app(535) > [APP 2111] Call the resume_cb
04-30 15:56:49.379+0700 I/CAPI_APPFW_APPLICATION( 2111): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-30 15:56:49.379+0700 W/AUL     (  923): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.toyota.realtimefeedback) pid(2111) status(fg) type(uiapp)
04-30 15:56:49.379+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb4a23f98 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:56:49.379+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb4a23f98 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:56:49.379+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb4a23f98 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:56:49.379+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb4a23f98 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:56:49.379+0700 W/STARTER ( 1143): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2111] goes to (3)
04-30 15:56:49.409+0700 E/wnoti-service( 1378): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 1, count : 0
04-30 15:56:49.429+0700 I/AUL     ( 1378): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-30 15:56:49.429+0700 I/AUL     ( 1378): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-30 15:56:49.439+0700 W/SHealthServiceCommon( 1770): NotificationServiceController.cpp: DeleteNotificationByPrivateId(928) > [1;40;33mdelete notification by private id = [1224][0;m
04-30 15:56:49.449+0700 E/APPS    ( 1234): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-30 15:56:49.449+0700 E/wnoti-service( 1378): wnoti-msg-builder.c: wnoti_service_publish_activity(1855) > param1 : 3, param2 : -1016, param3 : 0, param4 : 0, card_id : (null)
04-30 15:56:49.449+0700 E/wnoti-service( 1378): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 0, count : 0
04-30 15:56:49.449+0700 E/wnoti-service( 1378): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : 0, display_count : 0, 
04-30 15:56:49.449+0700 E/wnoti-service( 1378): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1224, op_type : 3  //insert = 1, update = 2, delete = 3
04-30 15:56:49.449+0700 E/wnoti-proxy( 1234): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1234, caller_id : 0, listener_type : 0
04-30 15:56:49.459+0700 I/wnotib  ( 2131): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 21, ret: -3, request_id: 0
04-30 15:56:49.459+0700 I/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_open_app_cb(3326) > Finish action successfully.
04-30 15:56:49.459+0700 I/wnotib  ( 2131): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 24, ret: -3, request_id: 0
04-30 15:56:49.469+0700 E/wnoti-service( 1378): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-30 15:56:49.469+0700 E/wnoti-service( 1378): wnoti-native-client.c: _get_native_application_info(55) > _query_step failed()
04-30 15:56:49.469+0700 E/wnoti-service( 1378): wnoti-native-client.c: _receive_notification_changed_cb(1722) > error_code from _get_native_application_info: -1
04-30 15:56:49.469+0700 E/wnoti-service( 1378): wnoti-native-client.c: __remove_notification(684) > application_id is 0.
04-30 15:56:49.469+0700 E/wnoti-proxy( 2131): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 2131, caller_id : 0, listener_type : 0
04-30 15:56:49.479+0700 E/wnoti-service( 1378): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-30 15:56:49.499+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb4a23f98 : elm_genlist] mx(0), my(917), minx(0), miny(0), px(0), py(0)
04-30 15:56:49.499+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb4a23f98 : elm_genlist] cw(360), ch(1277), pw(360), ph(360)
04-30 15:56:49.589+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(263) > fully_obscured: 1, 0
04-30 15:56:49.589+0700 I/wnotibp ( 2131): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x360000d 0x360000d 0x3600009]
04-30 15:56:49.589+0700 I/APP_CORE( 2131): appcore-efl.c: __do_app(453) > [APP 2131] Event: PAUSE State: RUNNING
04-30 15:56:49.589+0700 I/CAPI_APPFW_APPLICATION( 2131): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-30 15:56:49.599+0700 W/AUL     (  923): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.wnotiboard-popup) pid(2131) status(bg) type(uiapp)
04-30 15:56:49.599+0700 W/STARTER ( 1143): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2131] goes to (4)
04-30 15:56:49.599+0700 W/wnotib  ( 1234): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: 0, display count: 0
04-30 15:56:49.599+0700 I/wnotib  ( 1234): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-30 15:56:49.599+0700 W/wnotib  ( 1234): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-30 15:56:49.619+0700 W/wnotibp ( 2131): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup.c: _popup_app_pause(220) > [1, 0, 1, 0000]
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup.c: _popup_app_pause(221) > [1, 1]
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup.c: _popup_app_pause(222) > [1, 1, 4, 2, b74d75b8]
04-30 15:56:49.629+0700 I/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_service_changed_cb(243) > Handle this change type in idler.
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup-data.c: wnbp_data_apply_db_id_changes(560) > This notification's category is expired!!!
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_notification_change_cb(600) > [2,1392]
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_notification_change_cb(639) > ::APP:: This notification is removed by wnotification service :(1392),(1)
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup-control.c: _ctrl_notification_change_cb(641) > ::APP:: delete_reserved_category_id=156, win_id=(null)
04-30 15:56:49.629+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_goto_pause(1476) > state : (5, 1, 4)
04-30 15:56:49.629+0700 I/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_goto_pause(1489) > simple popup=b74d75b8, view_state=4
04-30 15:56:49.629+0700 I/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_goto_pause(1525) > ::APP:: this notification is removed by service, wating ack from host. or more option excution timeout.
04-30 15:56:49.639+0700 I/APP_CORE( 2111): appcore-efl.c: __do_app(453) > [APP 2111] Event: RESUME State: RUNNING
04-30 15:56:49.639+0700 I/GATE    ( 2111): <GATE-M>APP_FULLY_LOADED_realtimefeedback</GATE-M>
04-30 15:56:49.659+0700 E/wnoti-proxy( 1838): wnoti.c: run_action_caller_cb(3933) > activity_type : 3
04-30 15:56:49.679+0700 E/wnoti-proxy( 1937): wnoti.c: run_action_caller_cb(3933) > activity_type : 3
04-30 15:56:49.979+0700 E/AUL     (  923): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-30 15:56:50.399+0700 E/CAPI_MEDIA_CONTROLLER( 1372): media_controller_main.c: __mc_main_check_connection(34) > [0m[No-error] Timer is Called but there is working Process, connection_cnt = 3
04-30 15:56:50.509+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2111): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
04-30 15:56:50.509+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2111): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
04-30 15:56:50.509+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2111): preference.c: preference_get_string(1258) > preference_get_string(2111) : URL error
04-30 15:56:50.529+0700 I/CAPI_NETWORK_CONNECTION( 2111): connection.c: connection_create(453) > New handle created[0xb880cb00]
04-30 15:56:50.569+0700 E/JSON PARSING( 2111): No data could be display
04-30 15:56:50.569+0700 E/JSON PARSING( 2111): �γ����;
04-30 15:56:50.569+0700 I/CAPI_NETWORK_CONNECTION( 2111): connection.c: connection_destroy(471) > Destroy handle: 0xb880cb00
04-30 15:56:50.579+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-manager.c: _wfd_exit_timeout_cb(78) > Client count [1]
04-30 15:56:50.869+0700 E/EFL     ( 2111): ecore_x<2111> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=133529 button=1
04-30 15:56:50.869+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a23f98 : elm_genlist] mouse move
04-30 15:56:50.939+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a23f98 : elm_genlist] mouse move
04-30 15:56:50.939+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a23f98 : elm_genlist] hold(0), freeze(0)
04-30 15:56:50.949+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a23f98 : elm_genlist] mouse move
04-30 15:56:50.949+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a23f98 : elm_genlist] hold(0), freeze(0)
04-30 15:56:50.959+0700 E/EFL     ( 2111): ecore_x<2111> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=133627 button=1
04-30 15:56:50.989+0700 E/GL SELECTED( 2111): ID GL : 67022
04-30 15:56:50.999+0700 I/efl-extension( 2111): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-30 15:56:50.999+0700 I/efl-extension( 2111): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-30 15:56:50.999+0700 I/efl-extension( 2111): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8ac1b10, elm_image, _activated_obj : 0xb4a8de28, activated : 1
04-30 15:56:50.999+0700 I/efl-extension( 2111): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-30 15:56:51.119+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb4a344e8 : elm_scroller] mx(198), my(5074), minx(0), miny(0), px(0), py(0)
04-30 15:56:51.119+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb4a344e8 : elm_scroller] cw(198), ch(5074), pw(0), ph(0)
04-30 15:56:51.119+0700 E/EFL     ( 2111): elementary<2111> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-30 15:56:51.119+0700 E/EFL     ( 2111): elementary<2111> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb8ac29c0) will be pushed
04-30 15:56:51.169+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb4a344e8 : elm_scroller] mx(0), my(4829), minx(0), miny(0), px(0), py(0)
04-30 15:56:51.169+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb4a344e8 : elm_scroller] cw(360), ch(5074), pw(360), ph(245)
04-30 15:56:51.169+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb4a344e8 : elm_scroller] mx(0), my(4829), minx(0), miny(0), px(0), py(0)
04-30 15:56:51.169+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb4a344e8 : elm_scroller] cw(360), ch(5074), pw(360), ph(245)
04-30 15:56:51.189+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb4a344e8 : elm_scroller] mx(0), my(4714), minx(0), miny(0), px(0), py(0)
04-30 15:56:51.189+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb4a344e8 : elm_scroller] cw(360), ch(5074), pw(360), ph(360)
04-30 15:56:51.189+0700 E/EFL     ( 2111): <2111> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-30 15:56:51.199+0700 E/EFL     ( 2111): elementary<2111> elc_naviframe.c:2796 _push_transition_cb() item(0xb8ac29c0) will transition
04-30 15:56:51.329+0700 I/APP_CORE( 1144): appcore-efl.c: __do_app(453) > [APP 1144] Event: MEM_FLUSH State: PAUSED
04-30 15:56:51.369+0700 W/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_action_timer_cb(5080) > Go back to the previous view.
04-30 15:56:51.369+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_action_execution_timeout_cb(3496) > is_action_completed: 0
04-30 15:56:51.369+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_action_execution_timeout_cb(3513) > ::APP:: 4, 2, [1392, 156, -1016, 156], 5, 1392
04-30 15:56:51.369+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_goto_pause(1476) > state : (9, 5, 4)
04-30 15:56:51.369+0700 I/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_goto_pause(1489) > simple popup=b74d75b8, view_state=4
04-30 15:56:51.369+0700 I/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_goto_pause(1534) > ::APP:: go to pause.
04-30 15:56:51.369+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-30 15:56:51.369+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_reset_view_lock(703) > ::UI:: lock state = 0000
04-30 15:56:51.369+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_goto_pause(1551) > device_power_release_lock CPU>>>>>>>
04-30 15:56:51.369+0700 I/efl-extension( 2131): efl_extension_more_option.c: eext_more_option_opened_get(655) > called!!
04-30 15:56:51.369+0700 I/wnotibp ( 2131): w-notification-board-action.c: wnb_action_hide_drawer(5210) > Panel open state: 0 for 156, -1016
04-30 15:56:51.369+0700 I/efl-extension( 2131): efl_extension_more_option.c: eext_more_option_opened_get(655) > called!!
04-30 15:56:51.369+0700 I/wnotibp ( 2131): w-notification-board-action.c: wnb_action_deinitialize(5671) > Deinit drawer.
04-30 15:56:51.369+0700 I/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_terminate_input_selector(5550) > No need to close w-input-selector.
04-30 15:56:51.369+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:56:51.379+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-30 15:56:51.379+0700 I/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_terminate_noti_composer(5585) > ret : 0, is_running : 0
04-30 15:56:51.379+0700 I/wnotibp ( 2131): w-notification-board-action.c: _wnb_action_sending_popup_del_cb(705) > 0xb76a2f08, g_sending_popup_state: 0
04-30 15:56:51.379+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb76c5ce8 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:56:51.379+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb76c5ce8 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
04-30 15:56:51.389+0700 I/efl-extension( 2131): efl_extension_more_option.c: eext_more_option_items_clear(572) > called!!
04-30 15:56:51.389+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: eext_rotary_selector_items_clear(2473) > called!!
04-30 15:56:51.389+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-30 15:56:51.389+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_more_option.c: _more_option_del_cb(268) > called!!
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_more_option.c: _panel_del_cb(173) > called!!
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: _rotary_selector_del_cb(826) > called!!
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb768f588, elm_layout, func : 0xb58728c9
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb768f588, obj: 0xb768f588
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 15:56:51.409+0700 I/efl-extension( 2131): efl_extension_rotary_selector.c: _event_area_del_cb(494) > called!!
04-30 15:56:51.409+0700 I/wnotibp ( 2131): w-notification-board-action.c: wnb_action_deinitialize(5721) > g_wnb_action_data is freed.
04-30 15:56:51.409+0700 I/wnotibp ( 2131): wnotiboard-popup-view.c: wnbp_view_goto_pause(1586) > ::INFO:: call lower
04-30 15:56:51.409+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_simple_popup_del_cb(374) > ::UI:: (2, b74d75b8, b74d75b8, 0)
04-30 15:56:51.409+0700 I/wnotibp ( 2131): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1392
04-30 15:56:51.409+0700 W/wnotibp ( 2131): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(686) > Call wnbp_free_noti_detail..
04-30 15:56:51.409+0700 W/wnotibp ( 2131): wnotiboard-popup-common.c: wnbp_free_noti_detail(583) > Do free noti [0xb74ed918] / db id [1392]
04-30 15:56:51.409+0700 I/wnotibp ( 2131): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 0
04-30 15:56:51.409+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-30 15:56:51.409+0700 W/wnotibp ( 2131): wnotiboard-popup-manager.c: wnbp_mgr_reset_view_lock(703) > ::UI:: lock state = 0000
04-30 15:56:51.409+0700 W/wnotibp ( 2131): wnotiboard-popup-view.c: _view_simple_popup_del_cb(444) > ::UI:: drawer is NULL
04-30 15:56:51.429+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7517900 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:56:51.429+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7517900 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
04-30 15:56:51.429+0700 I/efl-extension( 2131): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-30 15:56:51.429+0700 I/efl-extension( 2131): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb75b7610
04-30 15:56:51.429+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb75b7610, elm_genlist, _activated_obj : 0x0, activated : 0
04-30 15:56:51.429+0700 I/efl-extension( 2131): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb75b7610, obj: 0xb75b7610
04-30 15:56:51.429+0700 I/efl-extension( 2131): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-30 15:56:51.429+0700 I/efl-extension( 2131): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-30 15:56:51.429+0700 I/efl-extension( 2131): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-30 15:56:51.429+0700 I/efl-extension( 2131): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 15:56:51.439+0700 E/EFL     ( 2131): elementary<2131> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 15:56:51.439+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb75b7610 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:56:51.439+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb75b7610 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:56:51.439+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb75b7610 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:56:51.439+0700 E/EFL     ( 2131): elementary<2131> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb75b7610 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:56:51.449+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:56:51.449+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb75b7610, elm_genlist, func : 0xb5868ea1
04-30 15:56:51.449+0700 I/efl-extension( 2131): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:56:51.449+0700 W/APP_CORE( 2131): appcore-efl.c: __hide_cb(882) > [EVENT_TEST][EVENT] GET HIDE EVENT!!!. WIN:360000d
04-30 15:56:51.619+0700 E/EFL     ( 2111): elementary<2111> elc_naviframe.c:1193 _on_item_push_finished() item(0xb4a83bb8) transition finished
04-30 15:56:51.619+0700 E/EFL     ( 2111): elementary<2111> elc_naviframe.c:1218 _on_item_show_finished() item(0xb8ac29c0) transition finished
04-30 15:56:51.959+0700 I/DOWNLOAD_PROVIDER( 1327): download-provider-client-manager.c: dp_client_manager(738) > client-manager's working is done
04-30 15:56:51.959+0700 E/EFL     (  891): ecore_x<891> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3200002 time=134041
04-30 15:56:51.969+0700 E/EFL     ( 2111): ecore_x<2111> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=134041
04-30 15:56:51.969+0700 E/EFL     (  891): ecore_x<891> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=134041
04-30 15:56:51.969+0700 W/WIFI_DIRECT( 1327): wifi-direct-client-proxy.c: __wfd_client_write_socket(589) > Write [32] bytes to socket [8].
04-30 15:56:51.969+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-client.c: _wfd_client_check_socket(310) > POLLIN from socket [13]
04-30 15:56:51.969+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-client.c: wfd_client_process_request(930) > Client request [3:WIFI_DIRECT_CMD_DEREGISTER], 32 bytes read from socket[13]
04-30 15:56:51.969+0700 W/WIFI_DIRECT( 1327): wifi-direct-client-proxy.c: __wfd_client_send_request(667) > Succeeded to send request [3: WIFI_DIRECT_CMD_DEREGISTER]
04-30 15:56:51.969+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-client.c: _wfd_check_client_privilege(886) > Security Server: API Access Validation Success
04-30 15:56:51.969+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-client.c: _wfd_client_check_socket(313) > POLLOUT from socket [13]
04-30 15:56:51.969+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-client.c: _wfd_send_to_client(355) > Succeeded to write data[116 bytes] into socket [13]
04-30 15:56:51.969+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-client.c: _wfd_client_find_by_id(272) > Client found. [13]
04-30 15:56:51.969+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-client.c: _wfd_deregister_client(586) > Client [13] is removed. 0 client left
04-30 15:56:51.969+0700 W/WIFI_DIRECT_MANAGER( 1416): wifi-direct-client.c: _wfd_remove_event_source(561) > Succeeded to remove GSource
04-30 15:56:51.969+0700 E/WIFI_DIRECT( 1327): wifi-direct-client-proxy.c: __wfd_client_check_socket(563) > Error! POLLHUP from socket[8]
04-30 15:56:51.969+0700 E/WIFI_DIRECT( 1327): wifi-direct-client-proxy.c: __wfd_client_read_socket(623) > Socket error
04-30 15:56:51.969+0700 E/WIFI_DIRECT( 1327): wifi-direct-client-proxy.c: __wfd_client_send_request(672) > Failed to read socket [-29753342]
04-30 15:56:51.969+0700 W/WIFI_DIRECT( 1327): wifi-direct-client-proxy.c: wifi_direct_deinitialize(1144) > Failed to deinitialize. But continue deinitialization
04-30 15:56:51.969+0700 I/CAPI_NETWORK_CONNECTION( 1327): connection.c: __connection_set_type_changed_callback(195) > Successfully de-registered(0)
04-30 15:56:52.089+0700 E/EFL     ( 2111): ecore_x<2111> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=134758 button=1
04-30 15:56:52.089+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.099+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.099+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.119+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.119+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.129+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.129+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.139+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.139+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.149+0700 I/CAPI_NETWORK_CONNECTION( 1327): connection.c: __connection_set_ip_changed_callback(295) > Successfully de-registered(0)
04-30 15:56:52.149+0700 I/CAPI_NETWORK_CONNECTION( 1327): connection.c: connection_destroy(471) > Destroy handle: 0xb80b4fb0
04-30 15:56:52.149+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.149+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.159+0700 I/DOWNLOAD_PROVIDER( 1327): download-provider-main.c: main(64) > download-provider's working is done
04-30 15:56:52.159+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.159+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.169+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.169+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.179+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.179+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.189+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.189+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.189+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4292 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] add hold animator
04-30 15:56:52.199+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] direction_x(0), direction_y(1)
04-30 15:56:52.199+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] drag_child_locked_y(0)
04-30 15:56:52.199+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] move content x(0), y(29)
04-30 15:56:52.209+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.209+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.209+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.209+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.219+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] direction_x(0), direction_y(1)
04-30 15:56:52.219+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] drag_child_locked_y(0)
04-30 15:56:52.219+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] move content x(0), y(73)
04-30 15:56:52.229+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.229+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.239+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] direction_x(0), direction_y(1)
04-30 15:56:52.239+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] drag_child_locked_y(0)
04-30 15:56:52.239+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] move content x(0), y(127)
04-30 15:56:52.249+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.249+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.249+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.249+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.249+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] direction_x(0), direction_y(1)
04-30 15:56:52.249+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] drag_child_locked_y(0)
04-30 15:56:52.249+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] move content x(0), y(167)
04-30 15:56:52.259+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.259+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.269+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] mouse move
04-30 15:56:52.269+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb4a344e8 : elm_scroller] hold(0), freeze(0)
04-30 15:56:52.269+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] direction_x(0), direction_y(1)
04-30 15:56:52.269+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] drag_child_locked_y(0)
04-30 15:56:52.269+0700 E/EFL     ( 2111): elementary<2111> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb4a344e8 : elm_scroller] move content x(0), y(189)
04-30 15:56:52.279+0700 E/EFL     ( 2111): ecore_x<2111> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=134945 button=1
04-30 15:56:52.489+0700 I/APP_CORE( 1234): appcore-efl.c: __do_app(453) > [APP 1234] Event: MEM_FLUSH State: PAUSED
04-30 15:56:53.109+0700 W/KEYROUTER(  891): e_mod_main.c: DeliverDeviceKeyEvents(3164) > Deliver KeyPress. value=2669, window=0x3200002
04-30 15:56:53.109+0700 W/KEYROUTER(  891): e_mod_main.c: DeliverDeviceKeyEvents(3175) > Deliver KeyRelease. value=2669, window=0x3200002
04-30 15:56:53.119+0700 E/EFL     ( 2111): ecore_x<2111> ecore_x_events.c:537 _ecore_x_event_handle_key_press() KeyEvent:press time=135600
04-30 15:56:53.119+0700 E/EFL     ( 2111): ecore_x<2111> ecore_x_events.c:551 _ecore_x_event_handle_key_release() KeyEvent:release time=135730
04-30 15:56:53.119+0700 E/efl-extension( 2111): efl_extension_events.c: _eext_key_grab_rect_key_up_cb(240) > key up called
04-30 15:56:53.679+0700 W/CRASH_MANAGER( 2162): worker.c: worker_job(1205) > 1102111726561152507861
