S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2350
Date: 2018-04-27 09:32:54+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2350, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb6fba445, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb262eb88, r7   = 0xbeee2150
r8   = 0x000186a0, r9   = 0xb89163f0
r10  = 0xb6eadb10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbeee20c8
lr   = 0xb6fba445, pc   = 0xb6aa46f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      2728 KB
Buffers:      6608 KB
Cached:     112368 KB
VmPeak:     123504 KB
VmSize:     115380 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       29904 KB
VmRSS:       29904 KB
VmData:      43008 KB
VmStk:         136 KB
VmExe:          40 KB
VmLib:       35904 KB
VmPTE:          88 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2350 TID = 2350
2350 2360 2363 2370 

Maps Information
b0400000 b0404000 r-xp /usr/lib/libogg.so.0.7.1
b040c000 b042e000 r-xp /usr/lib/libvorbis.so.0.4.3
b0436000 b047d000 r-xp /usr/lib/libsndfile.so.1.0.26
b0489000 b04d2000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b04db000 b04e0000 r-xp /usr/lib/libjson.so.0.0.1
b1d81000 b1e87000 r-xp /usr/lib/libicuuc.so.57.1
b1e9d000 b2025000 r-xp /usr/lib/libicui18n.so.57.1
b2035000 b2042000 r-xp /usr/lib/libail.so.0.1.0
b204b000 b204e000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b2056000 b208e000 r-xp /usr/lib/libpulse.so.0.16.2
b208f000 b2092000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b209a000 b20fb000 r-xp /usr/lib/libasound.so.2.0.0
b2105000 b211e000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b2127000 b212b000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b2133000 b213e000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b214b000 b214f000 r-xp /usr/lib/libmmfsession.so.0.0.0
b2158000 b2170000 r-xp /usr/lib/libmmfsound.so.0.1.0
b2181000 b2188000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b2190000 b219b000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b21a3000 b21a5000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b21ad000 b21b5000 r-xp /usr/lib/libfeedback.so.0.1.4
b2303000 b2304000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b230c000 b230d000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b234d000 b23d4000 rw-s anon_inode:dmabuf
b23fe000 b2485000 rw-s anon_inode:dmabuf
b2707000 b2708000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b2710000 b2713000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b272e000 b27b5000 rw-s anon_inode:dmabuf
b29a0000 b2a27000 rw-s anon_inode:dmabuf
b2a28000 b3227000 rw-p [stack:2370]
b33ce000 b33cf000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b34d7000 b3cd6000 rw-p [stack:2363]
b3cd6000 b3cd8000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3ce0000 b3cf7000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3f08000 b4707000 rw-p [stack:2360]
b471e000 b504a000 r-xp /usr/lib/libsc-a3xx.so
b52ae000 b52b0000 r-xp /usr/lib/libdri2.so.0.0.0
b52b8000 b52c0000 r-xp /usr/lib/libdrm.so.2.4.0
b52c8000 b52cc000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b52d4000 b52d7000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b52df000 b52e0000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b52e8000 b52f3000 r-xp /usr/lib/libtbm.so.1.0.0
b52fb000 b52fe000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b5306000 b5308000 r-xp /usr/lib/libgenlock.so
b5310000 b5315000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b531d000 b5458000 r-xp /usr/lib/egl/libGLESv2.so
b5494000 b5496000 r-xp /usr/lib/libadreno_utils.so
b54a0000 b54c7000 r-xp /usr/lib/libgsl.so
b54d6000 b54dd000 r-xp /usr/lib/egl/eglsubX11.so
b54e7000 b5509000 r-xp /usr/lib/egl/libEGL.so
b5512000 b5587000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5797000 b57a1000 r-xp /lib/libnss_files-2.13.so
b57aa000 b57ad000 r-xp /lib/libattr.so.1.1.0
b57b5000 b57bc000 r-xp /lib/libcrypt-2.13.so
b57ec000 b57ef000 r-xp /lib/libcap.so.2.21
b57f7000 b57f9000 r-xp /usr/lib/libiri.so
b5801000 b581e000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b5827000 b582b000 r-xp /usr/lib/libsmack.so.1.0.0
b5834000 b5863000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b586b000 b58ff000 r-xp /usr/lib/libstdc++.so.6.0.16
b5912000 b59e1000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b59f7000 b5a1b000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5a24000 b5aee000 r-xp /usr/lib/libCOREGL.so.4.0
b5b05000 b5b07000 r-xp /usr/lib/libXau.so.6.0.0
b5b10000 b5b20000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5b28000 b5b2b000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5b33000 b5b4b000 r-xp /usr/lib/liblzma.so.5.0.3
b5b54000 b5b56000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5b5e000 b5b61000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5b69000 b5b6d000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5b76000 b5b7b000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5b85000 b5ba8000 r-xp /usr/lib/libjpeg.so.8.0.2
b5bc0000 b5bd6000 r-xp /lib/libexpat.so.1.6.0
b5be0000 b5bf3000 r-xp /usr/lib/libxcb.so.1.1.0
b5bfc000 b5c02000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5c0a000 b5c0b000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5c15000 b5c2d000 r-xp /usr/lib/libpng12.so.0.50.0
b5c35000 b5c38000 r-xp /usr/lib/libEGL.so.1.4
b5c40000 b5c4e000 r-xp /usr/lib/libGLESv2.so.2.0
b5c57000 b5c58000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5c60000 b5c77000 r-xp /usr/lib/liblua-5.1.so
b5c81000 b5c88000 r-xp /usr/lib/libembryo.so.1.7.99
b5c90000 b5c9a000 r-xp /usr/lib/libXext.so.6.4.0
b5ca3000 b5ca7000 r-xp /usr/lib/libXtst.so.6.1.0
b5caf000 b5cb5000 r-xp /usr/lib/libXrender.so.1.3.0
b5cbd000 b5cc3000 r-xp /usr/lib/libXrandr.so.2.2.0
b5ccb000 b5ccc000 r-xp /usr/lib/libXinerama.so.1.0.0
b5cd6000 b5cd9000 r-xp /usr/lib/libXfixes.so.3.1.0
b5ce1000 b5ce3000 r-xp /usr/lib/libXgesture.so.7.0.0
b5ceb000 b5ced000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5cf5000 b5cf7000 r-xp /usr/lib/libXdamage.so.1.1.0
b5cff000 b5d06000 r-xp /usr/lib/libXcursor.so.1.0.2
b5d0f000 b5d1f000 r-xp /lib/libresolv-2.13.so
b5d23000 b5d25000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5d2d000 b5d32000 r-xp /usr/lib/libffi.so.5.0.10
b5d3a000 b5d3b000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5d43000 b5d8c000 r-xp /usr/lib/libmdm.so.1.2.70
b5d96000 b5d9c000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5da4000 b5daa000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5db2000 b5dcc000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5dd4000 b5df2000 r-xp /usr/lib/libsystemd.so.0.4.0
b5dfd000 b5dfe000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5e06000 b5e0b000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5e13000 b5e2a000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5e32000 b5e38000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5e41000 b5e4a000 r-xp /usr/lib/libcom-core.so.0.0.1
b5e54000 b5e56000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e5f000 b5eb5000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5ec2000 b5f18000 r-xp /usr/lib/libfreetype.so.6.11.3
b5f24000 b5f69000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5f72000 b5f85000 r-xp /usr/lib/libfribidi.so.0.3.1
b5f8e000 b5fa8000 r-xp /usr/lib/libecore_con.so.1.7.99
b5fb1000 b5fdb000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5fe4000 b5fed000 r-xp /usr/lib/libedbus.so.1.7.99
b5ff5000 b6006000 r-xp /usr/lib/libecore_input.so.1.7.99
b600e000 b6013000 r-xp /usr/lib/libecore_file.so.1.7.99
b601c000 b603e000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6047000 b6060000 r-xp /usr/lib/libeet.so.1.7.99
b6071000 b6099000 r-xp /usr/lib/libfontconfig.so.1.8.0
b609a000 b60a3000 r-xp /usr/lib/libXi.so.6.1.0
b60ab000 b618c000 r-xp /usr/lib/libX11.so.6.3.0
b6198000 b6250000 r-xp /usr/lib/libcairo.so.2.11200.14
b625b000 b62b9000 r-xp /usr/lib/libedje.so.1.7.99
b62c3000 b6313000 r-xp /usr/lib/libecore_x.so.1.7.99
b6315000 b637e000 r-xp /lib/libm-2.13.so
b6387000 b638d000 r-xp /lib/librt-2.13.so
b6396000 b63ac000 r-xp /lib/libz.so.1.2.5
b63b5000 b6547000 r-xp /usr/lib/libcrypto.so.1.0.0
b6568000 b65af000 r-xp /usr/lib/libssl.so.1.0.0
b65bb000 b65e9000 r-xp /usr/lib/libidn.so.11.5.44
b65f1000 b65fa000 r-xp /usr/lib/libcares.so.2.1.0
b6603000 b66cf000 r-xp /usr/lib/libxml2.so.2.7.8
b66dd000 b66df000 r-xp /usr/lib/libiniparser.so.0
b66e8000 b671c000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6725000 b67f8000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6803000 b681c000 r-xp /usr/lib/libnetwork.so.0.0.0
b6824000 b682d000 r-xp /usr/lib/libvconf.so.0.2.45
b6836000 b6906000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6907000 b6948000 r-xp /usr/lib/libeina.so.1.7.99
b6951000 b6956000 r-xp /usr/lib/libappcore-common.so.1.1
b695e000 b6964000 r-xp /usr/lib/libappcore-efl.so.1.1
b696c000 b696f000 r-xp /usr/lib/libbundle.so.0.1.22
b6977000 b697d000 r-xp /usr/lib/libappsvc.so.0.1.0
b6985000 b6999000 r-xp /lib/libpthread-2.13.so
b69a4000 b69c7000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b69cf000 b69dc000 r-xp /usr/lib/libaul.so.0.1.0
b69e6000 b69e8000 r-xp /lib/libdl-2.13.so
b69f1000 b69fc000 r-xp /lib/libunwind.so.8.0.1
b6a29000 b6a31000 r-xp /lib/libgcc_s-4.6.so.1
b6a32000 b6b56000 r-xp /lib/libc-2.13.so
b6b64000 b6bd9000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6be3000 b6bef000 r-xp /usr/lib/libnotification.so.0.1.0
b6bf8000 b6c07000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6c10000 b6cde000 r-xp /usr/lib/libevas.so.1.7.99
b6d04000 b6e40000 r-xp /usr/lib/libelementary.so.1.7.99
b6e57000 b6e78000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6e80000 b6e97000 r-xp /usr/lib/libecore.so.1.7.99
b6eae000 b6eb0000 r-xp /usr/lib/libdlog.so.0.0.0
b6eb8000 b6efc000 r-xp /usr/lib/libcurl.so.4.3.0
b6f05000 b6f0a000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6f12000 b6f17000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6f1f000 b6f2f000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6f37000 b6f3f000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6f47000 b6f4b000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6f53000 b6f57000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6f60000 b6f62000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6f6d000 b6f78000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6f82000 b6f86000 r-xp /usr/lib/libsys-assert.so
b6f8f000 b6fac000 r-xp /lib/ld-2.13.so
b6fb5000 b6fbf000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b88db000 b8d1f000 rw-p [heap]
beec2000 beee3000 rw-p [stack]
b88db000 b8d1f000 rw-p [heap]
beec2000 beee3000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2350)
Call Stack Count: 1
 0: realloc + 0x4c (0xb6aa46f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
terface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] drag_child_locked_x(0)
04-27 09:32:49.679+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] move content x(-7), y(0)
04-27 09:32:49.749+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:49.749+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:49.759+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] direction_x(1), direction_y(0)
04-27 09:32:49.759+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] drag_child_locked_x(0)
04-27 09:32:49.759+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] move content x(-7), y(0)
04-27 09:32:49.759+0700 E/NOTI    ( 2350): Total Defect 8
04-27 09:32:49.759+0700 E/NOTIF ID( 2350): 40209
04-27 09:32:49.759+0700 E/problem ( 2350): Scratch (Proses)
04-27 09:32:49.759+0700 E/problem_desc( 2350): Glass Door Rr Lh Glass Door Rr Lh Part Damage
04-27 09:32:49.769+0700 E/BODY NO ( 2350): 67022
04-27 09:32:49.769+0700 E/NOTIF ID( 2350): 40198
04-27 09:32:49.769+0700 E/problem ( 2350): Dirty
04-27 09:32:49.769+0700 E/problem_desc( 2350): 19A-Fender Lh Fender Dirty & Stain
04-27 09:32:49.779+0700 E/BODY NO ( 2350): 67026
04-27 09:32:49.779+0700 E/NOTIF ID( 2350): 40200
04-27 09:32:49.779+0700 E/problem ( 2350): Clearance
04-27 09:32:49.779+0700 E/problem_desc( 2350): Rear Bumper X Mufler Part Fitting
04-27 09:32:49.789+0700 E/BODY NO ( 2350): 67031
04-27 09:32:49.789+0700 E/NOTIF ID( 2350): 40211
04-27 09:32:49.789+0700 E/problem ( 2350): Interferance
04-27 09:32:49.789+0700 E/problem_desc( 2350): Fr Door Rh Close Door 3X Part Function
04-27 09:32:49.799+0700 E/BODY NO ( 2350): 67032
04-27 09:32:49.799+0700 E/NOTIF ID( 2350): 40204
04-27 09:32:49.799+0700 E/problem ( 2350): Whitening
04-27 09:32:49.799+0700 E/problem_desc( 2350): Cowl Side Rh Cowl Side Rh Part Damage
04-27 09:32:49.809+0700 E/BODY NO ( 2350): 67033
04-27 09:32:49.809+0700 E/NOTIF ID( 2350): 40203
04-27 09:32:49.809+0700 E/problem ( 2350): Dirty (Part)
04-27 09:32:49.809+0700 E/problem_desc( 2350): Meter Cluster Meter Cluster Dirty Part 
04-27 09:32:49.809+0700 E/BODY NO ( 2350): 67035
04-27 09:32:49.809+0700 E/NOTIF ID( 2350): 40730
04-27 09:32:49.809+0700 E/problem ( 2350): Cathing
04-27 09:32:49.809+0700 E/problem_desc( 2350): A/F sensor Specification
04-27 09:32:49.819+0700 E/BODY NO ( 2350): 84432
04-27 09:32:49.819+0700 E/NOTIF ID( 2350): 40732
04-27 09:32:49.819+0700 E/problem ( 2350): Cover Screw Unlock
04-27 09:32:49.819+0700 E/problem_desc( 2350): Cover Tweeter Fr Lh Broken Part Damage
04-27 09:32:49.829+0700 E/BODY NO ( 2350): 84466
04-27 09:32:49.829+0700 E/JSON PARSING( 2350): ������e
04-27 09:32:49.889+0700 I/AUL     ( 1143): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 09:32:49.899+0700 I/AUL     ( 1143): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 09:32:49.929+0700 I/AUL     ( 1429): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 09:32:49.949+0700 I/AUL     ( 1429): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 09:32:49.949+0700 E/wnoti-service( 1429): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1078, op_type : 1  //insert = 1, update = 2, delete = 3
04-27 09:32:49.969+0700 E/wnoti-service( 1429): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-27 09:32:49.969+0700 E/wnoti-service( 1429): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-27 09:32:49.969+0700 E/wnoti-service( 1429): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-27 09:32:49.969+0700 E/wnoti-service( 1429): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-27 09:32:49.969+0700 E/wnoti-service( 1429): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: (null), err: 0
04-27 09:32:49.979+0700 E/wnoti-service( 1429): wnoti-native-client.c: _insert_notification(1208) > fail to get metadata_value, ret : -2
04-27 09:32:49.979+0700 E/wnoti-service( 1429): wnoti-db-server.c: wnoti_update_panel(2712) > id : -1016, source : 1
04-27 09:32:49.979+0700 E/wnoti-service( 1429): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-27 09:32:49.979+0700 E/wnoti-service( 1429): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1078, application_id: -1016
04-27 09:32:49.989+0700 I/CAPI_NETWORK_CONNECTION( 2350): connection.c: connection_create(453) > New handle created[0xb8c63ee8]
04-27 09:32:49.989+0700 E/wnoti-service( 1429): wnoti-db-server.c: _wnoti_update_category(908) > re_table_id : 0
04-27 09:32:50.009+0700 E/wnoti-service( 1429): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 0, count : 1
04-27 09:32:50.019+0700 I/AUL     ( 1429): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 09:32:50.019+0700 I/CAPI_NETWORK_CONNECTION( 2350): connection.c: connection_destroy(471) > Destroy handle: 0xb8c63ee8
04-27 09:32:50.019+0700 I/CAPI_NETWORK_CONNECTION( 2350): connection.c: connection_destroy(471) > Destroy handle: 0xb8c8e938
04-27 09:32:50.029+0700 I/AUL     ( 1429): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 09:32:50.039+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:50.039+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:50.049+0700 E/APPS    ( 1214): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-27 09:32:50.059+0700 I/wnoti-service( 1429): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-27 09:32:50.059+0700 E/wnoti-service( 1429): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-27 09:32:50.059+0700 E/wnoti-service( 1429): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-27 09:32:50.059+0700 E/wnoti-service( 1429): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-27 09:32:50.059+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 09:32:50.059+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] direction_x(1), direction_y(0)
04-27 09:32:50.059+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] drag_child_locked_x(0)
04-27 09:32:50.059+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] move content x(-8), y(0)
04-27 09:32:50.069+0700 E/wnoti-proxy( 2239): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 2239, caller_id : 0, listener_type : 0
04-27 09:32:50.069+0700 E/wnoti-proxy( 1214): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1214, caller_id : 0, listener_type : 0
04-27 09:32:50.069+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2239
04-27 09:32:50.069+0700 E/wnoti-service( 1429): wnoti-sap-client.c: on_timer(275) > is_popup_running: 1, ret: 0
04-27 09:32:50.069+0700 E/wnoti-service( 1429): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524796370, g_use_aul_launch : 1524796370
04-27 09:32:50.089+0700 E/WMS     ( 1017): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-27 09:32:50.219+0700 I/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_service_changed_cb(243) > Handle this change type in idler.
04-27 09:32:50.219+0700 W/wnotib  ( 1214): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-27 09:32:50.219+0700 I/wnotib  ( 1214): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-27 09:32:50.219+0700 W/wnotib  ( 1214): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-27 09:32:50.219+0700 E/wnoti-service( 1429): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1114, is_duplicated : 0
04-27 09:32:50.219+0700 E/wnoti-service( 1429): wnoti-db-client.c: wnoti_get_alert_categories(1311) > view_type : 1, turn_screen_on : 1, allow_gesture : 1, is_used_popup : 0, feedback : 2
04-27 09:32:50.239+0700 E/wnoti-proxy( 2239): wnoti-client.c: _wnoti_parse_extra_sub(373) > JSON_IS_NULL
04-27 09:32:50.239+0700 I/wnotibp ( 2239): wnotiboard-popup-data.c: _data_get_alert_list(226) > application_name: Real Feed Back, application_id: -1016, category_id: 109, time: 1524796369, launch_app_id: (null), bg_image: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, extracted_icon_color: -3139560, disble_block_app_action: 0, support_large_icon 0
04-27 09:32:50.239+0700 I/wnotibp ( 2239): wnotiboard-popup-data.c: _data_get_alert_list(236) > noti_type: 1
04-27 09:32:50.239+0700 I/wnotibp ( 2239): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1114, noti_type: 1
04-27 09:32:50.239+0700 I/wnotibp ( 2239): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 0, content_id: 0, notification_id: 0
04-27 09:32:50.239+0700 W/wnotibp ( 2239): wnotiboard-popup-data.c: _data_convert_alert_data(67) > alert_type: 4, app_feedback_type: 2, popup_view_style: 0, feedback_pattern_app: -1
04-27 09:32:50.239+0700 W/wnotibp ( 2239): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 0, new_list count : 1
04-27 09:32:50.239+0700 W/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_notification_change_cb(600) > [1,1114]
04-27 09:32:50.239+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 09:32:50.239+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-27 09:32:50.239+0700 I/wnotibp ( 2239): wnotiboard-popup-common.c: _common_app_id_from_win(152) > 2350
04-27 09:32:50.249+0700 I/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_check_env_condition(437) > focus app is com.toyota.realtimefeedback, 0
04-27 09:32:50.249+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_add_notification(991) > Add noti_queue [1114, 0]
04-27 09:32:50.259+0700 I/wnotibp ( 2239): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1113
04-27 09:32:50.259+0700 I/wnotibp ( 2239): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 1
04-27 09:32:50.259+0700 I/wnotibp ( 2239): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1113
04-27 09:32:50.259+0700 I/wnotibp ( 2239): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 1
04-27 09:32:50.259+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(73) > ::APP:: CHECK STATE : 8, 0, (null)
04-27 09:32:50.259+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 09:32:50.259+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(79) > ::APP:: CHECK DATA : 1 1 0000
04-27 09:32:50.259+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 09:32:50.269+0700 W/wnotibp ( 2239): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-27 09:32:50.269+0700 I/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_get_active_app_id(1010) > [2350]
04-27 09:32:50.279+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 09:32:50.279+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1178) > [1114, 1, 0, 2, 0000]
04-27 09:32:50.279+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1179) > [0, 1, 0]
04-27 09:32:50.279+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1180) > [1, 0, 0, 0]
04-27 09:32:50.279+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-27 09:32:50.279+0700 I/wnotibp ( 2239): wnotiboard-popup-view.c: _view_create_detail_layout(3707) > wnotiboard_popup_vi_type: 2
04-27 09:32:50.279+0700 I/wnotibp ( 2239): wnotiboard-popup-view.c: _view_create_detail_layout(3712) > (1114, 1114)
04-27 09:32:50.279+0700 I/wnotibp ( 2239): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-27 09:32:50.299+0700 I/efl-extension( 2239): efl_extension_circle_surface.c: eext_circle_surface_layout_add(1290) > Put the surface[0xb8f0e870]'s widget[0xb8e641a8] to layout widget[0xb8f1b988]
04-27 09:32:50.309+0700 I/wnotibp ( 2239): wnotiboard-popup-view.c: _view_create_genlist(3639) > (1114, 1114)
04-27 09:32:50.319+0700 I/efl-extension( 2239): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 09:32:50.329+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-27 09:32:50.329+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-27 09:32:50.329+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 1
04-27 09:32:50.329+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 13 / signal : type_wifi_connected_01
04-27 09:32:50.329+0700 E/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 13) / (hide : 0)
04-27 09:32:50.329+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 09:32:50.329+0700 E/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 09:32:50.329+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-27 09:32:50.339+0700 I/wnotibp ( 2239): wnotiboard-popup-view.c: _view_create_card_data(3073) > 0xb8bdab40, 0xb8bda9c8, 0xb8bda9c8
04-27 09:32:50.339+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 09:32:50.339+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 09:32:50.359+0700 E/EFL     ( 2239): elementary<2239> elm_genlist.c:7236 elm_genlist_item_item_class_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-27 09:32:50.359+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2239): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 09:32:50.359+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2239): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 09:32:50.359+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2239): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 09:32:50.359+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2239): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 09:32:50.369+0700 W/wnotibp ( 2239): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: stack.separator
04-27 09:32:50.369+0700 I/wnotibp ( 2239): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 09:32:50.369+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.369+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.369+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.369+0700 I/wnotibp ( 2239): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 09:32:50.369+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.369+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.369+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.369+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.369+0700 I/wnotibp ( 2239): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 09:32:50.379+0700 I/wnotibp ( 2239): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 09:32:50.379+0700 I/wnotibp ( 2239): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 09:32:50.379+0700 E/EFL     ( 2239): elementary<2239> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 09:32:50.379+0700 W/wnotibp ( 2239): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(224) > can't get layout
04-27 09:32:50.379+0700 E/EFL     ( 2239): elementary<2239> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 09:32:50.379+0700 I/wnotibp ( 2239): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 09:32:50.379+0700 I/wnotibp ( 2239): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1114, 109, -1016.
04-27 09:32:50.399+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2239): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 09:32:50.399+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2239): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 09:32:50.399+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2239): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 09:32:50.399+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2239): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 09:32:50.429+0700 E/EFL     ( 2239): evas_main<2239> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 09:32:50.429+0700 E/EFL     ( 2239): evas_main<2239> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 09:32:50.429+0700 E/EFL     ( 2239): evas_main<2239> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 09:32:50.429+0700 I/wnotibp ( 2239): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 09:32:50.429+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.429+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.429+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.429+0700 I/wnotibp ( 2239): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 09:32:50.429+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.429+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.429+0700 F/EFL     ( 2239): evas_main<2239> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Expected: 747ad76c - Evas_Object (Image)
04-27 09:32:50.429+0700 F/EFL     ( 2239):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 09:32:50.429+0700 I/wnotibp ( 2239): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 09:32:50.429+0700 I/wnotibp ( 2239): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 09:32:50.429+0700 I/wnotibp ( 2239): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 09:32:50.429+0700 W/wnotibp ( 2239): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 09:32:50.429+0700 I/wnotibp ( 2239): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 09:32:50.429+0700 I/wnotibp ( 2239): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1114, 109, -1016.
04-27 09:32:50.439+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 09:32:50.439+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] cw(360), ch(360), pw(360), ph(360)
04-27 09:32:50.449+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] mx(0), my(52), minx(0), miny(0), px(0), py(0)
04-27 09:32:50.449+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] cw(360), ch(412), pw(360), ph(360)
04-27 09:32:50.459+0700 I/wnotibp ( 2239): wnotiboard-popup-view.c: wnbp_view_draw_small_view(4060) > ::UI:: window type is changed by unknown causes
04-27 09:32:50.479+0700 W/APP_CORE( 2239): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3200009
04-27 09:32:50.529+0700 W/wnotibp ( 2239): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2809) > ::UI:: VI TYPE : 2
04-27 09:32:50.529+0700 I/wnotibp ( 2239): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 09:32:50.529+0700 W/wnotibp ( 2239): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2816) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 09:32:50.529+0700 I/wnotibp ( 2239): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 09:32:50.529+0700 I/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_play_feedback(284) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 09:32:50.529+0700 W/TIZEN_N_RECORDER( 2239): recorder_product.c: recorder_is_in_recording(82) > pid =/proc/0 , state =0, alive=0
04-27 09:32:50.529+0700 W/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_play_feedback(298) > ::APP:: application_id: -1016, is_disaster: 0, disaster_info: (null), app_feedback_type: 2, feedback_pattern_app: -1, is_recording: 0
04-27 09:32:50.529+0700 I/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_play_feedback(307) > Gear side feedback setting is_sound_on: 0, is_vibration_on: 1, is_vibrate_when_noti_on: 0
04-27 09:32:50.529+0700 W/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_play_feedback(341) > ::APP:: Determined feedback: sound 0, vibration: 1
04-27 09:32:50.539+0700 I/wnotib  ( 2239): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 13, ret: -3, request_id: 0
04-27 09:32:50.539+0700 W/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_set_smart_relay(501) > Set the smart relay for 0, 109, -1016, Real Feed Back
04-27 09:32:50.539+0700 I/wnotibp ( 2239): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 09:32:50.539+0700 W/wnotibp ( 2239): wnotiboard-popup-control.c: _ctrl_turn_on_lcd(534) > [4, 0, 1, 0]
04-27 09:32:50.539+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2239] goes to (3)
04-27 09:32:50.539+0700 W/AUL_AMD (  929): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 09:32:50.539+0700 W/AUL_AMD (  929): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-27 09:32:50.539+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.wnotiboard-popup) pid(2239) status(fg) type(uiapp)
04-27 09:32:50.569+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(263) > fully_obscured: 0, 0
04-27 09:32:50.569+0700 I/wnotibp ( 2239): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x3200009 0x320000d 0x3200009]
04-27 09:32:50.569+0700 I/APP_CORE( 2239): appcore-efl.c: __do_app(453) > [APP 2239] Event: RESUME State: PAUSED
04-27 09:32:50.569+0700 I/CAPI_APPFW_APPLICATION( 2239): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-27 09:32:50.569+0700 I/wnotibp ( 2239): wnotiboard-popup.c: _popup_app_resume(229) > 
04-27 09:32:50.569+0700 I/GATE    ( 2239): <GATE-M>APP_FULLY_LOADED_wnotiboard-popup</GATE-M>
04-27 09:32:50.569+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(585) > ::UI:: [[[ ===> already [small popup] is LOCK, 0010 ]]]
04-27 09:32:50.569+0700 W/wnotibp ( 2239): wnotiboard-popup-view.c: _view_sub_popup_show_animator_pre_cb(1781) > ::UI:: start showing animation
04-27 09:32:50.659+0700 E/wnoti-service( 1429): wnoti-db-client.c: wnoti_get_alert_categories(1016) > _query_step failed(NO ROW)
04-27 09:32:50.659+0700 E/wnoti-service( 1429): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 2
04-27 09:32:50.669+0700 E/wnoti-service( 1429): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-27 09:32:50.669+0700 E/wnoti-proxy( 2239): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-27 09:32:50.669+0700 E/wnotibp ( 2239): wnotiboard-popup-data.c: _data_get_alert_list(311) > ::DATA:: No categories available.
04-27 09:32:50.669+0700 W/wnotibp ( 2239): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 1, new_list count : 0
04-27 09:32:50.879+0700 W/wnotibp ( 2239): wnotiboard-popup-view.c: _view_sub_popup_show_animator_cb(1684) > ::UI:: end show animation
04-27 09:32:50.879+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-27 09:32:50.969+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:50.969+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:50.969+0700 W/WATCH_CORE( 1356): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
04-27 09:32:50.969+0700 I/WATCH_CORE( 1356): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
04-27 09:32:50.969+0700 I/CAPI_WATCH_APPLICATION( 1356): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-27 09:32:50.969+0700 E/watchface-app( 1356): watchface.cpp: OnAppTimeTick(1157) > 
04-27 09:32:50.969+0700 I/watchface-app( 1356): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-27 09:32:50.979+0700 E/wnoti-service( 1429): wnoti-db-utility.c: context_wearonoff_status_cb(1781) > status changed from 2 to 1 
04-27 09:32:50.979+0700 E/wnoti-service( 1429): wnoti-native-client.c: handle_cache_notification(790) > >>
04-27 09:32:50.979+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] direction_x(1), direction_y(0)
04-27 09:32:50.979+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] drag_child_locked_x(0)
04-27 09:32:50.979+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] move content x(-8), y(0)
04-27 09:32:50.979+0700 E/WMS     ( 1017): wms_event_handler.c: _wms_event_handler_cb_wearonoff_monitor(23513) > wear_monitor_status update[0] = 2 -> 1
04-27 09:32:50.979+0700 E/WMS     ( 1017): wms_event_handler.c: _wms_event_handler_send_wear_monitor_status(10697) > show noti while wearing gear disabled
04-27 09:32:50.979+0700 W/WECONN  (  912): <__wc_feature_wearonoff_monitor_cb:533> { error=0, state=CONTEXT_WEARONOFF_MONITOR_STATUS_ON
04-27 09:32:50.979+0700 W/WECONN  (  912): <__wc_device_on_wear_onoff_changed_cb:363> { state=WC_FEATURE_STATE_ON
04-27 09:32:50.979+0700 W/WECONN  (  912): <__wc_device_on_wear_onoff_changed_cb:378> Disconnected manually : 0
04-27 09:32:50.979+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:50.979+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:50.979+0700 W/WECONN  (  912): <__wc_device_on_wear_onoff_changed_cb:379> Disconnected by auto switch : 0
04-27 09:32:50.989+0700 E/ALARM_MANAGER(  912): alarm-lib.c: alarmmgr_add_alarm_withcb(1178) > trigger_at_time(5), start(27-4-2018, 09:32:56), repeat(0), interval(0), type(-1073741822)
04-27 09:32:50.989+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [912].
04-27 09:32:50.999+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] direction_x(1), direction_y(0)
04-27 09:32:50.999+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] drag_child_locked_x(0)
04-27 09:32:50.999+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] move content x(-8), y(0)
04-27 09:32:51.019+0700 I/AUL     (  927): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/sbin/weconnd, ret : 0
04-27 09:32:51.019+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 15
04-27 09:32:51.029+0700 I/AUL_AMD (  929): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/sbin/weconnd, ret : 0
04-27 09:32:51.039+0700 W/SHealthServiceCommon( 1754): ContextRestingHeartrateProxy.cpp: OnRestingHrUpdatedCB(347) > [1;40;33mhrValue: 1007[0;m
04-27 09:32:51.039+0700 I/AUL_AMD (  929): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/sbin/weconnd, ret : 0
04-27 09:32:51.039+0700 E/ALARM_MANAGER(  927): alarm-manager-schedule.c: __alarm_next_duetime_once(174) > Final due_time = 1524796376, Fri Apr 27 09:32:56 2018
04-27 09:32:51.039+0700 E/ALARM_MANAGER(  927): alarm-manager-schedule.c: _alarm_next_duetime(509) > alarm_id: 539681414, next duetime: 1524796376
04-27 09:32:51.039+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __alarm_add_to_list(496) > [alarm-server]: After add alarm_id(539681414)
04-27 09:32:51.039+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __alarm_create(1061) > [alarm-server]:alarm_context.c_due_time(1524796702), due_time(1524796376)
04-27 09:32:51.049+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-27 09:32:51.049+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-27 09:32:51.049+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 27-4-2018, 02:32:56 (UTC).
04-27 09:32:51.049+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-27 09:32:51.049+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __save_module_log(1780) > The file is not ready.
04-27 09:32:51.059+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-27 09:32:51.059+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __save_module_log(1780) > The file is not ready.
04-27 09:32:51.059+0700 W/WECONN  (  912): <__wc_device_on_wear_onoff_changed_cb:393> }
04-27 09:32:51.059+0700 W/WECONN  (  912): <__wc_feature_wearonoff_monitor_cb:553> }
04-27 09:32:51.619+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:51.619+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:51.629+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] direction_x(1), direction_y(0)
04-27 09:32:51.629+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] drag_child_locked_x(0)
04-27 09:32:51.629+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] move content x(-7), y(0)
04-27 09:32:51.639+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:51.639+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:51.639+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] direction_x(1), direction_y(0)
04-27 09:32:51.639+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] drag_child_locked_x(0)
04-27 09:32:51.639+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] move content x(-7), y(0)
04-27 09:32:51.649+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:51.649+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:51.659+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:51.659+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:51.659+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] direction_x(1), direction_y(0)
04-27 09:32:51.659+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] drag_child_locked_x(0)
04-27 09:32:51.659+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb2646f48 : elm_genlist] move content x(-7), y(0)
04-27 09:32:51.669+0700 E/EFL     ( 2350): ecore_x<2350> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=235392 button=1
04-27 09:32:52.669+0700 E/EFL     (  886): ecore_x<886> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3e00003 time=235392
04-27 09:32:52.669+0700 E/EFL     ( 2350): ecore_x<2350> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=235392
04-27 09:32:52.669+0700 E/EFL     (  886): ecore_x<886> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=235392
04-27 09:32:53.339+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-27 09:32:53.339+0700 W/wnotibp ( 2239): wnotiboard-popup-view.c: _view_main_popup_timer_cb(1901) > ::UI:: start hiding animation
04-27 09:32:53.639+0700 W/wnotibp ( 2239): wnotiboard-popup-view.c: _view_sub_popup_hide_animator_cb(1842) > ::UI:: end hiding animation
04-27 09:32:53.639+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-27 09:32:53.639+0700 W/wnotibp ( 2239): wnotiboard-popup-view.c: wnbp_view_goto_pause(1476) > state : (8, 1, 0)
04-27 09:32:53.639+0700 I/wnotibp ( 2239): wnotiboard-popup-view.c: wnbp_view_goto_pause(1489) > simple popup=0, view_state=0
04-27 09:32:53.639+0700 I/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(632) > ::UI:: it is invalid object.
04-27 09:32:53.639+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [invalid object] is UNLOCK , 0000 <=== ]]]
04-27 09:32:53.639+0700 I/wnotibp ( 2239): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1114
04-27 09:32:53.639+0700 W/wnotibp ( 2239): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(686) > Call wnbp_free_noti_detail..
04-27 09:32:53.639+0700 W/wnotibp ( 2239): wnotiboard-popup-common.c: wnbp_free_noti_detail(583) > Do free noti [0xb8e1a2f8] / db id [1114]
04-27 09:32:53.639+0700 I/wnotibp ( 2239): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 0
04-27 09:32:53.639+0700 I/efl-extension( 2239): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb8e1c0b0, obj: 0xb8e1c0b0
04-27 09:32:53.639+0700 I/efl-extension( 2239): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-27 09:32:53.639+0700 I/efl-extension( 2239): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-27 09:32:53.639+0700 I/efl-extension( 2239): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-27 09:32:53.639+0700 I/efl-extension( 2239): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-27 09:32:53.649+0700 E/EFL     ( 2239): elementary<2239> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-27 09:32:53.659+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 09:32:53.659+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 09:32:53.659+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 09:32:53.659+0700 E/EFL     ( 2239): elementary<2239> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8e1c0b0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 09:32:53.659+0700 I/efl-extension( 2239): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 09:32:53.659+0700 I/efl-extension( 2239): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8e1c0b0, elm_genlist, func : 0xb6d1eea1
04-27 09:32:53.659+0700 I/efl-extension( 2239): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 09:32:53.659+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 09:32:53.659+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_reset_view_lock(703) > ::UI:: lock state = 0000
04-27 09:32:53.669+0700 I/wnotibp ( 2239): wnotiboard-popup-view.c: wnbp_view_goto_pause(1586) > ::INFO:: call lower
04-27 09:32:53.679+0700 W/APP_CORE( 2239): appcore-efl.c: __hide_cb(882) > [EVENT_TEST][EVENT] GET HIDE EVENT!!!. WIN:3200009
04-27 09:32:53.679+0700 I/APP_CORE( 2239): appcore-efl.c: __do_app(453) > [APP 2239] Event: PAUSE State: RUNNING
04-27 09:32:53.679+0700 I/CAPI_APPFW_APPLICATION( 2239): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-27 09:32:53.709+0700 W/wnotibp ( 2239): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-27 09:32:53.709+0700 W/wnotibp ( 2239): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 09:32:53.709+0700 W/wnotibp ( 2239): wnotiboard-popup.c: _popup_app_pause(220) > [0, 0, 2, 0000]
04-27 09:32:53.709+0700 W/wnotibp ( 2239): wnotiboard-popup.c: _popup_app_pause(221) > [1, 0]
04-27 09:32:53.709+0700 W/wnotibp ( 2239): wnotiboard-popup.c: _popup_app_pause(222) > [0, 1, 0, 2, 0]
04-27 09:32:54.219+0700 E/EFL     ( 2350): ecore_x<2350> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=237947 button=1
04-27 09:32:54.219+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:54.309+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:54.309+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:54.319+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] mouse move
04-27 09:32:54.319+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb2646f48 : elm_genlist] hold(0), freeze(0)
04-27 09:32:54.329+0700 E/EFL     ( 2350): ecore_x<2350> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=238055 button=1
04-27 09:32:54.359+0700 I/efl-extension( 2350): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 09:32:54.359+0700 I/efl-extension( 2350): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 09:32:54.359+0700 I/efl-extension( 2350): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb2215838, elm_image, _activated_obj : 0xb262a948, activated : 1
04-27 09:32:54.359+0700 I/efl-extension( 2350): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 09:32:54.369+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8c55b10 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-27 09:32:54.369+0700 E/EFL     ( 2350): elementary<2350> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8c55b10 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-27 09:32:54.829+0700 W/PROCESSMGR(  886): e_mod_processmgr.c: _e_mod_processmgr_send_update_watch_action(663) > [PROCESSMGR] =====================> send UpdateClock
04-27 09:32:54.829+0700 W/W_HOME  ( 1214): event_manager.c: _ecore_x_message_cb(421) > state: 1 -> 0
04-27 09:32:54.829+0700 W/W_HOME  ( 1214): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 09:32:54.829+0700 W/W_HOME  ( 1214): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 09:32:54.829+0700 W/W_HOME  ( 1214): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 09:32:54.829+0700 W/W_HOME  ( 1214): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 1
04-27 09:32:54.829+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 09:32:54.829+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 09:32:54.849+0700 W/CRASH_MANAGER( 2381): worker.c: worker_job(1205) > 1102350726561152479637
