S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2399
Date: 2018-04-27 13:20:15+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2399, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb58be248
r2   = 0xb58be248, r3   = 0xb58aa28d
r4   = 0xbe861444, r5   = 0xb5bd0800
r6   = 0x00000274, r7   = 0xbe860be8
r8   = 0xb034fed0, r9   = 0xb6f2b898
r10  = 0xb6f2b778, fp   = 0xb6f2b828
ip   = 0xb5bdb084, sp   = 0xbe860b78
lr   = 0xb58aa2af, pc   = 0xb58aa3a0
cpsr = 0xa0000030

Memory Information
MemTotal:   405512 KB
MemFree:      2748 KB
Buffers:      4936 KB
Cached:      99080 KB
VmPeak:     164860 KB
VmSize:     161312 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       33988 KB
VmRSS:       33808 KB
VmData:     101080 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:         128 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2399 TID = 2399
2399 2487 2495 2536 

Maps Information
ae60b000 ae60e000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
aee79000 aef00000 rw-s anon_inode:dmabuf
b156d000 b15f4000 rw-s anon_inode:dmabuf
b15f5000 b1f68000 rw-p [stack:2536]
b1f68000 b1f6c000 r-xp /usr/lib/libogg.so.0.7.1
b1f74000 b1f96000 r-xp /usr/lib/libvorbis.so.0.4.3
b1f9e000 b1fe5000 r-xp /usr/lib/libsndfile.so.1.0.26
b1ff1000 b203a000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b2043000 b2048000 r-xp /usr/lib/libjson.so.0.0.1
b38e9000 b39ef000 r-xp /usr/lib/libicuuc.so.57.1
b3a05000 b3b8d000 r-xp /usr/lib/libicui18n.so.57.1
b3b9d000 b3baa000 r-xp /usr/lib/libail.so.0.1.0
b3bb3000 b3bb6000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3bbe000 b3bf6000 r-xp /usr/lib/libpulse.so.0.16.2
b3bf7000 b3bfa000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3c02000 b3c63000 r-xp /usr/lib/libasound.so.2.0.0
b3c6d000 b3c86000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c8f000 b3c93000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3c9b000 b3ca6000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3cb3000 b3cb7000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3cc0000 b3cd8000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3ce9000 b3cf0000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3cf8000 b3d03000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3d0b000 b3d0d000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3d15000 b3d16000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3e5f000 b3e67000 r-xp /usr/lib/libfeedback.so.0.1.4
b3e77000 b3e78000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3e80000 b3e81000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3f36000 b3fbd000 rw-s anon_inode:dmabuf
b3fd2000 b4059000 rw-s anon_inode:dmabuf
b405a000 b4859000 rw-p [stack:2495]
b49fd000 b49fe000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b05000 b5304000 rw-p [stack:2487]
b5304000 b5306000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b530e000 b5325000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b5332000 b5334000 r-xp /usr/lib/libdri2.so.0.0.0
b533c000 b5347000 r-xp /usr/lib/libtbm.so.1.0.0
b534f000 b5357000 r-xp /usr/lib/libdrm.so.2.4.0
b535f000 b5361000 r-xp /usr/lib/libgenlock.so
b5369000 b536e000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5376000 b5381000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b558a000 b5654000 r-xp /usr/lib/libCOREGL.so.4.0
b5665000 b5675000 r-xp /usr/lib/libmdm-common.so.1.1.25
b567d000 b5683000 r-xp /usr/lib/libxcb-render.so.0.0.0
b568b000 b568c000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5695000 b5698000 r-xp /usr/lib/libEGL.so.1.4
b56a0000 b56ae000 r-xp /usr/lib/libGLESv2.so.2.0
b56b7000 b5700000 r-xp /usr/lib/libmdm.so.1.2.70
b5709000 b570f000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5717000 b5720000 r-xp /usr/lib/libcom-core.so.0.0.1
b5729000 b57e1000 r-xp /usr/lib/libcairo.so.2.11200.14
b57ec000 b5805000 r-xp /usr/lib/libnetwork.so.0.0.0
b580d000 b5819000 r-xp /usr/lib/libnotification.so.0.1.0
b5822000 b5831000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b583a000 b585b000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5863000 b5868000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5870000 b5875000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b587d000 b588d000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5895000 b589d000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b58a5000 b58af000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a54000 b5a5e000 r-xp /lib/libnss_files-2.13.so
b5a67000 b5b36000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b4c000 b5b70000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b79000 b5b7f000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b87000 b5b8b000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b98000 b5ba3000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5bab000 b5bad000 r-xp /usr/lib/libiniparser.so.0
b5bb6000 b5bbb000 r-xp /usr/lib/libappcore-common.so.1.1
b5bc3000 b5bc5000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5bce000 b5bd2000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5bdf000 b5be1000 r-xp /usr/lib/libXau.so.6.0.0
b5be9000 b5bf0000 r-xp /lib/libcrypt-2.13.so
b5c20000 b5c22000 r-xp /usr/lib/libiri.so
b5c2b000 b5dbd000 r-xp /usr/lib/libcrypto.so.1.0.0
b5dde000 b5e25000 r-xp /usr/lib/libssl.so.1.0.0
b5e31000 b5e5f000 r-xp /usr/lib/libidn.so.11.5.44
b5e67000 b5e70000 r-xp /usr/lib/libcares.so.2.1.0
b5e7a000 b5e8d000 r-xp /usr/lib/libxcb.so.1.1.0
b5e96000 b5e99000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ea1000 b5ea3000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5eac000 b5f78000 r-xp /usr/lib/libxml2.so.2.7.8
b5f85000 b5f87000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f90000 b5f95000 r-xp /usr/lib/libffi.so.5.0.10
b5f9d000 b5f9e000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5fa6000 b5fa9000 r-xp /lib/libattr.so.1.1.0
b5fb1000 b6045000 r-xp /usr/lib/libstdc++.so.6.0.16
b6058000 b6075000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b607f000 b6097000 r-xp /usr/lib/libpng12.so.0.50.0
b609f000 b60b5000 r-xp /lib/libexpat.so.1.6.0
b60bf000 b6103000 r-xp /usr/lib/libcurl.so.4.3.0
b610c000 b6116000 r-xp /usr/lib/libXext.so.6.4.0
b6120000 b6124000 r-xp /usr/lib/libXtst.so.6.1.0
b612c000 b6132000 r-xp /usr/lib/libXrender.so.1.3.0
b613a000 b6140000 r-xp /usr/lib/libXrandr.so.2.2.0
b6148000 b6149000 r-xp /usr/lib/libXinerama.so.1.0.0
b6152000 b615b000 r-xp /usr/lib/libXi.so.6.1.0
b6163000 b6166000 r-xp /usr/lib/libXfixes.so.3.1.0
b616f000 b6171000 r-xp /usr/lib/libXgesture.so.7.0.0
b6179000 b617b000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6183000 b6185000 r-xp /usr/lib/libXdamage.so.1.1.0
b618d000 b6194000 r-xp /usr/lib/libXcursor.so.1.0.2
b619c000 b619f000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b61a8000 b61ac000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b61b5000 b61ba000 r-xp /usr/lib/libecore_fb.so.1.7.99
b61c3000 b62a4000 r-xp /usr/lib/libX11.so.6.3.0
b62af000 b62d2000 r-xp /usr/lib/libjpeg.so.8.0.2
b62ea000 b6300000 r-xp /lib/libz.so.1.2.5
b6309000 b630b000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6313000 b6388000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6392000 b63ac000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b63b4000 b63e8000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63f1000 b64c4000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b64d0000 b64e0000 r-xp /lib/libresolv-2.13.so
b64e4000 b64fc000 r-xp /usr/lib/liblzma.so.5.0.3
b6504000 b6507000 r-xp /lib/libcap.so.2.21
b650f000 b653e000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6546000 b6547000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6550000 b6556000 r-xp /usr/lib/libecore_imf.so.1.7.99
b655e000 b6575000 r-xp /usr/lib/liblua-5.1.so
b657e000 b6585000 r-xp /usr/lib/libembryo.so.1.7.99
b658d000 b6593000 r-xp /lib/librt-2.13.so
b659c000 b65f2000 r-xp /usr/lib/libpixman-1.so.0.28.2
b6600000 b6656000 r-xp /usr/lib/libfreetype.so.6.11.3
b6662000 b668a000 r-xp /usr/lib/libfontconfig.so.1.8.0
b668b000 b66d0000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b66d9000 b66ec000 r-xp /usr/lib/libfribidi.so.0.3.1
b66f4000 b670e000 r-xp /usr/lib/libecore_con.so.1.7.99
b6718000 b6721000 r-xp /usr/lib/libedbus.so.1.7.99
b6729000 b6779000 r-xp /usr/lib/libecore_x.so.1.7.99
b677b000 b6784000 r-xp /usr/lib/libvconf.so.0.2.45
b678c000 b679d000 r-xp /usr/lib/libecore_input.so.1.7.99
b67a5000 b67aa000 r-xp /usr/lib/libecore_file.so.1.7.99
b67b2000 b67d4000 r-xp /usr/lib/libecore_evas.so.1.7.99
b67dd000 b681e000 r-xp /usr/lib/libeina.so.1.7.99
b6827000 b6840000 r-xp /usr/lib/libeet.so.1.7.99
b6851000 b68ba000 r-xp /lib/libm-2.13.so
b68c3000 b68c9000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b68d2000 b68d3000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b68db000 b68fe000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6906000 b690b000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6913000 b693d000 r-xp /usr/lib/libdbus-1.so.3.8.12
b6946000 b695d000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6965000 b6970000 r-xp /lib/libunwind.so.8.0.1
b699d000 b69bb000 r-xp /usr/lib/libsystemd.so.0.4.0
b69c5000 b6ae9000 r-xp /lib/libc-2.13.so
b6af7000 b6aff000 r-xp /lib/libgcc_s-4.6.so.1
b6b00000 b6b04000 r-xp /usr/lib/libsmack.so.1.0.0
b6b0d000 b6b13000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b1b000 b6beb000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6bec000 b6c4a000 r-xp /usr/lib/libedje.so.1.7.99
b6c54000 b6c6b000 r-xp /usr/lib/libecore.so.1.7.99
b6c82000 b6d50000 r-xp /usr/lib/libevas.so.1.7.99
b6d76000 b6eb2000 r-xp /usr/lib/libelementary.so.1.7.99
b6ec9000 b6edd000 r-xp /lib/libpthread-2.13.so
b6ee8000 b6eea000 r-xp /usr/lib/libdlog.so.0.0.0
b6ef2000 b6ef5000 r-xp /usr/lib/libbundle.so.0.1.22
b6efd000 b6eff000 r-xp /lib/libdl-2.13.so
b6f08000 b6f15000 r-xp /usr/lib/libaul.so.0.1.0
b6f27000 b6f2d000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f36000 b6f3a000 r-xp /usr/lib/libsys-assert.so
b6f43000 b6f60000 r-xp /lib/ld-2.13.so
b6f69000 b6f6e000 r-xp /usr/bin/launchpad-loader
b71bc000 b88b6000 rw-p [heap]
be841000 be862000 rw-p [stack]
be841000 be862000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2399)
Call Stack Count: 21
 0: loadAllNotification + 0xf (0xb58aa3a0) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53a0
 1: _on_resume_cb + 0x22 (0xb58aa2af) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x52af
 2: (0xb5bcf6ad) [/usr/lib/libcapi-appfw-application.so.0] + 0x16ad
 3: (0xb6f2a251) [/usr/lib/libappcore-efl.so.1] + 0x3251
 4: (0xb5bb7d1f) [/usr/lib/libappcore-common.so.1] + 0x1d1f
 5: (0xb6f0c495) [/usr/lib/libaul.so.0] + 0x4495
 6: (0xb6f0c927) [/usr/lib/libaul.so.0] + 0x4927
 7: (0xb6f0cfc1) [/usr/lib/libaul.so.0] + 0x4fc1
 8: (0xb6f0cf87) [/usr/lib/libaul.so.0] + 0x4f87
 9: g_main_context_dispatch + 0xbc (0xb6b507a9) [/usr/lib/libglib-2.0.so.0] + 0x357a9
10: (0xb6c64ca7) [/usr/lib/libecore.so.1] + 0x10ca7
11: (0xb6c5fb4f) [/usr/lib/libecore.so.1] + 0xbb4f
12: (0xb6c605a7) [/usr/lib/libecore.so.1] + 0xc5a7
13: ecore_main_loop_begin + 0x30 (0xb6c60879) [/usr/lib/libecore.so.1] + 0xc879
14: appcore_efl_main + 0x332 (0xb6f2ab47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
15: ui_app_main + 0xb0 (0xb5bcfed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16: uib_app_run + 0xea (0xb58aa227) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5227
17: main + 0x34 (0xb58aa64d) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x564d
18:  + 0x0 (0xb6f6aa53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
19: __libc_start_main + 0x114 (0xb69dc85c) [/lib/libc.so.6] + 0x1785c
20: (0xb6f6ae0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
66 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.749+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.749+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.749+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.749+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.749+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.749+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:12.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:12.769+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb8ece098 : elm_scroller] t_in(0.120000), pos_x(720)
04-27 13:20:12.769+0700 W/W_HOME  ( 1256): home_navigation.c: _anim_start_cb(1293) > anim start
04-27 13:20:12.769+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 13:20:12.769+0700 W/W_HOME  ( 1256): home_navigation.c: _up_cb(1250) > up
04-27 13:20:12.769+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 13:20:12.769+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_add(850) > add timer:1
04-27 13:20:12.769+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
04-27 13:20:12.769+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 3, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:12.779+0700 W/W_HOME  ( 1256): index.c: index_show(300) > is_paused:0 show VI:1 visibility:0 vi:(nil)
04-27 13:20:12.779+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb8ece098 : elm_scroller] time(0.153600)
04-27 13:20:12.779+0700 I/ELM_RPANEL( 1256): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
04-27 13:20:12.779+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb8ece098 : elm_scroller] ECORE_CALLBACK_RENEW : px(967), py(0)
04-27 13:20:12.829+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb8ece098 : elm_scroller] time(0.876388)
04-27 13:20:12.829+0700 I/wnotib  ( 1256): w-notification-board-common.c: wnb_common_set_panel_displayed_state(4327) > Set is_notiboard_displayed to 1.
04-27 13:20:12.839+0700 I/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_activate(3236) > page_index: 0.
04-27 13:20:12.839+0700 W/W_HOME  ( 1256): noti_broker.c: _handler_indicator_select(586) > 0
04-27 13:20:12.839+0700 W/W_HOME  ( 1256): noti_broker.c: _handler_indicator_select(596) > select index:1
04-27 13:20:12.839+0700 E/W_HOME  ( 1256): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-27 13:20:12.839+0700 W/W_HOME  ( 1256): noti_broker.c: _handler_indicator_show(539) > 
04-27 13:20:12.839+0700 W/W_HOME  ( 1256): index.c: index_show(300) > is_paused:0 show VI:1 visibility:1 vi:0xb935a120
04-27 13:20:12.839+0700 W/wnotib  ( 1256): w-notification-board-noti-manager.c: wnb_nm_control_home_indicator(59) > Hide home indicator.
04-27 13:20:12.839+0700 W/W_HOME  ( 1256): noti_broker.c: _handler_noti_indicator_hide(496) > 
04-27 13:20:12.839+0700 E/wnoti-service( 1419): wnoti-server-mgr-stub.c: __wnoti_change_new_flag_stub(3143) > change_type : 0, value : 0
04-27 13:20:12.849+0700 I/ELM_RPANEL( 1256): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
04-27 13:20:12.849+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb8ece098 : elm_scroller] ECORE_CALLBACK_RENEW : px(756), py(0)
04-27 13:20:12.879+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb8ece098 : elm_scroller] time(0.989264)
04-27 13:20:12.889+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:2556 _elm_scroll_scroll_to_x_animator() [0xb8ece098 : elm_scroller] animation stop!!
04-27 13:20:12.889+0700 W/wnotib  ( 1256): w-notification-board-panel-manager.c: _wnb_pm_anim_stop_cb(96) > notiboard scroller anim stop [720][0][360][360]
04-27 13:20:12.889+0700 W/wnotib  ( 1256): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(981) > No postponed update with is_for_VI: 1.
04-27 13:20:12.889+0700 W/W_HOME  ( 1256): home_navigation.c: _anim_stop_cb(1319) > anim stop
04-27 13:20:12.889+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 13:20:12.889+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_add(850) > add timer:1
04-27 13:20:12.889+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
04-27 13:20:12.889+0700 I/efl-extension( 1256): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(489) > [0xb8ece098 : elm_scroller] detent_count(0)
04-27 13:20:12.889+0700 I/efl-extension( 1256): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(490) > [0xb8ece098 : elm_scroller] pagenumber_v(0), pagenumber_h(2)
04-27 13:20:12.889+0700 I/efl-extension( 1256): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(512) > [0xb8ece098 : elm_scroller] CurrentPage(2)
04-27 13:20:12.889+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:2559 _elm_scroll_scroll_to_x_animator() [0xb8ece098 : elm_scroller] ECORE_CALLBACK_CANCEL : px(720), py(0)
04-27 13:20:12.909+0700 W/WATCH_CORE( 1358): appcore-watch.c: __widget_pause(1113) > widget_pause
04-27 13:20:12.909+0700 W/AUL     ( 1358): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1358) status(bg) type(watchapp)
04-27 13:20:12.909+0700 E/watchface-app( 1358): watchface.cpp: OnAppPause(1122) > 
04-27 13:20:13.009+0700 E/W_HOME  ( 1256): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
04-27 13:20:13.019+0700 W/W_HOME  ( 1256): event_manager.c: _home_scroll_cb(579) > scroll,will,done
04-27 13:20:13.019+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 1, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:13.019+0700 W/W_HOME  ( 1256): event_manager.c: _home_scroll_cb(579) > scroll,done
04-27 13:20:13.019+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:13.219+0700 I/CAPI_NETWORK_CONNECTION( 2399): connection.c: connection_create(453) > New handle created[0xb74fe630]
04-27 13:20:13.249+0700 E/JSON PARSING( 2399): No data could be display
04-27 13:20:13.249+0700 E/JSON PARSING( 2399): ����苵;
04-27 13:20:13.259+0700 I/CAPI_NETWORK_CONNECTION( 2399): connection.c: connection_destroy(471) > Destroy handle: 0xb74fe630
04-27 13:20:13.489+0700 W/W_HOME  ( 1256): noti_broker.c: _handler_indicator_hide(550) > 
04-27 13:20:13.489+0700 W/W_HOME  ( 1256): index.c: index_hide(338) > hide VI:1 visibility:1 vi:(nil)
04-27 13:20:13.499+0700 E/EFL     ( 1256): ecore_x<1256> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=352459 button=1
04-27 13:20:13.499+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:13.499+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 13:20:13.539+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:13.539+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:13.579+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] mouse move
04-27 13:20:13.579+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ece098 : elm_scroller] hold(0), freeze(0)
04-27 13:20:13.629+0700 E/EFL     ( 1256): ecore_x<1256> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=352579 button=1
04-27 13:20:13.629+0700 W/W_HOME  ( 1256): home_navigation.c: _up_cb(1250) > up
04-27 13:20:13.629+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 13:20:13.629+0700 W/W_HOME  ( 1256): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
04-27 13:20:13.629+0700 W/W_HOME  ( 1256): noti_broker.c: _handler_indicator_hide(550) > 
04-27 13:20:13.629+0700 W/W_HOME  ( 1256): index.c: index_hide(338) > hide VI:1 visibility:0 vi:(nil)
04-27 13:20:13.659+0700 W/wnotib  ( 1256): w-notification-board-basic-panel.c: wnb_bp_create_second_depth_view(2700) > is_real_canvas: 0, launch_thread_list_view: 0, noti_detail_type: 0
04-27 13:20:13.669+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 13:20:13.679+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xabe15ff0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:20:13.679+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xabe15ff0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 13:20:13.689+0700 E/EFL     ( 1256): <1256> elm_main.c:1278 elm_object_style_set() safety check failed: obj == NULL
04-27 13:20:13.699+0700 E/wnotib  ( 1256): w-notification-board-common.c: wnb_common_apply_color_and_font_class(6405) > Can not use color and font table
04-27 13:20:13.699+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:13.699+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:13.699+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:13.699+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: app.icon
04-27 13:20:13.699+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: vi.app.icon
04-27 13:20:13.699+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: badge.image
04-27 13:20:13.699+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:13.709+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:13.709+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 13:20:13.709+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 13:20:13.709+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:13.709+0700 I/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1249, 127, -1016.
04-27 13:20:13.729+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xabe15ff0 : elm_genlist] mx(0), my(35), minx(0), miny(0), px(0), py(0)
04-27 13:20:13.729+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xabe15ff0 : elm_genlist] cw(360), ch(395), pw(360), ph(360)
04-27 13:20:13.729+0700 W/ELM_RPANEL( 1256): elm-rpanel.c: _realized_cb(6399) > Manually render for dump image.
04-27 13:20:13.919+0700 I/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_set_current_detail_noti(1441) > Set current_detail_noti_db_id to 0
04-27 13:20:13.919+0700 I/efl-extension( 1256): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xabe15ff0, obj: 0xabe15ff0
04-27 13:20:13.919+0700 I/efl-extension( 1256): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-27 13:20:13.939+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xabe15ff0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:20:13.939+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xabe15ff0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 13:20:13.939+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xabe15ff0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:20:13.939+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xabe15ff0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 13:20:13.939+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 13:20:13.939+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xabe15ff0, elm_genlist, func : 0xb686cea1
04-27 13:20:13.939+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 13:20:14.099+0700 E/WMS     ( 1100): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-27 13:20:14.489+0700 I/wnotib  ( 1256): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 8, ret: -3, request_id: 0
04-27 13:20:14.489+0700 I/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_rpanel_item_panel_clicked_cb(4440) > noti_detail_type: 0
04-27 13:20:14.489+0700 W/wnotib  ( 1256): w-notification-board-basic-panel.c: wnb_bp_create_second_depth_view(2700) > is_real_canvas: 1, launch_thread_list_view: 0, noti_detail_type: 0
04-27 13:20:14.489+0700 I/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_set_scroll_config(521) > Before thumbscroll_sensitivity_friction: 1.000000
04-27 13:20:14.489+0700 I/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_set_scroll_config(523) > After thumbscroll_sensitivity_friction: 0.300000
04-27 13:20:14.489+0700 I/wnotib  ( 1256): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-27 13:20:14.509+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 13:20:14.519+0700 I/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_set_current_detail_noti(1441) > Set current_detail_noti_db_id to 1249
04-27 13:20:14.519+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xabe16a38, elm_genlist, _activated_obj : 0xb8ef24b0, activated : 1
04-27 13:20:14.519+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 13:20:14.519+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xabe16a38 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:20:14.519+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xabe16a38 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 13:20:14.529+0700 E/EFL     ( 1256): <1256> elm_main.c:1278 elm_object_style_set() safety check failed: obj == NULL
04-27 13:20:14.539+0700 E/wnotib  ( 1256): w-notification-board-common.c: wnb_common_apply_color_and_font_class(6405) > Can not use color and font table
04-27 13:20:14.539+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:14.539+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:14.539+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:14.539+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: app.icon
04-27 13:20:14.539+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: vi.app.icon
04-27 13:20:14.539+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: badge.image
04-27 13:20:14.539+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:14.549+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:14.549+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 13:20:14.549+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 13:20:14.549+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:14.549+0700 I/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1249, 127, -1016.
04-27 13:20:14.559+0700 I/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_init_action_drawer_for_second_depth(950) > Display the action drawer for 1249, 127, -1016
04-27 13:20:14.559+0700 W/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_init_action_drawer_for_second_depth(966) > Initialize the action drawer.
04-27 13:20:14.559+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_initialize(5603) > Init drawer.
04-27 13:20:14.559+0700 I/efl-extension( 1256): efl_extension_more_option.c: eext_more_option_add(325) > called!!
04-27 13:20:14.559+0700 I/efl-extension( 1256): efl_extension_more_option.c: _drawer_layout_create(185) > called!!
04-27 13:20:14.559+0700 I/efl-extension( 1256): efl_extension_more_option.c: _more_option_data_init(248) > mold is initialized!!
04-27 13:20:14.559+0700 I/efl-extension( 1256): efl_extension_more_option.c: _panel_create(204) > called!!
04-27 13:20:14.579+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_add(2165) > called!!
04-27 13:20:14.579+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: _eext_rotary_selector_data_init(591) > rsd is initialized!!
04-27 13:20:14.579+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 13:20:14.579+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: _event_area_callback_add(505) > called!!
04-27 13:20:14.599+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: _rotary_selector_show_cb(810) > called!!
04-27 13:20:14.599+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: _items_invalidate(924) > item_count is zero!!
04-27 13:20:14.599+0700 I/wnotib  ( 1256): w-notification-board-common.c: wnb_common_create_sending_popup(6515) > progress_start: 0, use_requesting: 0.
04-27 13:20:14.619+0700 E/EFL     (  936): ecore_x<936> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x2400003 time=352579
04-27 13:20:14.639+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb957e0b8 : elm_scroller] mx(360), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:20:14.639+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb957e0b8 : elm_scroller] cw(360), ch(0), pw(0), ph(0)
04-27 13:20:14.639+0700 W/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_init_action_drawer_for_second_depth(1011) > Set the drawer item now.
04-27 13:20:14.639+0700 W/wnotib  ( 1256): w-notification-board-action.c: wnb_action_set_item_info(5240) > db_id: 1249, category_id: 127, application_id: -1016, clear_all_items: 1
04-27 13:20:14.639+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_terminate_input_selector(5550) > No need to close w-input-selector.
04-27 13:20:14.649+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 13:20:14.659+0700 W/AUL_AMD (  991): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-27 13:20:14.659+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_terminate_noti_composer(5585) > ret : 0, is_running : 0
04-27 13:20:14.659+0700 I/efl-extension( 1256): efl_extension_more_option.c: eext_more_option_items_clear(572) > called!!
04-27 13:20:14.659+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_items_clear(2473) > called!!
04-27 13:20:14.659+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_create_pages(4129) > Create drawer pages for 127, -1016
04-27 13:20:14.659+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_create_pages(4929) > No need to add default actions for companion noti.
04-27 13:20:14.659+0700 I/efl-extension( 1256): efl_extension_more_option.c: eext_more_option_item_append(458) > called!!
04-27 13:20:14.659+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_append(2255) > called!!
04-27 13:20:14.679+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_domain_translatable_part_text_set(2579) > called!!
04-27 13:20:14.679+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_part_content_set(2638) > called!!
04-27 13:20:14.679+0700 I/efl-extension( 1256): efl_extension_more_option.c: eext_more_option_item_append(458) > called!!
04-27 13:20:14.679+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_append(2255) > called!!
04-27 13:20:14.689+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_domain_translatable_part_text_set(2579) > called!!
04-27 13:20:14.689+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_part_content_set(2638) > called!!
04-27 13:20:14.689+0700 I/efl-extension( 1256): efl_extension_more_option.c: eext_more_option_item_append(458) > called!!
04-27 13:20:14.689+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_append(2255) > called!!
04-27 13:20:14.699+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_domain_translatable_part_text_set(2579) > called!!
04-27 13:20:14.699+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_item_part_content_set(2638) > called!!
04-27 13:20:14.699+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_create_pages(5030) > Number of pages: 3 for -1016
04-27 13:20:14.719+0700 E/EFL     ( 1256): <1256> elm_main.c:1278 elm_object_style_set() safety check failed: obj == NULL
04-27 13:20:14.719+0700 E/wnotib  ( 1256): w-notification-board-common.c: wnb_common_apply_color_and_font_class(6405) > Can not use color and font table
04-27 13:20:14.719+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:14.719+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:14.719+0700 E/EFL     ( 1256): evas_main<1256> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 13:20:14.719+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: app.icon
04-27 13:20:14.719+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: vi.app.icon
04-27 13:20:14.729+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: badge.image
04-27 13:20:14.729+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:14.729+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:14.729+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 13:20:14.729+0700 W/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 13:20:14.729+0700 I/wnotib  ( 1256): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 13:20:14.729+0700 I/wnotib  ( 1256): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1249, 127, -1016.
04-27 13:20:14.749+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xabe16a38 : elm_genlist] mx(0), my(35), minx(0), miny(0), px(0), py(0)
04-27 13:20:14.749+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xabe16a38 : elm_genlist] cw(360), ch(395), pw(360), ph(360)
04-27 13:20:14.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb957e0b8 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:20:14.759+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb957e0b8 : elm_scroller] cw(360), ch(0), pw(360), ph(0)
04-27 13:20:14.769+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb957e0b8 : elm_scroller] mx(0), my(360), minx(0), miny(0), px(0), py(0)
04-27 13:20:14.769+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb957e0b8 : elm_scroller] cw(360), ch(360), pw(360), ph(0)
04-27 13:20:14.769+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb957e0b8 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:20:14.769+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb957e0b8 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
04-27 13:20:14.789+0700 E/EFL     ( 1256): ecore_x<1256> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=352579
04-27 13:20:14.789+0700 E/EFL     (  936): ecore_x<936> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=352579
04-27 13:20:14.799+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: _item_update_animator_cb(1546) > called
04-27 13:20:14.799+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: _item_update_animator_cb(1568) > item_list(0x-1185581936), count(3)
04-27 13:20:14.809+0700 I/efl-extension( 1256): efl_extension_more_option.c: _panel_inactive_cb(99) > more_option is closed!!
04-27 13:20:14.809+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_closed(5424) > More option closed!
04-27 13:20:14.809+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_closed(5427) > Set the rotary focus to the parent of drawer: 0xabe16a38
04-27 13:20:14.809+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xabe16a38, elm_genlist, _activated_obj : 0xabe16a38, activated : 1
04-27 13:20:14.809+0700 I/efl-extension( 1256): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xacb0bd98, elm_layout, _activated_obj : 0xabe16a38, activated : 0
04-27 13:20:14.809+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_get(3146) > called!!
04-27 13:20:14.809+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_set(3107) > called!!
04-27 13:20:14.809+0700 I/efl-extension( 1256): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_set(3140) > selected item index(0)
04-27 13:20:14.809+0700 W/efl-extension( 1256): efl_extension_events.c: eext_object_event_callback_del(325) > This object(0xacb020d0) hasn't been registered before
04-27 13:20:15.019+0700 W/SHealthCommon( 1754): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [5000][0;m
04-27 13:20:15.049+0700 W/SHealthCommon( 1754): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
04-27 13:20:15.069+0700 W/SHealthCommon( 1754): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
04-27 13:20:15.339+0700 E/EFL     ( 1256): ecore_x<1256> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=354303 button=1
04-27 13:20:15.339+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] mouse move
04-27 13:20:15.359+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] mouse move
04-27 13:20:15.359+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] hold(0), freeze(0)
04-27 13:20:15.409+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] mouse move
04-27 13:20:15.409+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] hold(0), freeze(0)
04-27 13:20:15.449+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] mouse move
04-27 13:20:15.449+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] hold(0), freeze(0)
04-27 13:20:15.459+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] mouse move
04-27 13:20:15.459+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] hold(0), freeze(0)
04-27 13:20:15.469+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] mouse move
04-27 13:20:15.469+0700 E/EFL     ( 1256): elementary<1256> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xabe16a38 : elm_genlist] hold(0), freeze(0)
04-27 13:20:15.479+0700 E/EFL     ( 1256): ecore_x<1256> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=354444 button=1
04-27 13:20:15.479+0700 W/wnotib  ( 1256): w-notification-board-basic-panel.c: _wnb_bp_inline_action_btn_click_cb(4770) > ACTION BUTTON IS CLICKED~~~~~~~~~~
04-27 13:20:15.479+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_open_app_cb(3281) > Action clicked for 127, -1016
04-27 13:20:15.479+0700 W/AUL     ( 1256): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.toyota.realtimefeedback)
04-27 13:20:15.489+0700 W/AUL_AMD (  991): amd_request.c: __request_handler(669) > __request_handler: 0
04-27 13:20:15.489+0700 W/AUL_AMD (  991): amd_launch.c: _start_app(1782) > caller pid : 1256
04-27 13:20:15.489+0700 I/AUL_AMD (  991): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
04-27 13:20:15.509+0700 W/AUL     (  991): app_signal.c: aul_send_app_resume_request_signal(567) > aul_send_app_resume_request_signal app(com.toyota.realtimefeedback) pid(2399) type(uiapp) bg(0)
04-27 13:20:15.509+0700 W/STARTER ( 1178): pkg-monitor.c: _app_mgr_status_cb(421) > [_app_mgr_status_cb:421] Resume request [2399]
04-27 13:20:15.519+0700 W/AUL_AMD (  991): amd_launch.c: __nofork_processing(1229) > __nofork_processing, cmd: 0, pid: 2399
04-27 13:20:15.519+0700 W/AUL_AMD (  991): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(2399), cmd(0)
04-27 13:20:15.519+0700 W/AUL     ( 1256): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2399)
04-27 13:20:15.519+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_trigger_action_timer(5096) > Remove previous action timer for 1249, -1016.
04-27 13:20:15.519+0700 E/wnotib  ( 1256): w-notification-board-common.c: wnb_common_perform_auto_deletion(3820) > g_auto_remove_timer is exist!.
04-27 13:20:15.519+0700 E/EFL     ( 1256): ecore<1256> ecore.c:573 _ecore_magic_fail() 
04-27 13:20:15.519+0700 E/EFL     ( 1256): *** ECORE ERROR: Ecore Magic Check Failed!!!
04-27 13:20:15.519+0700 E/EFL     ( 1256): *** IN FUNCTION: ecore_timer_del()
04-27 13:20:15.519+0700 E/EFL     ( 1256): ecore<1256> ecore.c:577 _ecore_magic_fail()   Input handle has already been freed!
04-27 13:20:15.519+0700 E/EFL     ( 1256): ecore<1256> ecore.c:586 _ecore_magic_fail() *** NAUGHTY PROGRAMMER!!!
04-27 13:20:15.519+0700 E/EFL     ( 1256): *** SPANK SPANK SPANK!!!
04-27 13:20:15.519+0700 E/EFL     ( 1256): *** Now go fix your code. Tut tut tut!
04-27 13:20:15.519+0700 W/wnotib  ( 1256): w-notification-board-common.c: wnb_common_perform_auto_deletion(3824) > auto_remove is set
04-27 13:20:15.519+0700 I/wnotib  ( 1256): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 21, ret: -3, request_id: 0
04-27 13:20:15.519+0700 I/wnotib  ( 1256): w-notification-board-action.c: _wnb_action_open_app_cb(3326) > Finish action successfully.
04-27 13:20:15.519+0700 I/wnotib  ( 1256): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 24, ret: -3, request_id: 0
04-27 13:20:15.529+0700 I/APP_CORE( 2399): appcore-efl.c: __do_app(453) > [APP 2399] Event: RESET State: PAUSED
04-27 13:20:15.529+0700 I/CAPI_APPFW_APPLICATION( 2399): app_main.c: _ui_app_appcore_reset(645) > app_appcore_reset
04-27 13:20:15.529+0700 I/APP_CORE( 2399): appcore-efl.c: __do_app(529) > Legacy lifecycle: 0
04-27 13:20:15.529+0700 I/APP_CORE( 2399): appcore-efl.c: __do_app(531) > [APP 2399] App already running, raise the window
04-27 13:20:15.549+0700 I/APP_CORE( 2399): appcore-efl.c: __do_app(535) > [APP 2399] Call the resume_cb
04-27 13:20:15.549+0700 I/CAPI_APPFW_APPLICATION( 2399): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-27 13:20:15.549+0700 W/AUL     (  991): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.toyota.realtimefeedback) pid(2399) status(fg) type(uiapp)
04-27 13:20:15.569+0700 W/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2399] goes to (3)
04-27 13:20:15.989+0700 I/CAPI_NETWORK_CONNECTION( 2399): connection.c: connection_create(453) > New handle created[0xb73f49d8]
04-27 13:20:16.089+0700 E/JSON PARSING( 2399): No data could be display
04-27 13:20:16.089+0700 E/JSON PARSING( 2399): `Q��苵;
04-27 13:20:16.089+0700 I/CAPI_NETWORK_CONNECTION( 2399): connection.c: connection_destroy(471) > Destroy handle: 0xb73f49d8
04-27 13:20:16.559+0700 W/W_HOME  ( 1256): event_manager.c: _ecore_x_message_cb(421) > state: 0 -> 1
04-27 13:20:16.559+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:16.569+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:16.569+0700 W/W_INDICATOR( 1181): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 13:20:16.569+0700 W/W_INDICATOR( 1181): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 13:20:16.579+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:16.579+0700 W/W_HOME  ( 1256): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 0
04-27 13:20:16.619+0700 W/W_HOME  ( 1256): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(1)
04-27 13:20:16.619+0700 W/W_HOME  ( 1256): event_manager.c: _window_visibility_cb(470) > state: 1 -> 0
04-27 13:20:16.619+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:16.619+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:6, app_state:1 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:16.619+0700 W/W_HOME  ( 1256): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(1)
04-27 13:20:16.629+0700 I/APP_CORE( 1256): appcore-efl.c: __do_app(453) > [APP 1256] Event: PAUSE State: RUNNING
04-27 13:20:16.629+0700 I/CAPI_APPFW_APPLICATION( 1256): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-27 13:20:16.629+0700 W/W_HOME  ( 1256): main.c: _appcore_pause_cb(489) > appcore pause
04-27 13:20:16.629+0700 W/W_HOME  ( 1256): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
04-27 13:20:16.629+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:16.629+0700 W/W_INDICATOR( 1181): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 13:20:16.629+0700 W/W_INDICATOR( 1181): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 13:20:16.639+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:16.639+0700 W/W_HOME  ( 1256): main.c: home_pause(550) > clock/widget paused
04-27 13:20:16.639+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 13:20:16.639+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:20:16.649+0700 E/wnotibp ( 2211): wnotiboard-popup-manager.c: _mgr_x_property_changed_cb(236) > ecore_x_netwm_pid_get failed
04-27 13:20:16.659+0700 W/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1256] goes to (4)
04-27 13:20:16.659+0700 E/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1256)'s state(4)
04-27 13:20:16.659+0700 I/wnotib  ( 1256): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 1
04-27 13:20:16.659+0700 I/efl-extension( 1256): efl_extension_more_option.c: eext_more_option_opened_get(655) > called!!
04-27 13:20:16.659+0700 W/wnotib  ( 1256): w-notification-board-noti-manager.c: wnb_nm_postpone_updating_job(997) > Set is_notiboard_update_postponed to true with is_for_VI 0, notiboard panel creation count [4], notiboard card appending count [7].
04-27 13:20:16.669+0700 W/AUL     (  991): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1256) status(bg) type(uiapp)
04-27 13:20:16.679+0700 W/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1256] goes to (3)
04-27 13:20:16.679+0700 E/STARTER ( 1178): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1256)'s state(3)
04-27 13:20:16.689+0700 W/AUL_AMD (  991): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 13:20:16.689+0700 W/AUL_AMD (  991): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-27 13:20:16.689+0700 W/AUL     (  991): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1256) status(fg) type(uiapp)
04-27 13:20:16.689+0700 W/MUSIC_CONTROL_SERVICE( 1758): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1758]   [com.samsung.w-home]register msg port [false][0m
04-27 13:20:16.729+0700 W/AUL_PAD ( 1934): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2399 pgid = 2399
04-27 13:20:16.789+0700 W/AUL_PAD ( 1934): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 11
04-27 13:20:16.909+0700 W/AUL_PAD ( 1934): sigchild.h: __launchpad_process_sigchld(197) > after __sigchild_action
04-27 13:20:16.909+0700 I/AUL_AMD (  991): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2399
04-27 13:20:16.909+0700 W/AUL     (  991): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2399)
04-27 13:20:17.049+0700 W/CRASH_MANAGER( 2568): worker.c: worker_job(1205) > 1102399726561152481001
