S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2057
Date: 2018-04-30 16:26:31+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2057, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb6f54974
r2   = 0xb6f54f80, r3   = 0x00000000
r4   = 0xb74c57b8, r5   = 0xb77a5088
r6   = 0x00000013, r7   = 0xbeb0c188
r8   = 0x00001f97, r9   = 0xb74c57b8
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb6f54bc8, sp   = 0xbeb0c168
lr   = 0xb6f4226d, pc   = 0xb6f422a2
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:     10132 KB
Buffers:      4736 KB
Cached:      88260 KB
VmPeak:     254928 KB
VmSize:     250084 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       30488 KB
VmRSS:       29952 KB
VmData:     177796 KB
VmStk:         136 KB
VmExe:          40 KB
VmLib:       35904 KB
VmPTE:         208 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2057 TID = 2057
2057 2062 2066 2067 

Maps Information
b04e6000 b04ea000 r-xp /usr/lib/libogg.so.0.7.1
b04f2000 b0514000 r-xp /usr/lib/libvorbis.so.0.4.3
b051c000 b0563000 r-xp /usr/lib/libsndfile.so.1.0.26
b056f000 b05b8000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b05c1000 b05c6000 r-xp /usr/lib/libjson.so.0.0.1
b1e67000 b1f6d000 r-xp /usr/lib/libicuuc.so.57.1
b1f83000 b210b000 r-xp /usr/lib/libicui18n.so.57.1
b211b000 b2128000 r-xp /usr/lib/libail.so.0.1.0
b2131000 b2134000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b213c000 b2174000 r-xp /usr/lib/libpulse.so.0.16.2
b2175000 b2178000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b2180000 b21e1000 r-xp /usr/lib/libasound.so.2.0.0
b21eb000 b2204000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b220d000 b2211000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b2219000 b2224000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b2231000 b2235000 r-xp /usr/lib/libmmfsession.so.0.0.0
b223e000 b2256000 r-xp /usr/lib/libmmfsound.so.0.1.0
b2267000 b226e000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b2276000 b2281000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b24a5000 b252c000 rw-s anon_inode:dmabuf
b25b5000 b25b7000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b25bf000 b25c7000 r-xp /usr/lib/libfeedback.so.0.1.4
b2706000 b2707000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b270f000 b2710000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b29ae000 b31ad000 rw-p [stack:2067]
b3354000 b3355000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b3427000 b3428000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3430000 b3433000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b345d000 b3c5c000 rw-p [stack:2066]
b3c5c000 b3c5e000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3c66000 b3c7d000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3e8e000 b468d000 rw-p [stack:2062]
b46a4000 b4fd0000 r-xp /usr/lib/libsc-a3xx.so
b5234000 b5236000 r-xp /usr/lib/libdri2.so.0.0.0
b523e000 b5246000 r-xp /usr/lib/libdrm.so.2.4.0
b524e000 b5252000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b525a000 b525d000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b5265000 b5266000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b526e000 b5279000 r-xp /usr/lib/libtbm.so.1.0.0
b5281000 b5284000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b528c000 b528e000 r-xp /usr/lib/libgenlock.so
b5296000 b529b000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b52a3000 b53de000 r-xp /usr/lib/egl/libGLESv2.so
b541a000 b541c000 r-xp /usr/lib/libadreno_utils.so
b5426000 b544d000 r-xp /usr/lib/libgsl.so
b545c000 b5463000 r-xp /usr/lib/egl/eglsubX11.so
b546d000 b548f000 r-xp /usr/lib/egl/libEGL.so
b5498000 b550d000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b571d000 b5727000 r-xp /lib/libnss_files-2.13.so
b5730000 b5733000 r-xp /lib/libattr.so.1.1.0
b573b000 b5742000 r-xp /lib/libcrypt-2.13.so
b5772000 b5775000 r-xp /lib/libcap.so.2.21
b577d000 b577f000 r-xp /usr/lib/libiri.so
b5787000 b57a4000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b57ad000 b57b1000 r-xp /usr/lib/libsmack.so.1.0.0
b57ba000 b57e9000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b57f1000 b5885000 r-xp /usr/lib/libstdc++.so.6.0.16
b5898000 b5967000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b597d000 b59a1000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b59aa000 b5a74000 r-xp /usr/lib/libCOREGL.so.4.0
b5a8b000 b5a8d000 r-xp /usr/lib/libXau.so.6.0.0
b5a96000 b5aa6000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5aae000 b5ab1000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ab9000 b5ad1000 r-xp /usr/lib/liblzma.so.5.0.3
b5ada000 b5adc000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5ae4000 b5ae7000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5aef000 b5af3000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5afc000 b5b01000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5b0b000 b5b2e000 r-xp /usr/lib/libjpeg.so.8.0.2
b5b46000 b5b5c000 r-xp /lib/libexpat.so.1.6.0
b5b66000 b5b79000 r-xp /usr/lib/libxcb.so.1.1.0
b5b82000 b5b88000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5b90000 b5b91000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5b9b000 b5bb3000 r-xp /usr/lib/libpng12.so.0.50.0
b5bbb000 b5bbe000 r-xp /usr/lib/libEGL.so.1.4
b5bc6000 b5bd4000 r-xp /usr/lib/libGLESv2.so.2.0
b5bdd000 b5bde000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5be6000 b5bfd000 r-xp /usr/lib/liblua-5.1.so
b5c07000 b5c0e000 r-xp /usr/lib/libembryo.so.1.7.99
b5c16000 b5c20000 r-xp /usr/lib/libXext.so.6.4.0
b5c29000 b5c2d000 r-xp /usr/lib/libXtst.so.6.1.0
b5c35000 b5c3b000 r-xp /usr/lib/libXrender.so.1.3.0
b5c43000 b5c49000 r-xp /usr/lib/libXrandr.so.2.2.0
b5c51000 b5c52000 r-xp /usr/lib/libXinerama.so.1.0.0
b5c5c000 b5c5f000 r-xp /usr/lib/libXfixes.so.3.1.0
b5c67000 b5c69000 r-xp /usr/lib/libXgesture.so.7.0.0
b5c71000 b5c73000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5c7b000 b5c7d000 r-xp /usr/lib/libXdamage.so.1.1.0
b5c85000 b5c8c000 r-xp /usr/lib/libXcursor.so.1.0.2
b5c95000 b5ca5000 r-xp /lib/libresolv-2.13.so
b5ca9000 b5cab000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5cb3000 b5cb8000 r-xp /usr/lib/libffi.so.5.0.10
b5cc0000 b5cc1000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5cc9000 b5d12000 r-xp /usr/lib/libmdm.so.1.2.70
b5d1c000 b5d22000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5d2a000 b5d30000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5d38000 b5d52000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5d5a000 b5d78000 r-xp /usr/lib/libsystemd.so.0.4.0
b5d83000 b5d84000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5d8c000 b5d91000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5d99000 b5db0000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5db8000 b5dbe000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5dc7000 b5dd0000 r-xp /usr/lib/libcom-core.so.0.0.1
b5dda000 b5ddc000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5de5000 b5e3b000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5e48000 b5e9e000 r-xp /usr/lib/libfreetype.so.6.11.3
b5eaa000 b5eef000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5ef8000 b5f0b000 r-xp /usr/lib/libfribidi.so.0.3.1
b5f14000 b5f2e000 r-xp /usr/lib/libecore_con.so.1.7.99
b5f37000 b5f61000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5f6a000 b5f73000 r-xp /usr/lib/libedbus.so.1.7.99
b5f7b000 b5f8c000 r-xp /usr/lib/libecore_input.so.1.7.99
b5f94000 b5f99000 r-xp /usr/lib/libecore_file.so.1.7.99
b5fa2000 b5fc4000 r-xp /usr/lib/libecore_evas.so.1.7.99
b5fcd000 b5fe6000 r-xp /usr/lib/libeet.so.1.7.99
b5ff7000 b601f000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6020000 b6029000 r-xp /usr/lib/libXi.so.6.1.0
b6031000 b6112000 r-xp /usr/lib/libX11.so.6.3.0
b611e000 b61d6000 r-xp /usr/lib/libcairo.so.2.11200.14
b61e1000 b623f000 r-xp /usr/lib/libedje.so.1.7.99
b6249000 b6299000 r-xp /usr/lib/libecore_x.so.1.7.99
b629b000 b6304000 r-xp /lib/libm-2.13.so
b630d000 b6313000 r-xp /lib/librt-2.13.so
b631c000 b6332000 r-xp /lib/libz.so.1.2.5
b633b000 b64cd000 r-xp /usr/lib/libcrypto.so.1.0.0
b64ee000 b6535000 r-xp /usr/lib/libssl.so.1.0.0
b6541000 b656f000 r-xp /usr/lib/libidn.so.11.5.44
b6577000 b6580000 r-xp /usr/lib/libcares.so.2.1.0
b6589000 b6655000 r-xp /usr/lib/libxml2.so.2.7.8
b6663000 b6665000 r-xp /usr/lib/libiniparser.so.0
b666e000 b66a2000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b66ab000 b677e000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6789000 b67a2000 r-xp /usr/lib/libnetwork.so.0.0.0
b67aa000 b67b3000 r-xp /usr/lib/libvconf.so.0.2.45
b67bc000 b688c000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b688d000 b68ce000 r-xp /usr/lib/libeina.so.1.7.99
b68d7000 b68dc000 r-xp /usr/lib/libappcore-common.so.1.1
b68e4000 b68ea000 r-xp /usr/lib/libappcore-efl.so.1.1
b68f2000 b68f5000 r-xp /usr/lib/libbundle.so.0.1.22
b68fd000 b6903000 r-xp /usr/lib/libappsvc.so.0.1.0
b690b000 b691f000 r-xp /lib/libpthread-2.13.so
b692a000 b694d000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6955000 b6962000 r-xp /usr/lib/libaul.so.0.1.0
b696c000 b696e000 r-xp /lib/libdl-2.13.so
b6977000 b6982000 r-xp /lib/libunwind.so.8.0.1
b69af000 b69b7000 r-xp /lib/libgcc_s-4.6.so.1
b69b8000 b6adc000 r-xp /lib/libc-2.13.so
b6aea000 b6b5f000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6b69000 b6b75000 r-xp /usr/lib/libnotification.so.0.1.0
b6b7e000 b6b8d000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6b96000 b6c64000 r-xp /usr/lib/libevas.so.1.7.99
b6c8a000 b6dc6000 r-xp /usr/lib/libelementary.so.1.7.99
b6ddd000 b6dfe000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6e06000 b6e1d000 r-xp /usr/lib/libecore.so.1.7.99
b6e34000 b6e36000 r-xp /usr/lib/libdlog.so.0.0.0
b6e3e000 b6e82000 r-xp /usr/lib/libcurl.so.4.3.0
b6e8b000 b6e90000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6e98000 b6e9d000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6ea5000 b6eb5000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6ebd000 b6ec5000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6ecd000 b6ed1000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6ed9000 b6edd000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6ee6000 b6ee8000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6ef3000 b6efe000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6f08000 b6f0c000 r-xp /usr/lib/libsys-assert.so
b6f15000 b6f32000 r-xp /lib/ld-2.13.so
b6f3b000 b6f45000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b7416000 b7ca1000 rw-p [heap]
beaec000 beb0d000 rw-p [stack]
b6f3b000 b6f45000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b7416000 b7ca1000 rw-p [heap]
beaec000 beb0d000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2057)
Call Stack Count: 20
 0: set_targeted_view + 0xd5 (0xb6f422a2) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x72a2
 1: pop_from_stack_uib_vc + 0x38 (0xb6f42759) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7759
 2: uib_views_destroy_callback + 0x38 (0xb6f42715) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7715
 3: (0xb6badaf9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6bc4c39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6bb737b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6bb7ab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb5fb1cf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6d88953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6e113f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6e0ee53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6e1246b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6e127db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6d4544d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x44c (0xb68e7c61) [/usr/lib/libappcore-efl.so.1] + 0x3c61
15: ui_app_main + 0xb0 (0xb6eceed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16: uib_app_run + 0xea (0xb6f4042f) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x542f
17: main + 0x34 (0xb6f40919) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5919
18: __libc_start_main + 0x114 (0xb69cf85c) [/lib/libc.so.6] + 0x1785c
19: (0xb6f3e114) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x3114
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
 [27:Real Feed Back], focusIdx[0]
04-30 16:26:28.489+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: __onSignalHideNextPage(7063) >  Hide next page [0->0]
04-30 16:26:28.489+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: onAutoLaunchTimerStart(5026) >  auto launch is disabled
04-30 16:26:28.489+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: setBubbleColor(2479) >  [249, 249, 249, 255]
04-30 16:26:28.499+0700 I/efl-extension( 1160): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.499+0700 I/efl-extension( 2057): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.499+0700 I/efl-extension( 2057): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.509+0700 I/efl-extension( 1249): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.509+0700 I/efl-extension( 1249): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb7ff4660, elm_layout, time_stamp : 750436
04-30 16:26:28.509+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: runRotaryBackwardAnimation(3721) >  EditMode[0], focusPage[0], focusIndex[0], ItemList size[28], FocusPrev[0], FocusNext[0], FocusRecent[0], HideNextPage[0]
04-30 16:26:28.509+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: runFocusAni(3491) >  nNewFocus[0], anim[0], autoLaunch[1], FocusNext[0], FocusPrev[0], FocusRecent[1], HideNextPage[0], ItemListSize[28]
04-30 16:26:28.509+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: onAutoLaunchTimerStart(5026) >  auto launch is disabled
04-30 16:26:28.509+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: setBubbleColor(2479) >  [249, 249, 249, 255]
04-30 16:26:28.529+0700 I/efl-extension( 1160): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.539+0700 I/efl-extension( 2057): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.539+0700 I/efl-extension( 1249): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.539+0700 I/efl-extension( 1249): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb7ff4660, elm_layout, time_stamp : 750463
04-30 16:26:28.539+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: onRotary(5361) >  Ignore Rotary event because of fastmoving[1], launching[0]
04-30 16:26:28.599+0700 I/efl-extension( 1160): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.599+0700 I/efl-extension( 2057): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.609+0700 I/efl-extension( 1249): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-30 16:26:28.609+0700 I/efl-extension( 1249): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb7ff4660, elm_layout, time_stamp : 750532
04-30 16:26:28.609+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: onRotary(5361) >  Ignore Rotary event because of fastmoving[1], launching[0]
04-30 16:26:28.739+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: onAutoLaunchTimerStart(5026) >  auto launch is disabled
04-30 16:26:28.769+0700 I/CAPI_NETWORK_CONNECTION( 2057): connection.c: connection_create(453) > New handle created[0xb2602140]
04-30 16:26:28.809+0700 E/JSON PARSING( 2057): No data could be display
04-30 16:26:28.809+0700 E/JSON PARSING( 2057): �Ƨ�O��;
04-30 16:26:28.809+0700 I/CAPI_NETWORK_CONNECTION( 2057): connection.c: connection_destroy(471) > Destroy handle: 0xb2602140
04-30 16:26:29.419+0700 E/EFL     (  934): ecore_x<934> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=751351 button=1
04-30 16:26:29.549+0700 E/EFL     (  934): ecore_x<934> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=751482 button=1
04-30 16:26:29.549+0700 E/EFL     (  934): <934> e_mod_processmgr.c:499 _e_mod_processmgr_anr_ping() safety check failed: bd == NULL
04-30 16:26:29.549+0700 E/EFL     ( 1249): ecore_x<1249> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=751482 button=1
04-30 16:26:29.549+0700 W/W_HOME  ( 1249): event_manager.c: home_event_manager_allowance_get(860) > editing:0 clocklist_state:0 addviewer:0 scrolling:0 apptray-state:0 apptray-visibility:1 apptray-edit_visibility:0
04-30 16:26:29.549+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: touchPressed(1663) >  TOUCH [267, 67]
04-30 16:26:29.549+0700 E/W_HOME  ( 1249): util.c: util_is_rdu_retailmode(1530) > Cannot get the vconf for retailmode
04-30 16:26:29.579+0700 E/EFL     ( 1249): ecore_x<1249> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=751482 button=1
04-30 16:26:29.579+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: touchReleased(1976) >  TOUCH [267, 67]->[267, 67]
04-30 16:26:29.579+0700 E/APPS    ( 1249): AppsViewNecklace.cpp: __GetAppsItemByTouchIndex(6941) >  Can't Find AppsItem at [-1]
04-30 16:26:29.579+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: runFocusAni(3491) >  nNewFocus[0], anim[1], autoLaunch[1], FocusNext[0], FocusPrev[0], FocusRecent[1], HideNextPage[0], ItemListSize[28]
04-30 16:26:29.579+0700 E/APPS    ( 1249): effect.c: apps_effect_play_sound(86) >  effect_info.sound_status: [0]
04-30 16:26:29.579+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: onClickedRecentApps(3256) >  item(Recent apps) launched, open(0)
04-30 16:26:29.579+0700 W/AUL     ( 1249): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.samsung.w-taskmanager)
04-30 16:26:29.579+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 0
04-30 16:26:29.579+0700 W/AUL_AMD ( 1020): amd_launch.c: _start_app(1782) > caller pid : 1249
04-30 16:26:29.579+0700 I/AUL_AMD ( 1020): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
04-30 16:26:29.599+0700 E/RESOURCED( 1155): vmpressure-lowmem-handler.c: lowmem_handle_request(1124) > Done: killed 0 processes reclaimed: 0 remaining: 0 shortfall: 0 status: 1
04-30 16:26:29.619+0700 W/AUL_AMD ( 1020): amd_launch.c: _start_app(2218) > pad pid(-5)
04-30 16:26:29.619+0700 W/AUL_PAD ( 1905): launchpad.c: __launchpad_main_loop(611) > Launch on type-based process-pool
04-30 16:26:29.619+0700 W/AUL_PAD ( 1905): launchpad.c: __send_result_to_caller(272) > Check app launching
04-30 16:26:29.629+0700 W/AUL     ( 2289): smack_util.c: set_app_smack_label(242) > thr_cnt: 1, signal count: 1,  try count 1, launchpad type: 1
04-30 16:26:29.629+0700 W/AUL_PAD ( 2289): launchpad_loader.c: __candidate_process_prepare_exec(259) > [candidate] before __set_access
04-30 16:26:29.629+0700 W/AUL_PAD ( 2289): launchpad_loader.c: __candidate_process_prepare_exec(264) > [candidate] after __set_access
04-30 16:26:29.629+0700 W/AUL_PAD ( 2289): launchpad_loader.c: __candidate_process_launchpad_main_loop(414) > update argument
04-30 16:26:29.629+0700 W/AUL_PAD ( 2289): launchpad_loader.c: main(680) > [candidate] dlopen(com.samsung.w-taskmanager)
04-30 16:26:29.639+0700 E/EFL     (  934): elementary<934> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8ceab30 in function: elm_layout_edje_get, of type: 'edje' when expecting type: 'elm_layout'
04-30 16:26:29.639+0700 E/UXT     (  934): uxt_theme_object.c: uxt_theme_object_set_changeable_ui_enabled(41) > failed to get edje from parent object.
04-30 16:26:29.649+0700 E/EFL     (  934): ecore_evas<934> ecore_evas_extn.c:2204 ecore_evas_extn_plug_connect() Extn plug failed to connect:ipctype=0, svcname=elm_indicator_portrait, svcnum=0, svcsys=0
04-30 16:26:29.669+0700 W/AUL_PAD ( 2289): launchpad_loader.c: main(690) > [candidate] dlsym
04-30 16:26:29.669+0700 W/AUL_PAD ( 2289): launchpad_loader.c: main(694) > [candidate] dl_main(com.samsung.w-taskmanager)
04-30 16:26:29.669+0700 I/CAPI_APPFW_APPLICATION( 2289): app_main.c: app_efl_main(129) > app_efl_main
04-30 16:26:29.679+0700 I/CAPI_APPFW_APPLICATION( 2289): app_main.c: app_appcore_create(152) > app_appcore_create
04-30 16:26:29.719+0700 W/AUL     ( 1020): app_signal.c: aul_send_app_launch_request_signal(521) > aul_send_app_launch_request_signal app(com.samsung.w-taskmanager) pid(2289) type(uiapp) bg(0)
04-30 16:26:29.719+0700 W/AUL_AMD ( 1020): amd_status.c: __socket_monitor_cb(1277) > (2289) was created
04-30 16:26:29.719+0700 E/AUL     ( 1020): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-30 16:26:29.719+0700 W/AUL     ( 1249): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2289)
04-30 16:26:29.719+0700 W/W_HOME  ( 1249): util.c: apps_util_launch_main_operation(785) > Launch app:[Recent apps] ret:[0]
04-30 16:26:29.719+0700 W/STARTER ( 1159): pkg-monitor.c: _app_mgr_status_cb(400) > [_app_mgr_status_cb:400] Launch request [2289]
04-30 16:26:29.719+0700 W/W_HOME  ( 1249): util.c: send_launch_appId(620) > launch appid[com.samsung.w-taskmanager]
04-30 16:26:29.729+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 16:26:29.729+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 16:26:29.729+0700 I/MESSAGE_PORT( 1008): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 16:26:29.729+0700 I/MESSAGE_PORT( 1008): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-30 16:26:29.729+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-30 16:26:29.729+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 16:26:29.729+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.app-shortcut-widget:Apptray.Message.Launch.AppId]
04-30 16:26:29.729+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: IsPreloaded(395) > _MessagePortService::IsPreloaded
04-30 16:26:29.819+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 16:26:29.819+0700 E/W_HOME  ( 1249): util.c: send_launch_appId(636) > There is no remote message port
04-30 16:26:29.969+0700 I/APP_CORE( 2289): appcore-efl.c: __do_app(453) > [APP 2289] Event: RESET State: CREATED
04-30 16:26:29.969+0700 I/CAPI_APPFW_APPLICATION( 2289): app_main.c: app_appcore_reset(245) > app_appcore_reset
04-30 16:26:30.089+0700 I/efl-extension( 2289): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-30 16:26:30.089+0700 I/efl-extension( 2289): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-30 16:26:30.089+0700 I/efl-extension( 2289): efl_extension_rotary.c: eext_rotary_event_handler_add(77) > init_count: 0
04-30 16:26:30.089+0700 I/efl-extension( 2289): efl_extension_rotary.c: _init_Xi2_system(314) > In
04-30 16:26:30.269+0700 I/efl-extension( 2289): efl_extension_rotary.c: _init_Xi2_system(375) > Done
04-30 16:26:30.269+0700 I/efl-extension( 2289): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8473e78, elm_image, _activated_obj : 0x0, activated : 1
04-30 16:26:30.309+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 16:26:30.309+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
04-30 16:26:30.309+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-30 16:26:30.309+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 16:26:30.309+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
04-30 16:26:30.309+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-30 16:26:30.319+0700 E/W_TASKMANAGER( 2289): util_wc1.c: close_button_disabled_set(375) > [close_button_disabled_set:375] (ad->ly_main == NULL) -> close_button_disabled_set() return
04-30 16:26:30.369+0700 E/W_TASKMANAGER( 2289): task.c: _app_list_cb(499) > [_app_list_cb:499] pkgmgrinfo_appinfo_get_label(com.samsung.skmsa) failed(0)
04-30 16:26:30.369+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-home)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (health-data-service)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.techgraphy.DigitalTick)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.weather-widget)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.shealth.widget.pedometer)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.remote-sensor-service)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.wusvc)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.shealth-service)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-music-player.music-control-service)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.call.consumer)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.watchface-service)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.message.consumer)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.weip.consumer)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.wnotiboard-popup)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.app-shortcut-widget)!!
04-30 16:26:30.379+0700 E/W_TASKMANAGER( 2289): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-taskmanager)!!
04-30 16:26:30.379+0700 E/RUA     ( 2289): rua.c: rua_history_load_db(278) > rua_history_load_db ok. nrows : 19, ncols : 5
04-30 16:26:30.399+0700 E/EFL     ( 2289): evas_main<2289> evas_stack.c:158 evas_object_stack_above() BITCH! evas_object_stack_above(), 0xb8515b28 not inside same smart as 0xb84aae08!
04-30 16:26:30.409+0700 E/EFL     ( 2289): evas_main<2289> evas_stack.c:158 evas_object_stack_above() BITCH! evas_object_stack_above(), 0xb84c0638 not inside same smart as 0xb8517e68!
04-30 16:26:30.409+0700 E/EFL     ( 2289): elementary<2289> elm_layout.c:1021 _elm_layout_smart_content_set() could not swallow 0xb8485650 into part 'elm.swallow.event.0'
04-30 16:26:30.409+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] mx(241), my(0), minx(0), miny(0), px(0), py(0)
04-30 16:26:30.409+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] cw(601), ch(360), pw(360), ph(360)
04-30 16:26:30.409+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-30 16:26:30.439+0700 I/APP_CORE( 2289): appcore-efl.c: __do_app(522) > Legacy lifecycle: 0
04-30 16:26:30.439+0700 I/APP_CORE( 2289): appcore-efl.c: __do_app(524) > [APP 2289] Initial Launching, call the resume_cb
04-30 16:26:30.439+0700 I/CAPI_APPFW_APPLICATION( 2289): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-30 16:26:30.469+0700 W/W_HOME  ( 1249): event_manager.c: _ecore_x_message_cb(421) > state: 0 -> 1
04-30 16:26:30.469+0700 W/W_HOME  ( 1249): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 16:26:30.469+0700 W/W_HOME  ( 1249): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 16:26:30.469+0700 W/W_HOME  ( 1249): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 16:26:30.469+0700 W/W_HOME  ( 1249): win.c: win_back_gesture_disable_set(170) > disable back gesture
04-30 16:26:30.469+0700 W/W_HOME  ( 1249): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 0
04-30 16:26:30.479+0700 W/W_INDICATOR( 1160): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 16:26:30.479+0700 W/W_INDICATOR( 1160): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 16:26:30.509+0700 W/APP_CORE( 2289): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:4000003
04-30 16:26:30.589+0700 E/AUL     ( 1020): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-30 16:26:30.589+0700 W/W_HOME  ( 1249): event_manager.c: _window_visibility_cb(460) > Window [0x1E00003] is now visible(1)
04-30 16:26:30.589+0700 W/W_HOME  ( 1249): event_manager.c: _window_visibility_cb(470) > state: 1 -> 0
04-30 16:26:30.589+0700 W/W_HOME  ( 1249): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 16:26:30.589+0700 W/W_HOME  ( 1249): event_manager.c: _state_control(176) > control:6, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 16:26:30.589+0700 W/W_HOME  ( 1249): main.c: _window_visibility_cb(996) > Window [0x1E00003] is now visible(1)
04-30 16:26:30.599+0700 I/APP_CORE( 1249): appcore-efl.c: __do_app(453) > [APP 1249] Event: PAUSE State: RUNNING
04-30 16:26:30.599+0700 I/CAPI_APPFW_APPLICATION( 1249): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-30 16:26:30.599+0700 W/W_HOME  ( 1249): main.c: _appcore_pause_cb(489) > appcore pause
04-30 16:26:30.599+0700 W/W_HOME  ( 1249): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
04-30 16:26:30.599+0700 W/W_HOME  ( 1249): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 16:26:30.599+0700 W/W_HOME  ( 1249): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 16:26:30.599+0700 W/W_HOME  ( 1249): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 16:26:30.599+0700 W/W_HOME  ( 1249): rotary.c: rotary_deattach(156) > rotary_deattach:0xb7ff4660
04-30 16:26:30.599+0700 I/efl-extension( 1249): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 16:26:30.599+0700 I/efl-extension( 1249): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7ff4660, elm_layout, func : 0xb6eb8455
04-30 16:26:30.599+0700 I/efl-extension( 1249): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-30 16:26:30.599+0700 I/efl-extension( 1249): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-30 16:26:30.599+0700 I/efl-extension( 1249): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 16:26:30.599+0700 I/efl-extension( 1249): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7f35dd0, elm_box, _activated_obj : 0xb7ff4660, activated : 1
04-30 16:26:30.599+0700 I/efl-extension( 1249): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 E/APPS    ( 1249): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 16:26:30.599+0700 W/W_HOME  ( 1249): win.c: win_back_gesture_disable_set(170) > disable back gesture
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 16:26:30.599+0700 W/W_INDICATOR( 1160): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 16:26:30.599+0700 W/W_INDICATOR( 1160): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-30 16:26:30.599+0700 I/MESSAGE_PORT( 1008): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 16:26:30.599+0700 I/wnotib  ( 1249): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 1
04-30 16:26:30.599+0700 E/wnotib  ( 1249): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-30 16:26:30.599+0700 W/wnotib  ( 1249): w-notification-board-noti-manager.c: wnb_nm_postpone_updating_job(997) > Set is_notiboard_update_postponed to true with is_for_VI 0, notiboard panel creation count [1], notiboard card appending count [1].
04-30 16:26:30.609+0700 W/STARTER ( 1159): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1249] goes to (4)
04-30 16:26:30.609+0700 E/STARTER ( 1159): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1249)'s state(4)
04-30 16:26:30.609+0700 W/AUL     ( 1020): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1249) status(bg) type(uiapp)
04-30 16:26:30.609+0700 W/STARTER ( 1159): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2289] goes to (3)
04-30 16:26:30.609+0700 W/AUL     ( 1020): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-taskmanager) pid(2289) status(fg) type(uiapp)
04-30 16:26:30.619+0700 W/MUSIC_CONTROL_SERVICE( 1807): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1807]   [com.samsung.w-home]register msg port [false][0m
04-30 16:26:30.619+0700 I/APP_CORE( 2289): appcore-efl.c: __do_app(453) > [APP 2289] Event: RESUME State: RUNNING
04-30 16:26:30.619+0700 I/GATE    ( 2289): <GATE-M>APP_FULLY_LOADED_w-taskmanager</GATE-M>
04-30 16:26:30.659+0700 W/APPS    ( 1249): AppsViewNecklace.cpp: onPausedIdlerCb(5178) >  elm_cache_all_flush
04-30 16:26:30.799+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 16:26:30.819+0700 W/AUL_AMD ( 1020): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2289
04-30 16:26:30.819+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 16:26:30.829+0700 W/AUL_AMD ( 1020): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2289
04-30 16:26:30.829+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 16:26:30.829+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 16:26:31.069+0700 E/EFL     ( 2289): ecore_x<2289> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=753007 button=1
04-30 16:26:31.099+0700 I/APP_CORE( 1249): appcore-efl.c: __do_app(453) > [APP 1249] Event: MEM_FLUSH State: PAUSED
04-30 16:26:31.139+0700 E/EFL     ( 2289): ecore_x<2289> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=753072 button=1
04-30 16:26:31.139+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 16:26:31.149+0700 W/AUL_AMD ( 1020): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2057
04-30 16:26:31.149+0700 W/AUL     ( 2289): launch.c: app_request_to_launchpad(284) > request cmd(27) to(2057)
04-30 16:26:31.149+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 27
04-30 16:26:31.149+0700 W/AUL     ( 1020): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2057) type(uiapp)
04-30 16:26:31.159+0700 W/AUL     ( 2289): launch.c: app_request_to_launchpad(298) > request cmd(27) result(0)
04-30 16:26:31.159+0700 I/APP_CORE( 2057): appcore-efl.c: __do_app(453) > [APP 2057] Event: TERMINATE State: PAUSED
04-30 16:26:31.159+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 22
04-30 16:26:31.159+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(999) > app status : 4
04-30 16:26:31.159+0700 W/APP_CORE( 2057): appcore-efl.c: appcore_efl_main(1788) > power off : 0
04-30 16:26:31.159+0700 W/APP_CORE( 2057): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3000003] -> redirected win[60020a] for com.toyota.realtimefeedback[2057]
04-30 16:26:31.249+0700 I/APP_CORE( 2057): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-30 16:26:31.249+0700 I/CAPI_APPFW_APPLICATION( 2057): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
04-30 16:26:31.279+0700 I/efl-extension( 2057): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 16:26:31.279+0700 I/efl-extension( 2057): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb76f8618 is freed
04-30 16:26:31.279+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb76de598 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 16:26:31.279+0700 I/efl-extension( 2057): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb76defe0, obj: 0xb76defe0
04-30 16:26:31.279+0700 I/efl-extension( 2057): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 16:26:31.289+0700 E/EFL     ( 2057): elementary<2057> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 16:26:31.289+0700 E/EFL     ( 2057): elementary<2057> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb76defe0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 16:26:31.289+0700 E/EFL     ( 2057): elementary<2057> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb76defe0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 16:26:31.289+0700 E/EFL     ( 2057): elementary<2057> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb76defe0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 16:26:31.289+0700 E/EFL     ( 2057): elementary<2057> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb76defe0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 16:26:31.289+0700 I/efl-extension( 2057): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 16:26:31.289+0700 I/efl-extension( 2057): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb770eb90 is freed
04-30 16:26:31.289+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 16:26:31.289+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb76defe0, elm_genlist, func : 0xb6deaea1
04-30 16:26:31.289+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 16:26:31.289+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb76de598 in function: elm_grid_clear, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 16:26:31.289+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb76de598 in function: elm_grid_size_set, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 16:26:31.289+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb76dea00, , _activated_obj : 0xb781bf60, activated : 1
04-30 16:26:31.289+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-30 16:26:31.289+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7713eb8 in function: elm_label_line_wrap_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7713eb8 in function: elm_label_wrap_width_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7713eb8 in function: elm_label_ellipsis_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb76de598 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb76de598 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb76de598 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb76de598 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 16:26:31.299+0700 I/efl-extension( 2057): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb77c5eb8, obj: 0xb77c5eb8
04-30 16:26:31.299+0700 I/efl-extension( 2057): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb77c5eb8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb77c5eb8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb77c5eb8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 16:26:31.299+0700 E/EFL     ( 2057): elementary<2057> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb77c5eb8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 16:26:31.299+0700 I/efl-extension( 2057): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 16:26:31.299+0700 I/efl-extension( 2057): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7806bc8 is freed
04-30 16:26:31.299+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 16:26:31.299+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb77c5eb8, elm_genlist, func : 0xb6deaea1
04-30 16:26:31.299+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 16:26:31.309+0700 I/APP_CORE( 2057): appcore-efl.c: __after_loop(1243) > [APP 2057] After terminate() callback
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb78f71f0 is freed
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb781a200, elm_scroller, func : 0xb6dee379
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb781bf60, elm_image, func : 0xb6dee379
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 16:26:31.309+0700 I/efl-extension( 2057): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb781a200 : elm_scroller] rotary callabck is deleted
04-30 16:26:31.659+0700 E/EFL     ( 2289): elementary<2289> elm_layout.c:1021 _elm_layout_smart_content_set() could not swallow 0xb8485650 into part 'elm.swallow.event.0'
04-30 16:26:31.659+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 16:26:31.659+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
04-30 16:26:31.659+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb8455e58 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-30 16:26:31.869+0700 I/efl-extension( 2300): efl_extension.c: eext_mod_init(40) > Init
04-30 16:26:31.949+0700 I/UXT     ( 2300): Uxt_ObjectManager.cpp: OnInitialized(753) > Initialized.
04-30 16:26:32.049+0700 I/AUL_PAD ( 2300): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-30 16:26:32.069+0700 E/EFL     ( 2289): ecore_x<2289> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=754003 button=1
04-30 16:26:32.069+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] mouse move
04-30 16:26:32.079+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] mouse move
04-30 16:26:32.079+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] hold(0), freeze(0)
04-30 16:26:32.089+0700 I/Adreno-EGL( 2300): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-30 16:26:32.089+0700 I/Adreno-EGL( 2300): OpenGL ES Shader Compiler Version: E031.24.00.16
04-30 16:26:32.089+0700 I/Adreno-EGL( 2300): Build Date: 09/02/15 Wed
04-30 16:26:32.089+0700 I/Adreno-EGL( 2300): Local Branch: 
04-30 16:26:32.089+0700 I/Adreno-EGL( 2300): Remote Branch: 
04-30 16:26:32.089+0700 I/Adreno-EGL( 2300): Local Patches: 
04-30 16:26:32.089+0700 I/Adreno-EGL( 2300): Reconstruct Branch: 
04-30 16:26:32.109+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] mouse move
04-30 16:26:32.109+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] hold(0), freeze(0)
04-30 16:26:32.109+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] mouse move
04-30 16:26:32.109+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] hold(0), freeze(0)
04-30 16:26:32.149+0700 E/EFL     (  934): ecore_x<934> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x4000003 time=753243
04-30 16:26:32.149+0700 E/EFL     ( 2289): ecore_x<2289> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=753243
04-30 16:26:32.149+0700 E/EFL     (  934): ecore_x<934> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=753243
04-30 16:26:32.189+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] mouse move
04-30 16:26:32.189+0700 E/EFL     ( 2289): elementary<2289> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8455e58 : elm_scroller] hold(0), freeze(0)
04-30 16:26:32.199+0700 E/EFL     ( 2289): ecore_x<2289> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=754133 button=1
04-30 16:26:32.199+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 16:26:32.219+0700 W/AUL_AMD ( 1020): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2038
04-30 16:26:32.219+0700 W/AUL     ( 2289): launch.c: app_request_to_launchpad(284) > request cmd(27) to(2038)
04-30 16:26:32.219+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 27
04-30 16:26:32.219+0700 W/AUL     ( 1020): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.samsung.weather) pid(2038) type(uiapp)
04-30 16:26:32.219+0700 I/APP_CORE( 2038): appcore-efl.c: __do_app(453) > [APP 2038] Event: TERMINATE State: PAUSED
04-30 16:26:32.219+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 22
04-30 16:26:32.219+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(999) > app status : 4
04-30 16:26:32.219+0700 W/AUL     ( 2289): launch.c: app_request_to_launchpad(298) > request cmd(27) result(0)
04-30 16:26:32.229+0700 W/APP_CORE( 2038): appcore-efl.c: appcore_efl_main(1788) > power off : 0
04-30 16:26:32.229+0700 W/APP_CORE( 2038): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3600003] -> redirected win[600417] for com.samsung.weather[2038]
04-30 16:26:32.249+0700 W/AUL     ( 2308): daemon-manager-release-agent.c: main(12) > release agent : [2:/com.toyota.realtimefeedback]
04-30 16:26:32.249+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(669) > __request_handler: 23
04-30 16:26:32.249+0700 W/AUL_AMD ( 1020): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-30 16:26:32.249+0700 W/AUL_AMD ( 1020): amd_request.c: __request_handler(1032) > pkg_status: installed, dead pid: 2057
04-30 16:26:32.249+0700 W/AUL_AMD ( 1020): amd_request.c: __send_app_termination_signal(528) > send dead signal done
04-30 16:26:32.259+0700 E/AUL     ( 2308): daemon-manager-release-agent.c: main(22) > APP_RELEASED request sending error
04-30 16:26:32.319+0700 E/APP_CORE( 2038): appcore.c: __del_vconf(453) > [FAILED]vconfkey_ignore_key_changed
04-30 16:26:32.319+0700 I/APP_CORE( 2038): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-30 16:26:32.319+0700 I/CAPI_APPFW_APPLICATION( 2038): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
04-30 16:26:32.319+0700 E/weather-app( 2038): AppMain.cpp: AppTerminate(671) > [0;40;31mAppTerminate start[0;m
04-30 16:26:32.319+0700 E/UXT     ( 2038): uxt_theme.c: uxt_theme_delete_changed_callback(541) > failed to get the callback from list
04-30 16:26:32.319+0700 E/weather-common( 2038): VconfUtil.cpp: RemoveVconfEventListener(224) > [0;40;31mFailed vconf_ignore_key_changed [key : memory/wms/wmanager_connected, ret : -1][0;m
04-30 16:26:32.319+0700 E/weather-app( 2038): AppMain.cpp: AppTerminate(740) > [0;40;31mAppTerminate end[0;m
04-30 16:26:32.319+0700 I/APP_CORE( 2038): appcore-efl.c: __after_loop(1243) > [APP 2038] After terminate() callback
04-30 16:26:32.329+0700 W/CRASH_MANAGER( 2306): worker.c: worker_job(1205) > 1102057726561152508039
