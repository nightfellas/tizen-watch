S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2383
Date: 2018-04-27 11:07:36+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2383, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb591c443, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb4932618, r7   = 0xbee321d0
r8   = 0x000186a0, r9   = 0xb7e830d0
r10  = 0xb6cf2b10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbee32150
lr   = 0xb591c443, pc   = 0xb6aa86f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      7180 KB
Buffers:      6448 KB
Cached:     112228 KB
VmPeak:      90740 KB
VmSize:      88576 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       22956 KB
VmRSS:       22956 KB
VmData:      28424 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24500 KB
VmPTE:          58 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2383 TID = 2383
2383 2431 2434 

Maps Information
b1eb0000 b1eb4000 r-xp /usr/lib/libogg.so.0.7.1
b1ebc000 b1ede000 r-xp /usr/lib/libvorbis.so.0.4.3
b1ee6000 b1f2d000 r-xp /usr/lib/libsndfile.so.1.0.26
b1f39000 b1f82000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1f8b000 b1f90000 r-xp /usr/lib/libjson.so.0.0.1
b3831000 b3937000 r-xp /usr/lib/libicuuc.so.57.1
b394d000 b3ad5000 r-xp /usr/lib/libicui18n.so.57.1
b3ae5000 b3af2000 r-xp /usr/lib/libail.so.0.1.0
b3afb000 b3afe000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3b06000 b3b3e000 r-xp /usr/lib/libpulse.so.0.16.2
b3b3f000 b3b42000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3b4a000 b3bab000 r-xp /usr/lib/libasound.so.2.0.0
b3bb5000 b3bce000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3bd7000 b3bdb000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3be3000 b3bee000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3bfb000 b3c13000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3c24000 b3c2b000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3c33000 b3c3e000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3c46000 b3c47000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3c7e000 b3d05000 rw-s anon_inode:dmabuf
b3d05000 b3d8c000 rw-s anon_inode:dmabuf
b3e67000 b3eee000 rw-s anon_inode:dmabuf
b3eee000 b3f75000 rw-s anon_inode:dmabuf
b3f76000 b4775000 rw-p [stack:2434]
b4a00000 b4a04000 r-xp /usr/lib/libmmfsession.so.0.0.0
b4a0d000 b4a0f000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b4a17000 b4a18000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4a20000 b4a28000 r-xp /usr/lib/libfeedback.so.0.1.4
b4a6f000 b4a70000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b77000 b5376000 rw-p [stack:2431]
b5376000 b5378000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b5380000 b5397000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b53a4000 b53a6000 r-xp /usr/lib/libdri2.so.0.0.0
b53ae000 b53b9000 r-xp /usr/lib/libtbm.so.1.0.0
b53c1000 b53c9000 r-xp /usr/lib/libdrm.so.2.4.0
b53d1000 b53d3000 r-xp /usr/lib/libgenlock.so
b53db000 b53e0000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b53e8000 b53f3000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b55fc000 b56c6000 r-xp /usr/lib/libCOREGL.so.4.0
b56d7000 b56e7000 r-xp /usr/lib/libmdm-common.so.1.1.25
b56ef000 b56f5000 r-xp /usr/lib/libxcb-render.so.0.0.0
b56fd000 b56fe000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5707000 b570a000 r-xp /usr/lib/libEGL.so.1.4
b5712000 b5720000 r-xp /usr/lib/libGLESv2.so.2.0
b5729000 b5772000 r-xp /usr/lib/libmdm.so.1.2.70
b577b000 b5781000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5789000 b5792000 r-xp /usr/lib/libcom-core.so.0.0.1
b579b000 b5853000 r-xp /usr/lib/libcairo.so.2.11200.14
b585e000 b5877000 r-xp /usr/lib/libnetwork.so.0.0.0
b587f000 b588b000 r-xp /usr/lib/libnotification.so.0.1.0
b5894000 b58a3000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b58ac000 b58cd000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58d5000 b58da000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58e2000 b58e7000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b58ef000 b58ff000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5907000 b590f000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5917000 b5920000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5ac5000 b5acf000 r-xp /lib/libnss_files-2.13.so
b5ad8000 b5ba7000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5bbd000 b5be1000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5bea000 b5bf0000 r-xp /usr/lib/libappsvc.so.0.1.0
b5bf8000 b5bfc000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5c09000 b5c14000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5c1c000 b5c1e000 r-xp /usr/lib/libiniparser.so.0
b5c27000 b5c2c000 r-xp /usr/lib/libappcore-common.so.1.1
b5c34000 b5c36000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c3f000 b5c43000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c50000 b5c52000 r-xp /usr/lib/libXau.so.6.0.0
b5c5a000 b5c61000 r-xp /lib/libcrypt-2.13.so
b5c91000 b5c93000 r-xp /usr/lib/libiri.so
b5c9c000 b5e2e000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e4f000 b5e96000 r-xp /usr/lib/libssl.so.1.0.0
b5ea2000 b5ed0000 r-xp /usr/lib/libidn.so.11.5.44
b5ed8000 b5ee1000 r-xp /usr/lib/libcares.so.2.1.0
b5eeb000 b5efe000 r-xp /usr/lib/libxcb.so.1.1.0
b5f07000 b5f0a000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5f12000 b5f14000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5f1d000 b5fe9000 r-xp /usr/lib/libxml2.so.2.7.8
b5ff6000 b5ff8000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b6001000 b6006000 r-xp /usr/lib/libffi.so.5.0.10
b600e000 b600f000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b6017000 b601a000 r-xp /lib/libattr.so.1.1.0
b6022000 b60b6000 r-xp /usr/lib/libstdc++.so.6.0.16
b60c9000 b60e6000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b60f0000 b6108000 r-xp /usr/lib/libpng12.so.0.50.0
b6110000 b6126000 r-xp /lib/libexpat.so.1.6.0
b6130000 b6174000 r-xp /usr/lib/libcurl.so.4.3.0
b617d000 b6187000 r-xp /usr/lib/libXext.so.6.4.0
b6191000 b6195000 r-xp /usr/lib/libXtst.so.6.1.0
b619d000 b61a3000 r-xp /usr/lib/libXrender.so.1.3.0
b61ab000 b61b1000 r-xp /usr/lib/libXrandr.so.2.2.0
b61b9000 b61ba000 r-xp /usr/lib/libXinerama.so.1.0.0
b61c3000 b61cc000 r-xp /usr/lib/libXi.so.6.1.0
b61d4000 b61d7000 r-xp /usr/lib/libXfixes.so.3.1.0
b61e0000 b61e2000 r-xp /usr/lib/libXgesture.so.7.0.0
b61ea000 b61ec000 r-xp /usr/lib/libXcomposite.so.1.0.0
b61f4000 b61f6000 r-xp /usr/lib/libXdamage.so.1.1.0
b61fe000 b6205000 r-xp /usr/lib/libXcursor.so.1.0.2
b620d000 b6210000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6219000 b621d000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6226000 b622b000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6234000 b6315000 r-xp /usr/lib/libX11.so.6.3.0
b6320000 b6343000 r-xp /usr/lib/libjpeg.so.8.0.2
b635b000 b6371000 r-xp /lib/libz.so.1.2.5
b637a000 b637c000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6384000 b63f9000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6403000 b641d000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6425000 b6459000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6462000 b6535000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6541000 b6551000 r-xp /lib/libresolv-2.13.so
b6555000 b656d000 r-xp /usr/lib/liblzma.so.5.0.3
b6575000 b6578000 r-xp /lib/libcap.so.2.21
b6580000 b65af000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b65b7000 b65b8000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b65c1000 b65c7000 r-xp /usr/lib/libecore_imf.so.1.7.99
b65cf000 b65e6000 r-xp /usr/lib/liblua-5.1.so
b65ef000 b65f6000 r-xp /usr/lib/libembryo.so.1.7.99
b65fe000 b6604000 r-xp /lib/librt-2.13.so
b660d000 b6663000 r-xp /usr/lib/libpixman-1.so.0.28.2
b6671000 b66c7000 r-xp /usr/lib/libfreetype.so.6.11.3
b66d3000 b66fb000 r-xp /usr/lib/libfontconfig.so.1.8.0
b66fc000 b6741000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b674a000 b675d000 r-xp /usr/lib/libfribidi.so.0.3.1
b6765000 b677f000 r-xp /usr/lib/libecore_con.so.1.7.99
b6789000 b6792000 r-xp /usr/lib/libedbus.so.1.7.99
b679a000 b67ea000 r-xp /usr/lib/libecore_x.so.1.7.99
b67ec000 b67f5000 r-xp /usr/lib/libvconf.so.0.2.45
b67fd000 b680e000 r-xp /usr/lib/libecore_input.so.1.7.99
b6816000 b681b000 r-xp /usr/lib/libecore_file.so.1.7.99
b6823000 b6845000 r-xp /usr/lib/libecore_evas.so.1.7.99
b684e000 b688f000 r-xp /usr/lib/libeina.so.1.7.99
b6898000 b68b1000 r-xp /usr/lib/libeet.so.1.7.99
b68c2000 b692b000 r-xp /lib/libm-2.13.so
b6934000 b693a000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6943000 b6944000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b694c000 b696f000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6977000 b697c000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6984000 b69ae000 r-xp /usr/lib/libdbus-1.so.3.8.12
b69b7000 b69ce000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69d6000 b69e1000 r-xp /lib/libunwind.so.8.0.1
b6a0e000 b6a2c000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a36000 b6b5a000 r-xp /lib/libc-2.13.so
b6b68000 b6b70000 r-xp /lib/libgcc_s-4.6.so.1
b6b71000 b6b75000 r-xp /usr/lib/libsmack.so.1.0.0
b6b7e000 b6b84000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b8c000 b6c5c000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c5d000 b6cbb000 r-xp /usr/lib/libedje.so.1.7.99
b6cc5000 b6cdc000 r-xp /usr/lib/libecore.so.1.7.99
b6cf3000 b6dc1000 r-xp /usr/lib/libevas.so.1.7.99
b6de7000 b6f23000 r-xp /usr/lib/libelementary.so.1.7.99
b6f3a000 b6f4e000 r-xp /lib/libpthread-2.13.so
b6f59000 b6f5b000 r-xp /usr/lib/libdlog.so.0.0.0
b6f63000 b6f66000 r-xp /usr/lib/libbundle.so.0.1.22
b6f6e000 b6f70000 r-xp /lib/libdl-2.13.so
b6f79000 b6f86000 r-xp /usr/lib/libaul.so.0.1.0
b6f98000 b6f9e000 r-xp /usr/lib/libappcore-efl.so.1.1
b6fa7000 b6fab000 r-xp /usr/lib/libsys-assert.so
b6fb4000 b6fd1000 r-xp /lib/ld-2.13.so
b6fda000 b6fdf000 r-xp /usr/bin/launchpad-loader
b7e4a000 b808d000 rw-p [heap]
bee12000 bee33000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2383)
Call Stack Count: 1
 0: realloc + 0x4c (0xb6aa86f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main

04-27 11:07:29.229+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xac491f58 : elm_genlist] mouse move
04-27 11:07:29.229+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xac491f58 : elm_genlist] hold(0), freeze(0)
04-27 11:07:29.269+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xac491f58 : elm_genlist] mouse move
04-27 11:07:29.269+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xac491f58 : elm_genlist] hold(0), freeze(0)
04-27 11:07:29.289+0700 E/EFL     ( 1251): ecore_x<1251> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=162034 button=1
04-27 11:07:29.289+0700 W/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_inline_action_btn_click_cb(4770) > ACTION BUTTON IS CLICKED~~~~~~~~~~
04-27 11:07:29.289+0700 I/wnotib  ( 1251): w-notification-board-action.c: _wnb_action_open_app_cb(3281) > Action clicked for 114, -1016
04-27 11:07:29.289+0700 W/AUL     ( 1251): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.toyota.realtimefeedback)
04-27 11:07:29.289+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 0
04-27 11:07:29.289+0700 W/AUL_AMD (  929): amd_launch.c: _start_app(1782) > caller pid : 1251
04-27 11:07:29.299+0700 I/AUL_AMD (  929): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
04-27 11:07:29.309+0700 E/RESOURCED( 1132): block.c: block_prelaunch_state(138) > insert data com.toyota.realtimefeedback, table num : 2
04-27 11:07:29.319+0700 W/AUL_AMD (  929): amd_launch.c: _start_app(2218) > pad pid(-5)
04-27 11:07:29.319+0700 W/AUL_PAD ( 1876): launchpad.c: __launchpad_main_loop(611) > Launch on type-based process-pool
04-27 11:07:29.319+0700 W/AUL_PAD ( 1876): launchpad.c: __send_result_to_caller(272) > Check app launching
04-27 11:07:29.319+0700 W/AUL_PAD ( 2383): launchpad_loader.c: __candidate_process_prepare_exec(259) > [candidate] before __set_access
04-27 11:07:29.319+0700 W/AUL_PAD ( 2383): launchpad_loader.c: __candidate_process_prepare_exec(264) > [candidate] after __set_access
04-27 11:07:29.329+0700 W/AUL_PAD ( 2383): launchpad_loader.c: __candidate_process_launchpad_main_loop(414) > update argument
04-27 11:07:29.329+0700 W/AUL_PAD ( 2383): launchpad_loader.c: main(680) > [candidate] dlopen(com.toyota.realtimefeedback)
04-27 11:07:29.359+0700 I/efl-extension( 2383): efl_extension.c: eext_mod_init(40) > Init
04-27 11:07:29.379+0700 W/AUL_PAD ( 2383): launchpad_loader.c: main(690) > [candidate] dlsym
04-27 11:07:29.379+0700 W/AUL_PAD ( 2383): launchpad_loader.c: main(694) > [candidate] dl_main(com.toyota.realtimefeedback)
04-27 11:07:29.389+0700 I/CAPI_APPFW_APPLICATION( 2383): app_main.c: ui_app_main(704) > app_efl_main
04-27 11:07:29.409+0700 I/CAPI_APPFW_APPLICATION( 2383): app_main.c: _ui_app_appcore_create(563) > app_appcore_create
04-27 11:07:29.419+0700 W/AUL     (  929): app_signal.c: aul_send_app_launch_request_signal(521) > aul_send_app_launch_request_signal app(com.toyota.realtimefeedback) pid(2383) type(uiapp) bg(0)
04-27 11:07:29.419+0700 W/AUL_AMD (  929): amd_status.c: __socket_monitor_cb(1277) > (2383) was created
04-27 11:07:29.419+0700 E/AUL     (  929): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 11:07:29.419+0700 W/AUL     ( 1251): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2383)
04-27 11:07:29.419+0700 I/wnotib  ( 1251): w-notification-board-action.c: _wnb_action_trigger_action_timer(5096) > Remove previous action timer for 1140, -1016.
04-27 11:07:29.419+0700 W/wnotib  ( 1251): w-notification-board-common.c: wnb_common_perform_auto_deletion(3824) > auto_remove is set
04-27 11:07:29.419+0700 I/wnotib  ( 1251): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 21, ret: -3, request_id: 0
04-27 11:07:29.419+0700 I/wnotib  ( 1251): w-notification-board-action.c: _wnb_action_open_app_cb(3326) > Finish action successfully.
04-27 11:07:29.429+0700 I/wnotib  ( 1251): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 24, ret: -3, request_id: 0
04-27 11:07:29.489+0700 W/STARTER ( 1141): pkg-monitor.c: _app_mgr_status_cb(400) > [_app_mgr_status_cb:400] Launch request [2383]
04-27 11:07:29.599+0700 W/W_HOME  ( 1251): event_manager.c: _ecore_x_message_cb(421) > state: 0 -> 1
04-27 11:07:29.599+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:29.599+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:29.609+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:29.609+0700 W/W_HOME  ( 1251): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 0
04-27 11:07:29.609+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 11:07:29.609+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 11:07:29.639+0700 E/EFL     ( 2383): ecore_evas<2383> ecore_evas_extn.c:2204 ecore_evas_extn_plug_connect() Extn plug failed to connect:ipctype=0, svcname=elm_indicator_portrait, svcnum=0, svcsys=0
04-27 11:07:29.659+0700 I/efl-extension( 2383): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:29.659+0700 E/EFL     ( 2383): elementary<2383> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7f78288 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-27 11:07:29.659+0700 E/EFL     ( 2383): elementary<2383> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7f78288 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-27 11:07:29.659+0700 E/EFL     ( 2383): elementary<2383> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7f78288 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-27 11:07:29.659+0700 E/EFL     ( 2383): elementary<2383> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7f78288 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-27 11:07:29.659+0700 E/EFL     ( 2383): elementary<2383> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7f78288 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-27 11:07:29.659+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7f786f0, elm_image, _activated_obj : 0x0, activated : 1
04-27 11:07:29.779+0700 I/efl-extension( 2383): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:29.779+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:29.779+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_event_handler_add(77) > init_count: 0
04-27 11:07:29.779+0700 I/efl-extension( 2383): efl_extension_rotary.c: _init_Xi2_system(314) > In
04-27 11:07:29.809+0700 I/efl-extension( 2383): efl_extension_rotary.c: _init_Xi2_system(375) > Done
04-27 11:07:29.809+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7f8c848, elm_image, _activated_obj : 0xb7f786f0, activated : 1
04-27 11:07:29.809+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:07:29.909+0700 E/EFL     ( 2383): elementary<2383> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7f786f0 in function: elm_progressbar_pulse_set, of type: 'elm_image' when expecting type: 'elm_progressbar'
04-27 11:07:29.909+0700 E/EFL     ( 2383): elementary<2383> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7f786f0 in function: elm_progressbar_pulse, of type: 'elm_image' when expecting type: 'elm_progressbar'
04-27 11:07:29.959+0700 E/EFL     ( 2383): elementary<2383> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:07:29.979+0700 E/EFL     ( 2383): elementary<2383> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb7fc21f8) will be pushed
04-27 11:07:29.999+0700 I/APP_CORE( 2383): appcore-efl.c: __do_app(453) > [APP 2383] Event: RESET State: CREATED
04-27 11:07:29.999+0700 I/CAPI_APPFW_APPLICATION( 2383): app_main.c: _ui_app_appcore_reset(645) > app_appcore_reset
04-27 11:07:30.019+0700 I/APP_CORE( 2383): appcore-efl.c: __do_app(522) > Legacy lifecycle: 0
04-27 11:07:30.019+0700 I/APP_CORE( 2383): appcore-efl.c: __do_app(524) > [APP 2383] Initial Launching, call the resume_cb
04-27 11:07:30.019+0700 I/CAPI_APPFW_APPLICATION( 2383): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-27 11:07:30.059+0700 W/APP_CORE( 2383): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3c00002
04-27 11:07:30.069+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7f78cd0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:30.069+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7f78cd0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:30.079+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7f78cd0 : elm_genlist] mx(99999639), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:30.079+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7f78cd0 : elm_genlist] cw(99999999), ch(0), pw(360), ph(360)
04-27 11:07:30.119+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1251) status(bg) type(uiapp)
04-27 11:07:30.119+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1251] goes to (4)
04-27 11:07:30.119+0700 E/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1251)'s state(4)
04-27 11:07:30.129+0700 W/W_HOME  ( 1251): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(1)
04-27 11:07:30.129+0700 W/W_HOME  ( 1251): event_manager.c: _window_visibility_cb(470) > state: 1 -> 0
04-27 11:07:30.129+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:30.129+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:6, app_state:1 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:30.129+0700 W/W_HOME  ( 1251): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(1)
04-27 11:07:30.129+0700 I/APP_CORE( 1251): appcore-efl.c: __do_app(453) > [APP 1251] Event: PAUSE State: RUNNING
04-27 11:07:30.129+0700 I/CAPI_APPFW_APPLICATION( 1251): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-27 11:07:30.139+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2383] goes to (3)
04-27 11:07:30.139+0700 W/W_HOME  ( 1251): main.c: _appcore_pause_cb(489) > appcore pause
04-27 11:07:30.139+0700 W/W_HOME  ( 1251): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
04-27 11:07:30.139+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:30.139+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.toyota.realtimefeedback) pid(2383) status(fg) type(uiapp)
04-27 11:07:30.149+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 11:07:30.149+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 11:07:30.149+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:30.149+0700 I/APP_CORE( 2383): appcore-efl.c: __do_app(453) > [APP 2383] Event: RESUME State: RUNNING
04-27 11:07:30.149+0700 W/W_HOME  ( 1251): main.c: home_pause(550) > clock/widget paused
04-27 11:07:30.149+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 11:07:30.149+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:07:30.149+0700 W/MUSIC_CONTROL_SERVICE( 1783): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1783]   [com.samsung.w-home]register msg port [false][0m
04-27 11:07:30.159+0700 I/GATE    ( 2383): <GATE-M>APP_FULLY_LOADED_realtimefeedback</GATE-M>
04-27 11:07:30.159+0700 I/wnotib  ( 1251): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 1
04-27 11:07:30.159+0700 I/efl-extension( 1251): efl_extension_more_option.c: eext_more_option_opened_get(655) > called!!
04-27 11:07:30.159+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: wnb_nm_postpone_updating_job(997) > Set is_notiboard_update_postponed to true with is_for_VI 0, notiboard panel creation count [3], notiboard card appending count [3].
04-27 11:07:30.519+0700 E/AUL     (  929): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 11:07:30.529+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:30.539+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2383
04-27 11:07:30.539+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 12
04-27 11:07:30.619+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:30.629+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2383
04-27 11:07:30.629+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 12
04-27 11:07:30.659+0700 I/APP_CORE( 1251): appcore-efl.c: __do_app(453) > [APP 1251] Event: MEM_FLUSH State: PAUSED
04-27 11:07:31.429+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: wnb_nm_notiboard_update_control_cb(1031) > is_postpone_requested 0
04-27 11:07:31.429+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(974) > No postponed work with is_for_VI: 1, postponed_for_VI: 0.
04-27 11:07:31.429+0700 W/wnotib  ( 1251): w-notification-board-action.c: _wnb_action_action_timer_cb(5085) > Noti is still valid.
04-27 11:07:31.429+0700 W/wnotib  ( 1251): w-notification-board-common.c: _wnb_auto_remove_timer_cb(3801) > 
04-27 11:07:31.429+0700 E/wnoti-service( 1443): wnoti-db-server.c: __delete_mini_app_image(29) > Gear application
04-27 11:07:31.429+0700 E/wnoti-service( 1443): wnoti-native-client.c: notify_to_native_noti(1849) > tizen_noti_id : 1080
04-27 11:07:31.459+0700 W/SHealthServiceCommon( 1795): NotificationServiceController.cpp: DeleteNotificationByPrivateId(928) > [1;40;33mdelete notification by private id = [1080][0;m
04-27 11:07:31.479+0700 E/wnoti-service( 1443): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 1, count : 0
04-27 11:07:31.489+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:31.499+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:31.529+0700 E/wnoti-service( 1443): wnoti-msg-builder.c: wnoti_service_publish_activity(1855) > param1 : 3, param2 : -1016, param3 : 0, param4 : 0, card_id : (null)
04-27 11:07:31.539+0700 E/wnoti-service( 1443): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 0, count : 0
04-27 11:07:31.539+0700 E/wnoti-service( 1443): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : 0, display_count : 0, 
04-27 11:07:31.539+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1080, op_type : 3  //insert = 1, update = 2, delete = 3
04-27 11:07:31.539+0700 E/wnoti-proxy( 1877): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1877, caller_id : 0, listener_type : 0
04-27 11:07:31.539+0700 E/wnoti-proxy( 1251): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1251, caller_id : 0, listener_type : 0
04-27 11:07:31.539+0700 E/APPS    ( 1251): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-27 11:07:31.539+0700 E/wnoti-service( 1443): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-27 11:07:31.549+0700 E/wnoti-service( 1443): wnoti-native-client.c: _get_native_application_info(55) > _query_step failed()
04-27 11:07:31.549+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1722) > error_code from _get_native_application_info: -1
04-27 11:07:31.549+0700 E/wnoti-service( 1443): wnoti-native-client.c: __remove_notification(684) > application_id is 0.
04-27 11:07:31.549+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-27 11:07:31.689+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_service_changed_cb(243) > Handle this change type in idler.
04-27 11:07:31.689+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1140
04-27 11:07:31.689+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 0
04-27 11:07:31.689+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: 0, display count: 0
04-27 11:07:31.689+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-27 11:07:31.689+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(846) > Proceed idler job.
04-27 11:07:31.689+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(444) > idler for type: 0
04-27 11:07:31.699+0700 E/wnoti-service( 1443): wnoti-db-client.c: _wnoti_get_categories_info(206) > _query_step failed(not SQLITE_ROW)
04-27 11:07:31.699+0700 E/wnoti-proxy( 1251): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-27 11:07:31.699+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(732) > No categories available. Num old_notifications: 1
04-27 11:07:31.699+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_remove_card(419) > db_id: 1140, application_id: -1016, application_name: Real Feed Back
04-27 11:07:31.699+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 114, application_id: -1016, type: 2
04-27 11:07:31.699+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3278) > db_id: 1140, is_request_from_noti_service: 1
04-27 11:07:31.699+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3432) > We don't need to delete the item for second_depth_type: 0.
04-27 11:07:31.699+0700 E/EFL     ( 1251): elementary<1251> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-27 11:07:31.699+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bpanel_coverview_gl_item_del(4095) > card[0xac4e72e8]
04-27 11:07:31.699+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3529) > Hide the drawer for the current panel.
04-27 11:07:31.699+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_destroy(5758) > Destory panel, panel application_id [-1016], panel category_id [114]
04-27 11:07:31.699+0700 I/wnotib  ( 1251): w-notification-board-action.c: wnb_action_deinitialize(5671) > Deinit drawer.
04-27 11:07:31.699+0700 I/wnotib  ( 1251): w-notification-board-action.c: _wnb_action_terminate_input_selector(5550) > No need to close w-input-selector.
04-27 11:07:31.699+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:31.709+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-27 11:07:31.709+0700 I/wnotib  ( 1251): w-notification-board-action.c: _wnb_action_terminate_noti_composer(5585) > ret : 0, is_running : 0
04-27 11:07:31.709+0700 I/wnotib  ( 1251): w-notification-board-action.c: _wnb_action_sending_popup_del_cb(705) > 0xac2051e8, g_sending_popup_state: 0
04-27 11:07:31.709+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xac21abe0 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:31.709+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xac21abe0 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:31.719+0700 I/efl-extension( 1251): efl_extension_more_option.c: eext_more_option_items_clear(572) > called!!
04-27 11:07:31.719+0700 I/efl-extension( 1251): efl_extension_rotary_selector.c: eext_rotary_selector_items_clear(2473) > called!!
04-27 11:07:31.719+0700 I/efl-extension( 1251): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-27 11:07:31.719+0700 I/efl-extension( 1251): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-27 11:07:31.719+0700 I/efl-extension( 1251): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
04-27 11:07:31.719+0700 I/efl-extension( 1251): efl_extension_more_option.c: _more_option_del_cb(268) > called!!
04-27 11:07:31.719+0700 I/efl-extension( 1251): efl_extension_more_option.c: _panel_del_cb(173) > called!!
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary_selector.c: _rotary_selector_del_cb(826) > called!!
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xae651fb8, elm_layout, func : 0xb687c8c9
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xae651fb8, obj: 0xae651fb8
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-27 11:07:31.729+0700 E/wnoti-proxy( 1939): wnoti.c: run_action_caller_cb(3933) > activity_type : 3
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary_selector.c: _event_area_del_cb(494) > called!!
04-27 11:07:31.729+0700 I/wnotib  ( 1251): w-notification-board-action.c: wnb_action_deinitialize(5721) > g_wnb_action_data is freed.
04-27 11:07:31.729+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_reset_scroll_config(528) > Reset thumbscroll_sensitivity_friction to 1.600000
04-27 11:07:31.729+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_reset_scroll_config(530) > After thumbscroll_sensitivity_friction: 1.000000
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xac491f58
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb74eea00, elm_box, _activated_obj : 0x0, activated : 1
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xac491f58, elm_genlist, _activated_obj : 0xb74eea00, activated : 0
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xac491f58, elm_genlist, func : 0xaf289fa1
04-27 11:07:31.729+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 11:07:31.729+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_set_current_detail_noti(1441) > Set current_detail_noti_db_id to 0
04-27 11:07:31.739+0700 I/efl-extension( 1251): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xac491f58, obj: 0xac491f58
04-27 11:07:31.739+0700 I/efl-extension( 1251): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-27 11:07:31.739+0700 E/EFL     ( 1251): elementary<1251> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-27 11:07:31.739+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xac491f58 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:31.739+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xac491f58 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:31.749+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xac491f58 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:31.749+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xac491f58 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:31.749+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 11:07:31.749+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xac491f58, elm_genlist, func : 0xb6872ea1
04-27 11:07:31.749+0700 I/efl-extension( 1251): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 11:07:31.749+0700 W/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_create_noti_detail_default_image(543) > Panel will be removed , skip make default image. stack list count [0].
04-27 11:07:31.749+0700 E/wnoti-proxy( 1883): wnoti.c: run_action_caller_cb(3933) > activity_type : 3
04-27 11:07:31.749+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xac4ac528 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:31.749+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xac4ac528 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:31.749+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xac4ac528 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:31.749+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xac4ac528 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:31.759+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb74d0980 : elm_scroller] mx(3240), my(0), minx(0), miny(0), px(360), py(0)
04-27 11:07:31.759+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb74d0980 : elm_scroller] cw(3600), ch(360), pw(360), ph(360)
04-27 11:07:31.759+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb74d0980 : elm_scroller] x(0), y(0), nx(0), px(360), ny(0) py(0)
04-27 11:07:31.759+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb74d0980 : elm_scroller] x(0), y(0)
04-27 11:07:31.759+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_select(586) > 0
04-27 11:07:31.759+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_select(596) > select index:1
04-27 11:07:31.759+0700 E/W_HOME  ( 1251): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
04-27 11:07:31.759+0700 E/W_HOME  ( 1251): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-27 11:07:31.759+0700 E/ELM_RPANEL( 1251): elm-rpanel.c: _panel_will_be_activated(954) > 
04-27 11:07:31.769+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb74d0980 : elm_scroller] mx(3240), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:31.769+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb74d0980 : elm_scroller] cw(3600), ch(360), pw(360), ph(360)
04-27 11:07:31.769+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb74d0980 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-27 11:07:31.769+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_update(560) > 0x1
04-27 11:07:31.809+0700 E/W_HOME  ( 1251): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
04-27 11:07:31.809+0700 E/W_HOME  ( 1251): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-27 11:07:31.809+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_select(586) > 0
04-27 11:07:31.809+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_select(596) > select index:1
04-27 11:07:31.809+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_destroy(5920) > Destory panel done.
04-27 11:07:31.809+0700 W/wnotib  ( 1251): w-notification-board-panel-manager.c: wnb_pm_destroy_panel(330) > page_instance 114, 0xac4799a8 is destoryed!
04-27 11:07:31.809+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_free_data(248) > Free noti manager data.
04-27 11:07:31.809+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_free_data(253) > Free previous notifications.
04-27 11:07:31.809+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_free_data(274) > Free previous categories.
04-27 11:07:31.809+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(747) > before_rpanel_count: 2, after_rpanel_count: 0.
04-27 11:07:31.809+0700 I/wnotib  ( 1251): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(60) > type: 0
04-27 11:07:31.809+0700 W/wnotib  ( 1251): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(79) > uncleared_category_count: 0, setting_power_saving_mode: 0
04-27 11:07:31.809+0700 W/wnotib  ( 1251): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(82) > Put empty view into panel body
04-27 11:07:31.809+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_select(586) > 0
04-27 11:07:31.809+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_select(596) > select index:1
04-27 11:07:31.809+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: wnb_nm_control_home_indicator(59) > Hide home indicator.
04-27 11:07:31.809+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_noti_indicator_hide(496) > 
04-27 11:07:31.819+0700 E/wnoti-service( 1443): wnoti-server-mgr-stub.c: __wnoti_change_new_flag_stub(3143) > change_type : 0, value : 0
04-27 11:07:31.819+0700 I/wnotib  ( 1251): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(99) > is_notification_enabled: 1, blocking mode: 0, is_connected_vendor_LO: 0
04-27 11:07:31.819+0700 I/wnotib  ( 1251): w-notification-board-empty-panel.c: _wnb_ep_set_texts(348) > is_connected_vendor_LO: 0, is_standalone_mode: 0
04-27 11:07:31.819+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1251): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:07:31.819+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1251): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:07:31.819+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1251): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:07:31.819+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1251): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:07:31.849+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_update(560) > 0x1
04-27 11:07:31.899+0700 E/W_HOME  ( 1251): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
04-27 11:07:31.899+0700 E/W_HOME  ( 1251): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-27 11:07:31.899+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_update(560) > 0x1
04-27 11:07:31.919+0700 E/W_HOME  ( 1251): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
04-27 11:07:31.939+0700 E/W_HOME  ( 1251): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-27 11:07:32.129+0700 I/APP_CORE( 1877): appcore-efl.c: __do_app(453) > [APP 1877] Event: MEM_FLUSH State: PAUSED
04-27 11:07:32.139+0700 I/CAPI_NETWORK_CONNECTION( 2383): connection.c: connection_create(453) > New handle created[0xb802cb78]
04-27 11:07:32.309+0700 I/CAPI_NETWORK_CONNECTION( 2383): connection.c: connection_destroy(471) > Destroy handle: 0xb802cb78
04-27 11:07:32.319+0700 E/EFL     ( 2383): <2383> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:32.339+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7f78cd0 : elm_genlist] mx(0), my(272), minx(0), miny(0), px(0), py(0)
04-27 11:07:32.349+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7f78cd0 : elm_genlist] cw(360), ch(632), pw(360), ph(360)
04-27 11:07:32.539+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_hide(550) > 
04-27 11:07:32.539+0700 W/W_HOME  ( 1251): index.c: index_hide(338) > hide VI:1 visibility:0 vi:(nil)
04-27 11:07:33.119+0700 E/EFL     ( 2383): ecore_x<2383> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=165875 button=1
04-27 11:07:33.129+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.129+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.139+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.139+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.149+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.149+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.169+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.169+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.179+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.179+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.199+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.199+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.219+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.219+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.239+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.239+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.249+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] mouse move
04-27 11:07:33.249+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f78cd0 : elm_genlist] hold(0), freeze(0)
04-27 11:07:33.259+0700 E/EFL     ( 2383): ecore_x<2383> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=166016 button=1
04-27 11:07:33.479+0700 E/GL SELECTED( 2383): ID GL : TRM1
04-27 11:07:33.739+0700 I/AUL_PAD ( 2418): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-27 11:07:34.059+0700 E/EFL     ( 2383): ecore_x<2383> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=166815 button=1
04-27 11:07:34.209+0700 E/EFL     ( 2383): ecore_x<2383> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=166966 button=1
04-27 11:07:34.219+0700 I/efl-extension( 2383): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:34.219+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:34.219+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb49336e8, elm_image, _activated_obj : 0xb7f8c848, activated : 1
04-27 11:07:34.219+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:07:34.219+0700 E/EFL     ( 2383): elementary<2383> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:07:34.229+0700 E/EFL     ( 2383): elementary<2383> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb4933b50) will be pushed
04-27 11:07:34.229+0700 E/EFL     ( 2383): <2383> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:34.229+0700 E/EFL     ( 2383): <2383> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:34.229+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8052470 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:34.229+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8052470 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:34.239+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8052470 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:34.239+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8052470 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-27 11:07:34.259+0700 E/EFL     ( 2383): elementary<2383> elc_naviframe.c:2796 _push_transition_cb() item(0xb4933b50) will transition
04-27 11:07:34.679+0700 E/EFL     ( 2383): elementary<2383> elc_naviframe.c:1193 _on_item_push_finished() item(0xb7fc21f8) transition finished
04-27 11:07:34.679+0700 E/EFL     ( 2383): elementary<2383> elc_naviframe.c:1218 _on_item_show_finished() item(0xb4933b50) transition finished
04-27 11:07:34.699+0700 I/GATE    ( 2399): <GATE-M>FORCE_CLOSED_realtimefeedback1</GATE-M>
04-27 11:07:34.719+0700 E/EFL     ( 2383): <2383> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:35.159+0700 I/APP_CORE( 1251): appcore-efl.c: __do_app(453) > [APP 1251] Event: MEM_FLUSH State: PAUSED
04-27 11:07:35.219+0700 E/EFL     (  893): ecore_x<893> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3c00002 time=167002
04-27 11:07:35.219+0700 E/EFL     ( 2383): ecore_x<2383> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=167002
04-27 11:07:35.219+0700 E/EFL     (  893): ecore_x<893> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=167002
04-27 11:07:36.099+0700 E/EFL     ( 2383): ecore_x<2383> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=168851 button=1
04-27 11:07:36.109+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8052470 : elm_genlist] mouse move
04-27 11:07:36.119+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8052470 : elm_genlist] mouse move
04-27 11:07:36.119+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8052470 : elm_genlist] hold(0), freeze(0)
04-27 11:07:36.129+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8052470 : elm_genlist] mouse move
04-27 11:07:36.129+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8052470 : elm_genlist] hold(0), freeze(0)
04-27 11:07:36.139+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8052470 : elm_genlist] mouse move
04-27 11:07:36.139+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8052470 : elm_genlist] hold(0), freeze(0)
04-27 11:07:36.209+0700 E/EFL     ( 2383): ecore_x<2383> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=168961 button=1
04-27 11:07:36.259+0700 I/efl-extension( 2383): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:36.259+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:36.259+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb1b067f8, elm_image, _activated_obj : 0xb49336e8, activated : 1
04-27 11:07:36.259+0700 I/efl-extension( 2383): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:07:36.259+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb1b04a98 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-27 11:07:36.259+0700 E/EFL     ( 2383): elementary<2383> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb1b04a98 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-27 11:07:36.399+0700 W/CRASH_MANAGER( 2399): worker.c: worker_job(1205) > 1102383726561152480205
