S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 3807
Date: 2018-04-20 18:08:37+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 3807, uid 5000)

Register Information
r0   = 0xb4823978, r1   = 0x00000000
r2   = 0xb6d52b88, r3   = 0x00000000
r4   = 0xb7d7a040, r5   = 0xb7ca1590
r6   = 0x00000000, r7   = 0x00000001
r8   = 0x00000000, r9   = 0x00000020
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb6c4ce9c, sp   = 0xb46fa2a0
lr   = 0xb6c00f1d, pc   = 0xb6cb1888
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:     17132 KB
Buffers:      7448 KB
Cached:     101392 KB
VmPeak:      89808 KB
VmSize:      87644 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23460 KB
VmRSS:       23460 KB
VmData:      27412 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24516 KB
VmPTE:          60 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 3807 TID = 3838
3807 3835 3838 

Maps Information
b1f32000 b1f36000 r-xp /usr/lib/libogg.so.0.7.1
b1f3e000 b1f60000 r-xp /usr/lib/libvorbis.so.0.4.3
b1f68000 b1faf000 r-xp /usr/lib/libsndfile.so.1.0.26
b1fbb000 b2004000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b200d000 b2012000 r-xp /usr/lib/libjson.so.0.0.1
b38b3000 b39b9000 r-xp /usr/lib/libicuuc.so.57.1
b39cf000 b3b57000 r-xp /usr/lib/libicui18n.so.57.1
b3b67000 b3b74000 r-xp /usr/lib/libail.so.0.1.0
b3b7d000 b3b80000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3b88000 b3bc0000 r-xp /usr/lib/libpulse.so.0.16.2
b3bc1000 b3bc4000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3bcc000 b3c2d000 r-xp /usr/lib/libasound.so.2.0.0
b3c37000 b3c50000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c59000 b3c5d000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3c65000 b3c70000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3c7d000 b3c81000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3c8a000 b3ca2000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3cb3000 b3cba000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3cc2000 b3ccd000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3cd5000 b3d5c000 rw-s anon_inode:dmabuf
b3d5c000 b3de3000 rw-s anon_inode:dmabuf
b3de3000 b3e6a000 rw-s anon_inode:dmabuf
b3e6a000 b3e6d000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3e75000 b3efc000 rw-s anon_inode:dmabuf
b3efd000 b46fc000 rw-p [stack:3838]
b4901000 b4903000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b490b000 b490c000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4914000 b491c000 r-xp /usr/lib/libfeedback.so.0.1.4
b4935000 b4936000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b4953000 b4954000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b49f8000 b49f9000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b00000 b52ff000 rw-p [stack:3835]
b52ff000 b5301000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b5309000 b5320000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b532d000 b532f000 r-xp /usr/lib/libdri2.so.0.0.0
b5337000 b5342000 r-xp /usr/lib/libtbm.so.1.0.0
b534a000 b5352000 r-xp /usr/lib/libdrm.so.2.4.0
b535a000 b535c000 r-xp /usr/lib/libgenlock.so
b5364000 b5369000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5371000 b537c000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b5585000 b564f000 r-xp /usr/lib/libCOREGL.so.4.0
b5660000 b5670000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5678000 b567e000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5686000 b5687000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5690000 b5693000 r-xp /usr/lib/libEGL.so.1.4
b569b000 b56a9000 r-xp /usr/lib/libGLESv2.so.2.0
b56b2000 b56fb000 r-xp /usr/lib/libmdm.so.1.2.70
b5704000 b570a000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5712000 b571b000 r-xp /usr/lib/libcom-core.so.0.0.1
b5724000 b57dc000 r-xp /usr/lib/libcairo.so.2.11200.14
b57e7000 b5800000 r-xp /usr/lib/libnetwork.so.0.0.0
b5808000 b5814000 r-xp /usr/lib/libnotification.so.0.1.0
b581d000 b582c000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b5835000 b5856000 r-xp /usr/lib/libefl-extension.so.0.1.0
b585e000 b5863000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b586b000 b5870000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b5878000 b5888000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5890000 b5898000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b58a0000 b58a9000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a4e000 b5a58000 r-xp /lib/libnss_files-2.13.so
b5a61000 b5b30000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b46000 b5b6a000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b73000 b5b79000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b81000 b5b85000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b92000 b5b9d000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5ba5000 b5ba7000 r-xp /usr/lib/libiniparser.so.0
b5bb0000 b5bb5000 r-xp /usr/lib/libappcore-common.so.1.1
b5bbd000 b5bbf000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5bc8000 b5bcc000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5bd9000 b5bdb000 r-xp /usr/lib/libXau.so.6.0.0
b5be3000 b5bea000 r-xp /lib/libcrypt-2.13.so
b5c1a000 b5c1c000 r-xp /usr/lib/libiri.so
b5c25000 b5db7000 r-xp /usr/lib/libcrypto.so.1.0.0
b5dd8000 b5e1f000 r-xp /usr/lib/libssl.so.1.0.0
b5e2b000 b5e59000 r-xp /usr/lib/libidn.so.11.5.44
b5e61000 b5e6a000 r-xp /usr/lib/libcares.so.2.1.0
b5e74000 b5e87000 r-xp /usr/lib/libxcb.so.1.1.0
b5e90000 b5e93000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e9b000 b5e9d000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5ea6000 b5f72000 r-xp /usr/lib/libxml2.so.2.7.8
b5f7f000 b5f81000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f8a000 b5f8f000 r-xp /usr/lib/libffi.so.5.0.10
b5f97000 b5f98000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5fa0000 b5fa3000 r-xp /lib/libattr.so.1.1.0
b5fab000 b603f000 r-xp /usr/lib/libstdc++.so.6.0.16
b6052000 b606f000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6079000 b6091000 r-xp /usr/lib/libpng12.so.0.50.0
b6099000 b60af000 r-xp /lib/libexpat.so.1.6.0
b60b9000 b60fd000 r-xp /usr/lib/libcurl.so.4.3.0
b6106000 b6110000 r-xp /usr/lib/libXext.so.6.4.0
b611a000 b611e000 r-xp /usr/lib/libXtst.so.6.1.0
b6126000 b612c000 r-xp /usr/lib/libXrender.so.1.3.0
b6134000 b613a000 r-xp /usr/lib/libXrandr.so.2.2.0
b6142000 b6143000 r-xp /usr/lib/libXinerama.so.1.0.0
b614c000 b6155000 r-xp /usr/lib/libXi.so.6.1.0
b615d000 b6160000 r-xp /usr/lib/libXfixes.so.3.1.0
b6169000 b616b000 r-xp /usr/lib/libXgesture.so.7.0.0
b6173000 b6175000 r-xp /usr/lib/libXcomposite.so.1.0.0
b617d000 b617f000 r-xp /usr/lib/libXdamage.so.1.1.0
b6187000 b618e000 r-xp /usr/lib/libXcursor.so.1.0.2
b6196000 b6199000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b61a2000 b61a6000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b61af000 b61b4000 r-xp /usr/lib/libecore_fb.so.1.7.99
b61bd000 b629e000 r-xp /usr/lib/libX11.so.6.3.0
b62a9000 b62cc000 r-xp /usr/lib/libjpeg.so.8.0.2
b62e4000 b62fa000 r-xp /lib/libz.so.1.2.5
b6303000 b6305000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b630d000 b6382000 r-xp /usr/lib/libsqlite3.so.0.8.6
b638c000 b63a6000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b63ae000 b63e2000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63eb000 b64be000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b64ca000 b64da000 r-xp /lib/libresolv-2.13.so
b64de000 b64f6000 r-xp /usr/lib/liblzma.so.5.0.3
b64fe000 b6501000 r-xp /lib/libcap.so.2.21
b6509000 b6538000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6540000 b6541000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b654a000 b6550000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6558000 b656f000 r-xp /usr/lib/liblua-5.1.so
b6578000 b657f000 r-xp /usr/lib/libembryo.so.1.7.99
b6587000 b658d000 r-xp /lib/librt-2.13.so
b6596000 b65ec000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65fa000 b6650000 r-xp /usr/lib/libfreetype.so.6.11.3
b665c000 b6684000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6685000 b66ca000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b66d3000 b66e6000 r-xp /usr/lib/libfribidi.so.0.3.1
b66ee000 b6708000 r-xp /usr/lib/libecore_con.so.1.7.99
b6712000 b671b000 r-xp /usr/lib/libedbus.so.1.7.99
b6723000 b6773000 r-xp /usr/lib/libecore_x.so.1.7.99
b6775000 b677e000 r-xp /usr/lib/libvconf.so.0.2.45
b6786000 b6797000 r-xp /usr/lib/libecore_input.so.1.7.99
b679f000 b67a4000 r-xp /usr/lib/libecore_file.so.1.7.99
b67ac000 b67ce000 r-xp /usr/lib/libecore_evas.so.1.7.99
b67d7000 b6818000 r-xp /usr/lib/libeina.so.1.7.99
b6821000 b683a000 r-xp /usr/lib/libeet.so.1.7.99
b684b000 b68b4000 r-xp /lib/libm-2.13.so
b68bd000 b68c3000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b68cc000 b68cd000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b68d5000 b68f8000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6900000 b6905000 r-xp /usr/lib/libxdgmime.so.1.1.0
b690d000 b6937000 r-xp /usr/lib/libdbus-1.so.3.8.12
b6940000 b6957000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b695f000 b696a000 r-xp /lib/libunwind.so.8.0.1
b6997000 b69b5000 r-xp /usr/lib/libsystemd.so.0.4.0
b69bf000 b6ae3000 r-xp /lib/libc-2.13.so
b6af1000 b6af9000 r-xp /lib/libgcc_s-4.6.so.1
b6afa000 b6afe000 r-xp /usr/lib/libsmack.so.1.0.0
b6b07000 b6b0d000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b15000 b6be5000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6be6000 b6c44000 r-xp /usr/lib/libedje.so.1.7.99
b6c4e000 b6c65000 r-xp /usr/lib/libecore.so.1.7.99
b6c7c000 b6d4a000 r-xp /usr/lib/libevas.so.1.7.99
b6d70000 b6eac000 r-xp /usr/lib/libelementary.so.1.7.99
b6ec3000 b6ed7000 r-xp /lib/libpthread-2.13.so
b6ee2000 b6ee4000 r-xp /usr/lib/libdlog.so.0.0.0
b6eec000 b6eef000 r-xp /usr/lib/libbundle.so.0.1.22
b6ef7000 b6ef9000 r-xp /lib/libdl-2.13.so
b6f02000 b6f0f000 r-xp /usr/lib/libaul.so.0.1.0
b6f21000 b6f27000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f30000 b6f34000 r-xp /usr/lib/libsys-assert.so
b6f3d000 b6f5a000 r-xp /lib/ld-2.13.so
b6f63000 b6f68000 r-xp /usr/bin/launchpad-loader
b7b86000 b8158000 rw-p [heap]
beb9a000 bebbb000 rw-p [stack]
End of Maps Information

Callstack Information (PID:3807)
Call Stack Count: 20
 0: evas_object_smart_need_recalculate_set + 0x4f (0xb6cb1888) [/usr/lib/libevas.so.1] + 0x35888
 1: (0xb6c00f1d) [/usr/lib/libedje.so.1] + 0x1af1d
 2: edje_object_size_min_restricted_calc + 0x8e (0xb6c364b7) [/usr/lib/libedje.so.1] + 0x504b7
 3: (0xb6dfa93d) [/usr/lib/libelementary.so.1] + 0x8a93d
 4: (0xb6dfc2c9) [/usr/lib/libelementary.so.1] + 0x8c2c9
 5: (0xb6dfc38d) [/usr/lib/libelementary.so.1] + 0x8c38d
 6: (0xb6dfc797) [/usr/lib/libelementary.so.1] + 0x8c797
 7: elm_genlist_item_append + 0x136 (0xb6e0612b) [/usr/lib/libelementary.so.1] + 0x9612b
 8: create_genlist_item + 0x12e (0xb58a75f9) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x75f9
 9: jsonParsing + 0x1f8 (0xb58a4401) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x4401
10: WriteCallback + 0x42 (0xb58a4187) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x4187
11: (0xb60c73af) [/usr/lib/libcurl.so.4] + 0xe3af
12: (0xb60d4ceb) [/usr/lib/libcurl.so.4] + 0x1bceb
13: (0xb60daa67) [/usr/lib/libcurl.so.4] + 0x21a67
14: curl_multi_perform + 0x82 (0xb60db413) [/usr/lib/libcurl.so.4] + 0x22413
15: (0xb60d59f5) [/usr/lib/libcurl.so.4] + 0x1c9f5
16: CurlRequestHTTP + 0x108 (0xb58a4711) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x4711
17: threadMain + 0x66 (0xb58a4127) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x4127
18: (0xb6c5d831) [/usr/lib/libecore.so.1] + 0xf831
19: (0xb6ec9b8c) [/lib/libpthread.so.0] + 0x6b8c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.145+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.145+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.145+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-49)
04-20 18:08:35.155+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.155+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.165+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.165+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.165+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.165+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.165+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-51)
04-20 18:08:35.175+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.175+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.185+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.185+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.185+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.185+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.185+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.195+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.195+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.195+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.225+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.225+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.235+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.235+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.235+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.265+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.265+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.265+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.265+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.265+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.295+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.295+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.295+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.295+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.295+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.335+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.335+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.335+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.335+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.335+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.345+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.345+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.345+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.345+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.345+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.365+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.365+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.365+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.365+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.365+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.375+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.375+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.395+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.395+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.395+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.395+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.395+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.405+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.405+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.415+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.415+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.415+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.415+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.415+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.425+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.425+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.435+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.435+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.435+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-52)
04-20 18:08:35.435+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.435+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:35.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:35.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:35.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:35.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(-51)
04-20 18:08:35.465+0700 E/EFL     ( 3807): ecore_x<3807> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=815728 button=1
04-20 18:08:35.815+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-20 18:08:35.815+0700 W/wnotibp ( 3224): wnotiboard-popup-view.c: _view_main_popup_timer_cb(1901) > ::UI:: start hiding animation
04-20 18:08:36.075+0700 E/EFL     ( 3807): ecore_x<3807> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=816340 button=1
04-20 18:08:36.075+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.085+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.085+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.095+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.095+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.105+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.105+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.115+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.115+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.125+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.125+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.135+0700 W/wnotibp ( 3224): wnotiboard-popup-view.c: _view_sub_popup_hide_animator_cb(1842) > ::UI:: end hiding animation
04-20 18:08:36.135+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-20 18:08:36.135+0700 W/wnotibp ( 3224): wnotiboard-popup-view.c: wnbp_view_goto_pause(1476) > state : (8, 1, 0)
04-20 18:08:36.135+0700 I/wnotibp ( 3224): wnotiboard-popup-view.c: wnbp_view_goto_pause(1489) > simple popup=0, view_state=0
04-20 18:08:36.135+0700 I/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(632) > ::UI:: it is invalid object.
04-20 18:08:36.135+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [invalid object] is UNLOCK , 0000 <=== ]]]
04-20 18:08:36.135+0700 I/wnotibp ( 3224): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 874
04-20 18:08:36.135+0700 W/wnotibp ( 3224): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(686) > Call wnbp_free_noti_detail..
04-20 18:08:36.135+0700 W/wnotibp ( 3224): wnotiboard-popup-common.c: wnbp_free_noti_detail(583) > Do free noti [0xb031c8a8] / db id [874]
04-20 18:08:36.135+0700 I/wnotibp ( 3224): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 0
04-20 18:08:36.135+0700 I/efl-extension( 3224): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb8178868, obj: 0xb8178868
04-20 18:08:36.135+0700 I/efl-extension( 3224): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-20 18:08:36.135+0700 I/efl-extension( 3224): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-20 18:08:36.135+0700 I/efl-extension( 3224): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-20 18:08:36.135+0700 I/efl-extension( 3224): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-20 18:08:36.135+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.135+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.135+0700 E/EFL     ( 3224): elementary<3224> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-20 18:08:36.145+0700 E/EFL     ( 3224): elementary<3224> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8178868 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-20 18:08:36.145+0700 E/EFL     ( 3224): elementary<3224> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8178868 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-20 18:08:36.145+0700 E/EFL     ( 3224): elementary<3224> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8178868 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-20 18:08:36.145+0700 E/EFL     ( 3224): elementary<3224> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8178868 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-20 18:08:36.145+0700 I/efl-extension( 3224): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-20 18:08:36.145+0700 I/efl-extension( 3224): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8178868, elm_genlist, func : 0xb584eea1
04-20 18:08:36.145+0700 I/efl-extension( 3224): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-20 18:08:36.145+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-20 18:08:36.145+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_reset_view_lock(703) > ::UI:: lock state = 0000
04-20 18:08:36.145+0700 I/wnotibp ( 3224): wnotiboard-popup-view.c: wnbp_view_goto_pause(1586) > ::INFO:: call lower
04-20 18:08:36.145+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.145+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.155+0700 W/APP_CORE( 3224): appcore-efl.c: __hide_cb(882) > [EVENT_TEST][EVENT] GET HIDE EVENT!!!. WIN:3c00009
04-20 18:08:36.155+0700 I/APP_CORE( 3224): appcore-efl.c: __do_app(453) > [APP 3224] Event: PAUSE State: RUNNING
04-20 18:08:36.155+0700 I/CAPI_APPFW_APPLICATION( 3224): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-20 18:08:36.155+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.155+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.165+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.165+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.175+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.175+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.175+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4292 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] add hold animator
04-20 18:08:36.185+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:36.185+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:36.185+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(20)
04-20 18:08:36.185+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.225+0700 W/wnotibp ( 3224): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-20 18:08:36.235+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-20 18:08:36.235+0700 W/wnotibp ( 3224): wnotiboard-popup.c: _popup_app_pause(220) > [0, 0, 16, 0000]
04-20 18:08:36.235+0700 W/wnotibp ( 3224): wnotiboard-popup.c: _popup_app_pause(221) > [1, 0]
04-20 18:08:36.235+0700 W/wnotibp ( 3224): wnotiboard-popup.c: _popup_app_pause(222) > [1, 1, 0, 2, 0]
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:36.255+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(55)
04-20 18:08:36.265+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:36.315+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(91)
04-20 18:08:36.325+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.375+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.375+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.375+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.375+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.375+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.375+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:36.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:36.385+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(119)
04-20 18:08:36.385+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.435+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.435+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:36.445+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(142)
04-20 18:08:36.445+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.485+0700 E/EFL     (  933): ecore_x<933> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3000002 time=816532
04-20 18:08:36.495+0700 E/EFL     ( 3807): ecore_x<3807> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=816532
04-20 18:08:36.495+0700 E/EFL     (  933): ecore_x<933> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=816532
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:36.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(157)
04-20 18:08:36.505+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:36.565+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(168)
04-20 18:08:36.565+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] direction_x(0), direction_y(1)
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] drag_child_locked_y(0)
04-20 18:08:36.625+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7d90f18 : elm_genlist] move content x(0), y(182)
04-20 18:08:36.625+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.685+0700 E/EFL     ( 3807): ecore_x<3807> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=816918 button=1
04-20 18:08:36.685+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.685+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.695+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:36.695+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:36.695+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.745+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.795+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.835+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.885+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.935+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:36.975+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.185+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.205+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.225+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.245+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.285+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.315+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.335+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.355+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.375+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.375+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.375+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.375+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.375+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.385+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.385+0700 E/EFL     ( 3807): <3807> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:08:37.405+0700 I/GATE    (  969): <GATE-M>BATTERY_LEVEL_61</GATE-M>
04-20 18:08:37.415+0700 I/watchface-viewer( 1350): viewer-data-provider.cpp: AddPendingChanges(2527) > added [60] to pending list
04-20 18:08:37.415+0700 W/W_INDICATOR( 1117): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
04-20 18:08:37.415+0700 W/W_INDICATOR( 1117): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(61), length(2)
04-20 18:08:37.415+0700 W/W_INDICATOR( 1117): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 61%
04-20 18:08:37.415+0700 W/W_INDICATOR( 1117): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 61, isCharging: 0
04-20 18:08:37.415+0700 W/W_INDICATOR( 1117): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_65
04-20 18:08:37.415+0700 W/W_INDICATOR( 1117): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 65
04-20 18:08:37.415+0700 W/W_INDICATOR( 1117): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
04-20 18:08:37.465+0700 E/EFL     ( 3807): ecore_x<3807> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=817733 button=1
04-20 18:08:37.465+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:37.475+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:37.475+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:37.485+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:37.485+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:37.495+0700 I/CAPI_NETWORK_CONNECTION( 3807): connection.c: connection_create(453) > New handle created[0xb4808510]
04-20 18:08:37.495+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:37.495+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:37.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:37.505+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:37.515+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] mouse move
04-20 18:08:37.515+0700 E/EFL     ( 3807): elementary<3807> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7d90f18 : elm_genlist] hold(0), freeze(0)
04-20 18:08:38.465+0700 W/CRASH_MANAGER( 3877): worker.c: worker_job(1205) > 1103807726561152422251
