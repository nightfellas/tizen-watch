S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2099
Date: 2018-04-26 17:26:17+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2099, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb5847445, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb8002878, r7   = 0xbee401d0
r8   = 0x000186a0, r9   = 0xb7d6ecf8
r10  = 0xb6c1eb10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbee40148
lr   = 0xb5847445, pc   = 0xb69d46f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      5040 KB
Buffers:      8768 KB
Cached:     112640 KB
VmPeak:      97268 KB
VmSize:      96724 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       24192 KB
VmRSS:       24192 KB
VmData:      36572 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24504 KB
VmPTE:          66 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2099 TID = 2099
2099 2183 2191 

Maps Information
b1edb000 b1edf000 r-xp /usr/lib/libogg.so.0.7.1
b1ee7000 b1f09000 r-xp /usr/lib/libvorbis.so.0.4.3
b1f11000 b1f58000 r-xp /usr/lib/libsndfile.so.1.0.26
b1f64000 b1fad000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1fb6000 b1fbb000 r-xp /usr/lib/libjson.so.0.0.1
b385c000 b3962000 r-xp /usr/lib/libicuuc.so.57.1
b3978000 b3b00000 r-xp /usr/lib/libicui18n.so.57.1
b3b10000 b3b1d000 r-xp /usr/lib/libail.so.0.1.0
b3b26000 b3b29000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3b31000 b3b69000 r-xp /usr/lib/libpulse.so.0.16.2
b3b6a000 b3b6d000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3b75000 b3bd6000 r-xp /usr/lib/libasound.so.2.0.0
b3be0000 b3bf9000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c02000 b3c06000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3c0e000 b3c19000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3c26000 b3c2a000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3c33000 b3c4b000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3c5c000 b3c63000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3c6b000 b3c76000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3c7e000 b3c80000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3c88000 b3c89000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3c91000 b3c99000 r-xp /usr/lib/libfeedback.so.0.1.4
b3cb2000 b3cb3000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3f6f000 b3ff6000 rw-s anon_inode:dmabuf
b3ff7000 b47f6000 rw-p [stack:2191]
b499a000 b499b000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4aa2000 b52a1000 rw-p [stack:2183]
b52a1000 b52a3000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52ab000 b52c2000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52cf000 b52d1000 r-xp /usr/lib/libdri2.so.0.0.0
b52d9000 b52e4000 r-xp /usr/lib/libtbm.so.1.0.0
b52ec000 b52f4000 r-xp /usr/lib/libdrm.so.2.4.0
b52fc000 b52fe000 r-xp /usr/lib/libgenlock.so
b5306000 b530b000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5313000 b531e000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b5527000 b55f1000 r-xp /usr/lib/libCOREGL.so.4.0
b5602000 b5612000 r-xp /usr/lib/libmdm-common.so.1.1.25
b561a000 b5620000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5628000 b5629000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5632000 b5635000 r-xp /usr/lib/libEGL.so.1.4
b563d000 b564b000 r-xp /usr/lib/libGLESv2.so.2.0
b5654000 b569d000 r-xp /usr/lib/libmdm.so.1.2.70
b56a6000 b56ac000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56b4000 b56bd000 r-xp /usr/lib/libcom-core.so.0.0.1
b56c6000 b577e000 r-xp /usr/lib/libcairo.so.2.11200.14
b5789000 b57a2000 r-xp /usr/lib/libnetwork.so.0.0.0
b57aa000 b57b6000 r-xp /usr/lib/libnotification.so.0.1.0
b57bf000 b57ce000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b57d7000 b57f8000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5800000 b5805000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b580d000 b5812000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b581a000 b582a000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5832000 b583a000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5842000 b584c000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b59f1000 b59fb000 r-xp /lib/libnss_files-2.13.so
b5a04000 b5ad3000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5ae9000 b5b0d000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b16000 b5b1c000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b24000 b5b28000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b35000 b5b40000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b48000 b5b4a000 r-xp /usr/lib/libiniparser.so.0
b5b53000 b5b58000 r-xp /usr/lib/libappcore-common.so.1.1
b5b60000 b5b62000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b6b000 b5b6f000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5b7c000 b5b7e000 r-xp /usr/lib/libXau.so.6.0.0
b5b86000 b5b8d000 r-xp /lib/libcrypt-2.13.so
b5bbd000 b5bbf000 r-xp /usr/lib/libiri.so
b5bc8000 b5d5a000 r-xp /usr/lib/libcrypto.so.1.0.0
b5d7b000 b5dc2000 r-xp /usr/lib/libssl.so.1.0.0
b5dce000 b5dfc000 r-xp /usr/lib/libidn.so.11.5.44
b5e04000 b5e0d000 r-xp /usr/lib/libcares.so.2.1.0
b5e17000 b5e2a000 r-xp /usr/lib/libxcb.so.1.1.0
b5e33000 b5e36000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e3e000 b5e40000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e49000 b5f15000 r-xp /usr/lib/libxml2.so.2.7.8
b5f22000 b5f24000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f2d000 b5f32000 r-xp /usr/lib/libffi.so.5.0.10
b5f3a000 b5f3b000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f43000 b5f46000 r-xp /lib/libattr.so.1.1.0
b5f4e000 b5fe2000 r-xp /usr/lib/libstdc++.so.6.0.16
b5ff5000 b6012000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b601c000 b6034000 r-xp /usr/lib/libpng12.so.0.50.0
b603c000 b6052000 r-xp /lib/libexpat.so.1.6.0
b605c000 b60a0000 r-xp /usr/lib/libcurl.so.4.3.0
b60a9000 b60b3000 r-xp /usr/lib/libXext.so.6.4.0
b60bd000 b60c1000 r-xp /usr/lib/libXtst.so.6.1.0
b60c9000 b60cf000 r-xp /usr/lib/libXrender.so.1.3.0
b60d7000 b60dd000 r-xp /usr/lib/libXrandr.so.2.2.0
b60e5000 b60e6000 r-xp /usr/lib/libXinerama.so.1.0.0
b60ef000 b60f8000 r-xp /usr/lib/libXi.so.6.1.0
b6100000 b6103000 r-xp /usr/lib/libXfixes.so.3.1.0
b610c000 b610e000 r-xp /usr/lib/libXgesture.so.7.0.0
b6116000 b6118000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6120000 b6122000 r-xp /usr/lib/libXdamage.so.1.1.0
b612a000 b6131000 r-xp /usr/lib/libXcursor.so.1.0.2
b6139000 b613c000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6145000 b6149000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6152000 b6157000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6160000 b6241000 r-xp /usr/lib/libX11.so.6.3.0
b624c000 b626f000 r-xp /usr/lib/libjpeg.so.8.0.2
b6287000 b629d000 r-xp /lib/libz.so.1.2.5
b62a6000 b62a8000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62b0000 b6325000 r-xp /usr/lib/libsqlite3.so.0.8.6
b632f000 b6349000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6351000 b6385000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b638e000 b6461000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b646d000 b647d000 r-xp /lib/libresolv-2.13.so
b6481000 b6499000 r-xp /usr/lib/liblzma.so.5.0.3
b64a1000 b64a4000 r-xp /lib/libcap.so.2.21
b64ac000 b64db000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b64e3000 b64e4000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b64ed000 b64f3000 r-xp /usr/lib/libecore_imf.so.1.7.99
b64fb000 b6512000 r-xp /usr/lib/liblua-5.1.so
b651b000 b6522000 r-xp /usr/lib/libembryo.so.1.7.99
b652a000 b6530000 r-xp /lib/librt-2.13.so
b6539000 b658f000 r-xp /usr/lib/libpixman-1.so.0.28.2
b659d000 b65f3000 r-xp /usr/lib/libfreetype.so.6.11.3
b65ff000 b6627000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6628000 b666d000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6676000 b6689000 r-xp /usr/lib/libfribidi.so.0.3.1
b6691000 b66ab000 r-xp /usr/lib/libecore_con.so.1.7.99
b66b5000 b66be000 r-xp /usr/lib/libedbus.so.1.7.99
b66c6000 b6716000 r-xp /usr/lib/libecore_x.so.1.7.99
b6718000 b6721000 r-xp /usr/lib/libvconf.so.0.2.45
b6729000 b673a000 r-xp /usr/lib/libecore_input.so.1.7.99
b6742000 b6747000 r-xp /usr/lib/libecore_file.so.1.7.99
b674f000 b6771000 r-xp /usr/lib/libecore_evas.so.1.7.99
b677a000 b67bb000 r-xp /usr/lib/libeina.so.1.7.99
b67c4000 b67dd000 r-xp /usr/lib/libeet.so.1.7.99
b67ee000 b6857000 r-xp /lib/libm-2.13.so
b6860000 b6866000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b686f000 b6870000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6878000 b689b000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68a3000 b68a8000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68b0000 b68da000 r-xp /usr/lib/libdbus-1.so.3.8.12
b68e3000 b68fa000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6902000 b690d000 r-xp /lib/libunwind.so.8.0.1
b693a000 b6958000 r-xp /usr/lib/libsystemd.so.0.4.0
b6962000 b6a86000 r-xp /lib/libc-2.13.so
b6a94000 b6a9c000 r-xp /lib/libgcc_s-4.6.so.1
b6a9d000 b6aa1000 r-xp /usr/lib/libsmack.so.1.0.0
b6aaa000 b6ab0000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ab8000 b6b88000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6b89000 b6be7000 r-xp /usr/lib/libedje.so.1.7.99
b6bf1000 b6c08000 r-xp /usr/lib/libecore.so.1.7.99
b6c1f000 b6ced000 r-xp /usr/lib/libevas.so.1.7.99
b6d13000 b6e4f000 r-xp /usr/lib/libelementary.so.1.7.99
b6e66000 b6e7a000 r-xp /lib/libpthread-2.13.so
b6e85000 b6e87000 r-xp /usr/lib/libdlog.so.0.0.0
b6e8f000 b6e92000 r-xp /usr/lib/libbundle.so.0.1.22
b6e9a000 b6e9c000 r-xp /lib/libdl-2.13.so
b6ea5000 b6eb2000 r-xp /usr/lib/libaul.so.0.1.0
b6ec4000 b6eca000 r-xp /usr/lib/libappcore-efl.so.1.1
b6ed3000 b6ed7000 r-xp /usr/lib/libsys-assert.so
b6ee0000 b6efd000 r-xp /lib/ld-2.13.so
b6f06000 b6f0b000 r-xp /usr/bin/launchpad-loader
b7d36000 b8884000 rw-p [heap]
bee20000 bee41000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2099)
Call Stack Count: 1
 0: realloc + 0x4c (0xb69d46f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
/problem_desc( 2099): Glass Door Rr Lh Glass Door Rr Lh Part Damage
04-26 17:26:16.079+0700 E/BODY NO ( 2099): 67022
04-26 17:26:16.079+0700 E/NOTIF ID( 2099): 40198
04-26 17:26:16.079+0700 E/problem ( 2099): Dirty
04-26 17:26:16.079+0700 E/problem_desc( 2099): 19A-Fender Lh Fender Dirty & Stain
04-26 17:26:16.079+0700 E/BODY NO ( 2099): 67026
04-26 17:26:16.079+0700 E/NOTIF ID( 2099): 40200
04-26 17:26:16.079+0700 E/problem ( 2099): Clearance
04-26 17:26:16.079+0700 E/problem_desc( 2099): Rear Bumper X Mufler Part Fitting
04-26 17:26:16.099+0700 E/BODY NO ( 2099): 67031
04-26 17:26:16.099+0700 E/NOTIF ID( 2099): 40211
04-26 17:26:16.099+0700 E/problem ( 2099): Interferance
04-26 17:26:16.099+0700 E/problem_desc( 2099): Fr Door Rh Close Door 3X Part Function
04-26 17:26:16.109+0700 E/BODY NO ( 2099): 67032
04-26 17:26:16.109+0700 E/NOTIF ID( 2099): 40204
04-26 17:26:16.109+0700 E/problem ( 2099): Whitening
04-26 17:26:16.109+0700 E/problem_desc( 2099): Cowl Side Rh Cowl Side Rh Part Damage
04-26 17:26:16.119+0700 E/BODY NO ( 2099): 67033
04-26 17:26:16.119+0700 E/NOTIF ID( 2099): 40203
04-26 17:26:16.119+0700 E/problem ( 2099): Dirty (Part)
04-26 17:26:16.119+0700 E/problem_desc( 2099): Meter Cluster Meter Cluster Dirty Part 
04-26 17:26:16.129+0700 E/BODY NO ( 2099): 67035
04-26 17:26:16.129+0700 E/NOTIF ID( 2099): 40730
04-26 17:26:16.129+0700 E/problem ( 2099): Cathing
04-26 17:26:16.129+0700 E/problem_desc( 2099): A/F sensor Specification
04-26 17:26:16.139+0700 E/weather-agent( 2186): AgentMain.cpp: AppControl(328) > [0;40;31mdevice_power_request_lock success[0;m
04-26 17:26:16.139+0700 E/weather-agent( 2186): AgentMain.cpp: SetSelfKillTimer(208) > [0;40;31mAgentMain::SetSelfKillTimer start[0;m
04-26 17:26:16.139+0700 W/CAPI_APPFW_APP_CONTROL( 2186): app_control.c: app_control_error(136) > [app_control_get_extra_data] KEY_NOT_FOUND(0xffffff82)
04-26 17:26:16.139+0700 E/weather-agent( 2186): AgentLaunchScenarioProxy.cpp: PrepareLaunch(67) > [0;40;31mapp_control_get_extra_data(portId) failed[0;m
04-26 17:26:16.139+0700 E/weather-agent( 2186): AgentLaunchScenarioProxy.cpp: PrepareLaunch(80) > [0;40;31mappId:com.samsung.weather-consumer, portId:(null), message:REQUEST_NEED_TO_UPDATE_WEATHER_INFO[0;m
04-26 17:26:16.139+0700 E/weather-agent( 2186): AgentLaunchScenarioProxy.cpp: CheckSamsungAuthor(101) > [0;40;31mcaller(com.samsung.weather-consumer) is same author with weather agent[0;m
04-26 17:26:16.139+0700 E/weather-agent( 2186): AgentLaunchScenarioProxy.cpp: UpdateShareTarget(116) > [0;40;31m[UpdateShareTarget(): 116] (mPortId.length() < 1) [return][0;m
04-26 17:26:16.149+0700 E/BODY NO ( 2099): 84432
04-26 17:26:16.149+0700 E/NOTIF ID( 2099): 40732
04-26 17:26:16.149+0700 E/problem ( 2099): Cover Screw Unlock
04-26 17:26:16.149+0700 E/problem_desc( 2099): Cover Tweeter Fr Lh Broken Part Damage
04-26 17:26:16.159+0700 E/BODY NO ( 2099): 84466
04-26 17:26:16.159+0700 E/JSON PARSING( 2099): �������e
04-26 17:26:16.209+0700 I/AUL     ( 1125): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-26 17:26:16.209+0700 I/AUL     ( 1125): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-26 17:26:16.259+0700 I/AUL     ( 1447): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:26:16.289+0700 I/AUL     ( 1447): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:26:16.289+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1074, op_type : 1  //insert = 1, update = 2, delete = 3
04-26 17:26:16.289+0700 E/wnoti-service( 1447): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-26 17:26:16.289+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-26 17:26:16.289+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-26 17:26:16.289+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-26 17:26:16.289+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: (null), err: 0
04-26 17:26:16.299+0700 E/wnoti-service( 1447): wnoti-native-client.c: _insert_notification(1208) > fail to get metadata_value, ret : -2
04-26 17:26:16.319+0700 E/wnoti-service( 1447): wnoti-db-server.c: wnoti_update_panel(2712) > id : -1016, source : 1
04-26 17:26:16.319+0700 E/wnoti-service( 1447): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-26 17:26:16.319+0700 E/wnoti-service( 1447): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1074, application_id: -1016
04-26 17:26:16.329+0700 E/wnoti-service( 1447): wnoti-db-server.c: _wnoti_update_category(908) > re_table_id : 0
04-26 17:26:16.339+0700 E/wnoti-service( 1447): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 0, count : 1
04-26 17:26:16.349+0700 I/AUL     ( 1447): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:26:16.359+0700 I/CAPI_NETWORK_CONNECTION( 2099): connection.c: connection_create(453) > New handle created[0xb7fc9768]
04-26 17:26:16.359+0700 I/AUL     ( 1447): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:26:16.369+0700 E/APPS    ( 1203): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-26 17:26:16.739+0700 I/wnoti-service( 1447): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-26 17:26:16.739+0700 E/wnoti-service( 1447): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-26 17:26:16.739+0700 E/wnoti-service( 1447): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-26 17:26:16.739+0700 E/wnoti-service( 1447): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-26 17:26:16.739+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 14
04-26 17:26:16.739+0700 E/wnoti-proxy( 2044): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 2044, caller_id : 0, listener_type : 0
04-26 17:26:16.739+0700 E/wnoti-proxy( 1203): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1203, caller_id : 0, listener_type : 0
04-26 17:26:16.749+0700 W/AUL_AMD (  969): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2044
04-26 17:26:16.749+0700 E/wnoti-service( 1447): wnoti-sap-client.c: on_timer(275) > is_popup_running: 1, ret: 0
04-26 17:26:16.749+0700 E/wnoti-service( 1447): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524738376, g_use_aul_launch : 1524738376
04-26 17:26:16.849+0700 W/SHealthCommon( 1815): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_session_updated, pendingClientInfoList.size(): 0[0;m
04-26 17:26:16.849+0700 W/SHealthServiceCommon( 1815): ContextRestingHeartrateProxy.cpp: OnRestingHrUpdatedCB(347) > [1;40;33mhrValue: 1007[0;m
04-26 17:26:16.849+0700 W/SHealthCommon( 1815): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_summary_updated, pendingClientInfoList.size(): 0[0;m
04-26 17:26:16.849+0700 W/SHealthServiceCommon( 1815): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1524675600000.000000, end 1524738376862.000000, calories 1174.167714[0;m
04-26 17:26:16.849+0700 W/SHealthCommon( 1815): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
04-26 17:26:16.889+0700 I/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_service_changed_cb(243) > Handle this change type in idler.
04-26 17:26:16.889+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-26 17:26:16.889+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-26 17:26:16.889+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-26 17:26:16.899+0700 E/wnoti-service( 1447): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1035, is_duplicated : 0
04-26 17:26:16.899+0700 E/wnoti-service( 1447): wnoti-db-client.c: wnoti_get_alert_categories(1311) > view_type : 1, turn_screen_on : 1, allow_gesture : 1, is_used_popup : 0, feedback : 2
04-26 17:26:16.909+0700 E/wnoti-proxy( 2044): wnoti-client.c: _wnoti_parse_extra_sub(373) > JSON_IS_NULL
04-26 17:26:16.909+0700 I/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(226) > application_name: Real Feed Back, application_id: -1016, category_id: 82, time: 1524738376, launch_app_id: (null), bg_image: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, extracted_icon_color: -3139560, disble_block_app_action: 0, support_large_icon 0
04-26 17:26:16.909+0700 I/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(236) > noti_type: 1
04-26 17:26:16.909+0700 I/wnotibp ( 2044): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1035, noti_type: 1
04-26 17:26:16.909+0700 I/wnotibp ( 2044): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 0, content_id: 0, notification_id: 0
04-26 17:26:16.909+0700 W/wnotibp ( 2044): wnotiboard-popup-data.c: _data_convert_alert_data(67) > alert_type: 4, app_feedback_type: 2, popup_view_style: 0, feedback_pattern_app: -1
04-26 17:26:16.909+0700 W/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 0, new_list count : 1
04-26 17:26:16.909+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_notification_change_cb(600) > [1,1035]
04-26 17:26:16.909+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 14
04-26 17:26:16.909+0700 W/AUL_AMD (  969): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-26 17:26:16.909+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: _common_app_id_from_win(152) > 2099
04-26 17:26:16.939+0700 I/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_check_env_condition(437) > focus app is com.toyota.realtimefeedback, 0
04-26 17:26:16.939+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_add_notification(991) > Add noti_queue [1035, 0]
04-26 17:26:16.939+0700 I/wnotibp ( 2044): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1030
04-26 17:26:16.939+0700 I/wnotibp ( 2044): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 1
04-26 17:26:16.939+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(73) > ::APP:: CHECK STATE : 8, 0, (null)
04-26 17:26:16.939+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-26 17:26:16.939+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(79) > ::APP:: CHECK DATA : 1 1 0000
04-26 17:26:16.939+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-26 17:26:16.989+0700 W/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-26 17:26:16.999+0700 I/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_get_active_app_id(1010) > [2099]
04-26 17:26:17.009+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-26 17:26:17.009+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1178) > [1035, 1, 0, 2, 0000]
04-26 17:26:17.009+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1179) > [0, 1, 0]
04-26 17:26:17.009+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1180) > [1, 0, 0, 0]
04-26 17:26:17.009+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-26 17:26:17.009+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: _view_create_detail_layout(3707) > wnotiboard_popup_vi_type: 2
04-26 17:26:17.009+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: _view_create_detail_layout(3712) > (1035, 1035)
04-26 17:26:17.009+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-26 17:26:17.019+0700 I/efl-extension( 2044): efl_extension_circle_surface.c: eext_circle_surface_layout_add(1290) > Put the surface[0xb9078cb0]'s widget[0xb90e2f40] to layout widget[0xb90e2dc8]
04-26 17:26:17.069+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: _view_create_genlist(3639) > (1035, 1035)
04-26 17:26:17.079+0700 I/efl-extension( 2044): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-26 17:26:17.099+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: _view_create_card_data(3073) > 0xb8e7c6b8, 0xb8e7c540, 0xb8e7c540
04-26 17:26:17.099+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb901f878 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:26:17.099+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb901f878 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-26 17:26:17.119+0700 E/EFL     ( 2044): elementary<2044> elm_genlist.c:7236 elm_genlist_item_item_class_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-26 17:26:17.119+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2044): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-26 17:26:17.119+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2044): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-26 17:26:17.119+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2044): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-26 17:26:17.119+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2044): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-26 17:26:17.129+0700 W/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: stack.separator
04-26 17:26:17.129+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-26 17:26:17.129+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.129+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.129+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.129+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-26 17:26:17.129+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.129+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.129+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.129+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.129+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:26:17.139+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:26:17.139+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-26 17:26:17.139+0700 E/EFL     ( 2044): elementary<2044> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-26 17:26:17.139+0700 W/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(224) > can't get layout
04-26 17:26:17.139+0700 E/EFL     ( 2044): elementary<2044> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-26 17:26:17.139+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:26:17.139+0700 I/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1035, 82, -1016.
04-26 17:26:17.149+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2044): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-26 17:26:17.149+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2044): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-26 17:26:17.149+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2044): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-26 17:26:17.149+0700 E/TIZEN_N_SYSTEM_SETTINGS( 2044): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-26 17:26:17.179+0700 E/EFL     ( 2044): evas_main<2044> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-26 17:26:17.179+0700 E/EFL     ( 2044): evas_main<2044> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-26 17:26:17.179+0700 E/EFL     ( 2044): evas_main<2044> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-26 17:26:17.179+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-26 17:26:17.179+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.179+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.179+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.179+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-26 17:26:17.179+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.179+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.179+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:26:17.179+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:26:17.199+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:26:17.199+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:26:17.199+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-26 17:26:17.199+0700 W/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-26 17:26:17.199+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:26:17.199+0700 I/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1035, 82, -1016.
04-26 17:26:17.199+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb901f878 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:26:17.199+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb901f878 : elm_genlist] cw(360), ch(360), pw(360), ph(360)
04-26 17:26:17.209+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb901f878 : elm_genlist] mx(0), my(52), minx(0), miny(0), px(0), py(0)
04-26 17:26:17.209+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb901f878 : elm_genlist] cw(360), ch(412), pw(360), ph(360)
04-26 17:26:17.209+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: wnbp_view_draw_small_view(4060) > ::UI:: window type is changed by unknown causes
04-26 17:26:17.219+0700 E/EFL     ( 2099): ecore_x<2099> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=146483 button=1
04-26 17:26:17.219+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] mouse move
04-26 17:26:17.239+0700 W/APP_CORE( 2044): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3800009
04-26 17:26:17.239+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] mouse move
04-26 17:26:17.239+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] hold(0), freeze(0)
04-26 17:26:17.249+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] mouse move
04-26 17:26:17.249+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] hold(0), freeze(0)
04-26 17:26:17.259+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] mouse move
04-26 17:26:17.259+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] hold(0), freeze(0)
04-26 17:26:17.269+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] mouse move
04-26 17:26:17.269+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] hold(0), freeze(0)
04-26 17:26:17.279+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(263) > fully_obscured: 0, 0
04-26 17:26:17.279+0700 I/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x3800009 0x380000d 0x3800009]
04-26 17:26:17.279+0700 I/APP_CORE( 2044): appcore-efl.c: __do_app(453) > [APP 2044] Event: RESUME State: PAUSED
04-26 17:26:17.279+0700 I/CAPI_APPFW_APPLICATION( 2044): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-26 17:26:17.279+0700 I/wnotibp ( 2044): wnotiboard-popup.c: _popup_app_resume(229) > 
04-26 17:26:17.279+0700 W/STARTER ( 1122): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2044] goes to (3)
04-26 17:26:17.279+0700 W/AUL_AMD (  969): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-26 17:26:17.279+0700 W/AUL_AMD (  969): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-26 17:26:17.279+0700 W/AUL     (  969): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.wnotiboard-popup) pid(2044) status(fg) type(uiapp)
04-26 17:26:17.279+0700 I/GATE    ( 2044): <GATE-M>APP_FULLY_LOADED_wnotiboard-popup</GATE-M>
04-26 17:26:17.299+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] mouse move
04-26 17:26:17.299+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] hold(0), freeze(0)
04-26 17:26:17.299+0700 W/wnotibp ( 2044): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2809) > ::UI:: VI TYPE : 2
04-26 17:26:17.299+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-26 17:26:17.299+0700 W/wnotibp ( 2044): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2816) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-26 17:26:17.299+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-26 17:26:17.299+0700 I/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_play_feedback(284) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-26 17:26:17.299+0700 W/TIZEN_N_RECORDER( 2044): recorder_product.c: recorder_is_in_recording(82) > pid =/proc/0 , state =0, alive=0
04-26 17:26:17.299+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_play_feedback(298) > ::APP:: application_id: -1016, is_disaster: 0, disaster_info: (null), app_feedback_type: 2, feedback_pattern_app: -1, is_recording: 0
04-26 17:26:17.299+0700 I/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_play_feedback(307) > Gear side feedback setting is_sound_on: 0, is_vibration_on: 1, is_vibrate_when_noti_on: 0
04-26 17:26:17.299+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_play_feedback(341) > ::APP:: Determined feedback: sound 0, vibration: 1
04-26 17:26:17.299+0700 I/wnotib  ( 2044): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 13, ret: -3, request_id: 0
04-26 17:26:17.299+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_set_smart_relay(501) > Set the smart relay for 0, 82, -1016, Real Feed Back
04-26 17:26:17.299+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-26 17:26:17.299+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_turn_on_lcd(534) > [4, 0, 1, 0]
04-26 17:26:17.319+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] mouse move
04-26 17:26:17.319+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] hold(0), freeze(0)
04-26 17:26:17.349+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(585) > ::UI:: [[[ ===> already [small popup] is LOCK, 0010 ]]]
04-26 17:26:17.349+0700 W/wnotibp ( 2044): wnotiboard-popup-view.c: _view_sub_popup_show_animator_pre_cb(1781) > ::UI:: start showing animation
04-26 17:26:17.359+0700 E/wnoti-service( 1447): wnoti-db-client.c: wnoti_get_alert_categories(1016) > _query_step failed(NO ROW)
04-26 17:26:17.359+0700 E/EFL     ( 2099): ecore_x<2099> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=146604 button=1
04-26 17:26:17.359+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] mouse move
04-26 17:26:17.359+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7f19e50 : elm_genlist] hold(0), freeze(0)
04-26 17:26:17.359+0700 E/wnoti-service( 1447): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 2
04-26 17:26:17.369+0700 E/wnoti-service( 1447): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-26 17:26:17.369+0700 E/wnoti-proxy( 2044): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-26 17:26:17.369+0700 E/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(311) > ::DATA:: No categories available.
04-26 17:26:17.369+0700 W/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 1, new_list count : 0
04-26 17:26:17.399+0700 I/efl-extension( 2099): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-26 17:26:17.399+0700 I/efl-extension( 2099): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-26 17:26:17.399+0700 I/efl-extension( 2099): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb814e578, elm_image, _activated_obj : 0xb7ffebb8, activated : 1
04-26 17:26:17.399+0700 I/efl-extension( 2099): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-26 17:26:17.419+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8006420 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-26 17:26:17.419+0700 E/EFL     ( 2099): elementary<2099> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8006420 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-26 17:26:17.429+0700 E/weather-agent( 2186): AgentLaunchScenarioReal.cpp: Launch(350) > [0;40;31mweather info handle option:178[0;m
04-26 17:26:17.429+0700 I/LOCATION( 2186): location.c: location_add_setting_notify(867) > location_add_setting_notify method [0]
04-26 17:26:17.429+0700 I/LOCATION( 2186): location.c: __get_location_setting_value_from_method(202) > Remote is [Disconnected]. Method[0] is [On]
04-26 17:26:17.429+0700 E/weather-common( 2186): Engine.cpp: RequestUpdateNecessaryWeatherInfo(494) > [0;40;31m[1/1]locationId : e0ae79d8e25f1902b7edbb7c162fdd27b2def8c7cf049d7bd71824fa00491abe[0;m
04-26 17:26:17.429+0700 E/weather-common( 2186): Engine.cpp: RequestUpdateNecessaryWeatherInfo(503) > [0;40;31mno need to request for location id(e0ae79d8e25f1902b7edbb7c162fdd27b2def8c7cf049d7bd71824fa00491abe) [0;m
04-26 17:26:17.429+0700 E/weather-common( 2186): Engine.cpp: RequestUpdateNecessaryWeatherInfo(509) > [0;40;31mcityRequested is false[0;m
04-26 17:26:17.429+0700 E/weather-agent( 2186): AgentController.cpp: OnRequestFailed(784) > [0;40;31mOnRequestFailed[0;m
04-26 17:26:17.429+0700 E/weather-agent( 2186): AgentController.cpp: ShareCurrentWeatherInfo(666) > [0;40;31minvalid cityPtr[0;m
04-26 17:26:17.429+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: ShareWeatherInfo(2141) > [0;40;31mcityPtrVector is NULL[0;m
04-26 17:26:17.429+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: AddWeatherInfoToBundle(995) > [0;40;31m[AddWeatherInfoToBundle(): 995] (cityPtrVector->size() == 0) [return][0;m
04-26 17:26:17.429+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: AddSettingValueToBundle(972) > [0;40;31mlastRefreshedTime : 1524309436[0;m
04-26 17:26:17.429+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: SendBundleViaMessagePort(1108) > [0;40;31mhandleOption:178[0;m
04-26 17:26:17.429+0700 W/CAPI_APPFW_APP_CONTROL( 2186): app_control.c: app_control_error(136) > [app_control_get_extra_data] KEY_NOT_FOUND(0xffffff82)
04-26 17:26:17.429+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: SendBundleViaMessagePort(1119) > [0;40;31mapp_control_get_extra_data(portId) failed[0;m
04-26 17:26:17.429+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnConnectionRequest(411) > _MessagePortIpcServer::OnConnectionRequest
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcClientConnected(172) > MessagePort Ipc connected
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnConnectionRequest(411) > _MessagePortIpcServer::OnConnectionRequest
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.weather:com.samsung.weather.message.port.local]
04-26 17:26:17.479+0700 E/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(347) > _MessagePortService::SendMessage: Failed :MESSAGEPORT_ERROR_MESSAGEPORT_NOT_FOUND
04-26 17:26:17.479+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:17.509+0700 E/MESSAGE_PORT( 2186): MessagePortProxy.cpp: SendMessageInternal(533) > The remote message port is not found.
04-26 17:26:17.509+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: SendBundleViaMessagePort(1150) > [0;40;31mmessage_port_send_message failed [com.samsung.weather, com.samsung.weather.message.port.local][0;m
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.weather-widget:com.samsung.weather-widget.message.port.local]
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-26 17:26:17.509+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:17.509+0700 E/weather-widget( 1489): WidgetMain.cpp: MessagePortCallback(246) > [0;40;31mMessagePortCallback[0;m
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.weather-widget-setting:com.samsung.weather-widget-setting.message.port.local]
04-26 17:26:17.529+0700 E/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(347) > _MessagePortService::SendMessage: Failed :MESSAGEPORT_ERROR_MESSAGEPORT_NOT_FOUND
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:17.529+0700 E/MESSAGE_PORT( 2186): MessagePortProxy.cpp: SendMessageInternal(533) > The remote message port is not found.
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: SendBundleViaMessagePort(1157) > [0;40;31mmessage_port_send_message failed [com.samsung.weather-widget-setting, com.samsung.weather-widget-setting.message.port.local][0;m
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.weather:com.samsung.weather.message.port.local_for_add]
04-26 17:26:17.529+0700 E/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(347) > _MessagePortService::SendMessage: Failed :MESSAGEPORT_ERROR_MESSAGEPORT_NOT_FOUND
04-26 17:26:17.529+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:17.529+0700 E/MESSAGE_PORT( 2186): MessagePortProxy.cpp: SendMessageInternal(533) > The remote message port is not found.
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: SendBundleViaMessagePort(1160) > [0;40;31mmessage_port_send_message failed [com.samsung.weather, com.samsung.weather.message.port.local_for_add][0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(121) > [0;40;31mresult : 1[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(124) > [0;40;31mtemperatureUnit : c[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(125) > [0;40;31mcpType : TWC[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(126) > [0;40;31mshowCurrentCity : off[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(129) > [0;40;31mcityName : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(130) > [0;40;31mcityNameEng : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(133) > [0;40;31mcurrentTemperature : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(134) > [0;40;31mhighTemperature 	  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(135) > [0;40;31mlowTemperature 	  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(136) > [0;40;31miconID		  	  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(137) > [0;40;31mrefreshedTime 	  : 1524309436[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(138) > [0;40;31misCurrentLocation  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(140) > [0;40;31mWeatherStatus	  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(141) > [0;40;31mWeatherLongStatus  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(142) > [0;40;31mWindSpeed 	 	  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(143) > [0;40;31mWindSpeedUnit 	  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(144) > [0;40;31mWindDirection 	  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(145) > [0;40;31mWindCardinal 	  : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(149) > [0;40;31mPM10 : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(150) > [0;40;31mPM25 : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(151) > [0;40;31mUVIndex : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(152) > [0;40;31mAQI : [0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(153) > [0;40;31mPressure : [0;m
04-26 17:26:17.529+0700 E/weather-agent( 2186): AgentMain.cpp: StartServiceTerminateTimer(134) > [0;40;31mthere is same appControl. we should erase it from mAgentControllerPtrVector[0;m
04-26 17:26:17.529+0700 E/weather-agent( 2186): AgentMain.cpp: StartServiceTerminateTimer(152) > [0;40;31mStart ServiceTerminateTimer. Wait ...[0;m
04-26 17:26:17.529+0700 E/weather-common( 2186): Location.cpp: RemoveEventListener(470) > [0;40;31meventListener:0xb76ea458 not exist[0;m
04-26 17:26:17.559+0700 E/weather-common( 1489): CPType.cpp: Renew(90) > [0;40;31mCPType is renewed : 5[0;m
04-26 17:26:17.559+0700 E/weather-common( 1489): DataManager.cpp: LoadData(326) > [0;40;31mnewCpTypeStr : TWC[0;m
04-26 17:26:17.569+0700 E/weather-common( 1489): DataManager.cpp: LoadData(329) > [0;40;31mweather data loaded[0;m
04-26 17:26:17.569+0700 E/weather-widget( 1489): WidgetViewData.cpp: GetWidgetLifeCycleState(389) > [0;40;31mGetWidgetLifeCycleState, mWidgetLifeCycleState:4[0;m
04-26 17:26:17.639+0700 E/weather-widget( 1489): WidgetMain.cpp: RequestUpdateForEachInstances(138) > [0;40;31mrequest widget_service_trigger_update to /opt/usr/share/live_magazine/com.samsung.weather-widget_1203_13.878397.png[0;m
04-26 17:26:17.649+0700 W/wnotibp ( 2044): wnotiboard-popup-view.c: _view_sub_popup_show_animator_cb(1684) > ::UI:: end show animation
04-26 17:26:17.649+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-26 17:26:17.669+0700 E/weather-widget( 1489): WidgetMain.cpp: UpdateWidgetInstance(700) > [0;40;31mUpdateWidgetInstance[0;m
04-26 17:26:17.689+0700 W/CRASH_MANAGER( 2079): worker.c: worker_job(1205) > 1102099726561152473837
