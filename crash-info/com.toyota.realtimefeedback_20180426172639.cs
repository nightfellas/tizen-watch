S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2180
Date: 2018-04-26 17:26:39+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2180, uid 5000)

Register Information
r0   = 0x000000b4, r1   = 0x00000000
r2   = 0xb58831a0, r3   = 0xb167b0bc
r4   = 0xbec64444, r5   = 0xb5b95800
r6   = 0x00000274, r7   = 0xbec64098
r8   = 0x00000000, r9   = 0xffffffff
r10  = 0xb6c388e4, fp   = 0xb6c388e4
ip   = 0x00000000, sp   = 0xbec64030
lr   = 0xb586f4bd, pc   = 0xb586f4c4
cpsr = 0x80000030

Memory Information
MemTotal:   405512 KB
MemFree:      2892 KB
Buffers:      8588 KB
Cached:     111804 KB
VmPeak:     103840 KB
VmSize:     101676 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       24036 KB
VmRSS:       24036 KB
VmData:      41444 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          68 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2180 TID = 2180
2180 2230 2262 

Maps Information
b1f70000 b1f74000 r-xp /usr/lib/libogg.so.0.7.1
b1f7c000 b1f9e000 r-xp /usr/lib/libvorbis.so.0.4.3
b1fa6000 b1fed000 r-xp /usr/lib/libsndfile.so.1.0.26
b1ff9000 b2042000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b204b000 b2050000 r-xp /usr/lib/libjson.so.0.0.1
b38f1000 b39f7000 r-xp /usr/lib/libicuuc.so.57.1
b3a0d000 b3b95000 r-xp /usr/lib/libicui18n.so.57.1
b3ba5000 b3bb2000 r-xp /usr/lib/libail.so.0.1.0
b3bbb000 b3bbe000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3bc6000 b3bfe000 r-xp /usr/lib/libpulse.so.0.16.2
b3bff000 b3c02000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3c0a000 b3c6b000 r-xp /usr/lib/libasound.so.2.0.0
b3c75000 b3c8e000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c97000 b3c9b000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3ca3000 b3cae000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3cbb000 b3cbf000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3cc8000 b3d4f000 rw-s anon_inode:dmabuf
b3d4f000 b3dd6000 rw-s anon_inode:dmabuf
b3dd6000 b3dd9000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3de1000 b3e68000 rw-s anon_inode:dmabuf
b3e68000 b3eef000 rw-s anon_inode:dmabuf
b3ef0000 b46ef000 rw-p [stack:2262]
b4881000 b4899000 r-xp /usr/lib/libmmfsound.so.0.1.0
b48aa000 b48b1000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b48b9000 b48c4000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b48cc000 b48ce000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b48d6000 b48d7000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b48df000 b48e7000 r-xp /usr/lib/libfeedback.so.0.1.4
b4a08000 b4a09000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b4a26000 b4a27000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b4a41000 b4a42000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4aca000 b52c9000 rw-p [stack:2230]
b52c9000 b52cb000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52d3000 b52ea000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52f7000 b52f9000 r-xp /usr/lib/libdri2.so.0.0.0
b5301000 b530c000 r-xp /usr/lib/libtbm.so.1.0.0
b5314000 b531c000 r-xp /usr/lib/libdrm.so.2.4.0
b5324000 b5326000 r-xp /usr/lib/libgenlock.so
b532e000 b5333000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b533b000 b5346000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b554f000 b5619000 r-xp /usr/lib/libCOREGL.so.4.0
b562a000 b563a000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5642000 b5648000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5650000 b5651000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b565a000 b565d000 r-xp /usr/lib/libEGL.so.1.4
b5665000 b5673000 r-xp /usr/lib/libGLESv2.so.2.0
b567c000 b56c5000 r-xp /usr/lib/libmdm.so.1.2.70
b56ce000 b56d4000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56dc000 b56e5000 r-xp /usr/lib/libcom-core.so.0.0.1
b56ee000 b57a6000 r-xp /usr/lib/libcairo.so.2.11200.14
b57b1000 b57ca000 r-xp /usr/lib/libnetwork.so.0.0.0
b57d2000 b57de000 r-xp /usr/lib/libnotification.so.0.1.0
b57e7000 b57f6000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b57ff000 b5820000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5828000 b582d000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5835000 b583a000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b5842000 b5852000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b585a000 b5862000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b586a000 b5874000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a19000 b5a23000 r-xp /lib/libnss_files-2.13.so
b5a2c000 b5afb000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b11000 b5b35000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b3e000 b5b44000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b4c000 b5b50000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b5d000 b5b68000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b70000 b5b72000 r-xp /usr/lib/libiniparser.so.0
b5b7b000 b5b80000 r-xp /usr/lib/libappcore-common.so.1.1
b5b88000 b5b8a000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b93000 b5b97000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5ba4000 b5ba6000 r-xp /usr/lib/libXau.so.6.0.0
b5bae000 b5bb5000 r-xp /lib/libcrypt-2.13.so
b5be5000 b5be7000 r-xp /usr/lib/libiri.so
b5bf0000 b5d82000 r-xp /usr/lib/libcrypto.so.1.0.0
b5da3000 b5dea000 r-xp /usr/lib/libssl.so.1.0.0
b5df6000 b5e24000 r-xp /usr/lib/libidn.so.11.5.44
b5e2c000 b5e35000 r-xp /usr/lib/libcares.so.2.1.0
b5e3f000 b5e52000 r-xp /usr/lib/libxcb.so.1.1.0
b5e5b000 b5e5e000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e66000 b5e68000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e71000 b5f3d000 r-xp /usr/lib/libxml2.so.2.7.8
b5f4a000 b5f4c000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f55000 b5f5a000 r-xp /usr/lib/libffi.so.5.0.10
b5f62000 b5f63000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f6b000 b5f6e000 r-xp /lib/libattr.so.1.1.0
b5f76000 b600a000 r-xp /usr/lib/libstdc++.so.6.0.16
b601d000 b603a000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6044000 b605c000 r-xp /usr/lib/libpng12.so.0.50.0
b6064000 b607a000 r-xp /lib/libexpat.so.1.6.0
b6084000 b60c8000 r-xp /usr/lib/libcurl.so.4.3.0
b60d1000 b60db000 r-xp /usr/lib/libXext.so.6.4.0
b60e5000 b60e9000 r-xp /usr/lib/libXtst.so.6.1.0
b60f1000 b60f7000 r-xp /usr/lib/libXrender.so.1.3.0
b60ff000 b6105000 r-xp /usr/lib/libXrandr.so.2.2.0
b610d000 b610e000 r-xp /usr/lib/libXinerama.so.1.0.0
b6117000 b6120000 r-xp /usr/lib/libXi.so.6.1.0
b6128000 b612b000 r-xp /usr/lib/libXfixes.so.3.1.0
b6134000 b6136000 r-xp /usr/lib/libXgesture.so.7.0.0
b613e000 b6140000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6148000 b614a000 r-xp /usr/lib/libXdamage.so.1.1.0
b6152000 b6159000 r-xp /usr/lib/libXcursor.so.1.0.2
b6161000 b6164000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b616d000 b6171000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b617a000 b617f000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6188000 b6269000 r-xp /usr/lib/libX11.so.6.3.0
b6274000 b6297000 r-xp /usr/lib/libjpeg.so.8.0.2
b62af000 b62c5000 r-xp /lib/libz.so.1.2.5
b62ce000 b62d0000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62d8000 b634d000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6357000 b6371000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6379000 b63ad000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63b6000 b6489000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6495000 b64a5000 r-xp /lib/libresolv-2.13.so
b64a9000 b64c1000 r-xp /usr/lib/liblzma.so.5.0.3
b64c9000 b64cc000 r-xp /lib/libcap.so.2.21
b64d4000 b6503000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b650b000 b650c000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6515000 b651b000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6523000 b653a000 r-xp /usr/lib/liblua-5.1.so
b6543000 b654a000 r-xp /usr/lib/libembryo.so.1.7.99
b6552000 b6558000 r-xp /lib/librt-2.13.so
b6561000 b65b7000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65c5000 b661b000 r-xp /usr/lib/libfreetype.so.6.11.3
b6627000 b664f000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6650000 b6695000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b669e000 b66b1000 r-xp /usr/lib/libfribidi.so.0.3.1
b66b9000 b66d3000 r-xp /usr/lib/libecore_con.so.1.7.99
b66dd000 b66e6000 r-xp /usr/lib/libedbus.so.1.7.99
b66ee000 b673e000 r-xp /usr/lib/libecore_x.so.1.7.99
b6740000 b6749000 r-xp /usr/lib/libvconf.so.0.2.45
b6751000 b6762000 r-xp /usr/lib/libecore_input.so.1.7.99
b676a000 b676f000 r-xp /usr/lib/libecore_file.so.1.7.99
b6777000 b6799000 r-xp /usr/lib/libecore_evas.so.1.7.99
b67a2000 b67e3000 r-xp /usr/lib/libeina.so.1.7.99
b67ec000 b6805000 r-xp /usr/lib/libeet.so.1.7.99
b6816000 b687f000 r-xp /lib/libm-2.13.so
b6888000 b688e000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6897000 b6898000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b68a0000 b68c3000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68cb000 b68d0000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68d8000 b6902000 r-xp /usr/lib/libdbus-1.so.3.8.12
b690b000 b6922000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b692a000 b6935000 r-xp /lib/libunwind.so.8.0.1
b6962000 b6980000 r-xp /usr/lib/libsystemd.so.0.4.0
b698a000 b6aae000 r-xp /lib/libc-2.13.so
b6abc000 b6ac4000 r-xp /lib/libgcc_s-4.6.so.1
b6ac5000 b6ac9000 r-xp /usr/lib/libsmack.so.1.0.0
b6ad2000 b6ad8000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ae0000 b6bb0000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6bb1000 b6c0f000 r-xp /usr/lib/libedje.so.1.7.99
b6c19000 b6c30000 r-xp /usr/lib/libecore.so.1.7.99
b6c47000 b6d15000 r-xp /usr/lib/libevas.so.1.7.99
b6d3b000 b6e77000 r-xp /usr/lib/libelementary.so.1.7.99
b6e8e000 b6ea2000 r-xp /lib/libpthread-2.13.so
b6ead000 b6eaf000 r-xp /usr/lib/libdlog.so.0.0.0
b6eb7000 b6eba000 r-xp /usr/lib/libbundle.so.0.1.22
b6ec2000 b6ec4000 r-xp /lib/libdl-2.13.so
b6ecd000 b6eda000 r-xp /usr/lib/libaul.so.0.1.0
b6eec000 b6ef2000 r-xp /usr/lib/libappcore-efl.so.1.1
b6efb000 b6eff000 r-xp /usr/lib/libsys-assert.so
b6f08000 b6f25000 r-xp /lib/ld-2.13.so
b6f2e000 b6f33000 r-xp /usr/bin/launchpad-loader
b8be9000 b96ee000 rw-p [heap]
bec44000 bec65000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2180)
Call Stack Count: 15
 0: loadAllNotification + 0x133 (0xb586f4c4) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x54c4
 1: _on_resume_cb + 0x22 (0xb586f2af) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x52af
 2: (0xb5b946ad) [/usr/lib/libcapi-appfw-application.so.0] + 0x16ad
 3: (0xb6eef151) [/usr/lib/libappcore-efl.so.1] + 0x3151
 4: (0xb6eef527) [/usr/lib/libappcore-efl.so.1] + 0x3527
 5: (0xb6c21e53) [/usr/lib/libecore.so.1] + 0x8e53
 6: (0xb6c2546b) [/usr/lib/libecore.so.1] + 0xc46b
 7: ecore_main_loop_begin + 0x30 (0xb6c25879) [/usr/lib/libecore.so.1] + 0xc879
 8: appcore_efl_main + 0x332 (0xb6eefb47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
 9: ui_app_main + 0xb0 (0xb5b94ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
10: uib_app_run + 0xea (0xb586f227) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5227
11: main + 0x34 (0xb586f611) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5611
12:  + 0x0 (0xb6f2fa53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
13: __libc_start_main + 0x114 (0xb69a185c) [/lib/libc.so.6] + 0x1785c
14: (0xb6f2fe0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
ion_x(1), direction_y(1)
04-26 17:26:29.679+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] drag_child_locked_x(0)
04-26 17:26:29.679+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] drag_child_locked_y(0)
04-26 17:26:29.679+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] move content x(-7), y(68)
04-26 17:26:29.679+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] direction_x(1), direction_y(1)
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] drag_child_locked_x(0)
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] drag_child_locked_y(0)
04-26 17:26:29.719+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] move content x(-7), y(85)
04-26 17:26:29.719+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] direction_x(1), direction_y(1)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] drag_child_locked_x(0)
04-26 17:26:29.789+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] drag_child_locked_y(0)
04-26 17:26:29.799+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] move content x(-6), y(106)
04-26 17:26:29.799+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] direction_x(1), direction_y(1)
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] drag_child_locked_x(0)
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] drag_child_locked_y(0)
04-26 17:26:29.839+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8d17d10 : elm_genlist] move content x(-2), y(114)
04-26 17:26:29.839+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:29.929+0700 E/EFL     ( 2180): ecore_x<2180> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=159168 button=1
04-26 17:26:29.929+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.929+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.929+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.929+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.929+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.929+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.929+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:29.929+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:29.929+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:29.989+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.029+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.069+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.109+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.339+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.399+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.429+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.449+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.469+0700 E/EFL     ( 2180): ecore_x<2180> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=159717 button=1
04-26 17:26:30.469+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:30.469+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:30.469+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:30.469+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.479+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:30.479+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:30.479+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.509+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:30.509+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:30.509+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:30.509+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:30.509+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:30.509+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:30.509+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.539+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:30.579+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:30.579+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:30.589+0700 E/EFL     ( 2180): ecore_x<2180> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=159859 button=1
04-26 17:26:30.859+0700 E/GL SELECTED( 2180): ID GL : TRM2
04-26 17:26:31.599+0700 E/EFL     (  935): ecore_x<935> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3c00002 time=159859
04-26 17:26:31.599+0700 E/EFL     ( 2180): ecore_x<2180> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=159859
04-26 17:26:31.599+0700 E/EFL     (  935): ecore_x<935> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=159859
04-26 17:26:33.339+0700 E/EFL     ( 2180): ecore_x<2180> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=162605 button=1
04-26 17:26:33.339+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:33.349+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:33.349+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:33.359+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:33.359+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:33.369+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:33.369+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:33.379+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:33.379+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:33.389+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:33.389+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:33.429+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:33.429+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:33.479+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] mouse move
04-26 17:26:33.479+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8d17d10 : elm_genlist] hold(0), freeze(0)
04-26 17:26:33.489+0700 E/EFL     ( 2180): ecore_x<2180> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=162757 button=1
04-26 17:26:35.249+0700 E/EFL     ( 2180): ecore_x<2180> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=164513 button=1
04-26 17:26:35.399+0700 E/EFL     ( 2180): ecore_x<2180> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=164665 button=1
04-26 17:26:35.409+0700 I/efl-extension( 2180): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-26 17:26:35.409+0700 I/efl-extension( 2180): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-26 17:26:35.409+0700 I/efl-extension( 2180): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8ee78f8, elm_image, _activated_obj : 0xb4909bb0, activated : 1
04-26 17:26:35.409+0700 I/efl-extension( 2180): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-26 17:26:35.409+0700 E/EFL     ( 2180): elementary<2180> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-26 17:26:35.419+0700 E/EFL     ( 2180): elementary<2180> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb8ee7d60) will be pushed
04-26 17:26:35.419+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:35.419+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:35.419+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8dd14f8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:26:35.419+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8dd14f8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-26 17:26:35.429+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8dd14f8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:26:35.429+0700 E/EFL     ( 2180): elementary<2180> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8dd14f8 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-26 17:26:35.459+0700 E/EFL     ( 2180): elementary<2180> elc_naviframe.c:2796 _push_transition_cb() item(0xb8ee7d60) will transition
04-26 17:26:36.249+0700 E/EFL     ( 2180): <2180> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-26 17:26:36.279+0700 E/EFL     ( 2180): elementary<2180> elc_naviframe.c:1193 _on_item_push_finished() item(0xb8d2e7f8) transition finished
04-26 17:26:36.279+0700 E/EFL     ( 2180): elementary<2180> elc_naviframe.c:1218 _on_item_show_finished() item(0xb8ee7d60) transition finished
04-26 17:26:37.599+0700 I/CAPI_NETWORK_CONNECTION( 2180): connection.c: connection_create(453) > New handle created[0xb8ede440]
04-26 17:26:38.589+0700 E/WMS     ( 1041): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-26 17:26:38.909+0700 W/WATCH_CORE( 1346): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
04-26 17:26:38.909+0700 I/WATCH_CORE( 1346): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
04-26 17:26:38.909+0700 I/CAPI_WATCH_APPLICATION( 1346): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-26 17:26:38.909+0700 E/watchface-app( 1346): watchface.cpp: OnAppTimeTick(1157) > 
04-26 17:26:38.909+0700 I/watchface-app( 1346): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-26 17:26:39.009+0700 W/WATCH_CORE( 1346): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOff
04-26 17:26:39.019+0700 W/W_HOME  ( 1203): dbus.c: _dbus_message_recv_cb(204) > LCD off
04-26 17:26:39.019+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_cancel_schedule(226) > cancel schedule, manual render
04-26 17:26:39.019+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-26 17:26:39.019+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_enable(138) > 1
04-26 17:26:39.019+0700 W/W_HOME  ( 1203): event_manager.c: _lcd_off_cb(723) > lcd state: 0
04-26 17:26:39.019+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(0) pm_state:0 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
04-26 17:26:39.019+0700 W/STARTER ( 1122): clock-mgr.c: _on_lcd_signal_receive_cb(1284) > [_on_lcd_signal_receive_cb:1284] _on_lcd_signal_receive_cb, 1284, Pre LCD off by [gesture]
04-26 17:26:39.019+0700 W/STARTER ( 1122): clock-mgr.c: _pre_lcd_off(1089) > [_pre_lcd_off:1089] Will LCD OFF as wake_up_setting[1]
04-26 17:26:39.019+0700 W/WAKEUP-SERVICE( 1720): WakeupService.cpp: OnReceiveDisplayChanged(979) > [0;32mINFO: LCDOff receive data : -1226847476[0;m
04-26 17:26:39.019+0700 W/WAKEUP-SERVICE( 1720): WakeupService.cpp: OnReceiveDisplayChanged(980) > [0;32mINFO: WakeupServiceStop[0;m
04-26 17:26:39.019+0700 W/WAKEUP-SERVICE( 1720): WakeupService.cpp: WakeupServiceStop(399) > [0;32mINFO: SEAMLESS WAKEUP STOP REQUEST[0;m
04-26 17:26:39.019+0700 E/STARTER ( 1122): scontext_util.c: sconstext_util_check_lock_type(47) > [sconstext_util_check_lock_type:47] current lock state :[0],testmode[0]
04-26 17:26:39.019+0700 E/STARTER ( 1122): scontext_util.c: scontext_util_handle_lock_state(72) > [scontext_util_handle_lock_state:72] wear state[0],bPossible [0],usage [0]
04-26 17:26:39.019+0700 W/STARTER ( 1122): clock-mgr.c: _check_reserved_popup_status(211) > [_check_reserved_popup_status:211] Current reserved apps status : 0
04-26 17:26:39.019+0700 W/STARTER ( 1122): clock-mgr.c: _check_reserved_apps_status(247) > [_check_reserved_apps_status:247] Current reserved apps status : 0
04-26 17:26:39.049+0700 E/WAKEUP-SERVICE( 1720): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-26 17:26:39.049+0700 E/WAKEUP-SERVICE( 1720): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-26 17:26:39.049+0700 I/TIZEN_N_SOUND_MANAGER( 1720): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Disable start
04-26 17:26:39.089+0700 I/TIZEN_N_SOUND_MANAGER( 1720): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Disable end. (ret: 0x0)
04-26 17:26:39.089+0700 W/TIZEN_N_SOUND_MANAGER( 1720): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-26 17:26:39.089+0700 W/WAKEUP-SERVICE( 1720): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 0[0;m
04-26 17:26:39.089+0700 I/HIGEAR  ( 1720): WakeupService.cpp: WakeupServiceStop(403) > [svoice:Application:WakeupServiceStop:IN]
04-26 17:26:39.119+0700 W/W_INDICATOR( 1124): windicator_util.c: _pm_state_changed_cb(917) > [_pm_state_changed_cb:917] LCD off
04-26 17:26:39.119+0700 W/W_INDICATOR( 1124): windicator_connection.c: windicator_connection_pause(2268) > [windicator_connection_pause:2268] 
04-26 17:26:39.119+0700 W/W_INDICATOR( 1124): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-26 17:26:39.119+0700 W/SHealthCommon( 1496): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
04-26 17:26:39.129+0700 W/SHealthCommon( 1815): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
04-26 17:26:39.129+0700 W/SHealthServiceCommon( 1815): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
04-26 17:26:39.129+0700 W/STARTER ( 1122): clock-mgr.c: _on_lcd_signal_receive_cb(1297) > [_on_lcd_signal_receive_cb:1297] _on_lcd_signal_receive_cb, 1297, Post LCD off by [gesture]
04-26 17:26:39.129+0700 W/STARTER ( 1122): clock-mgr.c: _post_lcd_off(1190) > [_post_lcd_off:1190] LCD OFF as reserved app[(null)] alpm mode[0]
04-26 17:26:39.129+0700 W/STARTER ( 1122): clock-mgr.c: _post_lcd_off(1197) > [_post_lcd_off:1197] Current reserved apps status : 0
04-26 17:26:39.129+0700 W/STARTER ( 1122): clock-mgr.c: _post_lcd_off(1215) > [_post_lcd_off:1215] raise homescreen after 20 sec. home_visible[0]
04-26 17:26:39.129+0700 E/ALARM_MANAGER( 1122): alarm-lib.c: alarmmgr_add_alarm_withcb(1178) > trigger_at_time(20), start(26-4-2018, 17:26:59), repeat(1), interval(20), type(-1073741822)
04-26 17:26:39.129+0700 W/W_INDICATOR( 1124): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(620) > [_windicator_dbus_lcd_off_completed_cb:620] LCD Off completed signal is received
04-26 17:26:39.129+0700 W/W_INDICATOR( 1124): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(625) > [_windicator_dbus_lcd_off_completed_cb:625] Moment bar status -> idle. (Hide Moment bar)
04-26 17:26:39.129+0700 W/W_INDICATOR( 1124): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-26 17:26:39.129+0700 I/APP_CORE( 2180): appcore-efl.c: __do_app(453) > [APP 2180] Event: PAUSE State: RUNNING
04-26 17:26:39.129+0700 I/CAPI_APPFW_APPLICATION( 2180): app_main.c: _ui_app_appcore_pause(611) > app_appcore_pause
04-26 17:26:39.129+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1122].
04-26 17:26:39.139+0700 W/APP_CORE( 2180): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3c00002] -> redirected win[600841] for com.toyota.realtimefeedback[2180]
04-26 17:26:39.179+0700 E/ALARM_MANAGER(  972): alarm-manager-schedule.c: _alarm_next_duetime(509) > alarm_id: 259477176, next duetime: 1524738419
04-26 17:26:39.179+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __alarm_add_to_list(496) > [alarm-server]: After add alarm_id(259477176)
04-26 17:26:39.179+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __alarm_create(1061) > [alarm-server]:alarm_context.c_due_time(1524762001), due_time(1524738419)
04-26 17:26:39.189+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-26 17:26:39.199+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-26 17:26:39.199+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 26-4-2018, 10:26:59 (UTC).
04-26 17:26:39.199+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-26 17:26:39.199+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-26 17:26:39.609+0700 W/W_HOME  ( 1203): dbus.c: _dbus_message_recv_cb(169) > gesture:wristup
04-26 17:26:39.609+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_schedule(209) > schedule, manual render
04-26 17:26:39.609+0700 W/WAKEUP-SERVICE( 1720): WakeupService.cpp: OnReceiveGestureChanged(995) > [0;32mINFO: wakeup receive data : -1217674728[0;m
04-26 17:26:39.609+0700 W/WAKEUP-SERVICE( 1720): WakeupService.cpp: OnReceiveGestureChanged(996) > [0;32mINFO: WakeupServiceStart[0;m
04-26 17:26:39.609+0700 W/WAKEUP-SERVICE( 1720): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
04-26 17:26:39.609+0700 W/WAKEUP-SERVICE( 1720): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
04-26 17:26:39.609+0700 I/TIZEN_N_SOUND_MANAGER( 1720): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
04-26 17:26:39.609+0700 W/WATCH_CORE( 1346): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
04-26 17:26:39.609+0700 I/WATCH_CORE( 1346): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
04-26 17:26:39.609+0700 I/CAPI_WATCH_APPLICATION( 1346): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-26 17:26:39.609+0700 E/watchface-app( 1346): watchface.cpp: OnAppTimeTick(1157) > 
04-26 17:26:39.609+0700 I/watchface-app( 1346): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-26 17:26:39.619+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_lcd_on_cb(93) > ::APP:: view state=0, 2, 0
04-26 17:26:39.619+0700 I/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_lcd_on_cb(141) > There is no additional work.
04-26 17:26:39.639+0700 W/WATCH_CORE( 1346): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOn
04-26 17:26:39.639+0700 I/WATCH_CORE( 1346): appcore-watch.c: __signal_lcd_status_handler(1250) > Call the time_tick_cb
04-26 17:26:39.639+0700 I/CAPI_WATCH_APPLICATION( 1346): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-26 17:26:39.639+0700 E/watchface-app( 1346): watchface.cpp: OnAppTimeTick(1157) > 
04-26 17:26:39.639+0700 I/watchface-app( 1346): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-26 17:26:39.639+0700 I/WATCH_CORE( 1346): appcore-watch.c: __signal_lcd_status_handler(1257) > Call widget_provider_update_event
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): dbus.c: _dbus_message_recv_cb(186) > LCD on
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_disable_timer_set(167) > timer set
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): gesture.c: _apps_status_get(128) > apps status:0
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): gesture.c: _lcd_on_cb(303) > move_to_clock:0 clock_visible:0 info->offtime:655
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_schedule(209) > schedule, manual render
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): event_manager.c: _lcd_on_cb(715) > lcd state: 1
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): gesture.c: _widget_updated_cb(188) > widget updated
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-26 17:26:39.639+0700 W/W_HOME  ( 1203): gesture.c: _manual_render(182) > 
04-26 17:26:39.639+0700 W/STARTER ( 1122): clock-mgr.c: _on_lcd_signal_receive_cb(1258) > [_on_lcd_signal_receive_cb:1258] _on_lcd_signal_receive_cb, 1258, Pre LCD on by [gesture] after screen off time [655]ms
04-26 17:26:39.639+0700 W/STARTER ( 1122): clock-mgr.c: _pre_lcd_on(1027) > [_pre_lcd_on:1027] Will LCD ON as reserved app[(null)] alpm mode[0]
04-26 17:26:39.639+0700 W/W_INDICATOR( 1124): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(538) > [_windicator_dbus_lcd_changed_cb:538] LCD ON signal is received
04-26 17:26:39.639+0700 W/W_INDICATOR( 1124): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(559) > [_windicator_dbus_lcd_changed_cb:559] 559, str=[gesture],charge : 0, connected : 0
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [starter:org.tizen.idled.ReservedApp]
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [starter:org.tizen.idled.ReservedApp]
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-26 17:26:39.639+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:39.649+0700 I/APP_CORE( 2180): appcore-efl.c: __do_app(453) > [APP 2180] Event: RESUME State: PAUSED
04-26 17:26:39.649+0700 I/CAPI_APPFW_APPLICATION( 2180): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-26 17:26:39.689+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1122].
04-26 17:26:39.689+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __alarm_remove_from_list(575) > [alarm-server]:Remove alarm id(259477176)
04-26 17:26:39.689+0700 E/ALARM_MANAGER(  972): alarm-manager-schedule.c: __find_next_alarm_to_be_scheduled(547) > The duetime of alarm(1286760160) is OVER.
04-26 17:26:39.699+0700 W/TIZEN_N_SOUND_MANAGER( 1720): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
04-26 17:26:39.719+0700 W/W_INDICATOR( 1124): windicator_util.c: _pm_state_changed_cb(912) > [_pm_state_changed_cb:912] LCD on
04-26 17:26:39.719+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(57) > [windicator_ongoing_info_shealth_update:57] windicator_shealth_update
04-26 17:26:39.719+0700 W/SHealthCommon( 1496): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
04-26 17:26:39.799+0700 I/HealthDataService( 1282): RequestHandler.cpp: OnHealthIpcMessageSync(123) > [1;35mServer Received: SHARE_GET[0;m
04-26 17:26:39.799+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-26 17:26:39.799+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-26 17:26:39.799+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 26-4-2018, 17:00:01 (UTC).
04-26 17:26:39.799+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-26 17:26:39.809+0700 W/WATCH_CORE( 1346): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
04-26 17:26:39.809+0700 I/WATCH_CORE( 1346): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
04-26 17:26:39.809+0700 I/CAPI_WATCH_APPLICATION( 1346): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-26 17:26:39.809+0700 E/watchface-app( 1346): watchface.cpp: OnAppTimeTick(1157) > 
04-26 17:26:39.809+0700 I/watchface-app( 1346): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-26 17:26:39.809+0700 E/wnoti-service( 1447): wnoti-db-utility.c: context_wearonoff_status_cb(1781) > status changed from 1 to 2 
04-26 17:26:39.809+0700 E/WMS     ( 1041): wms_event_handler.c: _wms_event_handler_cb_wearonoff_monitor(23513) > wear_monitor_status update[0] = 1 -> 2
04-26 17:26:39.809+0700 W/WECONN  (  958): <__wc_feature_wearonoff_monitor_cb:533> { error=0, state=CONTEXT_WEARONOFF_MONITOR_STATUS_OFF
04-26 17:26:39.809+0700 W/WECONN  (  958): <__wc_device_on_wear_onoff_changed_cb:363> { state=WC_FEATURE_STATE_OFF
04-26 17:26:39.809+0700 W/WECONN  (  958): <__wc_device_on_wear_onoff_changed_cb:393> }
04-26 17:26:39.809+0700 W/WECONN  (  958): <__wc_feature_wearonoff_monitor_cb:553> }
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(78) > [windicator_ongoing_info_shealth_update:78] Result : 0
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(99) > [windicator_ongoing_info_shealth_update:99] status : none
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(103) > [windicator_ongoing_info_shealth_update:103] application_id: 
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(112) > [windicator_ongoing_info_shealth_update:112] launch_operation : 
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(118) > [windicator_ongoing_info_shealth_update:118] extra_data_key : 
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(124) > [windicator_ongoing_info_shealth_update:124] extra_data_value : 
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(132) > [windicator_ongoing_info_shealth_update:132] image_path : 
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(135) > [windicator_ongoing_info_shealth_update:135] image_path_sub : 
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(138) > [windicator_ongoing_info_shealth_update:138] message_string :  
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(144) > [windicator_ongoing_info_shealth_update:144] [Update] SHealth status is none, so hide icon and text!
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info.c: windicator_ongoing_info_remove(191) > [windicator_ongoing_info_remove:191] Ongoing info type[1]
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_ongoing_info.c: windicator_ongoing_info_remove(191) > [windicator_ongoing_info_remove:191] Ongoing info type[2]
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-26 17:26:39.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:39.819+0700 E/EFL     ( 1124): <1124> elm_main.c:1622 elm_object_signal_emit() safety check failed: obj == NULL
04-26 17:26:39.819+0700 I/TIZEN_N_SOUND_MANAGER( 1124): sound_manager.c: sound_manager_get_volume(84) > returns : type=3, volume=11, ret=0x0
04-26 17:26:39.819+0700 W/TIZEN_N_SOUND_MANAGER( 1124): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_get_volume] ERROR_NONE (0x00000000)
04-26 17:26:39.819+0700 W/W_INDICATOR( 1124): windicator_quick_setting_brightness.c: windicator_quick_setting_brightness_update(94) > [windicator_quick_setting_brightness_update:94] hyun 80
04-26 17:26:39.829+0700 W/W_INDICATOR( 1124): windicator_connection.c: windicator_connection_resume(2158) > [windicator_connection_resume:2158] 
04-26 17:26:39.829+0700 W/W_INDICATOR( 1124): windicator_connection.c: windicator_connection_resume(2223) > [windicator_connection_resume:2223] primary rssi level : (0), s_info.rssi_level : (8) / primary svc type : (1)
04-26 17:26:39.829+0700 W/W_INDICATOR( 1124): windicator_connection.c: _rssi_changed_cb(1924) > [_rssi_changed_cb:1924] roaming status : 0
04-26 17:26:39.829+0700 W/W_INDICATOR( 1124): windicator_connection.c: _rssi_changed_cb(1933) > [_rssi_changed_cb:1933] svc type : 1
04-26 17:26:39.829+0700 W/W_INDICATOR( 1124): windicator_connection.c: _rssi_changed_cb(1968) > [_rssi_changed_cb:1968] Rssi level has already been 0!, Don't need to show rssi down animation. Just Show No Signal icon
04-26 17:26:39.829+0700 W/W_INDICATOR( 1124): windicator_connection.c: _rssi_icon_set(1124) > [_rssi_icon_set:1124] RSSI level : 8/5, (0xb854b6e0)
04-26 17:26:39.829+0700 E/W_INDICATOR( 1124): windicator_connection.c: _rssi_icon_set(1147) > [_rssi_icon_set:1147] Set RSSI SHOW sw.icon_0 (rssi_level : 8) (rssi_hide : 0)(b854b6e0)
04-26 17:26:39.829+0700 W/W_INDICATOR( 1124): windicator_connection.c: _rssi_icon_set(1215) > [_rssi_icon_set:1215] NETWORK_ATT or NETWORK_TMB : there is no roaming icon
04-26 17:26:39.829+0700 W/W_INDICATOR( 1124): windicator_connection.c: _rssi_icon_set(1217) > [_rssi_icon_set:1217] rssi name : set_rssi_verizon_No_signal (0xb854b6e0)
04-26 17:26:39.829+0700 E/ALARM_MANAGER(  972): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-26 17:26:39.829+0700 E/ALARM_MANAGER(  972): alarm-manager.c: alarm_manager_alarm_delete(2462) > alarm_id[259477176] is removed.
04-26 17:26:39.839+0700 W/STARTER ( 1122): clock-mgr.c: __reserved_apps_message_received_cb(586) > [__reserved_apps_message_received_cb:586] appid[com.samsung.windicator]
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.windicator:org.tizen.idled.ReservedApp]
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-26 17:26:39.839+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:26:39.839+0700 W/STARTER ( 1122): clock-mgr.c: _on_lcd_signal_receive_cb(1271) > [_on_lcd_signal_receive_cb:1271] _on_lcd_signal_receive_cb, 1271, Post LCD on by [gesture]
04-26 17:26:39.839+0700 W/STARTER ( 1122): clock-mgr.c: _post_lcd_on(1059) > [_post_lcd_on:1059] LCD ON as reserved app[(null)] alpm mode[0]
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _tapi_changed_cb(2115) > [_tapi_changed_cb:2115] modem_power : 0
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _connection_type_changed_cb(1324) > [_connection_type_changed_cb:1324] wifi state : 2
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _connection_type_changed_cb(1327) > [_connection_type_changed_cb:1327] Show wifi icon!
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 1
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 13 / signal : type_wifi_connected_01
04-26 17:26:39.849+0700 E/W_INDICATOR( 1124): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 13) / (hide : 0)
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-26 17:26:39.849+0700 E/W_INDICATOR( 1124): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-26 17:26:39.849+0700 W/W_INDICATOR( 1124): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-26 17:26:39.849+0700 E/W_INDICATOR( 1124): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-26 17:26:39.929+0700 W/W_HOME  ( 1203): gesture.c: _manual_render(182) > 
04-26 17:26:39.989+0700 W/W_HOME  ( 1203): gesture.c: _manual_render_enable(138) > 0
04-26 17:26:40.029+0700 W/W_INDICATOR( 1124): windicator_dbus.c: _msg_reserved_app_cb(341) > [_msg_reserved_app_cb:341] Moment view is already shown or call is enabled. moment view [0]
04-26 17:26:40.149+0700 W/CRASH_MANAGER( 2079): worker.c: worker_job(1205) > 1102180726561152473839
