S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2132
Date: 2018-04-27 13:15:59+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2132, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb6f6e238
r2   = 0xb6f6e238, r3   = 0xb6f5a28d
r4   = 0xbe9233c4, r5   = 0xb6ee9800
r6   = 0x00000274, r7   = 0xbe923018
r8   = 0x00000000, r9   = 0xffffffff
r10  = 0xb6e3f8e4, fp   = 0xb6e3f8e4
ip   = 0xb5a9f084, sp   = 0xbe922fb0
lr   = 0xb6f5a2af, pc   = 0xb6f5a3a0
cpsr = 0xa0000030

Memory Information
MemTotal:   405512 KB
MemFree:      5168 KB
Buffers:      9464 KB
Cached:     124700 KB
VmPeak:     117056 KB
VmSize:     108912 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       29232 KB
VmRSS:       29232 KB
VmData:      36540 KB
VmStk:         136 KB
VmExe:          40 KB
VmLib:       35904 KB
VmPTE:          76 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2132 TID = 2132
2132 2137 2148 2156 

Maps Information
b0495000 b0499000 r-xp /usr/lib/libogg.so.0.7.1
b04a1000 b04c3000 r-xp /usr/lib/libvorbis.so.0.4.3
b04cb000 b0512000 r-xp /usr/lib/libsndfile.so.1.0.26
b051e000 b0567000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b0570000 b0575000 r-xp /usr/lib/libjson.so.0.0.1
b1e16000 b1f1c000 r-xp /usr/lib/libicuuc.so.57.1
b1f32000 b20ba000 r-xp /usr/lib/libicui18n.so.57.1
b20ca000 b20d7000 r-xp /usr/lib/libail.so.0.1.0
b20e0000 b20e3000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b20eb000 b2123000 r-xp /usr/lib/libpulse.so.0.16.2
b2124000 b2127000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b212f000 b2190000 r-xp /usr/lib/libasound.so.2.0.0
b219a000 b21b3000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b21bc000 b21c0000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b21c8000 b21d3000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b2253000 b226b000 r-xp /usr/lib/libmmfsound.so.0.1.0
b22b1000 b22b5000 r-xp /usr/lib/libmmfsession.so.0.0.0
b22be000 b22c5000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b22cd000 b22d8000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b22e9000 b22eb000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b22f3000 b22f4000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b22fc000 b22fd000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b2305000 b238c000 rw-s anon_inode:dmabuf
b23b6000 b243d000 rw-s anon_inode:dmabuf
b24ed000 b2574000 rw-s anon_inode:dmabuf
b2601000 b2609000 r-xp /usr/lib/libfeedback.so.0.1.4
b2622000 b2623000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b262f000 b2632000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b27ee000 b2875000 rw-s anon_inode:dmabuf
b2876000 b3075000 rw-p [stack:2156]
b33ed000 b33ee000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b3477000 b3c76000 rw-p [stack:2148]
b3c76000 b3c78000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3c80000 b3c97000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3ea8000 b46a7000 rw-p [stack:2137]
b46be000 b4fea000 r-xp /usr/lib/libsc-a3xx.so
b524e000 b5250000 r-xp /usr/lib/libdri2.so.0.0.0
b5258000 b5260000 r-xp /usr/lib/libdrm.so.2.4.0
b5268000 b526c000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b5274000 b5277000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b527f000 b5280000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b5288000 b5293000 r-xp /usr/lib/libtbm.so.1.0.0
b529b000 b529e000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b52a6000 b52a8000 r-xp /usr/lib/libgenlock.so
b52b0000 b52b5000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b52bd000 b53f8000 r-xp /usr/lib/egl/libGLESv2.so
b5434000 b5436000 r-xp /usr/lib/libadreno_utils.so
b5440000 b5467000 r-xp /usr/lib/libgsl.so
b5476000 b547d000 r-xp /usr/lib/egl/eglsubX11.so
b5487000 b54a9000 r-xp /usr/lib/egl/libEGL.so
b54b2000 b5527000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5737000 b5741000 r-xp /lib/libnss_files-2.13.so
b574a000 b574d000 r-xp /lib/libattr.so.1.1.0
b5755000 b575c000 r-xp /lib/libcrypt-2.13.so
b578c000 b578f000 r-xp /lib/libcap.so.2.21
b5797000 b5799000 r-xp /usr/lib/libiri.so
b57a1000 b57be000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b57c7000 b57cb000 r-xp /usr/lib/libsmack.so.1.0.0
b57d4000 b5803000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b580b000 b589f000 r-xp /usr/lib/libstdc++.so.6.0.16
b58b2000 b5981000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5997000 b59bb000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b59c4000 b5a8e000 r-xp /usr/lib/libCOREGL.so.4.0
b5aa5000 b5aa7000 r-xp /usr/lib/libXau.so.6.0.0
b5ab0000 b5ac0000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5ac8000 b5acb000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ad3000 b5aeb000 r-xp /usr/lib/liblzma.so.5.0.3
b5af4000 b5af6000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5afe000 b5b01000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5b09000 b5b0d000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5b16000 b5b1b000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5b25000 b5b48000 r-xp /usr/lib/libjpeg.so.8.0.2
b5b60000 b5b76000 r-xp /lib/libexpat.so.1.6.0
b5b80000 b5b93000 r-xp /usr/lib/libxcb.so.1.1.0
b5b9c000 b5ba2000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5baa000 b5bab000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5bb5000 b5bcd000 r-xp /usr/lib/libpng12.so.0.50.0
b5bd5000 b5bd8000 r-xp /usr/lib/libEGL.so.1.4
b5be0000 b5bee000 r-xp /usr/lib/libGLESv2.so.2.0
b5bf7000 b5bf8000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5c00000 b5c17000 r-xp /usr/lib/liblua-5.1.so
b5c21000 b5c28000 r-xp /usr/lib/libembryo.so.1.7.99
b5c30000 b5c3a000 r-xp /usr/lib/libXext.so.6.4.0
b5c43000 b5c47000 r-xp /usr/lib/libXtst.so.6.1.0
b5c4f000 b5c55000 r-xp /usr/lib/libXrender.so.1.3.0
b5c5d000 b5c63000 r-xp /usr/lib/libXrandr.so.2.2.0
b5c6b000 b5c6c000 r-xp /usr/lib/libXinerama.so.1.0.0
b5c76000 b5c79000 r-xp /usr/lib/libXfixes.so.3.1.0
b5c81000 b5c83000 r-xp /usr/lib/libXgesture.so.7.0.0
b5c8b000 b5c8d000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5c95000 b5c97000 r-xp /usr/lib/libXdamage.so.1.1.0
b5c9f000 b5ca6000 r-xp /usr/lib/libXcursor.so.1.0.2
b5caf000 b5cbf000 r-xp /lib/libresolv-2.13.so
b5cc3000 b5cc5000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5ccd000 b5cd2000 r-xp /usr/lib/libffi.so.5.0.10
b5cda000 b5cdb000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5ce3000 b5d2c000 r-xp /usr/lib/libmdm.so.1.2.70
b5d36000 b5d3c000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5d44000 b5d4a000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5d52000 b5d6c000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5d74000 b5d92000 r-xp /usr/lib/libsystemd.so.0.4.0
b5d9d000 b5d9e000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5da6000 b5dab000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5db3000 b5dca000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5dd2000 b5dd8000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5de1000 b5dea000 r-xp /usr/lib/libcom-core.so.0.0.1
b5df4000 b5df6000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5dff000 b5e55000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5e62000 b5eb8000 r-xp /usr/lib/libfreetype.so.6.11.3
b5ec4000 b5f09000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5f12000 b5f25000 r-xp /usr/lib/libfribidi.so.0.3.1
b5f2e000 b5f48000 r-xp /usr/lib/libecore_con.so.1.7.99
b5f51000 b5f7b000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5f84000 b5f8d000 r-xp /usr/lib/libedbus.so.1.7.99
b5f95000 b5fa6000 r-xp /usr/lib/libecore_input.so.1.7.99
b5fae000 b5fb3000 r-xp /usr/lib/libecore_file.so.1.7.99
b5fbc000 b5fde000 r-xp /usr/lib/libecore_evas.so.1.7.99
b5fe7000 b6000000 r-xp /usr/lib/libeet.so.1.7.99
b6011000 b6039000 r-xp /usr/lib/libfontconfig.so.1.8.0
b603a000 b6043000 r-xp /usr/lib/libXi.so.6.1.0
b604b000 b612c000 r-xp /usr/lib/libX11.so.6.3.0
b6138000 b61f0000 r-xp /usr/lib/libcairo.so.2.11200.14
b61fb000 b6259000 r-xp /usr/lib/libedje.so.1.7.99
b6263000 b62b3000 r-xp /usr/lib/libecore_x.so.1.7.99
b62b5000 b631e000 r-xp /lib/libm-2.13.so
b6327000 b632d000 r-xp /lib/librt-2.13.so
b6336000 b634c000 r-xp /lib/libz.so.1.2.5
b6355000 b64e7000 r-xp /usr/lib/libcrypto.so.1.0.0
b6508000 b654f000 r-xp /usr/lib/libssl.so.1.0.0
b655b000 b6589000 r-xp /usr/lib/libidn.so.11.5.44
b6591000 b659a000 r-xp /usr/lib/libcares.so.2.1.0
b65a3000 b666f000 r-xp /usr/lib/libxml2.so.2.7.8
b667d000 b667f000 r-xp /usr/lib/libiniparser.so.0
b6688000 b66bc000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b66c5000 b6798000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b67a3000 b67bc000 r-xp /usr/lib/libnetwork.so.0.0.0
b67c4000 b67cd000 r-xp /usr/lib/libvconf.so.0.2.45
b67d6000 b68a6000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b68a7000 b68e8000 r-xp /usr/lib/libeina.so.1.7.99
b68f1000 b68f6000 r-xp /usr/lib/libappcore-common.so.1.1
b68fe000 b6904000 r-xp /usr/lib/libappcore-efl.so.1.1
b690c000 b690f000 r-xp /usr/lib/libbundle.so.0.1.22
b6917000 b691d000 r-xp /usr/lib/libappsvc.so.0.1.0
b6925000 b6939000 r-xp /lib/libpthread-2.13.so
b6944000 b6967000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b696f000 b697c000 r-xp /usr/lib/libaul.so.0.1.0
b6986000 b6988000 r-xp /lib/libdl-2.13.so
b6991000 b699c000 r-xp /lib/libunwind.so.8.0.1
b69c9000 b69d1000 r-xp /lib/libgcc_s-4.6.so.1
b69d2000 b6af6000 r-xp /lib/libc-2.13.so
b6b04000 b6b79000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6b83000 b6b8f000 r-xp /usr/lib/libnotification.so.0.1.0
b6b98000 b6ba7000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6bb0000 b6c7e000 r-xp /usr/lib/libevas.so.1.7.99
b6ca4000 b6de0000 r-xp /usr/lib/libelementary.so.1.7.99
b6df7000 b6e18000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6e20000 b6e37000 r-xp /usr/lib/libecore.so.1.7.99
b6e4e000 b6e50000 r-xp /usr/lib/libdlog.so.0.0.0
b6e58000 b6e9c000 r-xp /usr/lib/libcurl.so.4.3.0
b6ea5000 b6eaa000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6eb2000 b6eb7000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6ebf000 b6ecf000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6ed7000 b6edf000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6ee7000 b6eeb000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6ef3000 b6ef7000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6f00000 b6f02000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6f0d000 b6f18000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6f22000 b6f26000 r-xp /usr/lib/libsys-assert.so
b6f2f000 b6f4c000 r-xp /lib/ld-2.13.so
b6f55000 b6f5f000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b8675000 b8b5c000 rw-p [heap]
be903000 be924000 rw-p [stack]
b6f55000 b6f5f000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b8675000 b8b5c000 rw-p [heap]
be903000 be924000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2132)
Call Stack Count: 14
 0: loadAllNotification + 0xf (0xb6f5a3a0) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53a0
 1: _on_resume_cb + 0x22 (0xb6f5a2af) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x52af
 2: (0xb6ee86ad) [/usr/lib/libcapi-appfw-application.so.0] + 0x16ad
 3: (0xb6901151) [/usr/lib/libappcore-efl.so.1] + 0x3151
 4: (0xb6901527) [/usr/lib/libappcore-efl.so.1] + 0x3527
 5: (0xb6e28e53) [/usr/lib/libecore.so.1] + 0x8e53
 6: (0xb6e2c46b) [/usr/lib/libecore.so.1] + 0xc46b
 7: ecore_main_loop_begin + 0x30 (0xb6e2c879) [/usr/lib/libecore.so.1] + 0xc879
 8: appcore_efl_main + 0x332 (0xb6901b47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
 9: ui_app_main + 0xb0 (0xb6ee8ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
10: uib_app_run + 0xea (0xb6f5a227) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5227
11: main + 0x34 (0xb6f5a641) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5641
12: __libc_start_main + 0x114 (0xb69e985c) [/lib/libc.so.6] + 0x1785c
13: (0xb6f580cc) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x30cc
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
00 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:40.969+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] direction_x(0), direction_y(1)
04-27 13:15:40.969+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] drag_child_locked_y(0)
04-27 13:15:40.969+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] move content x(0), y(106)
04-27 13:15:40.969+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:40.979+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:40.979+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:40.989+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:40.989+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:40.989+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] direction_x(0), direction_y(1)
04-27 13:15:40.989+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] drag_child_locked_y(0)
04-27 13:15:40.989+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] move content x(0), y(106)
04-27 13:15:40.999+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:40.999+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:40.999+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] direction_x(0), direction_y(1)
04-27 13:15:40.999+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] drag_child_locked_y(0)
04-27 13:15:40.999+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] move content x(0), y(105)
04-27 13:15:40.999+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.009+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:41.009+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:41.019+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:41.019+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:41.019+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] direction_x(0), direction_y(1)
04-27 13:15:41.019+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] drag_child_locked_y(0)
04-27 13:15:41.019+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb893e440 : elm_genlist] move content x(0), y(105)
04-27 13:15:41.029+0700 E/EFL     ( 2132): ecore_x<2132> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=79992 button=1
04-27 13:15:41.039+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.049+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.069+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.089+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.099+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.119+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.139+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.149+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.169+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.189+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.399+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.409+0700 W/AUL_AMD (  991): amd_status.c: __app_terminate_timer_cb(168) > send SIGKILL: No such process
04-27 13:15:41.419+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.439+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.449+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.469+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.489+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.499+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.519+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.539+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.549+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.569+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.589+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.599+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:41.889+0700 E/EFL     ( 2132): ecore_x<2132> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=80851 button=1
04-27 13:15:41.889+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:41.899+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:41.899+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:41.919+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:41.919+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:41.929+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:41.929+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:41.939+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:41.939+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:41.949+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:41.949+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:42.019+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:42.019+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:42.029+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:42.029+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:42.029+0700 E/EFL     (  936): ecore_x<936> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3200003 time=79992
04-27 13:15:42.029+0700 E/EFL     ( 2132): ecore_x<2132> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=79992
04-27 13:15:42.029+0700 E/EFL     (  936): ecore_x<936> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=79992
04-27 13:15:42.039+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] mouse move
04-27 13:15:42.039+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb893e440 : elm_genlist] hold(0), freeze(0)
04-27 13:15:42.049+0700 E/EFL     ( 2132): ecore_x<2132> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=81014 button=1
04-27 13:15:42.289+0700 E/GL SELECTED( 2132): ID GL : TRM2
04-27 13:15:44.059+0700 W/W_INDICATOR( 1181): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-27 13:15:44.059+0700 W/W_INDICATOR( 1181): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-27 13:15:44.059+0700 W/W_INDICATOR( 1181): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
04-27 13:15:44.059+0700 W/W_INDICATOR( 1181): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
04-27 13:15:44.059+0700 E/W_INDICATOR( 1181): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
04-27 13:15:44.059+0700 W/W_INDICATOR( 1181): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 13:15:44.059+0700 E/W_INDICATOR( 1181): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 13:15:44.059+0700 W/W_INDICATOR( 1181): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-27 13:15:48.299+0700 E/EFL     ( 2132): ecore_x<2132> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=87268 button=1
04-27 13:15:48.449+0700 E/EFL     ( 2132): ecore_x<2132> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=87410 button=1
04-27 13:15:48.449+0700 I/efl-extension( 2132): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 13:15:48.449+0700 I/efl-extension( 2132): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 13:15:48.459+0700 I/efl-extension( 2132): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb89e6988, elm_image, _activated_obj : 0xb8953c80, activated : 1
04-27 13:15:48.459+0700 I/efl-extension( 2132): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 13:15:48.459+0700 E/EFL     ( 2132): elementary<2132> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 13:15:48.469+0700 E/EFL     ( 2132): elementary<2132> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb89e6df0) will be pushed
04-27 13:15:48.469+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:48.469+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:48.469+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb327b2e8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:15:48.469+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb327b2e8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 13:15:48.479+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb327b2e8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 13:15:48.489+0700 E/EFL     ( 2132): elementary<2132> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb327b2e8 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-27 13:15:48.499+0700 E/EFL     ( 2132): elementary<2132> elc_naviframe.c:2796 _push_transition_cb() item(0xb89e6df0) will transition
04-27 13:15:48.929+0700 E/EFL     ( 2132): elementary<2132> elc_naviframe.c:1193 _on_item_push_finished() item(0xb8968730) transition finished
04-27 13:15:48.929+0700 E/EFL     ( 2132): elementary<2132> elc_naviframe.c:1218 _on_item_show_finished() item(0xb89e6df0) transition finished
04-27 13:15:48.959+0700 E/EFL     ( 2132): <2132> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 13:15:50.549+0700 I/CAPI_NETWORK_CONNECTION( 2132): connection.c: connection_create(453) > New handle created[0xb8a9b060]
04-27 13:15:50.709+0700 I/CAPI_NETWORK_CONNECTION( 2132): connection.c: connection_destroy(471) > Destroy handle: 0xb8a9b060
04-27 13:15:51.229+0700 W/SHealthServiceCommon( 1754): SleepEfficiencyMonitorSensorProxy.cpp: SOnContextAutoSleepMonitorUpdatedCB(199) > [1;40;33mSleep monitor cb is called[0;m
04-27 13:15:51.239+0700 W/SHealthServiceCommon( 1754): SleepEfficiencyDataController.cpp: CacheSleepEfficiencyData(65) > [1;40;33m1[0;m
04-27 13:15:55.739+0700 I/CAPI_NETWORK_CONNECTION( 2132): connection.c: connection_create(453) > New handle created[0xb8ab1398]
04-27 13:15:57.499+0700 E/WMS     ( 1100): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-27 13:15:58.379+0700 E/WMS     ( 1100): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-27 13:15:58.449+0700 W/WATCH_CORE( 1358): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
04-27 13:15:58.449+0700 I/WATCH_CORE( 1358): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
04-27 13:15:58.449+0700 I/CAPI_WATCH_APPLICATION( 1358): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-27 13:15:58.449+0700 E/watchface-app( 1358): watchface.cpp: OnAppTimeTick(1157) > 
04-27 13:15:58.449+0700 I/watchface-app( 1358): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-27 13:15:58.479+0700 W/WATCH_CORE( 1358): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOff
04-27 13:15:58.479+0700 W/W_HOME  ( 1256): dbus.c: _dbus_message_recv_cb(204) > LCD off
04-27 13:15:58.479+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_cancel_schedule(226) > cancel schedule, manual render
04-27 13:15:58.479+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-27 13:15:58.479+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_enable(138) > 1
04-27 13:15:58.479+0700 W/W_HOME  ( 1256): event_manager.c: _lcd_off_cb(723) > lcd state: 0
04-27 13:15:58.479+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(0) pm_state:0 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:15:58.479+0700 W/STARTER ( 1178): clock-mgr.c: _on_lcd_signal_receive_cb(1284) > [_on_lcd_signal_receive_cb:1284] _on_lcd_signal_receive_cb, 1284, Pre LCD off by [gesture]
04-27 13:15:58.479+0700 W/STARTER ( 1178): clock-mgr.c: _pre_lcd_off(1089) > [_pre_lcd_off:1089] Will LCD OFF as wake_up_setting[1]
04-27 13:15:58.479+0700 E/STARTER ( 1178): scontext_util.c: sconstext_util_check_lock_type(47) > [sconstext_util_check_lock_type:47] current lock state :[0],testmode[0]
04-27 13:15:58.479+0700 E/STARTER ( 1178): scontext_util.c: scontext_util_handle_lock_state(72) > [scontext_util_handle_lock_state:72] wear state[0],bPossible [0],usage [0]
04-27 13:15:58.479+0700 W/STARTER ( 1178): clock-mgr.c: _check_reserved_popup_status(211) > [_check_reserved_popup_status:211] Current reserved apps status : 0
04-27 13:15:58.479+0700 W/STARTER ( 1178): clock-mgr.c: _check_reserved_apps_status(247) > [_check_reserved_apps_status:247] Current reserved apps status : 0
04-27 13:15:58.499+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: OnReceiveDisplayChanged(979) > [0;32mINFO: LCDOff receive data : -1226884340[0;m
04-27 13:15:58.509+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: OnReceiveDisplayChanged(980) > [0;32mINFO: WakeupServiceStop[0;m
04-27 13:15:58.509+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: WakeupServiceStop(399) > [0;32mINFO: SEAMLESS WAKEUP STOP REQUEST[0;m
04-27 13:15:58.539+0700 E/WAKEUP-SERVICE( 1677): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-27 13:15:58.539+0700 E/WAKEUP-SERVICE( 1677): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-27 13:15:58.539+0700 I/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Disable start
04-27 13:15:58.569+0700 I/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Disable end. (ret: 0x0)
04-27 13:15:58.569+0700 W/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-27 13:15:58.569+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 0[0;m
04-27 13:15:58.569+0700 I/HIGEAR  ( 1677): WakeupService.cpp: WakeupServiceStop(403) > [svoice:Application:WakeupServiceStop:IN]
04-27 13:15:58.629+0700 I/CAPI_NETWORK_CONNECTION( 2132): connection.c: connection_destroy(471) > Destroy handle: 0xb8ab1398
04-27 13:15:58.649+0700 W/W_INDICATOR( 1181): windicator_util.c: _pm_state_changed_cb(917) > [_pm_state_changed_cb:917] LCD off
04-27 13:15:58.649+0700 W/W_INDICATOR( 1181): windicator_connection.c: windicator_connection_pause(2268) > [windicator_connection_pause:2268] 
04-27 13:15:58.649+0700 W/W_INDICATOR( 1181): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-27 13:15:58.649+0700 W/SHealthCommon( 1522): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
04-27 13:15:58.659+0700 W/SHealthCommon( 1754): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
04-27 13:15:58.659+0700 W/SHealthServiceCommon( 1754): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
04-27 13:15:58.669+0700 W/STARTER ( 1178): clock-mgr.c: _on_lcd_signal_receive_cb(1297) > [_on_lcd_signal_receive_cb:1297] _on_lcd_signal_receive_cb, 1297, Post LCD off by [gesture]
04-27 13:15:58.669+0700 W/STARTER ( 1178): clock-mgr.c: _post_lcd_off(1190) > [_post_lcd_off:1190] LCD OFF as reserved app[(null)] alpm mode[0]
04-27 13:15:58.669+0700 W/STARTER ( 1178): clock-mgr.c: _post_lcd_off(1197) > [_post_lcd_off:1197] Current reserved apps status : 0
04-27 13:15:58.669+0700 W/STARTER ( 1178): clock-mgr.c: _post_lcd_off(1215) > [_post_lcd_off:1215] raise homescreen after 20 sec. home_visible[0]
04-27 13:15:58.669+0700 E/ALARM_MANAGER( 1178): alarm-lib.c: alarmmgr_add_alarm_withcb(1178) > trigger_at_time(20), start(27-4-2018, 13:16:19), repeat(1), interval(20), type(-1073741822)
04-27 13:15:58.669+0700 W/W_INDICATOR( 1181): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(620) > [_windicator_dbus_lcd_off_completed_cb:620] LCD Off completed signal is received
04-27 13:15:58.669+0700 W/W_INDICATOR( 1181): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(625) > [_windicator_dbus_lcd_off_completed_cb:625] Moment bar status -> idle. (Hide Moment bar)
04-27 13:15:58.669+0700 W/W_INDICATOR( 1181): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-27 13:15:58.669+0700 I/APP_CORE( 2132): appcore-efl.c: __do_app(453) > [APP 2132] Event: PAUSE State: RUNNING
04-27 13:15:58.669+0700 I/CAPI_APPFW_APPLICATION( 2132): app_main.c: _ui_app_appcore_pause(611) > app_appcore_pause
04-27 13:15:58.679+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1178].
04-27 13:15:58.679+0700 W/APP_CORE( 2132): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3200003] -> redirected win[6007a9] for com.toyota.realtimefeedback[2132]
04-27 13:15:58.729+0700 E/ALARM_MANAGER(  994): alarm-manager-schedule.c: _alarm_next_duetime(509) > alarm_id: 1576803377, next duetime: 1524809779
04-27 13:15:58.729+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __alarm_add_to_list(496) > [alarm-server]: After add alarm_id(1576803377)
04-27 13:15:58.729+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __alarm_create(1061) > [alarm-server]:alarm_context.c_due_time(1524810179), due_time(1524809779)
04-27 13:15:58.729+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-27 13:15:58.729+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-27 13:15:58.729+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 27-4-2018, 06:16:19 (UTC).
04-27 13:15:58.729+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-27 13:15:58.739+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-27 13:15:59.339+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: OnReceiveGestureChanged(995) > [0;32mINFO: wakeup receive data : -1219435728[0;m
04-27 13:15:59.339+0700 W/WATCH_CORE( 1358): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
04-27 13:15:59.339+0700 I/WATCH_CORE( 1358): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
04-27 13:15:59.339+0700 I/CAPI_WATCH_APPLICATION( 1358): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-27 13:15:59.339+0700 E/watchface-app( 1358): watchface.cpp: OnAppTimeTick(1157) > 
04-27 13:15:59.339+0700 I/watchface-app( 1358): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-27 13:15:59.339+0700 W/W_HOME  ( 1256): dbus.c: _dbus_message_recv_cb(169) > gesture:wristup
04-27 13:15:59.339+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_schedule(209) > schedule, manual render
04-27 13:15:59.349+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: OnReceiveGestureChanged(996) > [0;32mINFO: WakeupServiceStart[0;m
04-27 13:15:59.349+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
04-27 13:15:59.349+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
04-27 13:15:59.359+0700 W/WATCH_CORE( 1358): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOn
04-27 13:15:59.359+0700 I/WATCH_CORE( 1358): appcore-watch.c: __signal_lcd_status_handler(1250) > Call the time_tick_cb
04-27 13:15:59.359+0700 I/CAPI_WATCH_APPLICATION( 1358): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-27 13:15:59.359+0700 E/watchface-app( 1358): watchface.cpp: OnAppTimeTick(1157) > 
04-27 13:15:59.359+0700 I/watchface-app( 1358): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-27 13:15:59.359+0700 I/WATCH_CORE( 1358): appcore-watch.c: __signal_lcd_status_handler(1257) > Call widget_provider_update_event
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): dbus.c: _dbus_message_recv_cb(186) > LCD on
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_disable_timer_set(167) > timer set
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): gesture.c: _apps_status_get(128) > apps status:0
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): gesture.c: _lcd_on_cb(303) > move_to_clock:0 clock_visible:0 info->offtime:881
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_schedule(209) > schedule, manual render
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): event_manager.c: _lcd_on_cb(715) > lcd state: 1
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): gesture.c: _widget_updated_cb(188) > widget updated
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-27 13:15:59.359+0700 W/W_HOME  ( 1256): gesture.c: _manual_render(182) > 
04-27 13:15:59.369+0700 W/STARTER ( 1178): clock-mgr.c: _on_lcd_signal_receive_cb(1258) > [_on_lcd_signal_receive_cb:1258] _on_lcd_signal_receive_cb, 1258, Pre LCD on by [gesture] after screen off time [881]ms
04-27 13:15:59.369+0700 W/STARTER ( 1178): clock-mgr.c: _pre_lcd_on(1027) > [_pre_lcd_on:1027] Will LCD ON as reserved app[(null)] alpm mode[0]
04-27 13:15:59.369+0700 W/W_HOME  ( 1256): gesture.c: _manual_render(182) > 
04-27 13:15:59.369+0700 W/W_INDICATOR( 1181): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(538) > [_windicator_dbus_lcd_changed_cb:538] LCD ON signal is received
04-27 13:15:59.369+0700 W/W_INDICATOR( 1181): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(559) > [_windicator_dbus_lcd_changed_cb:559] 559, str=[gesture],charge : 0, connected : 0
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [starter:org.tizen.idled.ReservedApp]
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [starter:org.tizen.idled.ReservedApp]
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 13:15:59.369+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:15:59.369+0700 W/W_HOME  ( 1256): gesture.c: _manual_render_enable(138) > 0
04-27 13:15:59.379+0700 I/APP_CORE( 2132): appcore-efl.c: __do_app(453) > [APP 2132] Event: RESUME State: PAUSED
04-27 13:15:59.379+0700 I/CAPI_APPFW_APPLICATION( 2132): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-27 13:15:59.389+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1178].
04-27 13:15:59.389+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __alarm_remove_from_list(575) > [alarm-server]:Remove alarm id(1576803377)
04-27 13:15:59.389+0700 E/ALARM_MANAGER(  994): alarm-manager-schedule.c: __find_next_alarm_to_be_scheduled(547) > The duetime of alarm(436816151) is OVER.
04-27 13:15:59.399+0700 I/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
04-27 13:15:59.399+0700 W/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
04-27 13:15:59.419+0700 E/WAKEUP-SERVICE( 1677): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-27 13:15:59.419+0700 E/WAKEUP-SERVICE( 1677): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-27 13:15:59.419+0700 I/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
04-27 13:15:59.419+0700 I/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
04-27 13:15:59.419+0700 W/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-27 13:15:59.419+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
04-27 13:15:59.419+0700 I/HIGEAR  ( 1677): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
04-27 13:15:59.419+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: OnReceiveDisplayChanged(970) > [0;32mINFO: LCDOn receive data : -1226884340[0;m
04-27 13:15:59.419+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: OnReceiveDisplayChanged(971) > [0;32mINFO: WakeupServiceStart[0;m
04-27 13:15:59.419+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
04-27 13:15:59.419+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
04-27 13:15:59.419+0700 I/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
04-27 13:15:59.419+0700 W/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
04-27 13:15:59.429+0700 E/WAKEUP-SERVICE( 1677): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-27 13:15:59.449+0700 W/W_INDICATOR( 1181): windicator_util.c: _pm_state_changed_cb(912) > [_pm_state_changed_cb:912] LCD on
04-27 13:15:59.449+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(57) > [windicator_ongoing_info_shealth_update:57] windicator_shealth_update
04-27 13:15:59.449+0700 W/SHealthCommon( 1522): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
04-27 13:15:59.499+0700 I/HealthDataService( 1337): RequestHandler.cpp: OnHealthIpcMessageSync(123) > [1;35mServer Received: SHARE_GET[0;m
04-27 13:15:59.499+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-27 13:15:59.499+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-27 13:15:59.499+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 27-4-2018, 06:22:59 (UTC).
04-27 13:15:59.499+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(78) > [windicator_ongoing_info_shealth_update:78] Result : 0
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(99) > [windicator_ongoing_info_shealth_update:99] status : none
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(103) > [windicator_ongoing_info_shealth_update:103] application_id: 
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(112) > [windicator_ongoing_info_shealth_update:112] launch_operation : 
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(118) > [windicator_ongoing_info_shealth_update:118] extra_data_key : 
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(124) > [windicator_ongoing_info_shealth_update:124] extra_data_value : 
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(132) > [windicator_ongoing_info_shealth_update:132] image_path : 
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(135) > [windicator_ongoing_info_shealth_update:135] image_path_sub : 
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(138) > [windicator_ongoing_info_shealth_update:138] message_string :  
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(144) > [windicator_ongoing_info_shealth_update:144] [Update] SHealth status is none, so hide icon and text!
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info.c: windicator_ongoing_info_remove(191) > [windicator_ongoing_info_remove:191] Ongoing info type[1]
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:15:59.509+0700 W/W_INDICATOR( 1181): windicator_ongoing_info.c: windicator_ongoing_info_remove(191) > [windicator_ongoing_info_remove:191] Ongoing info type[2]
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 13:15:59.509+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:15:59.519+0700 E/EFL     ( 1181): <1181> elm_main.c:1622 elm_object_signal_emit() safety check failed: obj == NULL
04-27 13:15:59.519+0700 I/TIZEN_N_SOUND_MANAGER( 1181): sound_manager.c: sound_manager_get_volume(84) > returns : type=3, volume=11, ret=0x0
04-27 13:15:59.519+0700 W/TIZEN_N_SOUND_MANAGER( 1181): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_get_volume] ERROR_NONE (0x00000000)
04-27 13:15:59.519+0700 W/W_INDICATOR( 1181): windicator_quick_setting_brightness.c: windicator_quick_setting_brightness_update(94) > [windicator_quick_setting_brightness_update:94] hyun 20
04-27 13:15:59.529+0700 E/ALARM_MANAGER(  994): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-27 13:15:59.529+0700 E/ALARM_MANAGER(  994): alarm-manager.c: alarm_manager_alarm_delete(2462) > alarm_id[1576803377] is removed.
04-27 13:15:59.529+0700 W/STARTER ( 1178): clock-mgr.c: __reserved_apps_message_received_cb(586) > [__reserved_apps_message_received_cb:586] appid[com.samsung.windicator]
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.windicator:org.tizen.idled.ReservedApp]
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 13:15:59.529+0700 I/MESSAGE_PORT(  987): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 13:15:59.529+0700 W/STARTER ( 1178): clock-mgr.c: _on_lcd_signal_receive_cb(1271) > [_on_lcd_signal_receive_cb:1271] _on_lcd_signal_receive_cb, 1271, Post LCD on by [gesture]
04-27 13:15:59.529+0700 W/STARTER ( 1178): clock-mgr.c: _post_lcd_on(1059) > [_post_lcd_on:1059] LCD ON as reserved app[(null)] alpm mode[0]
04-27 13:15:59.529+0700 W/W_INDICATOR( 1181): windicator_connection.c: windicator_connection_resume(2158) > [windicator_connection_resume:2158] 
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: windicator_connection_resume(2223) > [windicator_connection_resume:2223] primary rssi level : (0), s_info.rssi_level : (8) / primary svc type : (1)
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _rssi_changed_cb(1924) > [_rssi_changed_cb:1924] roaming status : 0
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _rssi_changed_cb(1933) > [_rssi_changed_cb:1933] svc type : 1
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _rssi_changed_cb(1968) > [_rssi_changed_cb:1968] Rssi level has already been 0!, Don't need to show rssi down animation. Just Show No Signal icon
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _rssi_icon_set(1124) > [_rssi_icon_set:1124] RSSI level : 8/5, (0xb73e8c88)
04-27 13:15:59.539+0700 E/W_INDICATOR( 1181): windicator_connection.c: _rssi_icon_set(1147) > [_rssi_icon_set:1147] Set RSSI SHOW sw.icon_0 (rssi_level : 8) (rssi_hide : 0)(b73e8c88)
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _rssi_icon_set(1215) > [_rssi_icon_set:1215] NETWORK_ATT or NETWORK_TMB : there is no roaming icon
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _rssi_icon_set(1217) > [_rssi_icon_set:1217] rssi name : set_rssi_verizon_No_signal (0xb73e8c88)
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _tapi_changed_cb(2115) > [_tapi_changed_cb:2115] modem_power : 0
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _connection_type_changed_cb(1324) > [_connection_type_changed_cb:1324] wifi state : 2
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _connection_type_changed_cb(1327) > [_connection_type_changed_cb:1327] Show wifi icon!
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-27 13:15:59.539+0700 W/W_INDICATOR( 1181): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-27 13:15:59.549+0700 W/W_INDICATOR( 1181): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
04-27 13:15:59.549+0700 W/W_INDICATOR( 1181): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
04-27 13:15:59.549+0700 E/W_INDICATOR( 1181): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
04-27 13:15:59.549+0700 W/W_INDICATOR( 1181): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 13:15:59.549+0700 E/W_INDICATOR( 1181): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 13:15:59.549+0700 W/W_INDICATOR( 1181): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-27 13:15:59.549+0700 W/W_INDICATOR( 1181): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 13:15:59.549+0700 E/W_INDICATOR( 1181): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 13:15:59.579+0700 W/SHealthCommon( 1754): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
04-27 13:15:59.579+0700 W/SHealthServiceCommon( 1754): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
04-27 13:15:59.579+0700 W/SHealthServiceCommon( 1754): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1524762000000.000000, end 1524809759596.000000, calories 892.704901[0;m
04-27 13:15:59.649+0700 W/W_INDICATOR( 1181): windicator_dbus.c: _msg_reserved_app_cb(341) > [_msg_reserved_app_cb:341] Moment view is already shown or call is enabled. moment view [0]
04-27 13:15:59.659+0700 E/WAKEUP-SERVICE( 1677): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-27 13:15:59.659+0700 I/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
04-27 13:15:59.659+0700 I/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
04-27 13:15:59.659+0700 W/TIZEN_N_SOUND_MANAGER( 1677): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-27 13:15:59.659+0700 W/WAKEUP-SERVICE( 1677): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
04-27 13:15:59.669+0700 I/HIGEAR  ( 1677): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
04-27 13:15:59.689+0700 W/SHealthCommon( 1754): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
04-27 13:15:59.859+0700 W/KEYROUTER(  936): e_mod_main.c: DeliverDeviceKeyEvents(3244) > Deliver KeyPress to focus window. value=1997, window=0x3200003
04-27 13:15:59.859+0700 W/KEYROUTER(  936): e_mod_main.c: DeliverDeviceKeyEvents(3255) > Deliver KeyPress as shared grab. value=1997, window=0x2400003
04-27 13:15:59.859+0700 W/KEYROUTER(  936): e_mod_main.c: DeliverDeviceKeyEvents(3255) > Deliver KeyPress as shared grab. value=1997, window=0x1200002
04-27 13:15:59.859+0700 E/EFL     ( 1256): ecore_x<1256> ecore_x_events.c:537 _ecore_x_event_handle_key_press() KeyEvent:press time=98826
04-27 13:15:59.859+0700 W/STARTER ( 1178): hw_key.c: _key_press_cb(1477) > [_key_press_cb:1477] POWER Key is pressed
04-27 13:15:59.859+0700 W/STARTER ( 1178): hw_key.c: _key_press_cb(1480) > [_key_press_cb:1480] LCD state : 1
04-27 13:15:59.859+0700 W/W_HOME  ( 1256): main.c: _direct_home_key_cb(1479) > was_win_on_top:0 state:2 tts:0
04-27 13:15:59.859+0700 W/W_HOME  ( 1256): dbus_util.c: home_dbus_home_raise_signal_send(298) > Sending HOME RAISE signal, result:0
04-27 13:15:59.859+0700 E/W_HOME  ( 1256): key.c: _key_press_cb(219) > (APP_STATE_PAUSE == main_get_info()->state) -> _key_press_cb() return
04-27 13:15:59.859+0700 W/STARTER ( 1178): hw_key.c: _key_press_cb(1487) > [_key_press_cb:1487] PM state : 1
04-27 13:15:59.869+0700 E/STARTER ( 1178): hw_key.c: _key_press_cb(1493) > [_key_press_cb:1493] Failed to get VCONFKEY_SIMPLECLOCK_UI_STATUS
04-27 13:15:59.869+0700 W/STARTER ( 1178): hw_key.c: _key_press_cb(1496) > [_key_press_cb:1496] Simple Clock state : 0
04-27 13:15:59.869+0700 W/STARTER ( 1178): hw_key.c: _key_press_cb(1501) > [_key_press_cb:1501] powerkey count : 1
04-27 13:15:59.879+0700 W/W_INDICATOR( 1181): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-27 13:15:59.879+0700 W/CRASH_MANAGER( 2165): worker.c: worker_job(1205) > 1102132726561152480975
