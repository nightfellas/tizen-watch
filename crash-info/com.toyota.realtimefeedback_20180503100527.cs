S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2045
Date: 2018-05-03 10:05:27+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2045, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb585c61c
r2   = 0xb585cc30, r3   = 0x00000000
r4   = 0xb7b93290, r5   = 0xb1d008a0
r6   = 0x00000013, r7   = 0xbe8c8208
r8   = 0x0000640d, r9   = 0xb7b93290
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb585c870, sp   = 0xbe8c81e8
lr   = 0xb584a50f, pc   = 0xb584a544
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      3644 KB
Buffers:      4796 KB
Cached:     108128 KB
VmPeak:     174480 KB
VmSize:     173264 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       30972 KB
VmRSS:       30972 KB
VmData:      85300 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:         128 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2045 TID = 2045
2045 2070 2075 

Maps Information
b1e22000 b1e26000 r-xp /usr/lib/libogg.so.0.7.1
b1e2e000 b1e50000 r-xp /usr/lib/libvorbis.so.0.4.3
b1e58000 b1e9f000 r-xp /usr/lib/libsndfile.so.1.0.26
b1eab000 b1ef4000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1efd000 b1f02000 r-xp /usr/lib/libjson.so.0.0.1
b37a3000 b38a9000 r-xp /usr/lib/libicuuc.so.57.1
b38bf000 b3a47000 r-xp /usr/lib/libicui18n.so.57.1
b3a57000 b3a64000 r-xp /usr/lib/libail.so.0.1.0
b3a6d000 b3a70000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3a78000 b3ab0000 r-xp /usr/lib/libpulse.so.0.16.2
b3ab1000 b3ab4000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3abc000 b3b1d000 r-xp /usr/lib/libasound.so.2.0.0
b3b27000 b3b40000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3b49000 b3b4d000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3b55000 b3b60000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3b6d000 b3b71000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3b7a000 b3b92000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3ba3000 b3baa000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3bb2000 b3bbd000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3bc5000 b3bc7000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3bcf000 b3bd0000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3bd8000 b3bd9000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3be1000 b3be4000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3f07000 b3f0f000 r-xp /usr/lib/libfeedback.so.0.1.4
b3f28000 b3f29000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3f74000 b3ffb000 rw-s anon_inode:dmabuf
b3ffc000 b47fb000 rw-p [stack:2075]
b499b000 b499c000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4aa3000 b52a2000 rw-p [stack:2070]
b52a2000 b52a4000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52ac000 b52c3000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52d0000 b52d2000 r-xp /usr/lib/libdri2.so.0.0.0
b52da000 b52e5000 r-xp /usr/lib/libtbm.so.1.0.0
b52ed000 b52f5000 r-xp /usr/lib/libdrm.so.2.4.0
b52fd000 b52ff000 r-xp /usr/lib/libgenlock.so
b5307000 b530c000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5314000 b531f000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b5528000 b55f2000 r-xp /usr/lib/libCOREGL.so.4.0
b5603000 b5613000 r-xp /usr/lib/libmdm-common.so.1.1.25
b561b000 b5621000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5629000 b562a000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5633000 b5636000 r-xp /usr/lib/libEGL.so.1.4
b563e000 b564c000 r-xp /usr/lib/libGLESv2.so.2.0
b5655000 b569e000 r-xp /usr/lib/libmdm.so.1.2.70
b56a7000 b56ad000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56b5000 b56be000 r-xp /usr/lib/libcom-core.so.0.0.1
b56c7000 b577f000 r-xp /usr/lib/libcairo.so.2.11200.14
b578a000 b57a3000 r-xp /usr/lib/libnetwork.so.0.0.0
b57ab000 b57b7000 r-xp /usr/lib/libnotification.so.0.1.0
b57c0000 b57cf000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b57d8000 b57f9000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5801000 b5806000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b580e000 b5813000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b581b000 b582b000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5833000 b583b000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5843000 b584d000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b59f2000 b59fc000 r-xp /lib/libnss_files-2.13.so
b5a05000 b5ad4000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5aea000 b5b0e000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b17000 b5b1d000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b25000 b5b29000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b36000 b5b41000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b49000 b5b4b000 r-xp /usr/lib/libiniparser.so.0
b5b54000 b5b59000 r-xp /usr/lib/libappcore-common.so.1.1
b5b61000 b5b63000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b6c000 b5b70000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5b7d000 b5b7f000 r-xp /usr/lib/libXau.so.6.0.0
b5b87000 b5b8e000 r-xp /lib/libcrypt-2.13.so
b5bbe000 b5bc0000 r-xp /usr/lib/libiri.so
b5bc9000 b5d5b000 r-xp /usr/lib/libcrypto.so.1.0.0
b5d7c000 b5dc3000 r-xp /usr/lib/libssl.so.1.0.0
b5dcf000 b5dfd000 r-xp /usr/lib/libidn.so.11.5.44
b5e05000 b5e0e000 r-xp /usr/lib/libcares.so.2.1.0
b5e18000 b5e2b000 r-xp /usr/lib/libxcb.so.1.1.0
b5e34000 b5e37000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e3f000 b5e41000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e4a000 b5f16000 r-xp /usr/lib/libxml2.so.2.7.8
b5f23000 b5f25000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f2e000 b5f33000 r-xp /usr/lib/libffi.so.5.0.10
b5f3b000 b5f3c000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f44000 b5f47000 r-xp /lib/libattr.so.1.1.0
b5f4f000 b5fe3000 r-xp /usr/lib/libstdc++.so.6.0.16
b5ff6000 b6013000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b601d000 b6035000 r-xp /usr/lib/libpng12.so.0.50.0
b603d000 b6053000 r-xp /lib/libexpat.so.1.6.0
b605d000 b60a1000 r-xp /usr/lib/libcurl.so.4.3.0
b60aa000 b60b4000 r-xp /usr/lib/libXext.so.6.4.0
b60be000 b60c2000 r-xp /usr/lib/libXtst.so.6.1.0
b60ca000 b60d0000 r-xp /usr/lib/libXrender.so.1.3.0
b60d8000 b60de000 r-xp /usr/lib/libXrandr.so.2.2.0
b60e6000 b60e7000 r-xp /usr/lib/libXinerama.so.1.0.0
b60f0000 b60f9000 r-xp /usr/lib/libXi.so.6.1.0
b6101000 b6104000 r-xp /usr/lib/libXfixes.so.3.1.0
b610d000 b610f000 r-xp /usr/lib/libXgesture.so.7.0.0
b6117000 b6119000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6121000 b6123000 r-xp /usr/lib/libXdamage.so.1.1.0
b612b000 b6132000 r-xp /usr/lib/libXcursor.so.1.0.2
b613a000 b613d000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6146000 b614a000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6153000 b6158000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6161000 b6242000 r-xp /usr/lib/libX11.so.6.3.0
b624d000 b6270000 r-xp /usr/lib/libjpeg.so.8.0.2
b6288000 b629e000 r-xp /lib/libz.so.1.2.5
b62a7000 b62a9000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62b1000 b6326000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6330000 b634a000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6352000 b6386000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b638f000 b6462000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b646e000 b647e000 r-xp /lib/libresolv-2.13.so
b6482000 b649a000 r-xp /usr/lib/liblzma.so.5.0.3
b64a2000 b64a5000 r-xp /lib/libcap.so.2.21
b64ad000 b64dc000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b64e4000 b64e5000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b64ee000 b64f4000 r-xp /usr/lib/libecore_imf.so.1.7.99
b64fc000 b6513000 r-xp /usr/lib/liblua-5.1.so
b651c000 b6523000 r-xp /usr/lib/libembryo.so.1.7.99
b652b000 b6531000 r-xp /lib/librt-2.13.so
b653a000 b6590000 r-xp /usr/lib/libpixman-1.so.0.28.2
b659e000 b65f4000 r-xp /usr/lib/libfreetype.so.6.11.3
b6600000 b6628000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6629000 b666e000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6677000 b668a000 r-xp /usr/lib/libfribidi.so.0.3.1
b6692000 b66ac000 r-xp /usr/lib/libecore_con.so.1.7.99
b66b6000 b66bf000 r-xp /usr/lib/libedbus.so.1.7.99
b66c7000 b6717000 r-xp /usr/lib/libecore_x.so.1.7.99
b6719000 b6722000 r-xp /usr/lib/libvconf.so.0.2.45
b672a000 b673b000 r-xp /usr/lib/libecore_input.so.1.7.99
b6743000 b6748000 r-xp /usr/lib/libecore_file.so.1.7.99
b6750000 b6772000 r-xp /usr/lib/libecore_evas.so.1.7.99
b677b000 b67bc000 r-xp /usr/lib/libeina.so.1.7.99
b67c5000 b67de000 r-xp /usr/lib/libeet.so.1.7.99
b67ef000 b6858000 r-xp /lib/libm-2.13.so
b6861000 b6867000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6870000 b6871000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6879000 b689c000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68a4000 b68a9000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68b1000 b68db000 r-xp /usr/lib/libdbus-1.so.3.8.12
b68e4000 b68fb000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6903000 b690e000 r-xp /lib/libunwind.so.8.0.1
b693b000 b6959000 r-xp /usr/lib/libsystemd.so.0.4.0
b6963000 b6a87000 r-xp /lib/libc-2.13.so
b6a95000 b6a9d000 r-xp /lib/libgcc_s-4.6.so.1
b6a9e000 b6aa2000 r-xp /usr/lib/libsmack.so.1.0.0
b6aab000 b6ab1000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ab9000 b6b89000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6b8a000 b6be8000 r-xp /usr/lib/libedje.so.1.7.99
b6bf2000 b6c09000 r-xp /usr/lib/libecore.so.1.7.99
b6c20000 b6cee000 r-xp /usr/lib/libevas.so.1.7.99
b6d14000 b6e50000 r-xp /usr/lib/libelementary.so.1.7.99
b6e67000 b6e7b000 r-xp /lib/libpthread-2.13.so
b6e86000 b6e88000 r-xp /usr/lib/libdlog.so.0.0.0
b6e90000 b6e93000 r-xp /usr/lib/libbundle.so.0.1.22
b6e9b000 b6e9d000 r-xp /lib/libdl-2.13.so
b6ea6000 b6eb3000 r-xp /usr/lib/libaul.so.0.1.0
b6ec5000 b6ecb000 r-xp /usr/lib/libappcore-efl.so.1.1
b6ed4000 b6ed8000 r-xp /usr/lib/libsys-assert.so
b6ee1000 b6efe000 r-xp /lib/ld-2.13.so
b6f07000 b6f0c000 r-xp /usr/bin/launchpad-loader
b7a79000 b9280000 rw-p [heap]
be8a8000 be8c9000 rw-p [stack]
b7a79000 b9280000 rw-p [heap]
be8a8000 be8c9000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2045)
Call Stack Count: 21
 0: set_targeted_view + 0x87 (0xb584a544) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7544
 1: pop_from_stack_uib_vc + 0x38 (0xb584aa49) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a49
 2: uib_views_destroy_callback + 0x38 (0xb584aa05) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a05
 3: (0xb6c37af9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6c4ec39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6c4137b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6c41ab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb675fcf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6e12953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6bfd3f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6bfae53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6bfe46b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6bfe7db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6dcf44d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x454 (0xb6ec8c69) [/usr/lib/libappcore-efl.so.1] + 0x3c69
15: ui_app_main + 0xb0 (0xb5b6ded5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16:  + 0x0 (0xb584840f) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x540f
17: main + 0x34 (0xb5848b0d) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5b0d
18: (0xb6f08a53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
19: __libc_start_main + 0x114 (0xb697a85c) [/lib/libc.so.6] + 0x1785c
20: (0xb6f08e0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
00 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:13.879+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:13.889+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:13.889+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:13.889+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] direction_x(0), direction_y(1)
05-03 10:05:13.889+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] drag_child_locked_y(0)
05-03 10:05:13.889+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] move content x(0), y(171)
05-03 10:05:13.899+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:13.899+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:13.909+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] direction_x(0), direction_y(1)
05-03 10:05:13.909+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] drag_child_locked_y(0)
05-03 10:05:13.909+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] move content x(0), y(172)
05-03 10:05:13.909+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:13.909+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:13.919+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:13.919+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:13.929+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] direction_x(0), direction_y(1)
05-03 10:05:13.929+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] drag_child_locked_y(0)
05-03 10:05:13.929+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] move content x(0), y(176)
05-03 10:05:13.929+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:13.929+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:13.939+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:13.939+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:13.939+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] direction_x(0), direction_y(1)
05-03 10:05:13.939+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] drag_child_locked_y(0)
05-03 10:05:13.939+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb1d0b740 : elm_scroller] move content x(0), y(183)
05-03 10:05:13.949+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=171027 button=1
05-03 10:05:13.949+0700 E/EFL     (  935): <935> e_mod_processmgr.c:499 _e_mod_processmgr_anr_ping() safety check failed: bd == NULL
05-03 10:05:13.959+0700 I/efl-extension( 2045): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(489) > [0xb1d0b740 : elm_scroller] detent_count(0)
05-03 10:05:13.959+0700 I/efl-extension( 2045): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(490) > [0xb1d0b740 : elm_scroller] pagenumber_v(0), pagenumber_h(0)
05-03 10:05:15.119+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3164) > Deliver KeyPress. value=2669, window=0x3400002
05-03 10:05:15.119+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:537 _ecore_x_event_handle_key_press() KeyEvent:press time=172040
05-03 10:05:15.119+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3175) > Deliver KeyRelease. value=2669, window=0x3400002
05-03 10:05:15.119+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:551 _ecore_x_event_handle_key_release() KeyEvent:release time=172190
05-03 10:05:15.119+0700 E/efl-extension( 2045): efl_extension_events.c: _eext_key_grab_rect_key_up_cb(240) > key up called
05-03 10:05:15.119+0700 E/EFL     ( 2045): elementary<2045> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:05:15.119+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:05:15.119+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:05:15.119+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:05:15.119+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:05:15.159+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] mx(0), my(401), minx(0), miny(0), px(0), py(0)
05-03 10:05:15.159+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] cw(360), ch(761), pw(360), ph(360)
05-03 10:05:16.079+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=173148 button=1
05-03 10:05:16.119+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:16.119+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:16.129+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:16.129+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:16.169+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:16.169+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:16.179+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:16.179+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:16.189+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:16.189+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:16.209+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=173279 button=1
05-03 10:05:16.219+0700 E/GL SELECTED( 2045): ID GL : 67033
05-03 10:05:16.739+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3164) > Deliver KeyPress. value=2669, window=0x3400002
05-03 10:05:16.739+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3175) > Deliver KeyRelease. value=2669, window=0x3400002
05-03 10:05:16.739+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:537 _ecore_x_event_handle_key_press() KeyEvent:press time=173670
05-03 10:05:16.739+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:551 _ecore_x_event_handle_key_release() KeyEvent:release time=173810
05-03 10:05:16.739+0700 E/efl-extension( 2045): efl_extension_events.c: _eext_key_grab_rect_key_up_cb(240) > key up called
05-03 10:05:16.739+0700 E/EFL     ( 2045): elementary<2045> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:05:16.739+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:05:16.739+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:05:16.739+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:05:16.739+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:05:16.769+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] mx(0), my(272), minx(0), miny(0), px(0), py(0)
05-03 10:05:16.769+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c2d3d8 : elm_genlist] cw(360), ch(632), pw(360), ph(360)
05-03 10:05:17.149+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 10:05:17.149+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 10:05:17.149+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: preference_get_string(1258) > preference_get_string(2045) : URL error
05-03 10:05:17.179+0700 I/CAPI_NETWORK_CONNECTION( 2045): connection.c: connection_create(453) > New handle created[0xb3e00528]
05-03 10:05:17.239+0700 E/JSON PARSING( 2045): No data could be display
05-03 10:05:17.239+0700 E/JSON PARSING( 2045): �୐̅�;
05-03 10:05:17.239+0700 I/CAPI_NETWORK_CONNECTION( 2045): connection.c: connection_destroy(471) > Destroy handle: 0xb3e00528
05-03 10:05:17.409+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=174485 button=1
05-03 10:05:17.409+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.419+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.419+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.429+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.429+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.439+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.439+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.449+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.449+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.469+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.469+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.479+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.479+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.479+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4292 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] add hold animator
05-03 10:05:17.489+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.489+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.489+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:05:17.489+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] drag_child_locked_y(0)
05-03 10:05:17.489+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] move content x(0), y(66)
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] drag_child_locked_y(0)
05-03 10:05:17.529+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] move content x(0), y(134)
05-03 10:05:17.559+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.559+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.559+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.559+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.569+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.569+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.569+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.569+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.569+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:05:17.569+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] drag_child_locked_y(0)
05-03 10:05:17.569+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb7c2d3d8 : elm_genlist] move content x(0), y(155)
05-03 10:05:17.609+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=174671 button=1
05-03 10:05:17.609+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.609+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:17.609+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:17.609+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.369+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=175443 button=1
05-03 10:05:18.369+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.379+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.379+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.389+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.389+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.399+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.399+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.409+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.409+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.419+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.419+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.439+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.439+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.469+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.469+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.479+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] mouse move
05-03 10:05:18.479+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c2d3d8 : elm_genlist] hold(0), freeze(0)
05-03 10:05:18.489+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=175562 button=1
05-03 10:05:18.499+0700 E/GL SELECTED( 2045): ID GL : 84466
05-03 10:05:18.509+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb1d0b740 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(113)
05-03 10:05:18.509+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb1d0b740 : elm_scroller] cw(358), ch(360), pw(358), ph(360)
05-03 10:05:18.509+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb1d0b740 : elm_scroller] x(0), y(0)
05-03 10:05:19.039+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
05-03 10:05:19.579+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=176649 button=1
05-03 10:05:19.579+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:19.619+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:19.619+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:19.649+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:19.649+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:19.669+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] mouse move
05-03 10:05:19.669+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb1d0b740 : elm_scroller] hold(0), freeze(0)
05-03 10:05:19.679+0700 E/EFL     ( 2045): ecore_x<2045> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=176758 button=1
05-03 10:05:22.259+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 10:05:22.259+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 10:05:22.259+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: preference_get_string(1258) > preference_get_string(2045) : URL error
05-03 10:05:22.279+0700 I/CAPI_NETWORK_CONNECTION( 2045): connection.c: connection_create(453) > New handle created[0xb3e13528]
05-03 10:05:22.309+0700 E/JSON PARSING( 2045): No data could be display
05-03 10:05:22.309+0700 E/JSON PARSING( 2045): @ѭ�̅�;
05-03 10:05:22.309+0700 I/CAPI_NETWORK_CONNECTION( 2045): connection.c: connection_destroy(471) > Destroy handle: 0xb3e13528
05-03 10:05:24.489+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
05-03 10:05:25.609+0700 W/WATCH_CORE( 1302): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
05-03 10:05:25.609+0700 I/WATCH_CORE( 1302): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
05-03 10:05:25.609+0700 I/CAPI_WATCH_APPLICATION( 1302): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 10:05:25.609+0700 E/watchface-app( 1302): watchface.cpp: OnAppTimeTick(1157) > 
05-03 10:05:25.609+0700 I/watchface-app( 1302): watchface.cpp: OnAppTimeTick(1168) > set force update!!
05-03 10:05:25.609+0700 E/wnoti-service( 1360): wnoti-db-utility.c: context_wearonoff_status_cb(1781) > status changed from 1 to 2 
05-03 10:05:25.609+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_wearonoff_monitor(23513) > wear_monitor_status update[0] = 1 -> 2
05-03 10:05:25.669+0700 W/SHealthServiceCommon( 1749): ContextRestingHeartrateProxy.cpp: OnRestingHrUpdatedCB(347) > [1;40;33mhrValue: 1008[0;m
05-03 10:05:26.949+0700 E/PKGMGR  ( 2176): pkgmgr.c: __pkgmgr_client_uninstall(2061) > uninstall pkg start.
05-03 10:05:27.129+0700 E/PKGMGR_SERVER( 2178): pkgmgr-server.c: main(2242) > package manager server start
05-03 10:05:27.209+0700 E/PKGMGR_SERVER( 2178): pm-mdm.c: _pm_check_mdm_policy(1078) > [0;31m[_pm_check_mdm_policy(): 1078](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
05-03 10:05:27.209+0700 E/PKGMGR  ( 2176): pkgmgr.c: __pkgmgr_client_uninstall(2186) > uninstall pkg finish, ret=[21760002]
05-03 10:05:27.219+0700 E/PKGMGR_SERVER( 2179): pkgmgr-server.c: queue_job(1962) > UNINSTALL start, pkgid=[com.toyota.realtimefeedback]
05-03 10:05:27.299+0700 E/rpm-installer( 2179): rpm-appcore-intf.c: main(120) > ------------------------------------------------
05-03 10:05:27.299+0700 E/rpm-installer( 2179): rpm-appcore-intf.c: main(121) >  [START] rpm-installer: version=[201610629.1]
05-03 10:05:27.299+0700 E/rpm-installer( 2179): rpm-appcore-intf.c: main(122) > ------------------------------------------------
05-03 10:05:27.329+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 10:05:27.329+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 10:05:27.329+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2045): preference.c: preference_get_string(1258) > preference_get_string(2045) : URL error
05-03 10:05:27.369+0700 I/CAPI_NETWORK_CONNECTION( 2045): connection.c: connection_create(453) > New handle created[0xb3e15598]
05-03 10:05:27.399+0700 E/JSON PARSING( 2045): No data could be display
05-03 10:05:27.399+0700 E/JSON PARSING( 2045): ����̅�;
05-03 10:05:27.399+0700 I/CAPI_NETWORK_CONNECTION( 2045): connection.c: connection_destroy(471) > Destroy handle: 0xb3e15598
05-03 10:05:27.419+0700 E/rpm-installer( 2179): rpm-cmdline.c: __ri_is_core_tpk_app(358) > This is a core tpk app.
05-03 10:05:27.499+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:05:27.499+0700 I/watchface-app( 1302): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
05-03 10:05:27.499+0700 W/W_HOME  ( 1192): clock_event.c: _pkgmgr_event_cb(194) > Pkg:com.toyota.realtimefeedback is being uninstalled
05-03 10:05:27.499+0700 W/W_HOME  ( 1192): dbox.c: uninstall_cb(1434) > uninstalled:com.toyota.realtimefeedback
05-03 10:05:27.509+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2045
05-03 10:05:27.509+0700 E/rpm-installer( 2179): installer-util.c: __check_running_app(1774) > app[com.toyota.realtimefeedback] is running, need to terminate it.
05-03 10:05:27.509+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 10:05:27.509+0700 W/AUL     ( 2179): launch.c: app_request_to_launchpad(284) > request cmd(5) to(2045)
05-03 10:05:27.509+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 5
05-03 10:05:27.509+0700 W/AUL     (  971): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2045) type(uiapp)
05-03 10:05:27.519+0700 W/AUL_AMD (  971): amd_launch.c: __reply_handler(999) > listen fd(22) , send fd(15), pid(2045), cmd(4)
05-03 10:05:27.519+0700 I/APP_CORE( 2045): appcore-efl.c: __do_app(453) > [APP 2045] Event: TERMINATE State: RUNNING
05-03 10:05:27.519+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 22
05-03 10:05:27.519+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(999) > app status : 4
05-03 10:05:27.519+0700 W/AUL_AMD (  971): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(start), value(uninstall)
05-03 10:05:27.519+0700 W/AUL_AMD (  971): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(997) > __amd_pkgmgrinfo_start_handler
05-03 10:05:27.519+0700 W/APP_CORE( 2045): appcore-efl.c: appcore_efl_main(1788) > power off : 0
05-03 10:05:27.519+0700 W/APP_CORE( 2045): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3400002] -> redirected win[600245] for com.toyota.realtimefeedback[2045]
05-03 10:05:27.529+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [UNINSTALL, STARTED]
05-03 10:05:27.529+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 10:05:27.529+0700 W/AUL     ( 2179): launch.c: app_request_to_launchpad(298) > request cmd(5) result(0)
05-03 10:05:27.539+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:05:27.539+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2045
05-03 10:05:27.569+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 10:05:27.569+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 03:10:45 (UTC).
05-03 10:05:27.569+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 10:05:27.579+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 10:05:27.589+0700 I/APP_CORE( 2045): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
05-03 10:05:27.589+0700 I/CAPI_APPFW_APPLICATION( 2045): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
05-03 10:05:27.599+0700 I/efl-extension( 2045): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 10:05:27.599+0700 I/efl-extension( 2045): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7bba130 is freed
05-03 10:05:27.599+0700 E/EFL     ( 2045): elementary<2045> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7ba6b80 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 10:05:27.599+0700 I/efl-extension( 2045): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb7ba75c8, obj: 0xb7ba75c8
05-03 10:05:27.599+0700 I/efl-extension( 2045): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 10:05:27.599+0700 E/EFL     ( 2045): elementary<2045> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:05:27.599+0700 E/EFL     ( 2045): elementary<2045> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:05:27.599+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] mx(3240), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:05:27.609+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] cw(3600), ch(360), pw(360), ph(360)
05-03 10:05:27.609+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
05-03 10:05:27.609+0700 W/STARTER ( 1119): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1192] goes to (3)
05-03 10:05:27.609+0700 E/STARTER ( 1119): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1192)'s state(3)
05-03 10:05:27.609+0700 W/AUL_AMD (  971): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
05-03 10:05:27.609+0700 W/AUL_AMD (  971): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
05-03 10:05:27.609+0700 W/AUL     (  971): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1192) status(fg) type(uiapp)
05-03 10:05:27.609+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7ba75c8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:05:27.609+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7ba75c8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:05:27.609+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7ba75c8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:05:27.609+0700 E/EFL     ( 2045): elementary<2045> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7ba75c8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:05:27.609+0700 I/efl-extension( 2045): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 10:05:27.609+0700 I/efl-extension( 2045): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7bcfd20 is freed
05-03 10:05:27.609+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:05:27.609+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7ba75c8, elm_genlist, func : 0xb57e5ea1
05-03 10:05:27.609+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:05:27.619+0700 I/APP_CORE( 2045): appcore-efl.c: __after_loop(1243) > [APP 2045] After terminate() callback
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb1d111c8 is freed
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7c2d3d8, elm_genlist, func : 0xb57e5ea1
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb1d99318
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb1da80e0 is freed
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb1d0b740, elm_scroller, func : 0xb57e9379
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb1d99318, elm_image, func : 0xb57e9379
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:05:27.619+0700 I/efl-extension( 2045): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb1d0b740 : elm_scroller] rotary callabck is deleted
05-03 10:05:27.649+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:05:27.649+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2045
05-03 10:05:27.759+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:05:27.759+0700 W/PROCESSMGR(  935): e_mod_processmgr.c: _e_mod_processmgr_send_update_watch_action(663) > [PROCESSMGR] =====================> send UpdateClock
05-03 10:05:27.759+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2045
05-03 10:05:27.769+0700 W/WATCH_CORE( 1302): appcore-watch.c: __signal_process_manager_handler(1269) > process_manager_signal
05-03 10:05:27.769+0700 I/WATCH_CORE( 1302): appcore-watch.c: __signal_process_manager_handler(1285) > Call the time_tick_cb
05-03 10:05:27.769+0700 I/CAPI_WATCH_APPLICATION( 1302): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 10:05:27.769+0700 E/watchface-app( 1302): watchface.cpp: OnAppTimeTick(1157) > 
05-03 10:05:27.769+0700 I/watchface-app( 1302): watchface.cpp: OnAppTimeTick(1168) > set force update!!
05-03 10:05:27.769+0700 W/W_HOME  ( 1192): event_manager.c: _ecore_x_message_cb(421) > state: 1 -> 0
05-03 10:05:27.769+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:05:27.769+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:05:27.769+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 10:05:27.769+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 10:05:27.769+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:05:27.769+0700 W/W_HOME  ( 1192): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 1
05-03 10:05:27.779+0700 W/W_HOME  ( 1192): event_manager.c: _window_visibility_cb(460) > Window [0x2000003] is now visible(0)
05-03 10:05:27.779+0700 W/W_HOME  ( 1192): event_manager.c: _window_visibility_cb(470) > state: 0 -> 1
05-03 10:05:27.779+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:05:27.779+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:6, app_state:2 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:05:27.779+0700 W/W_HOME  ( 1192): main.c: _window_visibility_cb(996) > Window [0x2000003] is now visible(0)
05-03 10:05:27.779+0700 I/APP_CORE( 1192): appcore-efl.c: __do_app(453) > [APP 1192] Event: RESUME State: PAUSED
05-03 10:05:27.779+0700 I/CAPI_APPFW_APPLICATION( 1192): app_main.c: app_appcore_resume(223) > app_appcore_resume
05-03 10:05:27.779+0700 W/W_HOME  ( 1192): main.c: _appcore_resume_cb(480) > appcore resume
05-03 10:05:27.779+0700 W/W_HOME  ( 1192): event_manager.c: _app_resume_cb(373) > state: 2 -> 1
05-03 10:05:27.779+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:05:27.779+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 10:05:27.779+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 10:05:27.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:05:27.789+0700 W/W_HOME  ( 1192): main.c: home_resume(528) > journal_multimedia_screen_loaded_home called
05-03 10:05:27.789+0700 W/W_HOME  ( 1192): main.c: home_resume(532) > clock/widget resumed
05-03 10:05:27.789+0700 E/W_HOME  ( 1192): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
05-03 10:05:27.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:05:27.789+0700 I/GATE    ( 1192): <GATE-M>APP_FULLY_LOADED_w-home</GATE-M>
05-03 10:05:27.789+0700 I/wnotib  ( 1192): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 0
05-03 10:05:27.789+0700 E/wnotib  ( 1192): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
05-03 10:05:27.789+0700 W/wnotib  ( 1192): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(981) > No postponed update with is_for_VI: 0.
05-03 10:05:27.869+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:05:27.869+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2045
05-03 10:05:27.979+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:05:27.989+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2045
05-03 10:05:28.089+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:05:28.109+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
05-03 10:05:28.179+0700 E/PKGMGR_PARSER( 2179): pkgmgr_parser_signature.c: __ps_check_mdm_policy_by_pkgid(1056) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
05-03 10:05:28.219+0700 I/PRIVACY-MANAGER-CLIENT( 2179): SocketClient.cpp: SocketClient(37) > Client created
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: mainloop(227) > Got incoming connection
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-CLIENT( 2179): SocketStream.h: SocketStream(31) > Created
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-CLIENT( 2179): SocketConnection.h: SocketConnection(44) > Created
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-CLIENT( 2179): SocketClient.cpp: connect(62) > Client connected
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionThread(253) > Starting connection thread
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketStream.h: SocketStream(31) > Created
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketConnection.h: SocketConnection(44) > Created
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionService(304) > Calling service
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionService(307) > Removing client
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionService(311) > Call served
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionThread(262) > Client serviced
05-03 10:05:28.229+0700 I/PRIVACY-MANAGER-CLIENT( 2179): SocketClient.cpp: disconnect(72) > Client disconnected
05-03 10:05:28.279+0700 W/AUL_PAD ( 1882): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2045 pgid = 2045
05-03 10:05:28.279+0700 W/AUL_PAD ( 1882): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 11
05-03 10:05:28.279+0700 W/CRASH_MANAGER( 2183): worker.c: worker_job(1205) > 1102045726561152531672
