S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2037
Date: 2018-04-30 15:36:35+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2037, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb58fe954
r2   = 0xb58fef60, r3   = 0x00000000
r4   = 0xb8ca8cf0, r5   = 0xb8d16110
r6   = 0x00000013, r7   = 0xbe997208
r8   = 0x0000209d, r9   = 0xb8ca8cf0
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb58feba8, sp   = 0xbe9971e8
lr   = 0xb58ec24d, pc   = 0xb58ec282
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      3324 KB
Buffers:      8796 KB
Cached:     115160 KB
VmPeak:     108712 KB
VmSize:     108164 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23036 KB
VmRSS:       22244 KB
VmData:      47932 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          70 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2037 TID = 2037
2037 2065 2066 

Maps Information
b1c00000 b1c03000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b1eaa000 b1eae000 r-xp /usr/lib/libogg.so.0.7.1
b1eb6000 b1ed8000 r-xp /usr/lib/libvorbis.so.0.4.3
b1ee0000 b1f27000 r-xp /usr/lib/libsndfile.so.1.0.26
b1f33000 b1f7c000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1f85000 b1f8a000 r-xp /usr/lib/libjson.so.0.0.1
b382b000 b3931000 r-xp /usr/lib/libicuuc.so.57.1
b3947000 b3acf000 r-xp /usr/lib/libicui18n.so.57.1
b3adf000 b3aec000 r-xp /usr/lib/libail.so.0.1.0
b3af5000 b3af8000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3b00000 b3b38000 r-xp /usr/lib/libpulse.so.0.16.2
b3b39000 b3b3c000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3b44000 b3ba5000 r-xp /usr/lib/libasound.so.2.0.0
b3baf000 b3bc8000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3bd1000 b3bd5000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3bdd000 b3be8000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3bf5000 b3bf9000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3c02000 b3c1a000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3c2b000 b3c32000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3c3a000 b3c45000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3c4d000 b3c4f000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3c65000 b3c66000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3c6e000 b3c6f000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3ee7000 b3f6e000 rw-s anon_inode:dmabuf
b3f6f000 b476e000 rw-p [stack:2066]
b48f9000 b48fa000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b03000 b4b04000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4b0c000 b4b14000 r-xp /usr/lib/libfeedback.so.0.1.4
b4b45000 b5344000 rw-p [stack:2065]
b5344000 b5346000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b534e000 b5365000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b5372000 b5374000 r-xp /usr/lib/libdri2.so.0.0.0
b537c000 b5387000 r-xp /usr/lib/libtbm.so.1.0.0
b538f000 b5397000 r-xp /usr/lib/libdrm.so.2.4.0
b539f000 b53a1000 r-xp /usr/lib/libgenlock.so
b53a9000 b53ae000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b53b6000 b53c1000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b55ca000 b5694000 r-xp /usr/lib/libCOREGL.so.4.0
b56a5000 b56b5000 r-xp /usr/lib/libmdm-common.so.1.1.25
b56bd000 b56c3000 r-xp /usr/lib/libxcb-render.so.0.0.0
b56cb000 b56cc000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b56d5000 b56d8000 r-xp /usr/lib/libEGL.so.1.4
b56e0000 b56ee000 r-xp /usr/lib/libGLESv2.so.2.0
b56f7000 b5740000 r-xp /usr/lib/libmdm.so.1.2.70
b5749000 b574f000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5757000 b5760000 r-xp /usr/lib/libcom-core.so.0.0.1
b5769000 b5821000 r-xp /usr/lib/libcairo.so.2.11200.14
b582c000 b5845000 r-xp /usr/lib/libnetwork.so.0.0.0
b584d000 b5859000 r-xp /usr/lib/libnotification.so.0.1.0
b5862000 b5871000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b587a000 b589b000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58a3000 b58a8000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58b0000 b58b5000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b58bd000 b58cd000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b58d5000 b58dd000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b58e5000 b58ef000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a94000 b5a9e000 r-xp /lib/libnss_files-2.13.so
b5aa7000 b5b76000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b8c000 b5bb0000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5bb9000 b5bbf000 r-xp /usr/lib/libappsvc.so.0.1.0
b5bc7000 b5bcb000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5bd8000 b5be3000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5beb000 b5bed000 r-xp /usr/lib/libiniparser.so.0
b5bf6000 b5bfb000 r-xp /usr/lib/libappcore-common.so.1.1
b5c03000 b5c05000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c0e000 b5c12000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c1f000 b5c21000 r-xp /usr/lib/libXau.so.6.0.0
b5c29000 b5c30000 r-xp /lib/libcrypt-2.13.so
b5c60000 b5c62000 r-xp /usr/lib/libiri.so
b5c6b000 b5dfd000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e1e000 b5e65000 r-xp /usr/lib/libssl.so.1.0.0
b5e71000 b5e9f000 r-xp /usr/lib/libidn.so.11.5.44
b5ea7000 b5eb0000 r-xp /usr/lib/libcares.so.2.1.0
b5eba000 b5ecd000 r-xp /usr/lib/libxcb.so.1.1.0
b5ed6000 b5ed9000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ee1000 b5ee3000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5eec000 b5fb8000 r-xp /usr/lib/libxml2.so.2.7.8
b5fc5000 b5fc7000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5fd0000 b5fd5000 r-xp /usr/lib/libffi.so.5.0.10
b5fdd000 b5fde000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5fe6000 b5fe9000 r-xp /lib/libattr.so.1.1.0
b5ff1000 b6085000 r-xp /usr/lib/libstdc++.so.6.0.16
b6098000 b60b5000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b60bf000 b60d7000 r-xp /usr/lib/libpng12.so.0.50.0
b60df000 b60f5000 r-xp /lib/libexpat.so.1.6.0
b60ff000 b6143000 r-xp /usr/lib/libcurl.so.4.3.0
b614c000 b6156000 r-xp /usr/lib/libXext.so.6.4.0
b6160000 b6164000 r-xp /usr/lib/libXtst.so.6.1.0
b616c000 b6172000 r-xp /usr/lib/libXrender.so.1.3.0
b617a000 b6180000 r-xp /usr/lib/libXrandr.so.2.2.0
b6188000 b6189000 r-xp /usr/lib/libXinerama.so.1.0.0
b6192000 b619b000 r-xp /usr/lib/libXi.so.6.1.0
b61a3000 b61a6000 r-xp /usr/lib/libXfixes.so.3.1.0
b61af000 b61b1000 r-xp /usr/lib/libXgesture.so.7.0.0
b61b9000 b61bb000 r-xp /usr/lib/libXcomposite.so.1.0.0
b61c3000 b61c5000 r-xp /usr/lib/libXdamage.so.1.1.0
b61cd000 b61d4000 r-xp /usr/lib/libXcursor.so.1.0.2
b61dc000 b61df000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b61e8000 b61ec000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b61f5000 b61fa000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6203000 b62e4000 r-xp /usr/lib/libX11.so.6.3.0
b62ef000 b6312000 r-xp /usr/lib/libjpeg.so.8.0.2
b632a000 b6340000 r-xp /lib/libz.so.1.2.5
b6349000 b634b000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6353000 b63c8000 r-xp /usr/lib/libsqlite3.so.0.8.6
b63d2000 b63ec000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b63f4000 b6428000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6431000 b6504000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6510000 b6520000 r-xp /lib/libresolv-2.13.so
b6524000 b653c000 r-xp /usr/lib/liblzma.so.5.0.3
b6544000 b6547000 r-xp /lib/libcap.so.2.21
b654f000 b657e000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6586000 b6587000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6590000 b6596000 r-xp /usr/lib/libecore_imf.so.1.7.99
b659e000 b65b5000 r-xp /usr/lib/liblua-5.1.so
b65be000 b65c5000 r-xp /usr/lib/libembryo.so.1.7.99
b65cd000 b65d3000 r-xp /lib/librt-2.13.so
b65dc000 b6632000 r-xp /usr/lib/libpixman-1.so.0.28.2
b6640000 b6696000 r-xp /usr/lib/libfreetype.so.6.11.3
b66a2000 b66ca000 r-xp /usr/lib/libfontconfig.so.1.8.0
b66cb000 b6710000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6719000 b672c000 r-xp /usr/lib/libfribidi.so.0.3.1
b6734000 b674e000 r-xp /usr/lib/libecore_con.so.1.7.99
b6758000 b6761000 r-xp /usr/lib/libedbus.so.1.7.99
b6769000 b67b9000 r-xp /usr/lib/libecore_x.so.1.7.99
b67bb000 b67c4000 r-xp /usr/lib/libvconf.so.0.2.45
b67cc000 b67dd000 r-xp /usr/lib/libecore_input.so.1.7.99
b67e5000 b67ea000 r-xp /usr/lib/libecore_file.so.1.7.99
b67f2000 b6814000 r-xp /usr/lib/libecore_evas.so.1.7.99
b681d000 b685e000 r-xp /usr/lib/libeina.so.1.7.99
b6867000 b6880000 r-xp /usr/lib/libeet.so.1.7.99
b6891000 b68fa000 r-xp /lib/libm-2.13.so
b6903000 b6909000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6912000 b6913000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b691b000 b693e000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6946000 b694b000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6953000 b697d000 r-xp /usr/lib/libdbus-1.so.3.8.12
b6986000 b699d000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69a5000 b69b0000 r-xp /lib/libunwind.so.8.0.1
b69dd000 b69fb000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a05000 b6b29000 r-xp /lib/libc-2.13.so
b6b37000 b6b3f000 r-xp /lib/libgcc_s-4.6.so.1
b6b40000 b6b44000 r-xp /usr/lib/libsmack.so.1.0.0
b6b4d000 b6b53000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b5b000 b6c2b000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c2c000 b6c8a000 r-xp /usr/lib/libedje.so.1.7.99
b6c94000 b6cab000 r-xp /usr/lib/libecore.so.1.7.99
b6cc2000 b6d90000 r-xp /usr/lib/libevas.so.1.7.99
b6db6000 b6ef2000 r-xp /usr/lib/libelementary.so.1.7.99
b6f09000 b6f1d000 r-xp /lib/libpthread-2.13.so
b6f28000 b6f2a000 r-xp /usr/lib/libdlog.so.0.0.0
b6f32000 b6f35000 r-xp /usr/lib/libbundle.so.0.1.22
b6f3d000 b6f3f000 r-xp /lib/libdl-2.13.so
b6f48000 b6f55000 r-xp /usr/lib/libaul.so.0.1.0
b6f67000 b6f6d000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f76000 b6f7a000 r-xp /usr/lib/libsys-assert.so
b6f83000 b6fa0000 r-xp /lib/ld-2.13.so
b6fa9000 b6fae000 r-xp /usr/bin/launchpad-loader
b8b8c000 b90c8000 rw-p [heap]
be977000 be998000 rw-p [stack]
be977000 be998000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2037)
Call Stack Count: 21
 0: set_targeted_view + 0xd5 (0xb58ec282) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7282
 1: pop_from_stack_uib_vc + 0x38 (0xb58ec739) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7739
 2: uib_views_destroy_callback + 0x38 (0xb58ec6f5) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x76f5
 3: (0xb6cd9af9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6cf0c39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6ce337b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6ce3ab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb6801cf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6eb4953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6c9f3f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6c9ce53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6ca046b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6ca07db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6e7144d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x454 (0xb6f6ac69) [/usr/lib/libappcore-efl.so.1] + 0x3c69
15: ui_app_main + 0xb0 (0xb5c0fed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16: uib_app_run + 0xea (0xb58ea40b) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x540b
17: main + 0x34 (0xb58ea8f5) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x58f5
18:  + 0x0 (0xb6faaa53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
19: __libc_start_main + 0x114 (0xb6a1c85c) [/lib/libc.so.6] + 0x1785c
20: (0xb6faae0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(150) > [0;40;31mPM25 : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(151) > [0;40;31mUVIndex : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(152) > [0;40;31mAQI : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(153) > [0;40;31mPressure : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(155) > [0;40;31mcold : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(156) > [0;40;31mrunning : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(157) > [0;40;31mpollen : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(158) > [0;40;31mairdust : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(159) > [0;40;31mdryskin : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(160) > [0;40;31mdiscomfort : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(161) > [0;40;31mfrozenPipes : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(162) > [0;40;31mumbrella : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(163) > [0;40;31mfoodPoison : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(164) > [0;40;31mfrostbite : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(165) > [0;40;31mgolf : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(166) > [0;40;31mheat : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(193) > [0;40;31mhourlyForecastNumber : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): StringHelper.cpp: StringToInt(133) > [0;40;31m[StringToInt(): 133] (str.length() <= 0) [return][0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(211) > [0;40;31mdailyForecastNumber : [0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
04-30 15:36:33.240+0700 E/weather-common( 2116): StringHelper.cpp: StringToInt(133) > [0;40;31m[StringToInt(): 133] (str.length() <= 0) [return][0;m
04-30 15:36:33.240+0700 E/weather-widget( 1467): WidgetMain.cpp: RequestUpdateForEachInstances(138) > [0;40;31mrequest widget_service_trigger_update to /opt/usr/share/live_magazine/com.samsung.weather-widget_1210_14.484322.png[0;m
04-30 15:36:33.240+0700 E/weather-widget( 1467): WidgetMain.cpp: UpdateWidgetInstance(701) > [0;40;31mUpdateWidgetInstance[0;m
04-30 15:36:33.260+0700 E/weather-common( 1467): CPType.cpp: Renew(90) > [0;40;31mCPType is renewed : 5[0;m
04-30 15:36:33.260+0700 E/weather-common( 1467): DataManager.cpp: LoadData(326) > [0;40;31mnewCpTypeStr : TWC[0;m
04-30 15:36:33.260+0700 E/weather-common( 1467): DataManager.cpp: LoadData(329) > [0;40;31mweather data loaded[0;m
04-30 15:36:33.270+0700 E/weather-agent( 2116): AgentMain.cpp: StartServiceTerminateTimer(134) > [0;40;31mthere is same appControl. we should erase it from mAgentControllerPtrVector[0;m
04-30 15:36:33.270+0700 E/weather-widget( 1467): WidgetViewData.cpp: SetWidgetLifeCycleState(382) > [0;40;31mSetWidgetLifeCycleState, mWidgetLifeCycleState:1[0;m
04-30 15:36:33.270+0700 E/weather-widget( 1467): WidgetMain.cpp: UpdateWidgetInstance(727) > [0;40;31mcontent is NULL[0;m
04-30 15:36:33.270+0700 E/weather-widget( 1467): WidgetMainView.cpp: Resume(1236) > [0;40;31mWidgetMainView::Resume[0;m
04-30 15:36:33.270+0700 E/weather-agent( 2116): AgentMain.cpp: StartServiceTerminateTimer(152) > [0;40;31mStart ServiceTerminateTimer. Wait ...[0;m
04-30 15:36:33.270+0700 E/weather-common( 2116): Location.cpp: RemoveEventListener(470) > [0;40;31meventListener:0xb7792a20 not exist[0;m
04-30 15:36:33.320+0700 E/weather-widget( 1467): WidgetMainView.cpp: AddTouchEventCallback(143) > [0;40;31mAddTouchEventCallback[0;m
04-30 15:36:33.320+0700 E/weather-widget( 1467): WidgetMainView.cpp: UpdateViewPage(267) > [0;40;31mUpdateViewPage[0;m
04-30 15:36:33.320+0700 E/weather-widget( 1467): WidgetMainView.cpp: HideFindingLocationView(595) > [0;40;31m[HideFindingLocationView(): 595] (mFindingLocationView == NULL) [return][0;m
04-30 15:36:33.320+0700 E/weather-widget( 1467): WidgetMainView.cpp: UpdateViewPage(276) > [0;40;31mUpdateViewPage locationId : e0ae79d8e25f1902b7edbb7c162fdd27b2def8c7cf049d7bd71824fa00491abe[0;m
04-30 15:36:33.320+0700 E/weather-widget( 1467): WidgetMainView.cpp: UpdateViewExceptLifeIndexProgress(377) > [0;40;31mUpdateViewExceptLifeIndexProgress[0;m
04-30 15:36:33.330+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.350+0700 E/weather-widget( 1467): WidgetViewData.cpp: GetWidgetLifeCycleState(389) > [0;40;31mGetWidgetLifeCycleState, mWidgetLifeCycleState:1[0;m
04-30 15:36:33.350+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: CreateHighLowTemperature(249) > [0;40;31mLTR[0;m
04-30 15:36:33.360+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: CreateIconAndTemperature(355) > [0;40;31mLTR[0;m
04-30 15:36:33.360+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.380+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.380+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.380+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-30 15:36:33.380+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-30 15:36:33.380+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-30 15:36:33.390+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.390+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: CreateRightLifeIndexWeatherInfo(524) > [0;40;31mcolorCode : AO094[0;m
04-30 15:36:33.390+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-30 15:36:33.390+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-30 15:36:33.390+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-30 15:36:33.390+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.390+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.390+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.400+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
04-30 15:36:33.400+0700 E/weather-widget( 1467): WidgetMainViewData.cpp: GetProgressAngleOnUVIndexWithCityIndex(142) > [0;40;31muv_index : 0, deltaAngle : 16.000000[0;m
04-30 15:36:33.400+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: DrawLifeIndexText(603) > [0;40;31mleftText : 100%, leftFontCode : AT094[0;m
04-30 15:36:33.400+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: DrawLifeIndexText(604) > [0;40;31mrightText : Low, rightFontCode : AT0941[0;m
04-30 15:36:33.400+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: SetFontCodeForCairo(682) > [0;40;31mfont : Tizen:style=normal:weight=regular[0;m
04-30 15:36:33.410+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: DrawLifeIndexText(629) > [0;40;31m[Left] textAngle : 14.946725, endAngle : 223.999997, startAngle : 209.053272[0;m
04-30 15:36:33.420+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: SetFontCodeForCairo(682) > [0;40;31mfont : Tizen:style=normal:weight=regular[0;m
04-30 15:36:33.420+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: DrawLifeIndexText(660) > [0;40;31m[Right] textAngle : 11.032107, endAngle : 29.032107, startAngle : 18.000000[0;m
04-30 15:36:33.430+0700 E/weather-common( 1467): SamsungLogManager.cpp: OnWeatherWidgetResumed(248) > [0;40;31mOnWeatherWidgetResumed[0;m
04-30 15:36:33.430+0700 I/CAPI_WIDGET_APPLICATION( 1467): widget_app.c: __provider_update_cb(281) > received updating signal
04-30 15:36:33.480+0700 W/WATCH_CORE( 1313): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
04-30 15:36:33.480+0700 I/WATCH_CORE( 1313): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
04-30 15:36:33.480+0700 I/CAPI_WATCH_APPLICATION( 1313): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 15:36:33.480+0700 E/watchface-app( 1313): watchface.cpp: OnAppTimeTick(1157) > 
04-30 15:36:33.480+0700 I/watchface-app( 1313): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-30 15:36:33.480+0700 W/WAKEUP-SERVICE( 1654): WakeupService.cpp: OnReceiveGestureChanged(995) > [0;32mINFO: wakeup receive data : -1220382808[0;m
04-30 15:36:33.480+0700 W/WAKEUP-SERVICE( 1654): WakeupService.cpp: OnReceiveGestureChanged(996) > [0;32mINFO: WakeupServiceStart[0;m
04-30 15:36:33.480+0700 W/WAKEUP-SERVICE( 1654): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
04-30 15:36:33.480+0700 W/W_HOME  ( 1210): dbus.c: _dbus_message_recv_cb(169) > gesture:wristup
04-30 15:36:33.490+0700 W/WAKEUP-SERVICE( 1654): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
04-30 15:36:33.490+0700 I/TIZEN_N_SOUND_MANAGER( 1654): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
04-30 15:36:33.490+0700 W/TIZEN_N_SOUND_MANAGER( 1654): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
04-30 15:36:33.490+0700 E/WAKEUP-SERVICE( 1654): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-30 15:36:33.500+0700 E/WAKEUP-SERVICE( 1654): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-30 15:36:33.510+0700 I/TIZEN_N_SOUND_MANAGER( 1654): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
04-30 15:36:33.520+0700 I/TIZEN_N_SOUND_MANAGER( 1654): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
04-30 15:36:33.520+0700 W/TIZEN_N_SOUND_MANAGER( 1654): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-30 15:36:33.520+0700 W/WAKEUP-SERVICE( 1654): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
04-30 15:36:33.520+0700 I/HIGEAR  ( 1654): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
04-30 15:36:34.300+0700 E/weather-agent( 2116): AgentMain.cpp: TerminateService(97) > [0;40;31mTerminateService[0;m
04-30 15:36:34.300+0700 I/CAPI_APPFW_APPLICATION( 2116): service_app_main.c: service_app_exit(441) > service_app_exit
04-30 15:36:34.320+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 22
04-30 15:36:34.320+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(999) > app status : 4
04-30 15:36:34.320+0700 E/weather-agent( 2116): AgentMain.cpp: AppTerminate(283) > [0;40;31mAppTerminate[0;m
04-30 15:36:34.350+0700 E/PKGMGR  ( 2171): pkgmgr.c: __pkgmgr_client_uninstall(2061) > uninstall pkg start.
04-30 15:36:34.390+0700 E/weather-agent( 2116): AgentMain.cpp: AppTerminate(287) > [0;40;31mdevice_power_release_lock success[0;m
04-30 15:36:34.510+0700 E/PKGMGR_SERVER( 2173): pkgmgr-server.c: main(2242) > package manager server start
04-30 15:36:34.540+0700 E/CAPI_APPFW_APP_CONTROL( 2116): app_control.c: app_control_error(138) > [app_control_destroy] INVALID_PARAMETER(0xffffffea)
04-30 15:36:34.540+0700 E/weather-agent( 2116): AgentMain.cpp: ~AgentMain(77) > [0;40;31mapp_control_destroy failed[0;m
04-30 15:36:34.590+0700 E/PKGMGR_SERVER( 2173): pm-mdm.c: _pm_check_mdm_policy(1078) > [0;31m[_pm_check_mdm_policy(): 1078](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
04-30 15:36:34.590+0700 E/PKGMGR  ( 2171): pkgmgr.c: __pkgmgr_client_uninstall(2186) > uninstall pkg finish, ret=[21710002]
04-30 15:36:34.640+0700 E/PKGMGR_SERVER( 2174): pkgmgr-server.c: queue_job(1962) > UNINSTALL start, pkgid=[com.toyota.realtimefeedback]
04-30 15:36:34.680+0700 W/AUL_AMD (  972): amd_status.c: __app_terminate_timer_cb(168) > send SIGKILL: No such process
04-30 15:36:34.710+0700 E/rpm-installer( 2174): rpm-appcore-intf.c: main(120) > ------------------------------------------------
04-30 15:36:34.710+0700 E/rpm-installer( 2174): rpm-appcore-intf.c: main(121) >  [START] rpm-installer: version=[201610629.1]
04-30 15:36:34.710+0700 E/rpm-installer( 2174): rpm-appcore-intf.c: main(122) > ------------------------------------------------
04-30 15:36:34.800+0700 E/rpm-installer( 2174): rpm-cmdline.c: __ri_is_core_tpk_app(358) > This is a core tpk app.
04-30 15:36:34.870+0700 I/watchface-app( 1313): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
04-30 15:36:34.870+0700 W/W_HOME  ( 1210): clock_event.c: _pkgmgr_event_cb(194) > Pkg:com.toyota.realtimefeedback is being uninstalled
04-30 15:36:34.870+0700 W/W_HOME  ( 1210): dbox.c: uninstall_cb(1434) > uninstalled:com.toyota.realtimefeedback
04-30 15:36:34.880+0700 W/AUL_AMD (  972): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(start), value(uninstall)
04-30 15:36:34.880+0700 W/AUL_AMD (  972): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(997) > __amd_pkgmgrinfo_start_handler
04-30 15:36:34.900+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [UNINSTALL, STARTED]
04-30 15:36:34.900+0700 E/ALARM_MANAGER(  976): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-30 15:36:34.900+0700 E/ALARM_MANAGER(  976): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-30 15:36:34.900+0700 E/ALARM_MANAGER(  976): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 30-4-2018, 08:39:00 (UTC).
04-30 15:36:34.900+0700 E/ALARM_MANAGER(  976): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-30 15:36:34.910+0700 E/ALARM_MANAGER(  976): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-30 15:36:34.920+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:36:34.920+0700 W/AUL_AMD (  972): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2037
04-30 15:36:34.920+0700 E/rpm-installer( 2174): installer-util.c: __check_running_app(1774) > app[com.toyota.realtimefeedback] is running, need to terminate it.
04-30 15:36:34.920+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 15:36:34.930+0700 W/AUL     ( 2174): launch.c: app_request_to_launchpad(284) > request cmd(5) to(2037)
04-30 15:36:34.930+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 5
04-30 15:36:34.930+0700 W/AUL     (  972): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2037) type(uiapp)
04-30 15:36:34.930+0700 I/APP_CORE( 2037): appcore-efl.c: __do_app(453) > [APP 2037] Event: TERMINATE State: RUNNING
04-30 15:36:34.930+0700 W/AUL_AMD (  972): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(2037), cmd(4)
04-30 15:36:34.930+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 22
04-30 15:36:34.930+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(999) > app status : 4
04-30 15:36:34.930+0700 W/APP_CORE( 2037): appcore-efl.c: appcore_efl_main(1788) > power off : 0
04-30 15:36:34.930+0700 W/APP_CORE( 2037): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3000002] -> redirected win[600228] for com.toyota.realtimefeedback[2037]
04-30 15:36:34.930+0700 W/AUL     ( 2174): launch.c: app_request_to_launchpad(298) > request cmd(5) result(0)
04-30 15:36:34.930+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:36:34.940+0700 W/AUL_AMD (  972): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2037
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 15:36:34.950+0700 E/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: HandleReceivedMessage(588) > Connection closed
04-30 15:36:34.950+0700 E/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: HandleReceivedMessage(610) > All connections of client(2116) are closed. delete client info
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnIpcClientDisconnected(178) > MessagePort Ipc disconnected
04-30 15:36:34.950+0700 E/MESSAGE_PORT(  967): MessagePortStub.cpp: OnIpcClientDisconnected(181) > Unregister - client =  2116
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: UnregisterMessagePortByDiscon(273) > _MessagePortService::UnregisterMessagePortByDiscon
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.950+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-30 15:36:34.960+0700 E/RESOURCED( 1131): procfs.c: proc_get_oom_score_adj(178) > fopen /proc/2116/oom_score_adj failed
04-30 15:36:34.960+0700 E/RESOURCED( 1131): proc-main.c: resourced_proc_status_change(1501) > Empty pid or process not exists. 2116
04-30 15:36:34.980+0700 I/APP_CORE( 2037): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-30 15:36:34.980+0700 I/CAPI_APPFW_APPLICATION( 2037): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
04-30 15:36:34.990+0700 W/AUL     ( 2176): daemon-manager-release-agent.c: main(12) > release agent : [2:/com.samsung.weather-agent]
04-30 15:36:34.990+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 23
04-30 15:36:34.990+0700 W/AUL_AMD (  972): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 0
04-30 15:36:34.990+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(1032) > pkg_status: installed, dead pid: 2116
04-30 15:36:34.990+0700 W/AUL_AMD (  972): amd_request.c: __send_app_termination_signal(528) > send dead signal done
04-30 15:36:35.000+0700 I/efl-extension( 2037): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:36:35.000+0700 I/efl-extension( 2037): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb4a00b50 is freed
04-30 15:36:35.000+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cb9b10 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 15:36:35.000+0700 I/efl-extension( 2037): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb8cba558, obj: 0xb8cba558
04-30 15:36:35.000+0700 I/efl-extension( 2037): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 15:36:35.000+0700 E/EFL     ( 2037): elementary<2037> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 15:36:35.010+0700 W/STARTER ( 1138): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1210] goes to (3)
04-30 15:36:35.010+0700 E/STARTER ( 1138): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1210)'s state(3)
04-30 15:36:35.010+0700 W/AUL_AMD (  972): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-30 15:36:35.010+0700 W/AUL_AMD (  972): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-30 15:36:35.010+0700 W/AUL     (  972): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1210) status(fg) type(uiapp)
04-30 15:36:35.010+0700 E/EFL     ( 2037): elementary<2037> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8cba558 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:36:35.010+0700 E/EFL     ( 2037): elementary<2037> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8cba558 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:36:35.010+0700 I/AUL_AMD (  972): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2116
04-30 15:36:35.010+0700 W/AUL     (  972): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2116)
04-30 15:36:35.010+0700 E/EFL     ( 2037): elementary<2037> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8cba558 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:36:35.010+0700 E/EFL     ( 2037): elementary<2037> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8cba558 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:36:35.010+0700 I/efl-extension( 2037): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:36:35.010+0700 I/efl-extension( 2037): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb4a1bb68 is freed
04-30 15:36:35.010+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:36:35.010+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8cba558, elm_genlist, func : 0xb5887ea1
04-30 15:36:35.010+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cb9b10 in function: elm_grid_clear, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cb9b10 in function: elm_grid_size_set, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8cb9f78, , _activated_obj : 0xb8d14c80, activated : 1
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8ccfe00 in function: elm_label_line_wrap_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8ccfe00 in function: elm_label_wrap_width_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8ccfe00 in function: elm_label_ellipsis_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cb9b10 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cb9b10 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cb9b10 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cb9b10 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb8d33990, obj: 0xb8d33990
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8d33990 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8d33990 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8d33990 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:36:35.020+0700 E/EFL     ( 2037): elementary<2037> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8d33990 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb8d19f70 is freed
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8d33990, elm_genlist, func : 0xb5887ea1
04-30 15:36:35.020+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:36:35.050+0700 I/APP_CORE( 2037): appcore-efl.c: __after_loop(1243) > [APP 2037] After terminate() callback
04-30 15:36:35.050+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:36:35.060+0700 W/AUL_AMD (  972): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2037
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb8cc1f60 is freed
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8d12f20, elm_scroller, func : 0xb588b379
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8d14c80, elm_image, func : 0xb588b379
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:36:35.060+0700 I/efl-extension( 2037): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb8d12f20 : elm_scroller] rotary callabck is deleted
04-30 15:36:35.110+0700 W/PROCESSMGR(  932): e_mod_processmgr.c: _e_mod_processmgr_send_update_watch_action(663) > [PROCESSMGR] =====================> send UpdateClock
04-30 15:36:35.120+0700 W/WATCH_CORE( 1313): appcore-watch.c: __signal_process_manager_handler(1269) > process_manager_signal
04-30 15:36:35.120+0700 I/WATCH_CORE( 1313): appcore-watch.c: __signal_process_manager_handler(1285) > Call the time_tick_cb
04-30 15:36:35.120+0700 I/CAPI_WATCH_APPLICATION( 1313): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 15:36:35.120+0700 E/watchface-app( 1313): watchface.cpp: OnAppTimeTick(1157) > 
04-30 15:36:35.120+0700 I/watchface-app( 1313): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-30 15:36:35.120+0700 W/W_HOME  ( 1210): event_manager.c: _ecore_x_message_cb(421) > state: 1 -> 0
04-30 15:36:35.120+0700 W/W_HOME  ( 1210): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 15:36:35.120+0700 W/W_HOME  ( 1210): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 15:36:35.120+0700 W/W_INDICATOR( 1139): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 15:36:35.120+0700 W/W_INDICATOR( 1139): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 15:36:35.120+0700 W/W_HOME  ( 1210): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 15:36:35.120+0700 W/W_HOME  ( 1210): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 1
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(0)
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): event_manager.c: _window_visibility_cb(470) > state: 0 -> 1
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): event_manager.c: _state_control(176) > control:6, app_state:2 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(0)
04-30 15:36:35.130+0700 I/APP_CORE( 1210): appcore-efl.c: __do_app(453) > [APP 1210] Event: RESUME State: PAUSED
04-30 15:36:35.130+0700 I/CAPI_APPFW_APPLICATION( 1210): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): main.c: _appcore_resume_cb(480) > appcore resume
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): event_manager.c: _app_resume_cb(373) > state: 2 -> 1
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): main.c: home_resume(528) > journal_multimedia_screen_loaded_home called
04-30 15:36:35.130+0700 W/W_HOME  ( 1210): main.c: home_resume(532) > clock/widget resumed
04-30 15:36:35.140+0700 E/W_HOME  ( 1210): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
04-30 15:36:35.140+0700 W/W_HOME  ( 1210): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 15:36:35.140+0700 I/GATE    ( 1210): <GATE-M>APP_FULLY_LOADED_w-home</GATE-M>
04-30 15:36:35.140+0700 W/W_INDICATOR( 1139): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 15:36:35.140+0700 W/W_INDICATOR( 1139): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 15:36:35.140+0700 I/wnotib  ( 1210): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 0
04-30 15:36:35.140+0700 E/wnotib  ( 1210): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-30 15:36:35.140+0700 W/wnotib  ( 1210): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(958) > Do the postponed update job with is_for_VI: 0, postponed_for_VI: 0.
04-30 15:36:35.140+0700 W/wnotib  ( 1210): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(444) > idler for type: 0
04-30 15:36:35.140+0700 E/wnoti-service( 1384): wnoti-db-client.c: _wnoti_get_categories_info(206) > _query_step failed(not SQLITE_ROW)
04-30 15:36:35.150+0700 E/wnoti-proxy( 1210): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-30 15:36:35.150+0700 W/wnotib  ( 1210): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(732) > No categories available. Num old_notifications: 0
04-30 15:36:35.150+0700 I/wnotib  ( 1210): w-notification-board-noti-manager.c: _wnb_nm_free_data(248) > Free noti manager data.
04-30 15:36:35.150+0700 I/wnotib  ( 1210): w-notification-board-noti-manager.c: _wnb_nm_free_data(253) > Free previous notifications.
04-30 15:36:35.150+0700 I/wnotib  ( 1210): w-notification-board-noti-manager.c: _wnb_nm_free_data(274) > Free previous categories.
04-30 15:36:35.150+0700 I/wnotib  ( 1210): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(747) > before_rpanel_count: 0, after_rpanel_count: 0.
04-30 15:36:35.150+0700 W/W_HOME  ( 1210): noti_broker.c: _handler_indicator_update(560) > 0x1
04-30 15:36:35.160+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:36:35.160+0700 W/AUL_AMD (  972): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2037
04-30 15:36:35.230+0700 W/SHealthCommon( 1477): SHealthMessagePortConnection.cpp: Send(509) > [1;40;33mSEND CLIENT MESSAGE [feature type: 1, msg type: 2, msg id: app_resumed, name: app_resumed][0;m
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.shealth-service:com.samsung.shealth.server]
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-30 15:36:35.230+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 15:36:35.230+0700 W/SHealthCommon( 1739): SHealthMessagePortConnection.cpp: OnPortMessageReceived(135) > [1;40;33mfeatureType: 1, clientMsgType: 2, messageName: app_resumed[0;m
04-30 15:36:35.230+0700 I/CAPI_WIDGET_APPLICATION( 1477): widget_app.c: __provider_resume_cb(316) > widget obj was resumed
04-30 15:36:35.230+0700 W/AUL     ( 1477): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.shealth.widget.pedometer) pid(1477) status(fg) type(widgetapp)
04-30 15:36:35.230+0700 I/CAPI_WIDGET_APPLICATION( 1477): widget_app.c: __check_status_for_cgroup(134) > enter foreground group
04-30 15:36:35.240+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2037): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
04-30 15:36:35.240+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2037): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
04-30 15:36:35.240+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2037): preference.c: preference_get_string(1258) > preference_get_string(2037) : URL error
04-30 15:36:35.270+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:36:35.270+0700 W/AUL_AMD (  972): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2037
04-30 15:36:35.290+0700 I/CAPI_NETWORK_CONNECTION( 2037): connection.c: connection_create(453) > New handle created[0xb4a3eab0]
04-30 15:36:35.330+0700 E/JSON PARSING( 2037): No data could be display
04-30 15:36:35.330+0700 E/JSON PARSING( 2037): ����;
04-30 15:36:35.340+0700 I/CAPI_NETWORK_CONNECTION( 2037): connection.c: connection_destroy(471) > Destroy handle: 0xb4a3eab0
04-30 15:36:35.380+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:36:35.390+0700 W/AUL_AMD (  972): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2037
04-30 15:36:35.490+0700 W/AUL_AMD (  972): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:36:35.500+0700 W/AUL_AMD (  972): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-30 15:36:35.550+0700 E/PKGMGR_PARSER( 2174): pkgmgr_parser_signature.c: __ps_check_mdm_policy_by_pkgid(1056) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
04-30 15:36:35.610+0700 I/PRIVACY-MANAGER-CLIENT( 2174): SocketClient.cpp: SocketClient(37) > Client created
04-30 15:36:35.610+0700 I/PRIVACY-MANAGER-SERVER(  970): SocketService.cpp: mainloop(227) > Got incoming connection
04-30 15:36:35.610+0700 I/PRIVACY-MANAGER-CLIENT( 2174): SocketStream.h: SocketStream(31) > Created
04-30 15:36:35.610+0700 I/PRIVACY-MANAGER-CLIENT( 2174): SocketConnection.h: SocketConnection(44) > Created
04-30 15:36:35.610+0700 I/PRIVACY-MANAGER-CLIENT( 2174): SocketClient.cpp: connect(62) > Client connected
04-30 15:36:35.620+0700 I/PRIVACY-MANAGER-SERVER(  970): SocketService.cpp: connectionThread(253) > Starting connection thread
04-30 15:36:35.620+0700 I/PRIVACY-MANAGER-SERVER(  970): SocketStream.h: SocketStream(31) > Created
04-30 15:36:35.620+0700 I/PRIVACY-MANAGER-SERVER(  970): SocketConnection.h: SocketConnection(44) > Created
04-30 15:36:35.620+0700 I/PRIVACY-MANAGER-SERVER(  970): SocketService.cpp: connectionService(304) > Calling service
04-30 15:36:35.630+0700 I/PRIVACY-MANAGER-CLIENT( 2174): SocketClient.cpp: disconnect(72) > Client disconnected
04-30 15:36:35.630+0700 I/GATE    ( 1210): <GATE-M>SCREEN_LOADED_HOME</GATE-M>
04-30 15:36:35.640+0700 I/PRIVACY-MANAGER-SERVER(  970): SocketService.cpp: connectionService(307) > Removing client
04-30 15:36:35.640+0700 I/PRIVACY-MANAGER-SERVER(  970): SocketService.cpp: connectionService(311) > Call served
04-30 15:36:35.640+0700 I/PRIVACY-MANAGER-SERVER(  970): SocketService.cpp: connectionThread(262) > Client serviced
04-30 15:36:35.650+0700 W/CRASH_MANAGER( 2178): worker.c: worker_job(1205) > 1102037726561152507739
