S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 3827
Date: 2018-04-20 18:09:12+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 3827, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0x00000000
r2   = 0x00000000, r3   = 0xb71668e0
r4   = 0xb7076b28, r5   = 0x00000000
r6   = 0xb7076cb0, r7   = 0xb7076ca8
r8   = 0xb6d098e8, r9   = 0xb6d098e8
r10  = 0x00000001, fp   = 0x00000000
ip   = 0x0000000f, sp   = 0xbefd51e0
lr   = 0x00000003, pc   = 0xb6c5eb82
cpsr = 0x20000030

Memory Information
MemTotal:   405512 KB
MemFree:      8272 KB
Buffers:      9000 KB
Cached:     105256 KB
VmPeak:      89196 KB
VmSize:      87032 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23300 KB
VmRSS:       23300 KB
VmData:      26800 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24516 KB
VmPTE:          58 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 3827 TID = 3827
3827 3905 3909 

Maps Information
b1e0f000 b1e96000 rw-s anon_inode:dmabuf
b1f15000 b1f19000 r-xp /usr/lib/libogg.so.0.7.1
b1f21000 b1f43000 r-xp /usr/lib/libvorbis.so.0.4.3
b1f4b000 b1f92000 r-xp /usr/lib/libsndfile.so.1.0.26
b1f9e000 b1fe7000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1ff0000 b1ff5000 r-xp /usr/lib/libjson.so.0.0.1
b3896000 b399c000 r-xp /usr/lib/libicuuc.so.57.1
b39b2000 b3b3a000 r-xp /usr/lib/libicui18n.so.57.1
b3b4a000 b3b57000 r-xp /usr/lib/libail.so.0.1.0
b3b60000 b3b63000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3b6b000 b3ba3000 r-xp /usr/lib/libpulse.so.0.16.2
b3ba4000 b3ba7000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3baf000 b3c10000 r-xp /usr/lib/libasound.so.2.0.0
b3c1a000 b3c33000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c3c000 b3c40000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3c48000 b3c53000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3c60000 b3c64000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3c6d000 b3c85000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3c96000 b3c9d000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3ca5000 b3cb0000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3cb8000 b3cba000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3cc2000 b3cc3000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3ccb000 b3cd3000 r-xp /usr/lib/libfeedback.so.0.1.4
b3cec000 b3ced000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3d68000 b3def000 rw-s anon_inode:dmabuf
b3def000 b3df0000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3df8000 b3dfb000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3ede000 b3f65000 rw-s anon_inode:dmabuf
b3f7a000 b4001000 rw-s anon_inode:dmabuf
b4002000 b4801000 rw-p [stack:3909]
b49a5000 b49a6000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4aad000 b52ac000 rw-p [stack:3905]
b52ac000 b52ae000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52b6000 b52cd000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52da000 b52dc000 r-xp /usr/lib/libdri2.so.0.0.0
b52e4000 b52ef000 r-xp /usr/lib/libtbm.so.1.0.0
b52f7000 b52ff000 r-xp /usr/lib/libdrm.so.2.4.0
b5307000 b5309000 r-xp /usr/lib/libgenlock.so
b5311000 b5316000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b531e000 b5329000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b5532000 b55fc000 r-xp /usr/lib/libCOREGL.so.4.0
b560d000 b561d000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5625000 b562b000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5633000 b5634000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b563d000 b5640000 r-xp /usr/lib/libEGL.so.1.4
b5648000 b5656000 r-xp /usr/lib/libGLESv2.so.2.0
b565f000 b56a8000 r-xp /usr/lib/libmdm.so.1.2.70
b56b1000 b56b7000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56bf000 b56c8000 r-xp /usr/lib/libcom-core.so.0.0.1
b56d1000 b5789000 r-xp /usr/lib/libcairo.so.2.11200.14
b5794000 b57ad000 r-xp /usr/lib/libnetwork.so.0.0.0
b57b5000 b57c1000 r-xp /usr/lib/libnotification.so.0.1.0
b57ca000 b57d9000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b57e2000 b5803000 r-xp /usr/lib/libefl-extension.so.0.1.0
b580b000 b5810000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5818000 b581d000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b5825000 b5835000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b583d000 b5845000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b584d000 b5856000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b59fb000 b5a05000 r-xp /lib/libnss_files-2.13.so
b5a0e000 b5add000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5af3000 b5b17000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b20000 b5b26000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b2e000 b5b32000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b3f000 b5b4a000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b52000 b5b54000 r-xp /usr/lib/libiniparser.so.0
b5b5d000 b5b62000 r-xp /usr/lib/libappcore-common.so.1.1
b5b6a000 b5b6c000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b75000 b5b79000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5b86000 b5b88000 r-xp /usr/lib/libXau.so.6.0.0
b5b90000 b5b97000 r-xp /lib/libcrypt-2.13.so
b5bc7000 b5bc9000 r-xp /usr/lib/libiri.so
b5bd2000 b5d64000 r-xp /usr/lib/libcrypto.so.1.0.0
b5d85000 b5dcc000 r-xp /usr/lib/libssl.so.1.0.0
b5dd8000 b5e06000 r-xp /usr/lib/libidn.so.11.5.44
b5e0e000 b5e17000 r-xp /usr/lib/libcares.so.2.1.0
b5e21000 b5e34000 r-xp /usr/lib/libxcb.so.1.1.0
b5e3d000 b5e40000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e48000 b5e4a000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e53000 b5f1f000 r-xp /usr/lib/libxml2.so.2.7.8
b5f2c000 b5f2e000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f37000 b5f3c000 r-xp /usr/lib/libffi.so.5.0.10
b5f44000 b5f45000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f4d000 b5f50000 r-xp /lib/libattr.so.1.1.0
b5f58000 b5fec000 r-xp /usr/lib/libstdc++.so.6.0.16
b5fff000 b601c000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6026000 b603e000 r-xp /usr/lib/libpng12.so.0.50.0
b6046000 b605c000 r-xp /lib/libexpat.so.1.6.0
b6066000 b60aa000 r-xp /usr/lib/libcurl.so.4.3.0
b60b3000 b60bd000 r-xp /usr/lib/libXext.so.6.4.0
b60c7000 b60cb000 r-xp /usr/lib/libXtst.so.6.1.0
b60d3000 b60d9000 r-xp /usr/lib/libXrender.so.1.3.0
b60e1000 b60e7000 r-xp /usr/lib/libXrandr.so.2.2.0
b60ef000 b60f0000 r-xp /usr/lib/libXinerama.so.1.0.0
b60f9000 b6102000 r-xp /usr/lib/libXi.so.6.1.0
b610a000 b610d000 r-xp /usr/lib/libXfixes.so.3.1.0
b6116000 b6118000 r-xp /usr/lib/libXgesture.so.7.0.0
b6120000 b6122000 r-xp /usr/lib/libXcomposite.so.1.0.0
b612a000 b612c000 r-xp /usr/lib/libXdamage.so.1.1.0
b6134000 b613b000 r-xp /usr/lib/libXcursor.so.1.0.2
b6143000 b6146000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b614f000 b6153000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b615c000 b6161000 r-xp /usr/lib/libecore_fb.so.1.7.99
b616a000 b624b000 r-xp /usr/lib/libX11.so.6.3.0
b6256000 b6279000 r-xp /usr/lib/libjpeg.so.8.0.2
b6291000 b62a7000 r-xp /lib/libz.so.1.2.5
b62b0000 b62b2000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62ba000 b632f000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6339000 b6353000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b635b000 b638f000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6398000 b646b000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6477000 b6487000 r-xp /lib/libresolv-2.13.so
b648b000 b64a3000 r-xp /usr/lib/liblzma.so.5.0.3
b64ab000 b64ae000 r-xp /lib/libcap.so.2.21
b64b6000 b64e5000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b64ed000 b64ee000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b64f7000 b64fd000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6505000 b651c000 r-xp /usr/lib/liblua-5.1.so
b6525000 b652c000 r-xp /usr/lib/libembryo.so.1.7.99
b6534000 b653a000 r-xp /lib/librt-2.13.so
b6543000 b6599000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65a7000 b65fd000 r-xp /usr/lib/libfreetype.so.6.11.3
b6609000 b6631000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6632000 b6677000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6680000 b6693000 r-xp /usr/lib/libfribidi.so.0.3.1
b669b000 b66b5000 r-xp /usr/lib/libecore_con.so.1.7.99
b66bf000 b66c8000 r-xp /usr/lib/libedbus.so.1.7.99
b66d0000 b6720000 r-xp /usr/lib/libecore_x.so.1.7.99
b6722000 b672b000 r-xp /usr/lib/libvconf.so.0.2.45
b6733000 b6744000 r-xp /usr/lib/libecore_input.so.1.7.99
b674c000 b6751000 r-xp /usr/lib/libecore_file.so.1.7.99
b6759000 b677b000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6784000 b67c5000 r-xp /usr/lib/libeina.so.1.7.99
b67ce000 b67e7000 r-xp /usr/lib/libeet.so.1.7.99
b67f8000 b6861000 r-xp /lib/libm-2.13.so
b686a000 b6870000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6879000 b687a000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6882000 b68a5000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68ad000 b68b2000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68ba000 b68e4000 r-xp /usr/lib/libdbus-1.so.3.8.12
b68ed000 b6904000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b690c000 b6917000 r-xp /lib/libunwind.so.8.0.1
b6944000 b6962000 r-xp /usr/lib/libsystemd.so.0.4.0
b696c000 b6a90000 r-xp /lib/libc-2.13.so
b6a9e000 b6aa6000 r-xp /lib/libgcc_s-4.6.so.1
b6aa7000 b6aab000 r-xp /usr/lib/libsmack.so.1.0.0
b6ab4000 b6aba000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ac2000 b6b92000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6b93000 b6bf1000 r-xp /usr/lib/libedje.so.1.7.99
b6bfb000 b6c12000 r-xp /usr/lib/libecore.so.1.7.99
b6c29000 b6cf7000 r-xp /usr/lib/libevas.so.1.7.99
b6d1d000 b6e59000 r-xp /usr/lib/libelementary.so.1.7.99
b6e70000 b6e84000 r-xp /lib/libpthread-2.13.so
b6e8f000 b6e91000 r-xp /usr/lib/libdlog.so.0.0.0
b6e99000 b6e9c000 r-xp /usr/lib/libbundle.so.0.1.22
b6ea4000 b6ea6000 r-xp /lib/libdl-2.13.so
b6eaf000 b6ebc000 r-xp /usr/lib/libaul.so.0.1.0
b6ece000 b6ed4000 r-xp /usr/lib/libappcore-efl.so.1.1
b6edd000 b6ee1000 r-xp /usr/lib/libsys-assert.so
b6eea000 b6f07000 r-xp /lib/ld-2.13.so
b6f10000 b6f15000 r-xp /usr/bin/launchpad-loader
b6f75000 b734a000 rw-p [heap]
befb5000 befd6000 rw-p [stack]
b6f75000 b734a000 rw-p [heap]
befb5000 befd6000 rw-p [stack]
End of Maps Information

Callstack Information (PID:3827)
Call Stack Count: 14
 0: (0xb6c5eb82) [/usr/lib/libevas.so.1] + 0x35b82
 1: (0xb6c843c5) [/usr/lib/libevas.so.1] + 0x5b3c5
 2: (0xb676b1e5) [/usr/lib/libecore_evas.so.1] + 0x121e5
 3: (0xb67687bd) [/usr/lib/libecore_evas.so.1] + 0xf7bd
 4: (0xb6c05ee5) [/usr/lib/libecore.so.1] + 0xaee5
 5: (0xb6c074c9) [/usr/lib/libecore.so.1] + 0xc4c9
 6: ecore_main_loop_begin + 0x30 (0xb6c07879) [/usr/lib/libecore.so.1] + 0xc879
 7: appcore_efl_main + 0x332 (0xb6ed1b47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
 8: ui_app_main + 0xb0 (0xb5b76ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
 9: uib_app_run + 0xea (0xb5851fcf) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x4fcf
10: main + 0x34 (0xb58522b9) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x52b9
11:  + 0x0 (0xb6f11a53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
12: __libc_start_main + 0x114 (0xb698385c) [/lib/libc.so.6] + 0x1785c
13: (0xb6f11e0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
ace_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(233)
04-20 18:09:08.665+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:08.705+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.370599)
04-20 18:09:08.705+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(233)
04-20 18:09:08.705+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.370599)
04-20 18:09:08.705+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(209)
04-20 18:09:08.705+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:08.755+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.605691)
04-20 18:09:08.755+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(209)
04-20 18:09:08.755+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.605691)
04-20 18:09:08.755+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(179)
04-20 18:09:08.755+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:08.795+0700 I/efl-extension( 1193): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:08.795+0700 I/efl-extension( 1117): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:08.795+0700 I/efl-extension( 3224): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:08.815+0700 I/efl-extension( 3827): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:08.815+0700 I/efl-extension( 3827): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb7169228, elm_image, time_stamp : 849067
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.797748)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(179)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.797748)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(154)
04-20 18:09:08.815+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] mx(0), my(257), minx(0), miny(0), px(0), py(154)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] cw(360), ch(618), pw(360), ph(361)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1910 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] scrollto.x.animator(0xb7153210)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1916 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] scrollto.y.animator(0xb7153350)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] x(0), y(0)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb717f888 : elm_genlist] t_in(0.320000), pos_x(0)
04-20 18:09:08.815+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb717f888 : elm_genlist] t_in(0.320000), pos_y(0)
04-20 18:09:08.865+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.291441)
04-20 18:09:08.865+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(154)
04-20 18:09:08.865+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.291441)
04-20 18:09:08.865+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(109)
04-20 18:09:08.865+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:08.915+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.519034)
04-20 18:09:08.915+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(109)
04-20 18:09:08.915+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.519034)
04-20 18:09:08.915+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(74)
04-20 18:09:08.915+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:08.965+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.739815)
04-20 18:09:08.965+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(74)
04-20 18:09:08.965+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.739815)
04-20 18:09:08.975+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(40)
04-20 18:09:08.975+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.005+0700 I/efl-extension( 1193): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:09.005+0700 I/efl-extension( 1117): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:09.005+0700 I/efl-extension( 3224): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:09.015+0700 I/efl-extension( 3827): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:09.015+0700 I/efl-extension( 3827): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb7169228, elm_image, time_stamp : 849276
04-20 18:09:09.015+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.872447)
04-20 18:09:09.015+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(40)
04-20 18:09:09.015+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.872447)
04-20 18:09:09.015+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(19)
04-20 18:09:09.015+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.015+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] mx(0), my(257), minx(0), miny(0), px(0), py(19)
04-20 18:09:09.025+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] cw(360), ch(618), pw(360), ph(361)
04-20 18:09:09.025+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1910 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] scrollto.x.animator(0xb71535d0)
04-20 18:09:09.025+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1916 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] scrollto.y.animator(0xb7153710)
04-20 18:09:09.025+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] x(0), y(0)
04-20 18:09:09.025+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb717f888 : elm_genlist] t_in(0.320000), pos_x(0)
04-20 18:09:09.025+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb717f888 : elm_genlist] t_in(0.320000), pos_y(0)
04-20 18:09:09.075+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.326500)
04-20 18:09:09.075+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(19)
04-20 18:09:09.075+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.326500)
04-20 18:09:09.075+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(12)
04-20 18:09:09.075+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.125+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.536408)
04-20 18:09:09.125+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(12)
04-20 18:09:09.125+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.536408)
04-20 18:09:09.125+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(8)
04-20 18:09:09.125+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.135+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-20 18:09:09.135+0700 W/wnotibp ( 3224): wnotiboard-popup-view.c: _view_main_popup_timer_cb(1901) > ::UI:: start hiding animation
04-20 18:09:09.175+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.736770)
04-20 18:09:09.175+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(8)
04-20 18:09:09.175+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.736770)
04-20 18:09:09.175+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(5)
04-20 18:09:09.175+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.265+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.924173)
04-20 18:09:09.265+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(5)
04-20 18:09:09.265+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.924173)
04-20 18:09:09.265+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(1)
04-20 18:09:09.265+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.285+0700 I/efl-extension( 1193): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:09.285+0700 I/efl-extension( 1117): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:09.285+0700 I/efl-extension( 3224): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:09.365+0700 I/efl-extension( 3827): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:09.365+0700 I/efl-extension( 3827): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb7169228, elm_image, time_stamp : 849555
04-20 18:09:09.375+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.993409)
04-20 18:09:09.375+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2559 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(1)
04-20 18:09:09.375+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.993409)
04-20 18:09:09.375+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] animation stop!!
04-20 18:09:09.375+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(0)
04-20 18:09:09.375+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.445+0700 W/wnotibp ( 3224): wnotiboard-popup-view.c: _view_sub_popup_hide_animator_cb(1842) > ::UI:: end hiding animation
04-20 18:09:09.445+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-20 18:09:09.445+0700 W/wnotibp ( 3224): wnotiboard-popup-view.c: wnbp_view_goto_pause(1476) > state : (8, 1, 0)
04-20 18:09:09.445+0700 I/wnotibp ( 3224): wnotiboard-popup-view.c: wnbp_view_goto_pause(1489) > simple popup=0, view_state=0
04-20 18:09:09.445+0700 I/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(632) > ::UI:: it is invalid object.
04-20 18:09:09.445+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [invalid object] is UNLOCK , 0000 <=== ]]]
04-20 18:09:09.445+0700 I/wnotibp ( 3224): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 877
04-20 18:09:09.445+0700 W/wnotibp ( 3224): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(686) > Call wnbp_free_noti_detail..
04-20 18:09:09.445+0700 W/wnotibp ( 3224): wnotiboard-popup-common.c: wnbp_free_noti_detail(583) > Do free noti [0xb0350158] / db id [877]
04-20 18:09:09.445+0700 I/wnotibp ( 3224): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 0
04-20 18:09:09.455+0700 I/efl-extension( 3224): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb8251648, obj: 0xb8251648
04-20 18:09:09.455+0700 I/efl-extension( 3224): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-20 18:09:09.455+0700 I/efl-extension( 3224): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-20 18:09:09.455+0700 I/efl-extension( 3224): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-20 18:09:09.455+0700 I/efl-extension( 3224): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-20 18:09:09.475+0700 E/EFL     ( 3224): elementary<3224> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-20 18:09:09.495+0700 E/EFL     ( 3224): elementary<3224> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8251648 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-20 18:09:09.495+0700 E/EFL     ( 3224): elementary<3224> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8251648 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-20 18:09:09.495+0700 E/EFL     ( 3224): elementary<3224> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8251648 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-20 18:09:09.495+0700 E/EFL     ( 3224): elementary<3224> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8251648 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-20 18:09:09.495+0700 I/efl-extension( 3224): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-20 18:09:09.495+0700 I/efl-extension( 3224): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8251648, elm_genlist, func : 0xb584eea1
04-20 18:09:09.495+0700 I/efl-extension( 3224): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-20 18:09:09.495+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-20 18:09:09.495+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_reset_view_lock(703) > ::UI:: lock state = 0000
04-20 18:09:09.515+0700 I/wnotibp ( 3224): wnotiboard-popup-view.c: wnbp_view_goto_pause(1586) > ::INFO:: call lower
04-20 18:09:09.535+0700 W/APP_CORE( 3224): appcore-efl.c: __hide_cb(882) > [EVENT_TEST][EVENT] GET HIDE EVENT!!!. WIN:3c00009
04-20 18:09:09.535+0700 I/APP_CORE( 3224): appcore-efl.c: __do_app(453) > [APP 3224] Event: PAUSE State: RUNNING
04-20 18:09:09.535+0700 I/CAPI_APPFW_APPLICATION( 3224): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-20 18:09:09.575+0700 W/wnotibp ( 3224): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-20 18:09:09.575+0700 W/wnotibp ( 3224): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-20 18:09:09.575+0700 W/wnotibp ( 3224): wnotiboard-popup.c: _popup_app_pause(220) > [0, 0, 19, 0000]
04-20 18:09:09.575+0700 W/wnotibp ( 3224): wnotiboard-popup.c: _popup_app_pause(221) > [1, 0]
04-20 18:09:09.575+0700 W/wnotibp ( 3224): wnotiboard-popup.c: _popup_app_pause(222) > [1, 1, 0, 2, 0]
04-20 18:09:09.635+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.675+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.695+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.715+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.735+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.755+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.785+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.805+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.805+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.805+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.805+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.815+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.825+0700 I/efl-extension( 1117): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:09.825+0700 I/efl-extension( 1193): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:09.825+0700 I/efl-extension( 3827): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:09.825+0700 I/efl-extension( 3827): efl_extension_rotary.c: _rotary_rotate_handler_cb(537) > Deliver clockwise rotary event to object: 0xb7169228, elm_image, time_stamp : 850090
04-20 18:09:09.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] mx(0), my(257), minx(0), miny(0), px(0), py(0)
04-20 18:09:09.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] cw(360), ch(618), pw(360), ph(361)
04-20 18:09:09.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] x(0), y(129)
04-20 18:09:09.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb717f888 : elm_genlist] t_in(0.320000), pos_x(0)
04-20 18:09:09.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb717f888 : elm_genlist] t_in(0.320000), pos_y(129)
04-20 18:09:09.845+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.136759)
04-20 18:09:09.845+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(0)
04-20 18:09:09.845+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.136759)
04-20 18:09:09.845+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(17)
04-20 18:09:09.845+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.905+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.425823)
04-20 18:09:09.905+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(17)
04-20 18:09:09.905+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.425823)
04-20 18:09:09.905+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(54)
04-20 18:09:09.905+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.925+0700 I/efl-extension( 1117): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:09.925+0700 I/efl-extension( 1193): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:09.945+0700 I/efl-extension( 3827): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:09.945+0700 I/efl-extension( 3827): efl_extension_rotary.c: _rotary_rotate_handler_cb(537) > Deliver clockwise rotary event to object: 0xb7169228, elm_image, time_stamp : 850190
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.634750)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(54)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.634750)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(81)
04-20 18:09:09.955+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] mx(0), my(257), minx(0), miny(0), px(0), py(81)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] cw(360), ch(618), pw(360), ph(361)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1910 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] scrollto.x.animator(0xb7153490)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1916 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] scrollto.y.animator(0xb71535d0)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] x(0), y(257)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb717f888 : elm_genlist] t_in(0.320000), pos_x(0)
04-20 18:09:09.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb717f888 : elm_genlist] t_in(0.320000), pos_y(257)
04-20 18:09:09.995+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.279640)
04-20 18:09:09.995+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(81)
04-20 18:09:09.995+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.279640)
04-20 18:09:10.005+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(130)
04-20 18:09:10.005+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.035+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.465832)
04-20 18:09:10.035+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(130)
04-20 18:09:10.035+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.465832)
04-20 18:09:10.035+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(162)
04-20 18:09:10.035+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.085+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.657470)
04-20 18:09:10.085+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(162)
04-20 18:09:10.085+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.657470)
04-20 18:09:10.085+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(196)
04-20 18:09:10.085+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.125+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.802632)
04-20 18:09:10.125+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(196)
04-20 18:09:10.125+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.802632)
04-20 18:09:10.125+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(222)
04-20 18:09:10.125+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.165+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.900904)
04-20 18:09:10.165+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(222)
04-20 18:09:10.165+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.900904)
04-20 18:09:10.175+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(239)
04-20 18:09:10.175+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.205+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.965273)
04-20 18:09:10.215+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(239)
04-20 18:09:10.215+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.965273)
04-20 18:09:10.215+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(250)
04-20 18:09:10.215+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.245+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.995731)
04-20 18:09:10.245+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(250)
04-20 18:09:10.245+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.995731)
04-20 18:09:10.255+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(256)
04-20 18:09:10.255+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.285+0700 I/efl-extension( 1117): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:10.285+0700 I/efl-extension( 1193): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:10.295+0700 I/efl-extension( 3827): efl_extension_rotary.c: _process_raw_event(444) > direction: Clockwise
04-20 18:09:10.295+0700 I/efl-extension( 3827): efl_extension_rotary.c: _rotary_rotate_handler_cb(537) > Deliver clockwise rotary event to object: 0xb7169228, elm_image, time_stamp : 850558
04-20 18:09:10.295+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.995630)
04-20 18:09:10.295+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2559 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(256)
04-20 18:09:10.295+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.995630)
04-20 18:09:10.295+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] animation stop!!
04-20 18:09:10.295+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(257)
04-20 18:09:10.295+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.295+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] mx(0), my(257), minx(0), miny(0), px(0), py(257)
04-20 18:09:10.295+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] cw(360), ch(618), pw(360), ph(361)
04-20 18:09:10.495+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.545+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.575+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.605+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.615+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.635+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.655+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.675+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.695+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.695+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:10.805+0700 I/CAPI_NETWORK_CONNECTION( 3827): connection.c: connection_create(453) > New handle created[0xb1d01b68]
04-20 18:09:11.735+0700 I/efl-extension( 1117): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:11.735+0700 I/efl-extension( 1193): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:11.735+0700 I/efl-extension( 3827): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:11.735+0700 I/efl-extension( 3827): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb7169228, elm_image, time_stamp : 851999
04-20 18:09:11.735+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] mx(0), my(257), minx(0), miny(0), px(0), py(257)
04-20 18:09:11.735+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] cw(360), ch(618), pw(360), ph(361)
04-20 18:09:11.735+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb717f888 : elm_genlist] x(0), y(129)
04-20 18:09:11.735+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb717f888 : elm_genlist] t_in(0.320000), pos_x(0)
04-20 18:09:11.735+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb717f888 : elm_genlist] t_in(0.320000), pos_y(129)
04-20 18:09:11.745+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.103329)
04-20 18:09:11.745+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(257)
04-20 18:09:11.745+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.103329)
04-20 18:09:11.745+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(243)
04-20 18:09:11.745+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:11.755+0700 E/EFL     ( 3827): evas_main<3827> evas_object_image.c:3445 evas_object_image_render_pre() 0xb7166608 has invalid fill size: 0x0. Ignored
04-20 18:09:11.785+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.279153)
04-20 18:09:11.785+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(243)
04-20 18:09:11.785+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.279153)
04-20 18:09:11.785+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(221)
04-20 18:09:11.785+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:11.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.487828)
04-20 18:09:11.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(221)
04-20 18:09:11.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.487828)
04-20 18:09:11.825+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(194)
04-20 18:09:11.825+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:11.865+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.664489)
04-20 18:09:11.865+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(194)
04-20 18:09:11.865+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.664489)
04-20 18:09:11.865+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(171)
04-20 18:09:11.865+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:11.905+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.800839)
04-20 18:09:11.905+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(171)
04-20 18:09:11.905+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.800839)
04-20 18:09:11.915+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(154)
04-20 18:09:11.915+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:11.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.905428)
04-20 18:09:11.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(154)
04-20 18:09:11.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.905428)
04-20 18:09:11.955+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(141)
04-20 18:09:11.955+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:12.005+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.976095)
04-20 18:09:12.005+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(141)
04-20 18:09:12.005+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.976095)
04-20 18:09:12.005+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(132)
04-20 18:09:12.005+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:12.055+0700 E/EFL     ( 3827): edje<3827> edje_calc.c:3521 _edje_part_recalc() Circular dependency when calculating part "elm.text.1". Already calculating XY [03] axes. Need to calculate XY [03] axes
04-20 18:09:12.055+0700 E/EFL     ( 3827): edje<3827> edje_calc.c:3521 _edje_part_recalc() Circular dependency when calculating part "event_block". Already calculating XY [03] axes. Need to calculate XY [03] axes
04-20 18:09:12.055+0700 E/EFL     ( 3827): edje<3827> edje_calc.c:3521 _edje_part_recalc() Circular dependency when calculating part "elm.text.1". Already calculating XY [03] axes. Need to calculate XY [03] axes
04-20 18:09:12.055+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] time(0.999993)
04-20 18:09:12.055+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(132)
04-20 18:09:12.055+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] time(0.999993)
04-20 18:09:12.055+0700 E/EFL     ( 3827): elementary<3827> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb717f888 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(129)
04-20 18:09:12.055+0700 E/EFL     ( 3827): <3827> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-20 18:09:12.115+0700 E/NOTI    ( 3827): 67026
04-20 18:09:12.115+0700 E/NOTIF ID( 3827): 40198
04-20 18:09:12.115+0700 E/problem ( 3827): Dirty
04-20 18:09:12.115+0700 E/problem_desc( 3827): 19A-Fender Lh Fender Dirty & Stain
04-20 18:09:12.115+0700 E/NOTIF ID( 3827): 40199
04-20 18:09:12.115+0700 E/problem ( 3827): Dirty
04-20 18:09:12.115+0700 E/problem_desc( 3827): 19A-Fender Rh Fender Dirty & Stain
04-20 18:09:12.115+0700 E/BODY NO ( 3827): 67026
04-20 18:09:12.115+0700 E/JSON PARSING( 3827): �ѱ�g��
04-20 18:09:12.135+0700 I/efl-extension( 1117): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:12.135+0700 I/efl-extension( 1193): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
04-20 18:09:12.215+0700 E/wnoti-service( 1434): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 836, op_type : 3  //insert = 1, update = 2, delete = 3
04-20 18:09:12.215+0700 W/SHealthServiceCommon( 1796): NotificationServiceController.cpp: DeleteNotificationByPrivateId(928) > [1;40;33mdelete notification by private id = [836][0;m
04-20 18:09:12.225+0700 E/wnoti-service( 1434): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-20 18:09:12.225+0700 E/wnoti-service( 1434): wnoti-native-client.c: _receive_notification_changed_cb(1722) > error_code from _get_native_application_info: 0
04-20 18:09:12.245+0700 E/wnoti-service( 1434): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 1, count : 0
04-20 18:09:12.255+0700 I/AUL     ( 1119): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-20 18:09:12.265+0700 I/AUL     ( 1434): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-20 18:09:12.265+0700 I/AUL     ( 1119): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-20 18:09:12.295+0700 I/AUL     ( 1434): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-20 18:09:12.315+0700 E/APPS    ( 1193): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-20 18:09:12.325+0700 E/wnoti-service( 1434): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-20 18:09:12.345+0700 I/AUL     ( 1434): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-20 18:09:12.355+0700 I/AUL     ( 1434): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-20 18:09:12.355+0700 E/wnoti-service( 1434): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 837, op_type : 1  //insert = 1, update = 2, delete = 3
04-20 18:09:12.365+0700 E/wnoti-service( 1434): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-20 18:09:12.365+0700 E/wnoti-service( 1434): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-20 18:09:12.365+0700 E/wnoti-service( 1434): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-20 18:09:12.365+0700 E/wnoti-service( 1434): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-20 18:09:12.365+0700 E/wnoti-service( 1434): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: (null), err: 0
04-20 18:09:12.365+0700 W/CRASH_MANAGER( 3877): worker.c: worker_job(1205) > 1103827726561152422255
