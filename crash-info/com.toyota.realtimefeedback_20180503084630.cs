S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2326
Date: 2018-05-03 08:46:30+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2326, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb6f9b668
r2   = 0xb6f9bc78, r3   = 0x00000000
r4   = 0xb8528408, r5   = 0xb881b3c0
r6   = 0x00000013, r7   = 0xbed87188
r8   = 0x00006ef5, r9   = 0xb8528408
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb6f9b8bc, sp   = 0xbed87168
lr   = 0xb6f8950f, pc   = 0xb6f89544
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      6180 KB
Buffers:      5136 KB
Cached:     102608 KB
VmPeak:     158284 KB
VmSize:     151384 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       33332 KB
VmRSS:       32828 KB
VmData:      79096 KB
VmStk:         136 KB
VmExe:          40 KB
VmLib:       35904 KB
VmPTE:         112 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2326 TID = 2326
2326 2333 2364 2365 

Maps Information
b04f6000 b04fa000 r-xp /usr/lib/libogg.so.0.7.1
b0502000 b0524000 r-xp /usr/lib/libvorbis.so.0.4.3
b052c000 b0573000 r-xp /usr/lib/libsndfile.so.1.0.26
b057f000 b05c8000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b05d1000 b05d6000 r-xp /usr/lib/libjson.so.0.0.1
b1e77000 b1f7d000 r-xp /usr/lib/libicuuc.so.57.1
b1f93000 b211b000 r-xp /usr/lib/libicui18n.so.57.1
b212b000 b2138000 r-xp /usr/lib/libail.so.0.1.0
b2141000 b2144000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b214c000 b2184000 r-xp /usr/lib/libpulse.so.0.16.2
b2185000 b2188000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b2190000 b21f1000 r-xp /usr/lib/libasound.so.2.0.0
b21fb000 b2214000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b221d000 b2221000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b2229000 b2234000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b2241000 b2245000 r-xp /usr/lib/libmmfsession.so.0.0.0
b224e000 b2266000 r-xp /usr/lib/libmmfsound.so.0.1.0
b2277000 b227e000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b2286000 b2291000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b2299000 b229b000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b22a3000 b22a4000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b22ac000 b22b4000 r-xp /usr/lib/libfeedback.so.0.1.4
b22cd000 b22ce000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b2352000 b23d9000 rw-s anon_inode:dmabuf
b23d9000 b2460000 rw-s anon_inode:dmabuf
b24f0000 b2577000 rw-s anon_inode:dmabuf
b2965000 b29ec000 rw-s anon_inode:dmabuf
b29ed000 b31ec000 rw-p [stack:2365]
b3393000 b3394000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b3408000 b3409000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3411000 b3414000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b349c000 b3c9b000 rw-p [stack:2364]
b3c9b000 b3c9d000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3ca5000 b3cbc000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3ed5000 b46d4000 rw-p [stack:2333]
b46eb000 b5017000 r-xp /usr/lib/libsc-a3xx.so
b527b000 b527d000 r-xp /usr/lib/libdri2.so.0.0.0
b5285000 b528d000 r-xp /usr/lib/libdrm.so.2.4.0
b5295000 b5299000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b52a1000 b52a4000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b52ac000 b52ad000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b52b5000 b52c0000 r-xp /usr/lib/libtbm.so.1.0.0
b52c8000 b52cb000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b52d3000 b52d5000 r-xp /usr/lib/libgenlock.so
b52dd000 b52e2000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b52ea000 b5425000 r-xp /usr/lib/egl/libGLESv2.so
b5461000 b5463000 r-xp /usr/lib/libadreno_utils.so
b546d000 b5494000 r-xp /usr/lib/libgsl.so
b54a3000 b54aa000 r-xp /usr/lib/egl/eglsubX11.so
b54b4000 b54d6000 r-xp /usr/lib/egl/libEGL.so
b54df000 b5554000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5764000 b576e000 r-xp /lib/libnss_files-2.13.so
b5777000 b577a000 r-xp /lib/libattr.so.1.1.0
b5782000 b5789000 r-xp /lib/libcrypt-2.13.so
b57b9000 b57bc000 r-xp /lib/libcap.so.2.21
b57c4000 b57c6000 r-xp /usr/lib/libiri.so
b57ce000 b57eb000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b57f4000 b57f8000 r-xp /usr/lib/libsmack.so.1.0.0
b5801000 b5830000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b5838000 b58cc000 r-xp /usr/lib/libstdc++.so.6.0.16
b58df000 b59ae000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b59c4000 b59e8000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b59f1000 b5abb000 r-xp /usr/lib/libCOREGL.so.4.0
b5ad2000 b5ad4000 r-xp /usr/lib/libXau.so.6.0.0
b5add000 b5aed000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5af5000 b5af8000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5b00000 b5b18000 r-xp /usr/lib/liblzma.so.5.0.3
b5b21000 b5b23000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5b2b000 b5b2e000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5b36000 b5b3a000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5b43000 b5b48000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5b52000 b5b75000 r-xp /usr/lib/libjpeg.so.8.0.2
b5b8d000 b5ba3000 r-xp /lib/libexpat.so.1.6.0
b5bad000 b5bc0000 r-xp /usr/lib/libxcb.so.1.1.0
b5bc9000 b5bcf000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5bd7000 b5bd8000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5be2000 b5bfa000 r-xp /usr/lib/libpng12.so.0.50.0
b5c02000 b5c05000 r-xp /usr/lib/libEGL.so.1.4
b5c0d000 b5c1b000 r-xp /usr/lib/libGLESv2.so.2.0
b5c24000 b5c25000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5c2d000 b5c44000 r-xp /usr/lib/liblua-5.1.so
b5c4e000 b5c55000 r-xp /usr/lib/libembryo.so.1.7.99
b5c5d000 b5c67000 r-xp /usr/lib/libXext.so.6.4.0
b5c70000 b5c74000 r-xp /usr/lib/libXtst.so.6.1.0
b5c7c000 b5c82000 r-xp /usr/lib/libXrender.so.1.3.0
b5c8a000 b5c90000 r-xp /usr/lib/libXrandr.so.2.2.0
b5c98000 b5c99000 r-xp /usr/lib/libXinerama.so.1.0.0
b5ca3000 b5ca6000 r-xp /usr/lib/libXfixes.so.3.1.0
b5cae000 b5cb0000 r-xp /usr/lib/libXgesture.so.7.0.0
b5cb8000 b5cba000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5cc2000 b5cc4000 r-xp /usr/lib/libXdamage.so.1.1.0
b5ccc000 b5cd3000 r-xp /usr/lib/libXcursor.so.1.0.2
b5cdc000 b5cec000 r-xp /lib/libresolv-2.13.so
b5cf0000 b5cf2000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5cfa000 b5cff000 r-xp /usr/lib/libffi.so.5.0.10
b5d07000 b5d08000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5d10000 b5d59000 r-xp /usr/lib/libmdm.so.1.2.70
b5d63000 b5d69000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5d71000 b5d77000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5d7f000 b5d99000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5da1000 b5dbf000 r-xp /usr/lib/libsystemd.so.0.4.0
b5dca000 b5dcb000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5dd3000 b5dd8000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5de0000 b5df7000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5dff000 b5e05000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5e0e000 b5e17000 r-xp /usr/lib/libcom-core.so.0.0.1
b5e21000 b5e23000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e2c000 b5e82000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5e8f000 b5ee5000 r-xp /usr/lib/libfreetype.so.6.11.3
b5ef1000 b5f36000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5f3f000 b5f52000 r-xp /usr/lib/libfribidi.so.0.3.1
b5f5b000 b5f75000 r-xp /usr/lib/libecore_con.so.1.7.99
b5f7e000 b5fa8000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5fb1000 b5fba000 r-xp /usr/lib/libedbus.so.1.7.99
b5fc2000 b5fd3000 r-xp /usr/lib/libecore_input.so.1.7.99
b5fdb000 b5fe0000 r-xp /usr/lib/libecore_file.so.1.7.99
b5fe9000 b600b000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6014000 b602d000 r-xp /usr/lib/libeet.so.1.7.99
b603e000 b6066000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6067000 b6070000 r-xp /usr/lib/libXi.so.6.1.0
b6078000 b6159000 r-xp /usr/lib/libX11.so.6.3.0
b6165000 b621d000 r-xp /usr/lib/libcairo.so.2.11200.14
b6228000 b6286000 r-xp /usr/lib/libedje.so.1.7.99
b6290000 b62e0000 r-xp /usr/lib/libecore_x.so.1.7.99
b62e2000 b634b000 r-xp /lib/libm-2.13.so
b6354000 b635a000 r-xp /lib/librt-2.13.so
b6363000 b6379000 r-xp /lib/libz.so.1.2.5
b6382000 b6514000 r-xp /usr/lib/libcrypto.so.1.0.0
b6535000 b657c000 r-xp /usr/lib/libssl.so.1.0.0
b6588000 b65b6000 r-xp /usr/lib/libidn.so.11.5.44
b65be000 b65c7000 r-xp /usr/lib/libcares.so.2.1.0
b65d0000 b669c000 r-xp /usr/lib/libxml2.so.2.7.8
b66aa000 b66ac000 r-xp /usr/lib/libiniparser.so.0
b66b5000 b66e9000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b66f2000 b67c5000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b67d0000 b67e9000 r-xp /usr/lib/libnetwork.so.0.0.0
b67f1000 b67fa000 r-xp /usr/lib/libvconf.so.0.2.45
b6803000 b68d3000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b68d4000 b6915000 r-xp /usr/lib/libeina.so.1.7.99
b691e000 b6923000 r-xp /usr/lib/libappcore-common.so.1.1
b692b000 b6931000 r-xp /usr/lib/libappcore-efl.so.1.1
b6939000 b693c000 r-xp /usr/lib/libbundle.so.0.1.22
b6944000 b694a000 r-xp /usr/lib/libappsvc.so.0.1.0
b6952000 b6966000 r-xp /lib/libpthread-2.13.so
b6971000 b6994000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b699c000 b69a9000 r-xp /usr/lib/libaul.so.0.1.0
b69b3000 b69b5000 r-xp /lib/libdl-2.13.so
b69be000 b69c9000 r-xp /lib/libunwind.so.8.0.1
b69f6000 b69fe000 r-xp /lib/libgcc_s-4.6.so.1
b69ff000 b6b23000 r-xp /lib/libc-2.13.so
b6b31000 b6ba6000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6bb0000 b6bbc000 r-xp /usr/lib/libnotification.so.0.1.0
b6bc5000 b6bd4000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6bdd000 b6cab000 r-xp /usr/lib/libevas.so.1.7.99
b6cd1000 b6e0d000 r-xp /usr/lib/libelementary.so.1.7.99
b6e24000 b6e45000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6e4d000 b6e64000 r-xp /usr/lib/libecore.so.1.7.99
b6e7b000 b6e7d000 r-xp /usr/lib/libdlog.so.0.0.0
b6e85000 b6ec9000 r-xp /usr/lib/libcurl.so.4.3.0
b6ed2000 b6ed7000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6edf000 b6ee4000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6eec000 b6efc000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6f04000 b6f0c000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6f14000 b6f18000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6f20000 b6f24000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6f2d000 b6f2f000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6f3a000 b6f45000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6f4f000 b6f53000 r-xp /usr/lib/libsys-assert.so
b6f5c000 b6f79000 r-xp /lib/ld-2.13.so
b6f82000 b6f8c000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b8479000 ba066000 rw-p [heap]
bed67000 bed88000 rw-p [stack]
b6f82000 b6f8c000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b8479000 ba066000 rw-p [heap]
bed67000 bed88000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2326)
Call Stack Count: 20
 0: set_targeted_view + 0x87 (0xb6f89544) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7544
 1: pop_from_stack_uib_vc + 0x38 (0xb6f89a49) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a49
 2: uib_views_destroy_callback + 0x38 (0xb6f89a05) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a05
 3: (0xb6bf4af9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6c0bc39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6bfe37b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6bfeab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb5ff8cf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6dcf953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6e583f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6e55e53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6e5946b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6e597db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6d8c44d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x44c (0xb692ec61) [/usr/lib/libappcore-efl.so.1] + 0x3c61
15: ui_app_main + 0xb0 (0xb6f15ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16: uib_app_run + 0xea (0xb6f87437) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5437
17: main + 0x34 (0xb6f87b11) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5b11
18: __libc_start_main + 0x114 (0xb6a1685c) [/lib/libc.so.6] + 0x1785c
19: (0xb6f85124) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x3124
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 08:46:26.570+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 08:46:26.570+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: onBubbleButtonEffect(2446) >  [249, 249, 249, 255]
05-03 08:46:26.580+0700 E/AUL     (  927): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
05-03 08:46:26.580+0700 E/AUL     ( 1120): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
05-03 08:46:26.580+0700 I/APP_CORE( 1187): appcore-efl.c: __do_app(529) > Legacy lifecycle: 1
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): event_manager.c: _move_module_anim_end_cb(674) > state: 1 -> 0
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:3, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): rotary.c: rotary_deattach(156) > rotary_deattach:0xb808be78
05-03 08:46:26.880+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:46:26.880+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb808be78, elm_layout, func : 0xb6e8e455
05-03 08:46:26.880+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 08:46:26.880+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 08:46:26.880+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:46:26.880+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7fbccf8, elm_box, _activated_obj : 0xb808be78, activated : 1
05-03 08:46:26.880+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): event_manager.c: _apptray_visibility_cb(598) > apps,show
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): event_manager.c: _apptray_visibility_cb(618) > state: 1 -> 1
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:0(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): main.c: home_pause(550) > clock/widget paused
05-03 08:46:26.880+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:0(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:26.880+0700 W/APPS    ( 1187): apps_main.c: _time_changed_cb(409) >  date : 3->3
05-03 08:46:26.880+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 08:46:26.880+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 08:46:26.890+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: setBubbleColor(2479) >  [249, 249, 249, 255]
05-03 08:46:26.890+0700 W/W_HOME  ( 1187): rotary.c: rotary_attach(138) > rotary_attach:0xafb46e50
05-03 08:46:26.890+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xafb46e50, elm_layout, _activated_obj : 0xb7fbccf8, activated : 1
05-03 08:46:26.890+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
05-03 08:46:26.890+0700 I/GATE    ( 1187): <GATE-M>SCREEN_LOADED_APP_MENU_1</GATE-M>
05-03 08:46:26.890+0700 W/W_HOME  ( 1187): win.c: win_back_gesture_disable_set(170) > disable back gesture
05-03 08:46:26.890+0700 W/W_HOME  ( 1187): win.c: win_back_gesture_disable_set(170) > disable back gesture
05-03 08:46:26.890+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:26.890+0700 E/APPS    ( 1187): apps_main.c: apps_main_resume(1123) >  resumed already
05-03 08:46:26.910+0700 W/WATCH_CORE( 1306): appcore-watch.c: __widget_pause(1113) > widget_pause
05-03 08:46:26.910+0700 W/AUL     ( 1306): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1306) status(bg) type(watchapp)
05-03 08:46:26.910+0700 E/watchface-app( 1306): watchface.cpp: OnAppPause(1122) > 
05-03 08:46:27.560+0700 W/AUL_AMD (  927): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
05-03 08:46:27.560+0700 W/AUL_AMD (  927): amd_launch.c: __grab_timeout_handler(1453) > back key ungrab error
05-03 08:46:27.690+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2326): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 08:46:27.690+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2326): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 08:46:27.690+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2326): preference.c: preference_get_string(1258) > preference_get_string(2326) : URL error
05-03 08:46:27.720+0700 I/CAPI_NETWORK_CONNECTION( 2326): connection.c: connection_create(453) > New handle created[0xb26023d0]
05-03 08:46:27.750+0700 E/JSON PARSING( 2326): No data could be display
05-03 08:46:27.750+0700 E/JSON PARSING( 2326): �J�ؼ��;
05-03 08:46:27.750+0700 I/CAPI_NETWORK_CONNECTION( 2326): connection.c: connection_destroy(471) > Destroy handle: 0xb26023d0
05-03 08:46:27.910+0700 I/efl-extension( 1122): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
05-03 08:46:27.920+0700 I/efl-extension( 2326): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
05-03 08:46:27.920+0700 I/efl-extension( 1187): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
05-03 08:46:27.920+0700 I/efl-extension( 1187): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xafb46e50, elm_layout, time_stamp : 310719
05-03 08:46:27.920+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: onRotary(5387) >  __nTimeStamp:[0], __isFastMoving[0]
05-03 08:46:27.920+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: runRotaryBackwardAnimation(3721) >  EditMode[0], focusPage[0], focusIndex[0], ItemList size[28], FocusPrev[0], FocusNext[0], FocusRecent[0], HideNextPage[0]
05-03 08:46:27.920+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: runFocusAni(3491) >  nNewFocus[-1], anim[0], autoLaunch[1], FocusNext[0], FocusPrev[0], FocusRecent[1], HideNextPage[0], ItemListSize[28]
05-03 08:46:27.920+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: setBubbleColor(2479) >  [249, 249, 249, 255]
05-03 08:46:28.130+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: onAutoLaunchTimerStart(5026) >  auto launch is disabled
05-03 08:46:28.770+0700 E/EFL     (  892): ecore_x<892> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=311575 button=1
05-03 08:46:28.770+0700 W/W_HOME  ( 1187): event_manager.c: home_event_manager_allowance_get(860) > editing:0 clocklist_state:0 addviewer:0 scrolling:0 apptray-state:0 apptray-visibility:1 apptray-edit_visibility:0
05-03 08:46:28.910+0700 E/EFL     (  892): ecore_x<892> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=311717 button=1
05-03 08:46:28.910+0700 E/EFL     (  892): <892> e_mod_processmgr.c:499 _e_mod_processmgr_anr_ping() safety check failed: bd == NULL
05-03 08:46:28.910+0700 E/EFL     ( 1187): ecore_x<1187> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=311717 button=1
05-03 08:46:28.910+0700 E/EFL     ( 1187): ecore_x<1187> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=311717 button=1
05-03 08:46:28.910+0700 W/W_HOME  ( 1187): event_manager.c: home_event_manager_allowance_get(860) > editing:0 clocklist_state:0 addviewer:0 scrolling:0 apptray-state:0 apptray-visibility:1 apptray-edit_visibility:0
05-03 08:46:28.910+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: touchPressed(1663) >  TOUCH [234, 51]
05-03 08:46:28.920+0700 E/W_HOME  ( 1187): util.c: util_is_rdu_retailmode(1530) > Cannot get the vconf for retailmode
05-03 08:46:28.920+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: touchReleased(1976) >  TOUCH [234, 51]->[234, 51]
05-03 08:46:28.920+0700 E/APPS    ( 1187): AppsViewNecklace.cpp: __GetAppsItemByTouchIndex(6941) >  Can't Find AppsItem at [-1]
05-03 08:46:28.920+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: runFocusAni(3491) >  nNewFocus[0], anim[1], autoLaunch[1], FocusNext[0], FocusPrev[0], FocusRecent[1], HideNextPage[0], ItemListSize[28]
05-03 08:46:28.920+0700 E/APPS    ( 1187): effect.c: apps_effect_play_sound(86) >  effect_info.sound_status: [0]
05-03 08:46:28.920+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: onClickedRecentApps(3256) >  item(Recent apps) launched, open(0)
05-03 08:46:28.920+0700 W/AUL     ( 1187): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.samsung.w-taskmanager)
05-03 08:46:28.920+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 0
05-03 08:46:28.920+0700 W/AUL_AMD (  927): amd_launch.c: _start_app(1782) > caller pid : 1187
05-03 08:46:28.920+0700 I/AUL_AMD (  927): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
05-03 08:46:28.940+0700 E/RESOURCED( 1110): vmpressure-lowmem-handler.c: lowmem_handle_request(1124) > Done: killed 0 processes reclaimed: 0 remaining: 0 shortfall: 0 status: 1
05-03 08:46:28.950+0700 W/AUL_AMD (  927): amd_launch.c: _start_app(2218) > pad pid(-5)
05-03 08:46:28.950+0700 W/AUL_PAD ( 1879): launchpad.c: __launchpad_main_loop(611) > Launch on type-based process-pool
05-03 08:46:28.950+0700 W/AUL_PAD ( 1879): launchpad.c: __send_result_to_caller(272) > Check app launching
05-03 08:46:28.960+0700 W/AUL     ( 2305): smack_util.c: set_app_smack_label(242) > thr_cnt: 1, signal count: 1,  try count 1, launchpad type: 1
05-03 08:46:28.960+0700 W/AUL_PAD ( 2305): launchpad_loader.c: __candidate_process_prepare_exec(259) > [candidate] before __set_access
05-03 08:46:28.960+0700 W/AUL_PAD ( 2305): launchpad_loader.c: __candidate_process_prepare_exec(264) > [candidate] after __set_access
05-03 08:46:28.960+0700 W/AUL_PAD ( 2305): launchpad_loader.c: __candidate_process_launchpad_main_loop(414) > update argument
05-03 08:46:28.960+0700 W/AUL_PAD ( 2305): launchpad_loader.c: main(680) > [candidate] dlopen(com.samsung.w-taskmanager)
05-03 08:46:28.980+0700 W/AUL_PAD ( 2305): launchpad_loader.c: main(690) > [candidate] dlsym
05-03 08:46:28.980+0700 W/AUL_PAD ( 2305): launchpad_loader.c: main(694) > [candidate] dl_main(com.samsung.w-taskmanager)
05-03 08:46:28.980+0700 I/CAPI_APPFW_APPLICATION( 2305): app_main.c: app_efl_main(129) > app_efl_main
05-03 08:46:28.980+0700 E/EFL     (  892): elementary<892> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8a82ba8 in function: elm_layout_edje_get, of type: 'edje' when expecting type: 'elm_layout'
05-03 08:46:28.980+0700 E/UXT     (  892): uxt_theme_object.c: uxt_theme_object_set_changeable_ui_enabled(41) > failed to get edje from parent object.
05-03 08:46:28.980+0700 I/CAPI_APPFW_APPLICATION( 2305): app_main.c: app_appcore_create(152) > app_appcore_create
05-03 08:46:28.990+0700 E/EFL     (  892): ecore_evas<892> ecore_evas_extn.c:2204 ecore_evas_extn_plug_connect() Extn plug failed to connect:ipctype=0, svcname=elm_indicator_portrait, svcnum=0, svcsys=0
05-03 08:46:29.050+0700 W/AUL     (  927): app_signal.c: aul_send_app_launch_request_signal(521) > aul_send_app_launch_request_signal app(com.samsung.w-taskmanager) pid(2305) type(uiapp) bg(0)
05-03 08:46:29.050+0700 W/AUL     ( 1187): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2305)
05-03 08:46:29.050+0700 W/W_HOME  ( 1187): util.c: apps_util_launch_main_operation(785) > Launch app:[Recent apps] ret:[0]
05-03 08:46:29.050+0700 W/AUL_AMD (  927): amd_status.c: __socket_monitor_cb(1277) > (2305) was created
05-03 08:46:29.050+0700 E/AUL     (  927): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
05-03 08:46:29.060+0700 W/W_HOME  ( 1187): util.c: send_launch_appId(620) > launch appid[com.samsung.w-taskmanager]
05-03 08:46:29.060+0700 W/STARTER ( 1120): pkg-monitor.c: _app_mgr_status_cb(400) > [_app_mgr_status_cb:400] Launch request [2305]
05-03 08:46:29.060+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 08:46:29.060+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 08:46:29.060+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 08:46:29.060+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 08:46:29.060+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 08:46:29.060+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 08:46:29.060+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.app-shortcut-widget:Apptray.Message.Launch.AppId]
05-03 08:46:29.060+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: IsPreloaded(395) > _MessagePortService::IsPreloaded
05-03 08:46:29.100+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 08:46:29.100+0700 E/W_HOME  ( 1187): util.c: send_launch_appId(636) > There is no remote message port
05-03 08:46:29.190+0700 I/APP_CORE( 2305): appcore-efl.c: __do_app(453) > [APP 2305] Event: RESET State: CREATED
05-03 08:46:29.190+0700 I/CAPI_APPFW_APPLICATION( 2305): app_main.c: app_appcore_reset(245) > app_appcore_reset
05-03 08:46:29.260+0700 E/CAPI_MEDIA_CONTROLLER( 1365): media_controller_main.c: __mc_main_check_connection(34) > [0m[No-error] Timer is Called but there is working Process, connection_cnt = 3
05-03 08:46:29.300+0700 I/efl-extension( 2305): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
05-03 08:46:29.300+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
05-03 08:46:29.300+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_event_handler_add(77) > init_count: 0
05-03 08:46:29.300+0700 I/efl-extension( 2305): efl_extension_rotary.c: _init_Xi2_system(314) > In
05-03 08:46:29.350+0700 I/efl-extension( 2305): efl_extension_rotary.c: _init_Xi2_system(375) > Done
05-03 08:46:29.350+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb82cdc60, elm_image, _activated_obj : 0x0, activated : 1
05-03 08:46:29.400+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 08:46:29.400+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
05-03 08:46:29.400+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
05-03 08:46:29.400+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 08:46:29.400+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
05-03 08:46:29.400+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
05-03 08:46:29.410+0700 E/W_TASKMANAGER( 2305): util_wc1.c: close_button_disabled_set(375) > [close_button_disabled_set:375] (ad->ly_main == NULL) -> close_button_disabled_set() return
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _app_list_cb(499) > [_app_list_cb:499] pkgmgrinfo_appinfo_get_label(com.samsung.skmsa) failed(0)
05-03 08:46:29.470+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-home)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (health-data-service)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.techgraphy.DigitalTick)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.weather-widget)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.shealth.widget.pedometer)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.app-shortcut-widget)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.remote-sensor-service)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.wusvc)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-music-player.music-control-service)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.shealth-service)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.call.consumer)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.watchface-service)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.wnotiboard-popup)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.message.consumer)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.weip.consumer)!!
05-03 08:46:29.470+0700 E/W_TASKMANAGER( 2305): task.c: _iterfunc(531) > [_iterfunc:531] Fail to get ai table (com.samsung.w-taskmanager)!!
05-03 08:46:29.480+0700 E/RUA     ( 2305): rua.c: rua_history_load_db(278) > rua_history_load_db ok. nrows : 17, ncols : 5
05-03 08:46:29.490+0700 E/EFL     ( 2305): evas_main<2305> evas_stack.c:158 evas_object_stack_above() BITCH! evas_object_stack_above(), 0xb83de628 not inside same smart as 0xb8304ce0!
05-03 08:46:29.490+0700 E/EFL     ( 2305): elementary<2305> elm_layout.c:1021 _elm_layout_smart_content_set() could not swallow 0xb82df8d8 into part 'elm.swallow.event.0'
05-03 08:46:29.520+0700 I/APP_CORE( 2305): appcore-efl.c: __do_app(522) > Legacy lifecycle: 0
05-03 08:46:29.520+0700 I/APP_CORE( 2305): appcore-efl.c: __do_app(524) > [APP 2305] Initial Launching, call the resume_cb
05-03 08:46:29.520+0700 I/CAPI_APPFW_APPLICATION( 2305): app_main.c: app_appcore_resume(223) > app_appcore_resume
05-03 08:46:29.550+0700 W/W_HOME  ( 1187): event_manager.c: _ecore_x_message_cb(421) > state: 0 -> 1
05-03 08:46:29.550+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:29.550+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:29.550+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:29.550+0700 W/W_HOME  ( 1187): win.c: win_back_gesture_disable_set(170) > disable back gesture
05-03 08:46:29.550+0700 W/W_HOME  ( 1187): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 0
05-03 08:46:29.550+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 08:46:29.550+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 08:46:29.630+0700 W/APP_CORE( 2305): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3e00003
05-03 08:46:29.640+0700 E/AUL     (  927): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
05-03 08:46:29.640+0700 W/W_HOME  ( 1187): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(1)
05-03 08:46:29.640+0700 W/W_HOME  ( 1187): event_manager.c: _window_visibility_cb(470) > state: 1 -> 0
05-03 08:46:29.640+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:29.640+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:6, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:29.640+0700 W/W_HOME  ( 1187): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(1)
05-03 08:46:29.640+0700 I/APP_CORE( 1187): appcore-efl.c: __do_app(453) > [APP 1187] Event: PAUSE State: RUNNING
05-03 08:46:29.640+0700 I/CAPI_APPFW_APPLICATION( 1187): app_main.c: app_appcore_pause(202) > app_appcore_pause
05-03 08:46:29.640+0700 W/W_HOME  ( 1187): main.c: _appcore_pause_cb(489) > appcore pause
05-03 08:46:29.640+0700 W/W_HOME  ( 1187): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
05-03 08:46:29.640+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:29.650+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:29.650+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 08:46:29.650+0700 W/W_HOME  ( 1187): rotary.c: rotary_deattach(156) > rotary_deattach:0xafb46e50
05-03 08:46:29.650+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:46:29.650+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xafb46e50, elm_layout, func : 0xb6e8e455
05-03 08:46:29.650+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 08:46:29.650+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 08:46:29.650+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:46:29.650+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7fbccf8, elm_box, _activated_obj : 0xafb46e50, activated : 1
05-03 08:46:29.650+0700 I/efl-extension( 1187): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 E/APPS    ( 1187): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 08:46:29.650+0700 W/W_HOME  ( 1187): win.c: win_back_gesture_disable_set(170) > disable back gesture
05-03 08:46:29.650+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 08:46:29.650+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 08:46:29.650+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 08:46:29.650+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 08:46:29.650+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 08:46:29.650+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 08:46:29.650+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 08:46:29.650+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 08:46:29.650+0700 W/AUL     (  927): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1187) status(bg) type(uiapp)
05-03 08:46:29.650+0700 W/STARTER ( 1120): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1187] goes to (4)
05-03 08:46:29.650+0700 E/STARTER ( 1120): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1187)'s state(4)
05-03 08:46:29.650+0700 W/STARTER ( 1120): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2305] goes to (3)
05-03 08:46:29.660+0700 W/AUL     (  927): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-taskmanager) pid(2305) status(fg) type(uiapp)
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 08:46:29.660+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 08:46:29.660+0700 W/MUSIC_CONTROL_SERVICE( 1749): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1749]   [com.samsung.w-home]register msg port [false][0m
05-03 08:46:29.660+0700 I/wnotib  ( 1187): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 1
05-03 08:46:29.660+0700 E/wnotib  ( 1187): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
05-03 08:46:29.660+0700 W/wnotib  ( 1187): w-notification-board-noti-manager.c: wnb_nm_postpone_updating_job(997) > Set is_notiboard_update_postponed to true with is_for_VI 0, notiboard panel creation count [1], notiboard card appending count [3].
05-03 08:46:29.690+0700 W/APPS    ( 1187): AppsViewNecklace.cpp: onPausedIdlerCb(5178) >  elm_cache_all_flush
05-03 08:46:29.720+0700 I/APP_CORE( 2305): appcore-efl.c: __do_app(453) > [APP 2305] Event: RESUME State: RUNNING
05-03 08:46:29.720+0700 I/GATE    ( 2305): <GATE-M>APP_FULLY_LOADED_w-taskmanager</GATE-M>
05-03 08:46:29.990+0700 I/APP_CORE( 2326): appcore-efl.c: __do_app(453) > [APP 2326] Event: MEM_FLUSH State: PAUSED
05-03 08:46:30.110+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:46:30.120+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2305
05-03 08:46:30.120+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 08:46:30.130+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:46:30.140+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2305
05-03 08:46:30.140+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 08:46:30.160+0700 I/APP_CORE( 1187): appcore-efl.c: __do_app(453) > [APP 1187] Event: MEM_FLUSH State: PAUSED
05-03 08:46:30.320+0700 E/EFL     ( 2305): ecore_x<2305> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=313123 button=1
05-03 08:46:30.370+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb82afec0 : elm_scroller] mouse move
05-03 08:46:30.370+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb82afec0 : elm_scroller] hold(0), freeze(0)
05-03 08:46:30.380+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb82afec0 : elm_scroller] mouse move
05-03 08:46:30.380+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb82afec0 : elm_scroller] hold(0), freeze(0)
05-03 08:46:30.400+0700 E/EFL     ( 2305): ecore_x<2305> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=313199 button=1
05-03 08:46:30.400+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:46:30.410+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2326
05-03 08:46:30.410+0700 W/AUL     ( 2305): launch.c: app_request_to_launchpad(284) > request cmd(27) to(2326)
05-03 08:46:30.410+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 27
05-03 08:46:30.410+0700 W/AUL     (  927): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2326) type(uiapp)
05-03 08:46:30.410+0700 I/APP_CORE( 2326): appcore-efl.c: __do_app(453) > [APP 2326] Event: TERMINATE State: PAUSED
05-03 08:46:30.410+0700 W/AUL     ( 2305): launch.c: app_request_to_launchpad(298) > request cmd(27) result(0)
05-03 08:46:30.410+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 22
05-03 08:46:30.410+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(999) > app status : 4
05-03 08:46:30.410+0700 W/APP_CORE( 2326): appcore-efl.c: appcore_efl_main(1788) > power off : 0
05-03 08:46:30.410+0700 W/APP_CORE( 2326): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3a00003] -> redirected win[600617] for com.toyota.realtimefeedback[2326]
05-03 08:46:30.460+0700 I/APP_CORE( 2326): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
05-03 08:46:30.460+0700 I/CAPI_APPFW_APPLICATION( 2326): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb874e9c0 is freed
05-03 08:46:30.480+0700 E/EFL     ( 2326): elementary<2326> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8742280 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb8742cc8, obj: 0xb8742cc8
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 08:46:30.480+0700 E/EFL     ( 2326): elementary<2326> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 08:46:30.480+0700 E/EFL     ( 2326): elementary<2326> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 08:46:30.480+0700 E/EFL     ( 2326): elementary<2326> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8742cc8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 08:46:30.480+0700 E/EFL     ( 2326): elementary<2326> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8742cc8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 08:46:30.480+0700 E/EFL     ( 2326): elementary<2326> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8742cc8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 08:46:30.480+0700 E/EFL     ( 2326): elementary<2326> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8742cc8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb8769698 is freed
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8742cc8, elm_genlist, func : 0xb6e31ea1
05-03 08:46:30.480+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:46:30.480+0700 I/APP_CORE( 2326): appcore-efl.c: __after_loop(1243) > [APP 2326] After terminate() callback
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb88387f8 is freed
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb87910a0, elm_genlist, func : 0xb6e31ea1
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb88d3300
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb8841488 is freed
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb881ffd0, elm_scroller, func : 0xb6e35379
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb88d3300, elm_image, func : 0xb6e35379
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:46:30.490+0700 I/efl-extension( 2326): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb881ffd0 : elm_scroller] rotary callabck is deleted
05-03 08:46:30.630+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 22
05-03 08:46:30.630+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(999) > app status : 4
05-03 08:46:30.630+0700 E/APP_CORE( 2305): appcore.c: __del_vconf(453) > [FAILED]vconfkey_ignore_key_changed
05-03 08:46:30.630+0700 I/APP_CORE( 2305): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
05-03 08:46:30.630+0700 I/APP_CORE( 2305): appcore-efl.c: __after_loop(1234) > [APP 2305] PAUSE before termination
05-03 08:46:30.630+0700 I/CAPI_APPFW_APPLICATION( 2305): app_main.c: app_appcore_pause(202) > app_appcore_pause
05-03 08:46:30.630+0700 I/CAPI_APPFW_APPLICATION( 2305): app_main.c: app_appcore_terminate(177) > app_appcore_terminate
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb82afec0, obj: 0xb82afec0
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 08:46:30.630+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 08:46:30.630+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
05-03 08:46:30.630+0700 E/EFL     ( 2305): elementary<2305> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb82afec0 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb82cdc60
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb82c1898 is freed
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb82afec0, elm_scroller, func : 0xb376a379
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb82cdc60, elm_image, func : 0xb376a379
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:46:30.630+0700 I/efl-extension( 2305): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb82afec0 : elm_scroller] rotary callabck is deleted
05-03 08:46:30.650+0700 W/STARTER ( 1120): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1187] goes to (3)
05-03 08:46:30.650+0700 E/STARTER ( 1120): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1187)'s state(3)
05-03 08:46:30.650+0700 W/AUL_AMD (  927): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
05-03 08:46:30.650+0700 W/AUL_AMD (  927): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
05-03 08:46:30.650+0700 W/AUL     (  927): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1187) status(fg) type(uiapp)
05-03 08:46:30.650+0700 E/W_TASKMANAGER( 2305): task.c: taskmanager_remove_alltask(647) > [taskmanager_remove_alltask:647] App list is empty!
05-03 08:46:30.650+0700 I/APP_CORE( 2305): appcore-efl.c: __after_loop(1243) > [APP 2305] After terminate() callback
05-03 08:46:30.770+0700 I/UXT     ( 2305): uxt_theme_private.c: uxt_theme_delete_file_monitor(1655) > deleted color config file monitor
05-03 08:46:30.770+0700 I/UXT     ( 2305): uxt_theme_private.c: uxt_theme_delete_file_monitor(1662) > deleted font config file monitor
05-03 08:46:30.770+0700 I/UXT     ( 2305): Uxt_ObjectManager.cpp: OnTerminating(774) > Terminating.
05-03 08:46:30.880+0700 W/CRASH_MANAGER( 2403): worker.c: worker_job(1205) > 1102326726561152531199
