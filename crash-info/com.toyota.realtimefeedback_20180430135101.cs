S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2140
Date: 2018-04-30 13:51:01+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2140, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb58708f0
r2   = 0xb5870ef8, r3   = 0x00000000
r4   = 0xb85593e0, r5   = 0xb86736b0
r6   = 0x00000013, r7   = 0xbefbd208
r8   = 0x00003d24, r9   = 0xb85593e0
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb5870b44, sp   = 0xbefbd1e8
lr   = 0xb585e1e9, pc   = 0xb585e21e
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      4388 KB
Buffers:      6876 KB
Cached:     111752 KB
VmPeak:     128096 KB
VmSize:     125716 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       24892 KB
VmRSS:       24892 KB
VmData:      65484 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          90 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2140 TID = 2140
2140 2165 2166 

Maps Information
b1a0b000 b1a0e000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b1df0000 b1df4000 r-xp /usr/lib/libogg.so.0.7.1
b1dfc000 b1e1e000 r-xp /usr/lib/libvorbis.so.0.4.3
b1e26000 b1e6d000 r-xp /usr/lib/libsndfile.so.1.0.26
b1e79000 b1ec2000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1ecb000 b1ed0000 r-xp /usr/lib/libjson.so.0.0.1
b3771000 b3877000 r-xp /usr/lib/libicuuc.so.57.1
b388d000 b3a15000 r-xp /usr/lib/libicui18n.so.57.1
b3a25000 b3a32000 r-xp /usr/lib/libail.so.0.1.0
b3a3b000 b3a3e000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3a46000 b3a7e000 r-xp /usr/lib/libpulse.so.0.16.2
b3a7f000 b3a82000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3a8a000 b3aeb000 r-xp /usr/lib/libasound.so.2.0.0
b3af5000 b3b0e000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3b17000 b3b1b000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3b23000 b3b2e000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3b3b000 b3b3f000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3b48000 b3b60000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3b71000 b3b78000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3b80000 b3b8b000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3b93000 b3b95000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3b9d000 b3ba5000 r-xp /usr/lib/libfeedback.so.0.1.4
b3bb5000 b3bb6000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3f01000 b3f02000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3f0a000 b3f0b000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3f84000 b400b000 rw-s anon_inode:dmabuf
b400c000 b480b000 rw-p [stack:2166]
b49af000 b49b0000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4ab7000 b52b6000 rw-p [stack:2165]
b52b6000 b52b8000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52c0000 b52d7000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52e4000 b52e6000 r-xp /usr/lib/libdri2.so.0.0.0
b52ee000 b52f9000 r-xp /usr/lib/libtbm.so.1.0.0
b5301000 b5309000 r-xp /usr/lib/libdrm.so.2.4.0
b5311000 b5313000 r-xp /usr/lib/libgenlock.so
b531b000 b5320000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5328000 b5333000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b553c000 b5606000 r-xp /usr/lib/libCOREGL.so.4.0
b5617000 b5627000 r-xp /usr/lib/libmdm-common.so.1.1.25
b562f000 b5635000 r-xp /usr/lib/libxcb-render.so.0.0.0
b563d000 b563e000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5647000 b564a000 r-xp /usr/lib/libEGL.so.1.4
b5652000 b5660000 r-xp /usr/lib/libGLESv2.so.2.0
b5669000 b56b2000 r-xp /usr/lib/libmdm.so.1.2.70
b56bb000 b56c1000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56c9000 b56d2000 r-xp /usr/lib/libcom-core.so.0.0.1
b56db000 b5793000 r-xp /usr/lib/libcairo.so.2.11200.14
b579e000 b57b7000 r-xp /usr/lib/libnetwork.so.0.0.0
b57bf000 b57cb000 r-xp /usr/lib/libnotification.so.0.1.0
b57d4000 b57e3000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b57ec000 b580d000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5815000 b581a000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5822000 b5827000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b582f000 b583f000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5847000 b584f000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5857000 b5861000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a06000 b5a10000 r-xp /lib/libnss_files-2.13.so
b5a19000 b5ae8000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5afe000 b5b22000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b2b000 b5b31000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b39000 b5b3d000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b4a000 b5b55000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b5d000 b5b5f000 r-xp /usr/lib/libiniparser.so.0
b5b68000 b5b6d000 r-xp /usr/lib/libappcore-common.so.1.1
b5b75000 b5b77000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b80000 b5b84000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5b91000 b5b93000 r-xp /usr/lib/libXau.so.6.0.0
b5b9b000 b5ba2000 r-xp /lib/libcrypt-2.13.so
b5bd2000 b5bd4000 r-xp /usr/lib/libiri.so
b5bdd000 b5d6f000 r-xp /usr/lib/libcrypto.so.1.0.0
b5d90000 b5dd7000 r-xp /usr/lib/libssl.so.1.0.0
b5de3000 b5e11000 r-xp /usr/lib/libidn.so.11.5.44
b5e19000 b5e22000 r-xp /usr/lib/libcares.so.2.1.0
b5e2c000 b5e3f000 r-xp /usr/lib/libxcb.so.1.1.0
b5e48000 b5e4b000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e53000 b5e55000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e5e000 b5f2a000 r-xp /usr/lib/libxml2.so.2.7.8
b5f37000 b5f39000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f42000 b5f47000 r-xp /usr/lib/libffi.so.5.0.10
b5f4f000 b5f50000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f58000 b5f5b000 r-xp /lib/libattr.so.1.1.0
b5f63000 b5ff7000 r-xp /usr/lib/libstdc++.so.6.0.16
b600a000 b6027000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6031000 b6049000 r-xp /usr/lib/libpng12.so.0.50.0
b6051000 b6067000 r-xp /lib/libexpat.so.1.6.0
b6071000 b60b5000 r-xp /usr/lib/libcurl.so.4.3.0
b60be000 b60c8000 r-xp /usr/lib/libXext.so.6.4.0
b60d2000 b60d6000 r-xp /usr/lib/libXtst.so.6.1.0
b60de000 b60e4000 r-xp /usr/lib/libXrender.so.1.3.0
b60ec000 b60f2000 r-xp /usr/lib/libXrandr.so.2.2.0
b60fa000 b60fb000 r-xp /usr/lib/libXinerama.so.1.0.0
b6104000 b610d000 r-xp /usr/lib/libXi.so.6.1.0
b6115000 b6118000 r-xp /usr/lib/libXfixes.so.3.1.0
b6121000 b6123000 r-xp /usr/lib/libXgesture.so.7.0.0
b612b000 b612d000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6135000 b6137000 r-xp /usr/lib/libXdamage.so.1.1.0
b613f000 b6146000 r-xp /usr/lib/libXcursor.so.1.0.2
b614e000 b6151000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b615a000 b615e000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6167000 b616c000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6175000 b6256000 r-xp /usr/lib/libX11.so.6.3.0
b6261000 b6284000 r-xp /usr/lib/libjpeg.so.8.0.2
b629c000 b62b2000 r-xp /lib/libz.so.1.2.5
b62bb000 b62bd000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62c5000 b633a000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6344000 b635e000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6366000 b639a000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63a3000 b6476000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6482000 b6492000 r-xp /lib/libresolv-2.13.so
b6496000 b64ae000 r-xp /usr/lib/liblzma.so.5.0.3
b64b6000 b64b9000 r-xp /lib/libcap.so.2.21
b64c1000 b64f0000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b64f8000 b64f9000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6502000 b6508000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6510000 b6527000 r-xp /usr/lib/liblua-5.1.so
b6530000 b6537000 r-xp /usr/lib/libembryo.so.1.7.99
b653f000 b6545000 r-xp /lib/librt-2.13.so
b654e000 b65a4000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65b2000 b6608000 r-xp /usr/lib/libfreetype.so.6.11.3
b6614000 b663c000 r-xp /usr/lib/libfontconfig.so.1.8.0
b663d000 b6682000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b668b000 b669e000 r-xp /usr/lib/libfribidi.so.0.3.1
b66a6000 b66c0000 r-xp /usr/lib/libecore_con.so.1.7.99
b66ca000 b66d3000 r-xp /usr/lib/libedbus.so.1.7.99
b66db000 b672b000 r-xp /usr/lib/libecore_x.so.1.7.99
b672d000 b6736000 r-xp /usr/lib/libvconf.so.0.2.45
b673e000 b674f000 r-xp /usr/lib/libecore_input.so.1.7.99
b6757000 b675c000 r-xp /usr/lib/libecore_file.so.1.7.99
b6764000 b6786000 r-xp /usr/lib/libecore_evas.so.1.7.99
b678f000 b67d0000 r-xp /usr/lib/libeina.so.1.7.99
b67d9000 b67f2000 r-xp /usr/lib/libeet.so.1.7.99
b6803000 b686c000 r-xp /lib/libm-2.13.so
b6875000 b687b000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6884000 b6885000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b688d000 b68b0000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68b8000 b68bd000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68c5000 b68ef000 r-xp /usr/lib/libdbus-1.so.3.8.12
b68f8000 b690f000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6917000 b6922000 r-xp /lib/libunwind.so.8.0.1
b694f000 b696d000 r-xp /usr/lib/libsystemd.so.0.4.0
b6977000 b6a9b000 r-xp /lib/libc-2.13.so
b6aa9000 b6ab1000 r-xp /lib/libgcc_s-4.6.so.1
b6ab2000 b6ab6000 r-xp /usr/lib/libsmack.so.1.0.0
b6abf000 b6ac5000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6acd000 b6b9d000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6b9e000 b6bfc000 r-xp /usr/lib/libedje.so.1.7.99
b6c06000 b6c1d000 r-xp /usr/lib/libecore.so.1.7.99
b6c34000 b6d02000 r-xp /usr/lib/libevas.so.1.7.99
b6d28000 b6e64000 r-xp /usr/lib/libelementary.so.1.7.99
b6e7b000 b6e8f000 r-xp /lib/libpthread-2.13.so
b6e9a000 b6e9c000 r-xp /usr/lib/libdlog.so.0.0.0
b6ea4000 b6ea7000 r-xp /usr/lib/libbundle.so.0.1.22
b6eaf000 b6eb1000 r-xp /lib/libdl-2.13.so
b6eba000 b6ec7000 r-xp /usr/lib/libaul.so.0.1.0
b6ed9000 b6edf000 r-xp /usr/lib/libappcore-efl.so.1.1
b6ee8000 b6eec000 r-xp /usr/lib/libsys-assert.so
b6ef5000 b6f12000 r-xp /lib/ld-2.13.so
b6f1b000 b6f20000 r-xp /usr/bin/launchpad-loader
b843e000 b8e3a000 rw-p [heap]
bef9d000 befbe000 rw-p [stack]
b843e000 b8e3a000 rw-p [heap]
bef9d000 befbe000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2140)
Call Stack Count: 21
 0: set_targeted_view + 0xd5 (0xb585e21e) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x721e
 1: pop_from_stack_uib_vc + 0x38 (0xb585e6d5) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x76d5
 2: uib_views_destroy_callback + 0x38 (0xb585e691) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7691
 3: (0xb6c4baf9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6c62c39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6c5537b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6c55ab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb6773cf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6e26953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6c113f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6c0ee53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6c1246b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6c127db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6de344d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x454 (0xb6edcc69) [/usr/lib/libappcore-efl.so.1] + 0x3c69
15: ui_app_main + 0xb0 (0xb5b81ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16: uib_app_run + 0xea (0xb585c3fb) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53fb
17: main + 0x34 (0xb585c8e5) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x58e5
18:  + 0x0 (0xb6f1ca53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
19: __libc_start_main + 0x114 (0xb698e85c) [/lib/libc.so.6] + 0x1785c
20: (0xb6f1ce0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
tion_x(0), direction_y(1)
04-30 13:50:51.519+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] drag_child_locked_y(0)
04-30 13:50:51.519+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] move content x(0), y(1272)
04-30 13:50:51.539+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:51.539+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:51.539+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:51.539+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:51.539+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] direction_x(0), direction_y(1)
04-30 13:50:51.539+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] drag_child_locked_y(0)
04-30 13:50:51.539+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] move content x(0), y(1201)
04-30 13:50:51.569+0700 E/EFL     ( 2140): ecore_x<2140> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=257666 button=1
04-30 13:50:52.239+0700 E/EFL     ( 2140): ecore_x<2140> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=258325 button=1
04-30 13:50:52.239+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.239+0700 I/efl-extension( 2140): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(489) > [0xb868d780 : elm_scroller] detent_count(0)
04-30 13:50:52.239+0700 I/efl-extension( 2140): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(490) > [0xb868d780 : elm_scroller] pagenumber_v(0), pagenumber_h(0)
04-30 13:50:52.239+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.239+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.239+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.239+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.239+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.239+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.239+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4292 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] add hold animator
04-30 13:50:52.249+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] direction_x(0), direction_y(1)
04-30 13:50:52.249+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] drag_child_locked_y(0)
04-30 13:50:52.249+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] move content x(0), y(136)
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] direction_x(0), direction_y(1)
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] drag_child_locked_y(0)
04-30 13:50:52.279+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] move content x(0), y(86)
04-30 13:50:52.309+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.309+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.309+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.309+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.309+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] direction_x(0), direction_y(1)
04-30 13:50:52.309+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] drag_child_locked_y(0)
04-30 13:50:52.309+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb868d780 : elm_scroller] move content x(0), y(-1)
04-30 13:50:52.329+0700 E/EFL     ( 2140): ecore_x<2140> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=258434 button=1
04-30 13:50:52.329+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] mouse move
04-30 13:50:52.329+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb868d780 : elm_scroller] hold(0), freeze(0)
04-30 13:50:52.339+0700 I/efl-extension( 2140): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(489) > [0xb868d780 : elm_scroller] detent_count(0)
04-30 13:50:52.339+0700 I/efl-extension( 2140): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(490) > [0xb868d780 : elm_scroller] pagenumber_v(0), pagenumber_h(0)
04-30 13:50:53.319+0700 E/EFL     (  883): ecore_x<883> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3200002 time=258434
04-30 13:50:53.319+0700 E/EFL     ( 2140): ecore_x<2140> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=258434
04-30 13:50:53.319+0700 E/EFL     (  883): ecore_x<883> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=258434
04-30 13:50:54.269+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-30 13:50:55.689+0700 I/APP_CORE( 1142): appcore-efl.c: __do_app(453) > [APP 1142] Event: MEM_FLUSH State: PAUSED
04-30 13:50:55.839+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2140): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
04-30 13:50:55.839+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2140): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
04-30 13:50:55.839+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2140): preference.c: preference_get_string(1258) > preference_get_string(2140) : URL error
04-30 13:50:55.859+0700 I/CAPI_NETWORK_CONNECTION( 2140): connection.c: connection_create(453) > New handle created[0xb89e9950]
04-30 13:50:55.879+0700 E/JSON PARSING( 2140): No data could be display
04-30 13:50:55.879+0700 E/JSON PARSING( 2140): 
04-30 13:50:55.879+0700 I/CAPI_NETWORK_CONNECTION( 2140): connection.c: connection_destroy(471) > Destroy handle: 0xb89e9950
04-30 13:50:58.899+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-30 13:50:58.899+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-30 13:50:58.899+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
04-30 13:50:58.899+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
04-30 13:50:58.899+0700 E/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
04-30 13:50:58.899+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-30 13:50:58.899+0700 E/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-30 13:50:58.899+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-30 13:51:00.839+0700 E/PKGMGR  ( 2309): pkgmgr.c: __pkgmgr_client_uninstall(2061) > uninstall pkg start.
04-30 13:51:00.909+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2140): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
04-30 13:51:00.909+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2140): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
04-30 13:51:00.909+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2140): preference.c: preference_get_string(1258) > preference_get_string(2140) : URL error
04-30 13:51:00.959+0700 I/CAPI_NETWORK_CONNECTION( 2140): connection.c: connection_create(453) > New handle created[0xb86893b0]
04-30 13:51:00.979+0700 E/JSON PARSING( 2140): No data could be display
04-30 13:51:00.979+0700 E/JSON PARSING( 2140): �¯X��;
04-30 13:51:00.979+0700 I/CAPI_NETWORK_CONNECTION( 2140): connection.c: connection_destroy(471) > Destroy handle: 0xb86893b0
04-30 13:51:01.059+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: main(2242) > package manager server start
04-30 13:51:01.169+0700 E/PKGMGR_SERVER( 2311): pm-mdm.c: _pm_check_mdm_policy(1078) > [0;31m[_pm_check_mdm_policy(): 1078](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
04-30 13:51:01.169+0700 E/PKGMGR  ( 2309): pkgmgr.c: __pkgmgr_client_uninstall(2186) > uninstall pkg finish, ret=[23090002]
04-30 13:51:01.179+0700 E/PKGMGR_SERVER( 2312): pkgmgr-server.c: queue_job(1962) > UNINSTALL start, pkgid=[com.toyota.realtimefeedback]
04-30 13:51:01.279+0700 E/rpm-installer( 2312): rpm-appcore-intf.c: main(120) > ------------------------------------------------
04-30 13:51:01.279+0700 E/rpm-installer( 2312): rpm-appcore-intf.c: main(121) >  [START] rpm-installer: version=[201610629.1]
04-30 13:51:01.279+0700 E/rpm-installer( 2312): rpm-appcore-intf.c: main(122) > ------------------------------------------------
04-30 13:51:01.399+0700 E/rpm-installer( 2312): rpm-cmdline.c: __ri_is_core_tpk_app(358) > This is a core tpk app.
04-30 13:51:01.509+0700 I/watchface-app( 1307): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
04-30 13:51:01.509+0700 W/W_HOME  ( 1243): clock_event.c: _pkgmgr_event_cb(194) > Pkg:com.toyota.realtimefeedback is being uninstalled
04-30 13:51:01.509+0700 W/W_HOME  ( 1243): dbox.c: uninstall_cb(1434) > uninstalled:com.toyota.realtimefeedback
04-30 13:51:01.529+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:01.539+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2140
04-30 13:51:01.539+0700 W/AUL_AMD (  923): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(start), value(uninstall)
04-30 13:51:01.539+0700 W/AUL_AMD (  923): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(997) > __amd_pkgmgrinfo_start_handler
04-30 13:51:01.539+0700 E/rpm-installer( 2312): installer-util.c: __check_running_app(1774) > app[com.toyota.realtimefeedback] is running, need to terminate it.
04-30 13:51:01.539+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 13:51:01.549+0700 W/AUL     ( 2312): launch.c: app_request_to_launchpad(284) > request cmd(5) to(2140)
04-30 13:51:01.549+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 5
04-30 13:51:01.549+0700 W/AUL     (  923): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2140) type(uiapp)
04-30 13:51:01.549+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [UNINSTALL, STARTED]
04-30 13:51:01.549+0700 W/AUL_AMD (  923): amd_launch.c: __reply_handler(999) > listen fd(22) , send fd(17), pid(2140), cmd(4)
04-30 13:51:01.549+0700 I/APP_CORE( 2140): appcore-efl.c: __do_app(453) > [APP 2140] Event: TERMINATE State: RUNNING
04-30 13:51:01.549+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 22
04-30 13:51:01.549+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(999) > app status : 4
04-30 13:51:01.549+0700 W/APP_CORE( 2140): appcore-efl.c: appcore_efl_main(1788) > power off : 0
04-30 13:51:01.549+0700 W/APP_CORE( 2140): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3200002] -> redirected win[60067d] for com.toyota.realtimefeedback[2140]
04-30 13:51:01.559+0700 W/AUL     ( 2312): launch.c: app_request_to_launchpad(298) > request cmd(5) result(0)
04-30 13:51:01.559+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:01.569+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-30 13:51:01.579+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2140
04-30 13:51:01.659+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-30 13:51:01.659+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 30-4-2018, 06:55:03 (UTC).
04-30 13:51:01.659+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-30 13:51:01.669+0700 I/APP_CORE( 2140): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-30 13:51:01.669+0700 I/CAPI_APPFW_APPLICATION( 2140): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
04-30 13:51:01.679+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-30 13:51:01.689+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:01.689+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1243] goes to (3)
04-30 13:51:01.689+0700 E/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1243)'s state(3)
04-30 13:51:01.699+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2140
04-30 13:51:01.699+0700 W/AUL_AMD (  923): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-30 13:51:01.699+0700 W/AUL_AMD (  923): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-30 13:51:01.699+0700 W/AUL     (  923): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1243) status(fg) type(uiapp)
04-30 13:51:01.699+0700 I/efl-extension( 2140): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 13:51:01.719+0700 I/efl-extension( 2140): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb8548780 is freed
04-30 13:51:01.719+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb856bdb0 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 13:51:01.719+0700 I/efl-extension( 2140): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb856c7f8, obj: 0xb856c7f8
04-30 13:51:01.719+0700 I/efl-extension( 2140): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 13:51:01.719+0700 E/EFL     ( 2140): elementary<2140> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 13:51:01.719+0700 E/EFL     ( 2140): elementary<2140> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 13:51:01.729+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb856c7f8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 13:51:01.729+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb856c7f8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 13:51:01.729+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb856c7f8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 13:51:01.729+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb856c7f8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 13:51:01.729+0700 I/efl-extension( 2140): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 13:51:01.729+0700 I/efl-extension( 2140): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb858dc90 is freed
04-30 13:51:01.729+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 13:51:01.729+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb856c7f8, elm_genlist, func : 0xb57f9ea1
04-30 13:51:01.729+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb856bdb0 in function: elm_grid_clear, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb856bdb0 in function: elm_grid_size_set, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 13:51:01.739+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb856c218, , _activated_obj : 0xb86827b8, activated : 1
04-30 13:51:01.739+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb85cfe78 in function: elm_label_line_wrap_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb85cfe78 in function: elm_label_wrap_width_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb85cfe78 in function: elm_label_ellipsis_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb856bdb0 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb856bdb0 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb856bdb0 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 13:51:01.739+0700 E/EFL     ( 2140): elementary<2140> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb856bdb0 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 13:51:01.739+0700 I/efl-extension( 2140): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb3e21490, obj: 0xb3e21490
04-30 13:51:01.739+0700 I/efl-extension( 2140): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 13:51:01.749+0700 E/EFL     ( 2140): elementary<2140> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 13:51:01.749+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb3e21490 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 13:51:01.749+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb3e21490 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 13:51:01.749+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb3e21490 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 13:51:01.749+0700 E/EFL     ( 2140): elementary<2140> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb3e21490 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 13:51:01.749+0700 I/efl-extension( 2140): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 13:51:01.749+0700 I/efl-extension( 2140): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb3e46bc8 is freed
04-30 13:51:01.749+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 13:51:01.749+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb3e21490, elm_genlist, func : 0xb57f9ea1
04-30 13:51:01.749+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 13:51:01.759+0700 I/APP_CORE( 2140): appcore-efl.c: __after_loop(1243) > [APP 2140] After terminate() callback
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb861e3f8 is freed
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb868d780, elm_scroller, func : 0xb57fd379
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb86827b8, elm_image, func : 0xb57fd379
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 13:51:01.759+0700 I/efl-extension( 2140): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb868d780 : elm_scroller] rotary callabck is deleted
04-30 13:51:01.799+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:01.809+0700 W/PROCESSMGR(  883): e_mod_processmgr.c: _e_mod_processmgr_send_update_watch_action(663) > [PROCESSMGR] =====================> send UpdateClock
04-30 13:51:01.809+0700 W/W_HOME  ( 1243): event_manager.c: _ecore_x_message_cb(421) > state: 1 -> 0
04-30 13:51:01.809+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:01.809+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:01.809+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:01.809+0700 W/W_HOME  ( 1243): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 1
04-30 13:51:01.809+0700 W/WATCH_CORE( 1307): appcore-watch.c: __signal_process_manager_handler(1269) > process_manager_signal
04-30 13:51:01.809+0700 I/WATCH_CORE( 1307): appcore-watch.c: __signal_process_manager_handler(1285) > Call the time_tick_cb
04-30 13:51:01.809+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:01.809+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:01.809+0700 I/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-30 13:51:01.809+0700 I/watchface-viewer( 1307): viewer-part-resource-evas.cpp: CreateTextImage(1028) > style[DEFAULT='font=Default font_size=40 color=#FFFFFF ellipsis=-1 align=center ']
04-30 13:51:01.809+0700 I/watchface-viewer( 1307): viewer-part-resource-evas.cpp: CreateTextImage(1042) > formatted size 42x40
04-30 13:51:01.809+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2140
04-30 13:51:01.809+0700 I/watchface-viewer( 1307): viewer-part-resource-evas.cpp: CreateTextImage(1028) > style[DEFAULT='font=Default font_size=40 color=#FFFFFF ellipsis=-1 align=center ']
04-30 13:51:01.809+0700 I/watchface-viewer( 1307): viewer-part-resource-evas.cpp: CreateTextImage(1042) > formatted size 42x40
04-30 13:51:01.809+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 13:51:01.809+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 13:51:01.819+0700 I/watchface-viewer( 1307): viewer-image-file-loader.cpp: OnImageLoadingDoneIdlerCb(792) > ImagesLoadingDoneSignal().Emit()
04-30 13:51:01.829+0700 W/W_HOME  ( 1243): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(0)
04-30 13:51:01.829+0700 W/W_HOME  ( 1243): event_manager.c: _window_visibility_cb(470) > state: 0 -> 1
04-30 13:51:01.829+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:01.829+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:6, app_state:2 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:01.829+0700 W/W_HOME  ( 1243): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(0)
04-30 13:51:01.829+0700 I/APP_CORE( 1243): appcore-efl.c: __do_app(453) > [APP 1243] Event: RESUME State: PAUSED
04-30 13:51:01.829+0700 I/CAPI_APPFW_APPLICATION( 1243): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-30 13:51:01.829+0700 W/W_HOME  ( 1243): main.c: _appcore_resume_cb(480) > appcore resume
04-30 13:51:01.829+0700 W/W_HOME  ( 1243): event_manager.c: _app_resume_cb(373) > state: 2 -> 1
04-30 13:51:01.829+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 1
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_register(581) > [windicator_battery_vconfkey_register:581] Set battery cb
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(89), length(2)
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 89%
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 89, isCharging: 0
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_90
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 90
04-30 13:51:01.839+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
04-30 13:51:01.839+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:01.839+0700 W/W_HOME  ( 1243): main.c: home_resume(528) > journal_multimedia_screen_loaded_home called
04-30 13:51:01.839+0700 W/W_HOME  ( 1243): main.c: home_resume(532) > clock/widget resumed
04-30 13:51:01.839+0700 E/W_HOME  ( 1243): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
04-30 13:51:01.839+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:01.839+0700 I/GATE    ( 1243): <GATE-M>APP_FULLY_LOADED_w-home</GATE-M>
04-30 13:51:01.849+0700 I/wnotib  ( 1243): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 0
04-30 13:51:01.849+0700 E/wnotib  ( 1243): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-30 13:51:01.849+0700 W/wnotib  ( 1243): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(958) > Do the postponed update job with is_for_VI: 0, postponed_for_VI: 0.
04-30 13:51:01.849+0700 W/wnotib  ( 1243): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(444) > idler for type: 0
04-30 13:51:01.859+0700 E/wnoti-service( 1375): wnoti-db-client.c: _wnoti_get_categories_info(206) > _query_step failed(not SQLITE_ROW)
04-30 13:51:01.859+0700 E/wnoti-proxy( 1243): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-30 13:51:01.859+0700 W/wnotib  ( 1243): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(732) > No categories available. Num old_notifications: 1
04-30 13:51:01.859+0700 W/wnotib  ( 1243): w-notification-board-noti-manager.c: _wnb_nm_remove_card(419) > db_id: 1378, application_id: -1016, application_name: Real Feed Back
04-30 13:51:01.859+0700 I/wnotib  ( 1243): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 150, application_id: -1016, type: 2
04-30 13:51:01.859+0700 I/wnotib  ( 1243): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3278) > db_id: 1378, is_request_from_noti_service: 1
04-30 13:51:01.859+0700 I/wnotib  ( 1243): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3432) > We don't need to delete the item for second_depth_type: 0.
04-30 13:51:01.859+0700 E/EFL     ( 1243): elementary<1243> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 13:51:01.859+0700 I/wnotib  ( 1243): w-notification-board-basic-panel.c: _wnb_bpanel_coverview_gl_item_del(4095) > card[0xb810c220]
04-30 13:51:01.859+0700 I/wnotib  ( 1243): w-notification-board-basic-panel.c: _wnb_bp_destroy(5758) > Destory panel, panel application_id [-1016], panel category_id [150]
04-30 13:51:01.869+0700 E/EFL     ( 1243): elementary<1243> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb86817e0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 13:51:01.869+0700 E/EFL     ( 1243): elementary<1243> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb86817e0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 13:51:01.869+0700 E/EFL     ( 1243): elementary<1243> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb86817e0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 13:51:01.869+0700 E/EFL     ( 1243): elementary<1243> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb86817e0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 13:51:01.899+0700 W/W_HOME  ( 1243): noti_broker.c: _handler_indicator_update(560) > 0x1
04-30 13:51:01.909+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:01.919+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2140
04-30 13:51:01.969+0700 W/W_HOME  ( 1243): noti_broker.c: _handler_indicator_select(586) > 0
04-30 13:51:01.969+0700 W/W_HOME  ( 1243): noti_broker.c: _handler_indicator_select(596) > select index:1
04-30 13:51:01.999+0700 I/wnotib  ( 1243): w-notification-board-basic-panel.c: _wnb_bp_destroy(5920) > Destory panel done.
04-30 13:51:01.999+0700 W/wnotib  ( 1243): w-notification-board-panel-manager.c: wnb_pm_destroy_panel(330) > page_instance 150, 0xb81ca4a0 is destoryed!
04-30 13:51:01.999+0700 I/wnotib  ( 1243): w-notification-board-noti-manager.c: _wnb_nm_free_data(248) > Free noti manager data.
04-30 13:51:01.999+0700 I/wnotib  ( 1243): w-notification-board-noti-manager.c: _wnb_nm_free_data(253) > Free previous notifications.
04-30 13:51:01.999+0700 I/wnotib  ( 1243): w-notification-board-noti-manager.c: _wnb_nm_free_data(274) > Free previous categories.
04-30 13:51:01.999+0700 I/wnotib  ( 1243): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(747) > before_rpanel_count: 2, after_rpanel_count: 0.
04-30 13:51:01.999+0700 I/wnotib  ( 1243): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(60) > type: 0
04-30 13:51:01.999+0700 W/wnotib  ( 1243): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(79) > uncleared_category_count: 0, setting_power_saving_mode: 0
04-30 13:51:01.999+0700 W/wnotib  ( 1243): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(82) > Put empty view into panel body
04-30 13:51:01.999+0700 W/W_HOME  ( 1243): noti_broker.c: _handler_indicator_select(586) > 0
04-30 13:51:01.999+0700 W/W_HOME  ( 1243): noti_broker.c: _handler_indicator_select(596) > select index:1
04-30 13:51:01.999+0700 I/wnotib  ( 1243): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(99) > is_notification_enabled: 1, blocking mode: 0, is_connected_vendor_LO: 0
04-30 13:51:01.999+0700 I/wnotib  ( 1243): w-notification-board-empty-panel.c: _wnb_ep_set_texts(348) > is_connected_vendor_LO: 0, is_standalone_mode: 0
04-30 13:51:01.999+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1243): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-30 13:51:01.999+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1243): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-30 13:51:01.999+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1243): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-30 13:51:01.999+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1243): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-30 13:51:02.019+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:02.029+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2140
04-30 13:51:02.079+0700 W/W_HOME  ( 1243): noti_broker.c: _handler_indicator_update(560) > 0x1
04-30 13:51:02.139+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:02.149+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2140
04-30 13:51:02.169+0700 W/W_HOME  ( 1243): event_manager.c: _clock_view_obscured_cb(645) > state: 1 -> 0
04-30 13:51:02.169+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:02.169+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 13:51:02.169+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 13:51:02.179+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:02.189+0700 E/EFL     ( 1243): elementary<1243> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7fb7498 : elm_scroller] mx(2880), my(0), minx(0), miny(0), px(720), py(0)
04-30 13:51:02.189+0700 E/EFL     ( 1243): elementary<1243> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7fb7498 : elm_scroller] cw(3240), ch(360), pw(360), ph(360)
04-30 13:51:02.189+0700 E/EFL     ( 1243): elementary<1243> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7fb7498 : elm_scroller] x(360), y(0), nx(360), px(720), ny(0) py(0)
04-30 13:51:02.189+0700 E/EFL     ( 1243): elementary<1243> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb7fb7498 : elm_scroller] x(360), y(0)
04-30 13:51:02.189+0700 W/W_HOME  ( 1243): page.c: check_expired_widget_suggestion_page(1149) > check_expired_widget_suggestion_page
04-30 13:51:02.189+0700 E/W_HOME  ( 1243): page.c: check_expired_widget_suggestion_page(1157) > (!scroller_info->suggestion_page) -> check_expired_widget_suggestion_page() return
04-30 13:51:02.189+0700 W/W_HOME  ( 1243): event_manager.c: _clock_view_visible_cb(631) > state: 0 -> 1
04-30 13:51:02.189+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 1
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_register(581) > [windicator_battery_vconfkey_register:581] Set battery cb
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(89), length(2)
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 89%
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 89, isCharging: 0
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_90
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 90
04-30 13:51:02.189+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
04-30 13:51:02.199+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:02.249+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:02.249+0700 W/WATCH_CORE( 1307): appcore-watch.c: __widget_resume(1124) > widget_resume
04-30 13:51:02.249+0700 W/AUL     ( 1307): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1307) status(fg) type(watchapp)
04-30 13:51:02.249+0700 E/watchface-app( 1307): watchface.cpp: OnAppResume(1136) > 
04-30 13:51:02.249+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:02.249+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:02.259+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2140
04-30 13:51:02.269+0700 W/W_HOME  ( 1243): noti_broker.c: _handler_indicator_update(560) > 0x1
04-30 13:51:02.339+0700 W/WATCH_CORE( 1307): appcore-watch.c: __widget_pause(1113) > widget_pause
04-30 13:51:02.339+0700 W/AUL     ( 1307): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1307) status(bg) type(watchapp)
04-30 13:51:02.339+0700 E/watchface-app( 1307): watchface.cpp: OnAppPause(1122) > 
04-30 13:51:02.349+0700 I/GATE    ( 1243): <GATE-M>SCREEN_LOADED_HOME</GATE-M>
04-30 13:51:02.349+0700 W/WATCH_CORE( 1307): appcore-watch.c: __widget_resume(1124) > widget_resume
04-30 13:51:02.349+0700 W/AUL     ( 1307): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1307) status(fg) type(watchapp)
04-30 13:51:02.349+0700 E/watchface-app( 1307): watchface.cpp: OnAppResume(1136) > 
04-30 13:51:02.349+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:02.349+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:02.359+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:02.369+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-30 13:51:02.449+0700 E/PKGMGR_PARSER( 2312): pkgmgr_parser_signature.c: __ps_check_mdm_policy_by_pkgid(1056) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
04-30 13:51:02.529+0700 I/PRIVACY-MANAGER-CLIENT( 2312): SocketClient.cpp: SocketClient(37) > Client created
04-30 13:51:02.529+0700 I/PRIVACY-MANAGER-CLIENT( 2312): SocketStream.h: SocketStream(31) > Created
04-30 13:51:02.529+0700 I/PRIVACY-MANAGER-CLIENT( 2312): SocketConnection.h: SocketConnection(44) > Created
04-30 13:51:02.529+0700 I/PRIVACY-MANAGER-CLIENT( 2312): SocketClient.cpp: connect(62) > Client connected
04-30 13:51:02.539+0700 I/PRIVACY-MANAGER-SERVER(  921): SocketService.cpp: mainloop(227) > Got incoming connection
04-30 13:51:02.539+0700 I/PRIVACY-MANAGER-SERVER(  921): SocketService.cpp: connectionThread(253) > Starting connection thread
04-30 13:51:02.539+0700 I/PRIVACY-MANAGER-SERVER(  921): SocketStream.h: SocketStream(31) > Created
04-30 13:51:02.539+0700 I/PRIVACY-MANAGER-SERVER(  921): SocketConnection.h: SocketConnection(44) > Created
04-30 13:51:02.539+0700 I/PRIVACY-MANAGER-SERVER(  921): SocketService.cpp: connectionService(304) > Calling service
04-30 13:51:02.549+0700 I/PRIVACY-MANAGER-SERVER(  921): SocketService.cpp: connectionService(307) > Removing client
04-30 13:51:02.549+0700 I/PRIVACY-MANAGER-SERVER(  921): SocketService.cpp: connectionService(311) > Call served
04-30 13:51:02.549+0700 I/PRIVACY-MANAGER-SERVER(  921): SocketService.cpp: connectionThread(262) > Client serviced
04-30 13:51:02.549+0700 I/PRIVACY-MANAGER-CLIENT( 2312): SocketClient.cpp: disconnect(72) > Client disconnected
04-30 13:51:02.569+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2140 pgid = 2140
04-30 13:51:02.569+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 11
04-30 13:51:02.629+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(197) > after __sigchild_action
04-30 13:51:02.629+0700 I/AUL_AMD (  923): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2140
04-30 13:51:02.629+0700 W/AUL     (  923): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2140)
04-30 13:51:02.739+0700 W/CRASH_MANAGER( 2317): worker.c: worker_job(1205) > 1102140726561152507106
