S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2989
Date: 2018-05-03 10:21:52+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: 1
      address not mapped to object
      si_addr = 0x28

Register Information
r0   = 0xb592803c, r1   = 0x00000000
r2   = 0x00000000, r3   = 0x00000000
r4   = 0xb8e3d098, r5   = 0x00000000
r6   = 0xb8e4d020, r7   = 0xbefad210
r8   = 0x00000000, r9   = 0xb8e4d020
r10  = 0xbefad2a8, fp   = 0x00000000
ip   = 0xb8d98190, sp   = 0xbefad190
lr   = 0xb5913271, pc   = 0xb59136f8
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:     40696 KB
Buffers:      4608 KB
Cached:      87652 KB
VmPeak:      55948 KB
VmSize:      53784 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       19032 KB
VmRSS:       19032 KB
VmData:      24052 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       20060 KB
VmPTE:          48 KB
VmSwap:          0 KB

Threads Information
Threads: 2
PID = 2989 TID = 2989
2989 3064 

Maps Information
b3c90000 b3d17000 rw-s anon_inode:dmabuf
b3d17000 b3d9e000 rw-s anon_inode:dmabuf
b3e79000 b3f00000 rw-s anon_inode:dmabuf
b403b000 b40c2000 rw-s anon_inode:dmabuf
b4a66000 b4a67000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b6e000 b536d000 rw-p [stack:3064]
b536d000 b536f000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b5377000 b538e000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b539b000 b539d000 r-xp /usr/lib/libdri2.so.0.0.0
b53a5000 b53b0000 r-xp /usr/lib/libtbm.so.1.0.0
b53b8000 b53c0000 r-xp /usr/lib/libdrm.so.2.4.0
b53c8000 b53ca000 r-xp /usr/lib/libgenlock.so
b53d2000 b53d7000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b53df000 b53ea000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b55f3000 b56bd000 r-xp /usr/lib/libCOREGL.so.4.0
b56ce000 b56de000 r-xp /usr/lib/libmdm-common.so.1.1.25
b56e6000 b56ec000 r-xp /usr/lib/libxcb-render.so.0.0.0
b56f4000 b56f5000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b56fe000 b5701000 r-xp /usr/lib/libEGL.so.1.4
b5709000 b5717000 r-xp /usr/lib/libGLESv2.so.2.0
b5720000 b5769000 r-xp /usr/lib/libmdm.so.1.2.70
b5772000 b5778000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5780000 b5789000 r-xp /usr/lib/libcom-core.so.0.0.1
b5792000 b584a000 r-xp /usr/lib/libcairo.so.2.11200.14
b5855000 b586e000 r-xp /usr/lib/libnetwork.so.0.0.0
b5876000 b5882000 r-xp /usr/lib/libnotification.so.0.1.0
b588b000 b589a000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b58a3000 b58c4000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58cc000 b58d1000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58d9000 b58de000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b58e6000 b58f6000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b58fe000 b5906000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b590e000 b5918000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5abd000 b5ac7000 r-xp /lib/libnss_files-2.13.so
b5ad0000 b5b9f000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5bb5000 b5bd9000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5be2000 b5be8000 r-xp /usr/lib/libappsvc.so.0.1.0
b5bf0000 b5bf4000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5c01000 b5c0c000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5c14000 b5c16000 r-xp /usr/lib/libiniparser.so.0
b5c1f000 b5c24000 r-xp /usr/lib/libappcore-common.so.1.1
b5c2c000 b5c2e000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c37000 b5c3b000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c48000 b5c4a000 r-xp /usr/lib/libXau.so.6.0.0
b5c52000 b5c59000 r-xp /lib/libcrypt-2.13.so
b5c89000 b5c8b000 r-xp /usr/lib/libiri.so
b5c94000 b5e26000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e47000 b5e8e000 r-xp /usr/lib/libssl.so.1.0.0
b5e9a000 b5ec8000 r-xp /usr/lib/libidn.so.11.5.44
b5ed0000 b5ed9000 r-xp /usr/lib/libcares.so.2.1.0
b5ee3000 b5ef6000 r-xp /usr/lib/libxcb.so.1.1.0
b5eff000 b5f02000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5f0a000 b5f0c000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5f15000 b5fe1000 r-xp /usr/lib/libxml2.so.2.7.8
b5fee000 b5ff0000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5ff9000 b5ffe000 r-xp /usr/lib/libffi.so.5.0.10
b6006000 b6007000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b600f000 b6012000 r-xp /lib/libattr.so.1.1.0
b601a000 b60ae000 r-xp /usr/lib/libstdc++.so.6.0.16
b60c1000 b60de000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b60e8000 b6100000 r-xp /usr/lib/libpng12.so.0.50.0
b6108000 b611e000 r-xp /lib/libexpat.so.1.6.0
b6128000 b616c000 r-xp /usr/lib/libcurl.so.4.3.0
b6175000 b617f000 r-xp /usr/lib/libXext.so.6.4.0
b6189000 b618d000 r-xp /usr/lib/libXtst.so.6.1.0
b6195000 b619b000 r-xp /usr/lib/libXrender.so.1.3.0
b61a3000 b61a9000 r-xp /usr/lib/libXrandr.so.2.2.0
b61b1000 b61b2000 r-xp /usr/lib/libXinerama.so.1.0.0
b61bb000 b61c4000 r-xp /usr/lib/libXi.so.6.1.0
b61cc000 b61cf000 r-xp /usr/lib/libXfixes.so.3.1.0
b61d8000 b61da000 r-xp /usr/lib/libXgesture.so.7.0.0
b61e2000 b61e4000 r-xp /usr/lib/libXcomposite.so.1.0.0
b61ec000 b61ee000 r-xp /usr/lib/libXdamage.so.1.1.0
b61f6000 b61fd000 r-xp /usr/lib/libXcursor.so.1.0.2
b6205000 b6208000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6211000 b6215000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b621e000 b6223000 r-xp /usr/lib/libecore_fb.so.1.7.99
b622c000 b630d000 r-xp /usr/lib/libX11.so.6.3.0
b6318000 b633b000 r-xp /usr/lib/libjpeg.so.8.0.2
b6353000 b6369000 r-xp /lib/libz.so.1.2.5
b6372000 b6374000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b637c000 b63f1000 r-xp /usr/lib/libsqlite3.so.0.8.6
b63fb000 b6415000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b641d000 b6451000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b645a000 b652d000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6539000 b6549000 r-xp /lib/libresolv-2.13.so
b654d000 b6565000 r-xp /usr/lib/liblzma.so.5.0.3
b656d000 b6570000 r-xp /lib/libcap.so.2.21
b6578000 b65a7000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b65af000 b65b0000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b65b9000 b65bf000 r-xp /usr/lib/libecore_imf.so.1.7.99
b65c7000 b65de000 r-xp /usr/lib/liblua-5.1.so
b65e7000 b65ee000 r-xp /usr/lib/libembryo.so.1.7.99
b65f6000 b65fc000 r-xp /lib/librt-2.13.so
b6605000 b665b000 r-xp /usr/lib/libpixman-1.so.0.28.2
b6669000 b66bf000 r-xp /usr/lib/libfreetype.so.6.11.3
b66cb000 b66f3000 r-xp /usr/lib/libfontconfig.so.1.8.0
b66f4000 b6739000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6742000 b6755000 r-xp /usr/lib/libfribidi.so.0.3.1
b675d000 b6777000 r-xp /usr/lib/libecore_con.so.1.7.99
b6781000 b678a000 r-xp /usr/lib/libedbus.so.1.7.99
b6792000 b67e2000 r-xp /usr/lib/libecore_x.so.1.7.99
b67e4000 b67ed000 r-xp /usr/lib/libvconf.so.0.2.45
b67f5000 b6806000 r-xp /usr/lib/libecore_input.so.1.7.99
b680e000 b6813000 r-xp /usr/lib/libecore_file.so.1.7.99
b681b000 b683d000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6846000 b6887000 r-xp /usr/lib/libeina.so.1.7.99
b6890000 b68a9000 r-xp /usr/lib/libeet.so.1.7.99
b68ba000 b6923000 r-xp /lib/libm-2.13.so
b692c000 b6932000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b693b000 b693c000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6944000 b6967000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b696f000 b6974000 r-xp /usr/lib/libxdgmime.so.1.1.0
b697c000 b69a6000 r-xp /usr/lib/libdbus-1.so.3.8.12
b69af000 b69c6000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69ce000 b69d9000 r-xp /lib/libunwind.so.8.0.1
b6a06000 b6a24000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a2e000 b6b52000 r-xp /lib/libc-2.13.so
b6b60000 b6b68000 r-xp /lib/libgcc_s-4.6.so.1
b6b69000 b6b6d000 r-xp /usr/lib/libsmack.so.1.0.0
b6b76000 b6b7c000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b84000 b6c54000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c55000 b6cb3000 r-xp /usr/lib/libedje.so.1.7.99
b6cbd000 b6cd4000 r-xp /usr/lib/libecore.so.1.7.99
b6ceb000 b6db9000 r-xp /usr/lib/libevas.so.1.7.99
b6ddf000 b6f1b000 r-xp /usr/lib/libelementary.so.1.7.99
b6f32000 b6f46000 r-xp /lib/libpthread-2.13.so
b6f51000 b6f53000 r-xp /usr/lib/libdlog.so.0.0.0
b6f5b000 b6f5e000 r-xp /usr/lib/libbundle.so.0.1.22
b6f66000 b6f68000 r-xp /lib/libdl-2.13.so
b6f71000 b6f7e000 r-xp /usr/lib/libaul.so.0.1.0
b6f90000 b6f96000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f9f000 b6fa3000 r-xp /usr/lib/libsys-assert.so
b6fac000 b6fc9000 r-xp /lib/ld-2.13.so
b6fd2000 b6fd7000 r-xp /usr/bin/launchpad-loader
b8c92000 b8eb0000 rw-p [heap]
bef8d000 befae000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2989)
Call Stack Count: 16
 0: loadAllBodyDefect + 0xf (0xb59136f8) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x56f8
 1: nf_hw_back_cb + 0x20 (0xb5913271) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5271
 2: (0xb58aaf4d) [/usr/lib/libefl-extension.so.0] + 0x7f4d
 3: (0xb6d02af9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_event_feed_key_up + 0x4cc (0xb6d0ad81) [/usr/lib/libevas.so.1] + 0x1fd81
 5: (0xb6206c9d) [/usr/lib/libecore_input_evas.so.1] + 0x1c9d
 6: (0xb6cc5e53) [/usr/lib/libecore.so.1] + 0x8e53
 7: (0xb6cc946b) [/usr/lib/libecore.so.1] + 0xc46b
 8: ecore_main_loop_begin + 0x30 (0xb6cc9879) [/usr/lib/libecore.so.1] + 0xc879
 9: appcore_efl_main + 0x332 (0xb6f93b47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
10: ui_app_main + 0xb0 (0xb5c38ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
11: uib_app_run + 0xea (0xb59133cf) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53cf
12: main + 0x34 (0xb5913a9d) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5a9d
13: (0xb6fd3a53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
14: __libc_start_main + 0x114 (0xb6a4585c) [/lib/libc.so.6] + 0x1785c
15: (0xb6fd3e0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
ance_get(860) > editing:0 clocklist_state:0 addviewer:0 scrolling:0 apptray-state:0 apptray-visibility:1 apptray-edit_visibility:0
05-03 10:21:37.109+0700 E/EFL     ( 1192): ecore_x<1192> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=1154188 button=1
05-03 10:21:37.109+0700 W/APPS    ( 1192): AppsViewNecklace.cpp: touchReleased(1976) >  TOUCH [173, 211]->[173, 212]
05-03 10:21:37.119+0700 W/APPS    ( 1192): AppsViewNecklace.cpp: onBubbleButtonEffect(2446) >  [249, 249, 249, 255]
05-03 10:21:37.119+0700 W/APPS    ( 1192): AppsItem.cpp: onItemClicked(480) >  onItemClicked[0,27]
05-03 10:21:37.119+0700 E/APPS    ( 1192): effect.c: apps_effect_play_sound(86) >  effect_info.sound_status: [0]
05-03 10:21:37.119+0700 W/APPS    ( 1192): AppsItem.cpp: onItemClicked(504) >  item(Real Feed Back) launched, open(0), tts(0)
05-03 10:21:37.119+0700 W/AUL     ( 1192): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.toyota.realtimefeedback)
05-03 10:21:37.119+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 0
05-03 10:21:37.119+0700 W/AUL_AMD (  971): amd_launch.c: _start_app(1782) > caller pid : 1192
05-03 10:21:37.119+0700 I/AUL_AMD (  971): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
05-03 10:21:37.139+0700 W/AUL_AMD (  971): amd_launch.c: _start_app(2218) > pad pid(-5)
05-03 10:21:37.139+0700 E/RESOURCED( 1100): block.c: block_prelaunch_state(138) > insert data com.toyota.realtimefeedback, table num : 3
05-03 10:21:37.139+0700 W/AUL_PAD ( 1882): launchpad.c: __launchpad_main_loop(611) > Launch on type-based process-pool
05-03 10:21:37.139+0700 W/AUL_PAD ( 1882): launchpad.c: __send_result_to_caller(272) > Check app launching
05-03 10:21:37.139+0700 W/AUL_PAD ( 2989): launchpad_loader.c: __candidate_process_prepare_exec(259) > [candidate] before __set_access
05-03 10:21:37.139+0700 W/AUL_PAD ( 2989): launchpad_loader.c: __candidate_process_prepare_exec(264) > [candidate] after __set_access
05-03 10:21:37.139+0700 W/AUL_PAD ( 2989): launchpad_loader.c: __candidate_process_launchpad_main_loop(414) > update argument
05-03 10:21:37.139+0700 W/AUL_PAD ( 2989): launchpad_loader.c: main(680) > [candidate] dlopen(com.toyota.realtimefeedback)
05-03 10:21:37.169+0700 I/efl-extension( 2989): efl_extension.c: eext_mod_init(40) > Init
05-03 10:21:37.179+0700 W/AUL_PAD ( 2989): launchpad_loader.c: main(690) > [candidate] dlsym
05-03 10:21:37.179+0700 W/AUL_PAD ( 2989): launchpad_loader.c: main(694) > [candidate] dl_main(com.toyota.realtimefeedback)
05-03 10:21:37.179+0700 I/CAPI_APPFW_APPLICATION( 2989): app_main.c: ui_app_main(704) > app_efl_main
05-03 10:21:37.189+0700 I/CAPI_APPFW_APPLICATION( 2989): app_main.c: _ui_app_appcore_create(563) > app_appcore_create
05-03 10:21:37.239+0700 W/AUL     (  971): app_signal.c: aul_send_app_launch_request_signal(521) > aul_send_app_launch_request_signal app(com.toyota.realtimefeedback) pid(2989) type(uiapp) bg(0)
05-03 10:21:37.239+0700 W/AUL_AMD (  971): amd_status.c: __socket_monitor_cb(1277) > (2989) was created
05-03 10:21:37.239+0700 E/AUL     (  971): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
05-03 10:21:37.239+0700 W/AUL     ( 1192): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2989)
05-03 10:21:37.239+0700 W/W_HOME  ( 1192): util.c: apps_util_launch_main_operation(785) > Launch app:[Real Feed Back] ret:[0]
05-03 10:21:37.239+0700 W/W_HOME  ( 1192): util.c: send_launch_appId(620) > launch appid[com.toyota.realtimefeedback]
05-03 10:21:37.239+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 10:21:37.239+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 10:21:37.239+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 10:21:37.239+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 10:21:37.239+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 10:21:37.239+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 10:21:37.239+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.app-shortcut-widget:Apptray.Message.Launch.AppId]
05-03 10:21:37.239+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: IsPreloaded(395) > _MessagePortService::IsPreloaded
05-03 10:21:37.239+0700 W/STARTER ( 1119): pkg-monitor.c: _app_mgr_status_cb(400) > [_app_mgr_status_cb:400] Launch request [2989]
05-03 10:21:37.289+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 10:21:37.299+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 10:21:37.299+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 10:21:37.299+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 10:21:37.299+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 10:21:37.299+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.app-shortcut-widget:Apptray.Message.Launch.AppId]
05-03 10:21:37.299+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 10:21:37.299+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 10:21:37.299+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 10:21:37.299+0700 W/APPS    ( 1192): AppsItem.cpp: onItemClicked(538) >  appid(com.toyota.realtimefeedback)  pkgid(com.toyota.realtimefeedback), name(Real Feed Back)
05-03 10:21:37.419+0700 E/EFL     ( 2989): ecore_evas<2989> ecore_evas_extn.c:2204 ecore_evas_extn_plug_connect() Extn plug failed to connect:ipctype=0, svcname=elm_indicator_portrait, svcnum=0, svcsys=0
05-03 10:21:37.449+0700 I/efl-extension( 2989): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
05-03 10:21:37.449+0700 E/EFL     ( 2989): elementary<2989> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8dc0148 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 10:21:37.449+0700 E/EFL     ( 2989): elementary<2989> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8dc0148 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 10:21:37.449+0700 E/EFL     ( 2989): elementary<2989> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8dc0148 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 10:21:37.459+0700 E/EFL     ( 2989): elementary<2989> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8dc0148 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 10:21:37.459+0700 E/EFL     ( 2989): elementary<2989> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8dc0148 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 10:21:37.459+0700 I/efl-extension( 2989): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8dc05b0, elm_image, _activated_obj : 0x0, activated : 1
05-03 10:21:37.469+0700 W/W_HOME  ( 1192): event_manager.c: _ecore_x_message_cb(421) > state: 0 -> 1
05-03 10:21:37.469+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 10:21:37.469+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 10:21:37.469+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 10:21:37.469+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 10:21:37.469+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 10:21:37.469+0700 W/W_HOME  ( 1192): win.c: win_back_gesture_disable_set(170) > disable back gesture
05-03 10:21:37.469+0700 W/W_HOME  ( 1192): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 0
05-03 10:21:37.509+0700 E/APP_SHORTCUT_WIDGET( 1491): main.c: _setWidgetContentInfo(1582) >  0xb7df9298, org.example.uibuildernavigationview|2|0 com.samsung.clocksetting|15|0 com.samsung.message|14|0 PkGV3tmZSh.CNN|13|0 com.samsung.gearstore|12|0 com.toyota.realtimefeedback|5|0, 5
05-03 10:21:37.509+0700 I/CAPI_WIDGET_APPLICATION( 1491): widget_app.c: __provider_update_cb(281) > received updating signal
05-03 10:21:37.589+0700 I/efl-extension( 2989): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
05-03 10:21:37.589+0700 I/efl-extension( 2989): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
05-03 10:21:37.589+0700 I/efl-extension( 2989): efl_extension_rotary.c: eext_rotary_event_handler_add(77) > init_count: 0
05-03 10:21:37.589+0700 I/efl-extension( 2989): efl_extension_rotary.c: _init_Xi2_system(314) > In
05-03 10:21:37.599+0700 I/efl-extension( 2989): efl_extension_rotary.c: _init_Xi2_system(375) > Done
05-03 10:21:37.599+0700 I/efl-extension( 2989): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8dcf7c0, elm_image, _activated_obj : 0xb8dc05b0, activated : 1
05-03 10:21:37.599+0700 I/efl-extension( 2989): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
05-03 10:21:37.629+0700 E/EFL     ( 2989): elementary<2989> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8dc05b0 in function: elm_progressbar_pulse_set, of type: 'elm_image' when expecting type: 'elm_progressbar'
05-03 10:21:37.629+0700 E/EFL     ( 2989): elementary<2989> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8dc05b0 in function: elm_progressbar_pulse, of type: 'elm_image' when expecting type: 'elm_progressbar'
05-03 10:21:37.669+0700 E/EFL     ( 2989): elementary<2989> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
05-03 10:21:37.689+0700 E/EFL     ( 2989): elementary<2989> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb8dd0380) will be pushed
05-03 10:21:37.709+0700 I/APP_CORE( 2989): appcore-efl.c: __do_app(453) > [APP 2989] Event: RESET State: CREATED
05-03 10:21:37.709+0700 I/CAPI_APPFW_APPLICATION( 2989): app_main.c: _ui_app_appcore_reset(645) > app_appcore_reset
05-03 10:21:37.729+0700 I/APP_CORE( 2989): appcore-efl.c: __do_app(522) > Legacy lifecycle: 0
05-03 10:21:37.729+0700 I/APP_CORE( 2989): appcore-efl.c: __do_app(524) > [APP 2989] Initial Launching, call the resume_cb
05-03 10:21:37.729+0700 I/CAPI_APPFW_APPLICATION( 2989): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
05-03 10:21:37.739+0700 W/APP_CORE( 2989): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:4000002
05-03 10:21:37.739+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8dc0b90 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:21:37.739+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8dc0b90 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:21:37.749+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8dc0b90 : elm_genlist] mx(99999639), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:21:37.749+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8dc0b90 : elm_genlist] cw(99999999), ch(0), pw(360), ph(360)
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): event_manager.c: _window_visibility_cb(460) > Window [0x2000003] is now visible(1)
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): event_manager.c: _window_visibility_cb(470) > state: 1 -> 0
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:6, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): main.c: _window_visibility_cb(996) > Window [0x2000003] is now visible(1)
05-03 10:21:37.789+0700 I/APP_CORE( 1192): appcore-efl.c: __do_app(453) > [APP 1192] Event: PAUSE State: RUNNING
05-03 10:21:37.789+0700 I/CAPI_APPFW_APPLICATION( 1192): app_main.c: app_appcore_pause(202) > app_appcore_pause
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): main.c: _appcore_pause_cb(489) > appcore pause
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): rotary.c: rotary_deattach(156) > rotary_deattach:0xb166f4e0
05-03 10:21:37.789+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:21:37.789+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb166f4e0, elm_layout, func : 0xb6ece455
05-03 10:21:37.789+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 10:21:37.789+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 10:21:37.789+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:21:37.789+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7fb97a0, elm_box, _activated_obj : 0xb166f4e0, activated : 1
05-03 10:21:37.789+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 E/APPS    ( 1192): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
05-03 10:21:37.789+0700 W/W_HOME  ( 1192): win.c: win_back_gesture_disable_set(170) > disable back gesture
05-03 10:21:37.789+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 10:21:37.789+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 10:21:37.789+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 10:21:37.789+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 10:21:37.789+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 10:21:37.789+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 10:21:37.789+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 10:21:37.789+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 10:21:37.789+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 10:21:37.789+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 10:21:37.789+0700 W/AUL     (  971): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1192) status(bg) type(uiapp)
05-03 10:21:37.799+0700 W/AUL     (  971): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.toyota.realtimefeedback) pid(2989) status(fg) type(uiapp)
05-03 10:21:37.799+0700 W/STARTER ( 1119): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1192] goes to (4)
05-03 10:21:37.799+0700 E/STARTER ( 1119): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1192)'s state(4)
05-03 10:21:37.799+0700 W/STARTER ( 1119): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2989] goes to (3)
05-03 10:21:37.799+0700 I/APP_CORE( 2989): appcore-efl.c: __do_app(453) > [APP 2989] Event: RESUME State: RUNNING
05-03 10:21:37.799+0700 I/GATE    ( 2989): <GATE-M>APP_FULLY_LOADED_realtimefeedback</GATE-M>
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 10:21:37.809+0700 I/MESSAGE_PORT(  967): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 10:21:37.809+0700 I/wnotib  ( 1192): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 1
05-03 10:21:37.809+0700 E/wnotib  ( 1192): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
05-03 10:21:37.809+0700 W/wnotib  ( 1192): w-notification-board-noti-manager.c: wnb_nm_postpone_updating_job(997) > Set is_notiboard_update_postponed to true with is_for_VI 0, notiboard panel creation count [8], notiboard card appending count [12].
05-03 10:21:37.809+0700 I/GATE    (  973): <GATE-M>BATTERY_LEVEL_23</GATE-M>
05-03 10:21:37.809+0700 I/watchface-viewer( 1302): viewer-data-provider.cpp: AddPendingChanges(2527) > added [60] to pending list
05-03 10:21:37.819+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
05-03 10:21:37.819+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(23), length(2)
05-03 10:21:37.819+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 23%
05-03 10:21:37.819+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 23, isCharging: 0
05-03 10:21:37.819+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_25
05-03 10:21:37.819+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 25
05-03 10:21:37.819+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
05-03 10:21:37.839+0700 W/MUSIC_CONTROL_SERVICE( 1742): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1742]   [com.samsung.w-home]register msg port [false][0m
05-03 10:21:37.849+0700 W/APPS    ( 1192): AppsViewNecklace.cpp: onPausedIdlerCb(5178) >  elm_cache_all_flush
05-03 10:21:38.179+0700 E/AUL     (  971): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
05-03 10:21:38.309+0700 I/APP_CORE( 1192): appcore-efl.c: __do_app(453) > [APP 1192] Event: MEM_FLUSH State: PAUSED
05-03 10:21:38.339+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:21:38.349+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2989
05-03 10:21:38.349+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 10:21:38.369+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:21:38.379+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2989
05-03 10:21:38.379+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 10:21:38.639+0700 I/AUL_PAD ( 3059): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
05-03 10:21:39.749+0700 I/CAPI_NETWORK_CONNECTION( 2989): connection.c: connection_create(453) > New handle created[0xb8e54420]
05-03 10:21:39.829+0700 I/CAPI_NETWORK_CONNECTION( 2989): connection.c: connection_destroy(471) > Destroy handle: 0xb8e54420
05-03 10:21:39.829+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:39.859+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8dc0b90 : elm_genlist] mx(0), my(272), minx(0), miny(0), px(0), py(0)
05-03 10:21:39.859+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8dc0b90 : elm_genlist] cw(360), ch(632), pw(360), ph(360)
05-03 10:21:40.989+0700 E/EFL     ( 2989): ecore_x<2989> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=1158063 button=1
05-03 10:21:40.989+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.009+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.009+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.019+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.019+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.029+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.029+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.039+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.039+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.049+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.049+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.059+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.059+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.079+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.079+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.079+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4292 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] add hold animator
05-03 10:21:41.089+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.089+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.089+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:21:41.089+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] drag_child_locked_y(0)
05-03 10:21:41.089+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] move content x(0), y(33)
05-03 10:21:41.089+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] drag_child_locked_y(0)
05-03 10:21:41.139+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] move content x(0), y(58)
05-03 10:21:41.139+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] drag_child_locked_y(0)
05-03 10:21:41.199+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] move content x(0), y(89)
05-03 10:21:41.199+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.249+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.249+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.249+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.249+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.249+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.249+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.249+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.249+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.259+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:21:41.259+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] drag_child_locked_y(0)
05-03 10:21:41.259+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] move content x(0), y(96)
05-03 10:21:41.259+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.299+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.299+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.299+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:21:41.299+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] drag_child_locked_y(0)
05-03 10:21:41.299+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] move content x(0), y(98)
05-03 10:21:41.299+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] mouse move
05-03 10:21:41.349+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8dc0b90 : elm_genlist] hold(0), freeze(0)
05-03 10:21:41.359+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:21:41.359+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] drag_child_locked_y(0)
05-03 10:21:41.359+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8dc0b90 : elm_genlist] move content x(0), y(97)
05-03 10:21:41.359+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.399+0700 E/EFL     ( 2989): ecore_x<2989> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=1158434 button=1
05-03 10:21:41.399+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb8dc0b90 : elm_genlist] t_in(0.320000), pos_y(129)
05-03 10:21:41.409+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] time(0.153600)
05-03 10:21:41.409+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(101)
05-03 10:21:41.409+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.449+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] time(0.276257)
05-03 10:21:41.449+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(105)
05-03 10:21:41.449+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.499+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] time(0.499291)
05-03 10:21:41.499+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(112)
05-03 10:21:41.499+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.539+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] time(0.682656)
05-03 10:21:41.539+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(118)
05-03 10:21:41.539+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.599+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] time(0.846686)
05-03 10:21:41.599+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(124)
05-03 10:21:41.599+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.639+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] time(0.936939)
05-03 10:21:41.639+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(126)
05-03 10:21:41.639+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.689+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] time(0.985892)
05-03 10:21:41.689+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(128)
05-03 10:21:41.689+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.729+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] time(0.999913)
05-03 10:21:41.729+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] animation stop!!
05-03 10:21:41.729+0700 E/EFL     ( 2989): elementary<2989> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb8dc0b90 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(129)
05-03 10:21:41.729+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.939+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.979+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:41.999+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.019+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.039+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.059+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.069+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.089+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.109+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.129+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.129+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.129+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.139+0700 E/EFL     ( 2989): <2989> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
05-03 10:21:42.359+0700 E/EFL     (  935): ecore_x<935> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x4000002 time=1158434
05-03 10:21:42.359+0700 E/EFL     ( 2989): ecore_x<2989> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=1158434
05-03 10:21:42.359+0700 E/EFL     (  935): ecore_x<935> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=1158434
05-03 10:21:42.839+0700 I/APP_CORE( 1192): appcore-efl.c: __do_app(453) > [APP 1192] Event: MEM_FLUSH State: PAUSED
05-03 10:21:47.789+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
05-03 10:21:52.279+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
05-03 10:21:52.299+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3164) > Deliver KeyPress. value=2669, window=0x4000002
05-03 10:21:52.299+0700 E/EFL     ( 2989): ecore_x<2989> ecore_x_events.c:537 _ecore_x_event_handle_key_press() KeyEvent:press time=1169160
05-03 10:21:52.299+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3175) > Deliver KeyRelease. value=2669, window=0x4000002
05-03 10:21:52.299+0700 E/EFL     ( 2989): ecore_x<2989> ecore_x_events.c:551 _ecore_x_event_handle_key_release() KeyEvent:release time=1169370
05-03 10:21:52.299+0700 E/efl-extension( 2989): efl_extension_events.c: _eext_key_grab_rect_key_up_cb(240) > key up called
05-03 10:21:52.729+0700 W/CRASH_MANAGER( 3065): worker.c: worker_job(1205) > 1102989726561152531771
