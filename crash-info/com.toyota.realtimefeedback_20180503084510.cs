S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2143
Date: 2018-05-03 08:45:10+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2143, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb6f3c668
r2   = 0xb6f3cc78, r3   = 0x00000000
r4   = 0xb83b4760, r5   = 0xb0401158
r6   = 0x00000013, r7   = 0xbea94188
r8   = 0x00009728, r9   = 0xb83b4760
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb6f3c8bc, sp   = 0xbea94168
lr   = 0xb6f2a50f, pc   = 0xb6f2a544
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      2740 KB
Buffers:      5192 KB
Cached:     103356 KB
VmPeak:     196208 KB
VmSize:     189280 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       35640 KB
VmRSS:       35060 KB
VmData:      89260 KB
VmStk:         136 KB
VmExe:          40 KB
VmLib:       35904 KB
VmPTE:         146 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2143 TID = 2143
2143 2148 2157 2158 

Maps Information
afff0000 afff1000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b05b2000 b05b6000 r-xp /usr/lib/libogg.so.0.7.1
b05be000 b05e0000 r-xp /usr/lib/libvorbis.so.0.4.3
b05e8000 b062f000 r-xp /usr/lib/libsndfile.so.1.0.26
b063b000 b0684000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b068d000 b0692000 r-xp /usr/lib/libjson.so.0.0.1
b1f33000 b2039000 r-xp /usr/lib/libicuuc.so.57.1
b204f000 b21d7000 r-xp /usr/lib/libicui18n.so.57.1
b21e7000 b21f4000 r-xp /usr/lib/libail.so.0.1.0
b21fd000 b2200000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b2208000 b2240000 r-xp /usr/lib/libpulse.so.0.16.2
b2241000 b2244000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b224c000 b22ad000 r-xp /usr/lib/libasound.so.2.0.0
b22b7000 b22d0000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b22d9000 b22dd000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b22e5000 b22f0000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b22fd000 b2301000 r-xp /usr/lib/libmmfsession.so.0.0.0
b230a000 b2322000 r-xp /usr/lib/libmmfsound.so.0.1.0
b2333000 b233a000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b2342000 b234d000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b2355000 b2357000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b235f000 b2360000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b2368000 b2370000 r-xp /usr/lib/libfeedback.so.0.1.4
b2389000 b238a000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b241f000 b24a6000 rw-s anon_inode:dmabuf
b24a6000 b252d000 rw-s anon_inode:dmabuf
b25f7000 b267e000 rw-s anon_inode:dmabuf
b290e000 b2995000 rw-s anon_inode:dmabuf
b2996000 b3195000 rw-p [stack:2158]
b333c000 b333d000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b33ba000 b33bd000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3445000 b3c44000 rw-p [stack:2157]
b3c44000 b3c46000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3c4e000 b3c65000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3e76000 b4675000 rw-p [stack:2148]
b468c000 b4fb8000 r-xp /usr/lib/libsc-a3xx.so
b521c000 b521e000 r-xp /usr/lib/libdri2.so.0.0.0
b5226000 b522e000 r-xp /usr/lib/libdrm.so.2.4.0
b5236000 b523a000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b5242000 b5245000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b524d000 b524e000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b5256000 b5261000 r-xp /usr/lib/libtbm.so.1.0.0
b5269000 b526c000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b5274000 b5276000 r-xp /usr/lib/libgenlock.so
b527e000 b5283000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b528b000 b53c6000 r-xp /usr/lib/egl/libGLESv2.so
b5402000 b5404000 r-xp /usr/lib/libadreno_utils.so
b540e000 b5435000 r-xp /usr/lib/libgsl.so
b5444000 b544b000 r-xp /usr/lib/egl/eglsubX11.so
b5455000 b5477000 r-xp /usr/lib/egl/libEGL.so
b5480000 b54f5000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5705000 b570f000 r-xp /lib/libnss_files-2.13.so
b5718000 b571b000 r-xp /lib/libattr.so.1.1.0
b5723000 b572a000 r-xp /lib/libcrypt-2.13.so
b575a000 b575d000 r-xp /lib/libcap.so.2.21
b5765000 b5767000 r-xp /usr/lib/libiri.so
b576f000 b578c000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b5795000 b5799000 r-xp /usr/lib/libsmack.so.1.0.0
b57a2000 b57d1000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b57d9000 b586d000 r-xp /usr/lib/libstdc++.so.6.0.16
b5880000 b594f000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5965000 b5989000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5992000 b5a5c000 r-xp /usr/lib/libCOREGL.so.4.0
b5a73000 b5a75000 r-xp /usr/lib/libXau.so.6.0.0
b5a7e000 b5a8e000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5a96000 b5a99000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5aa1000 b5ab9000 r-xp /usr/lib/liblzma.so.5.0.3
b5ac2000 b5ac4000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5acc000 b5acf000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5ad7000 b5adb000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5ae4000 b5ae9000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5af3000 b5b16000 r-xp /usr/lib/libjpeg.so.8.0.2
b5b2e000 b5b44000 r-xp /lib/libexpat.so.1.6.0
b5b4e000 b5b61000 r-xp /usr/lib/libxcb.so.1.1.0
b5b6a000 b5b70000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5b78000 b5b79000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5b83000 b5b9b000 r-xp /usr/lib/libpng12.so.0.50.0
b5ba3000 b5ba6000 r-xp /usr/lib/libEGL.so.1.4
b5bae000 b5bbc000 r-xp /usr/lib/libGLESv2.so.2.0
b5bc5000 b5bc6000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5bce000 b5be5000 r-xp /usr/lib/liblua-5.1.so
b5bef000 b5bf6000 r-xp /usr/lib/libembryo.so.1.7.99
b5bfe000 b5c08000 r-xp /usr/lib/libXext.so.6.4.0
b5c11000 b5c15000 r-xp /usr/lib/libXtst.so.6.1.0
b5c1d000 b5c23000 r-xp /usr/lib/libXrender.so.1.3.0
b5c2b000 b5c31000 r-xp /usr/lib/libXrandr.so.2.2.0
b5c39000 b5c3a000 r-xp /usr/lib/libXinerama.so.1.0.0
b5c44000 b5c47000 r-xp /usr/lib/libXfixes.so.3.1.0
b5c4f000 b5c51000 r-xp /usr/lib/libXgesture.so.7.0.0
b5c59000 b5c5b000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5c63000 b5c65000 r-xp /usr/lib/libXdamage.so.1.1.0
b5c6d000 b5c74000 r-xp /usr/lib/libXcursor.so.1.0.2
b5c7d000 b5c8d000 r-xp /lib/libresolv-2.13.so
b5c91000 b5c93000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5c9b000 b5ca0000 r-xp /usr/lib/libffi.so.5.0.10
b5ca8000 b5ca9000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5cb1000 b5cfa000 r-xp /usr/lib/libmdm.so.1.2.70
b5d04000 b5d0a000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5d12000 b5d18000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5d20000 b5d3a000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5d42000 b5d60000 r-xp /usr/lib/libsystemd.so.0.4.0
b5d6b000 b5d6c000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5d74000 b5d79000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5d81000 b5d98000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5da0000 b5da6000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5daf000 b5db8000 r-xp /usr/lib/libcom-core.so.0.0.1
b5dc2000 b5dc4000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5dcd000 b5e23000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5e30000 b5e86000 r-xp /usr/lib/libfreetype.so.6.11.3
b5e92000 b5ed7000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5ee0000 b5ef3000 r-xp /usr/lib/libfribidi.so.0.3.1
b5efc000 b5f16000 r-xp /usr/lib/libecore_con.so.1.7.99
b5f1f000 b5f49000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5f52000 b5f5b000 r-xp /usr/lib/libedbus.so.1.7.99
b5f63000 b5f74000 r-xp /usr/lib/libecore_input.so.1.7.99
b5f7c000 b5f81000 r-xp /usr/lib/libecore_file.so.1.7.99
b5f8a000 b5fac000 r-xp /usr/lib/libecore_evas.so.1.7.99
b5fb5000 b5fce000 r-xp /usr/lib/libeet.so.1.7.99
b5fdf000 b6007000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6008000 b6011000 r-xp /usr/lib/libXi.so.6.1.0
b6019000 b60fa000 r-xp /usr/lib/libX11.so.6.3.0
b6106000 b61be000 r-xp /usr/lib/libcairo.so.2.11200.14
b61c9000 b6227000 r-xp /usr/lib/libedje.so.1.7.99
b6231000 b6281000 r-xp /usr/lib/libecore_x.so.1.7.99
b6283000 b62ec000 r-xp /lib/libm-2.13.so
b62f5000 b62fb000 r-xp /lib/librt-2.13.so
b6304000 b631a000 r-xp /lib/libz.so.1.2.5
b6323000 b64b5000 r-xp /usr/lib/libcrypto.so.1.0.0
b64d6000 b651d000 r-xp /usr/lib/libssl.so.1.0.0
b6529000 b6557000 r-xp /usr/lib/libidn.so.11.5.44
b655f000 b6568000 r-xp /usr/lib/libcares.so.2.1.0
b6571000 b663d000 r-xp /usr/lib/libxml2.so.2.7.8
b664b000 b664d000 r-xp /usr/lib/libiniparser.so.0
b6656000 b668a000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6693000 b6766000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6771000 b678a000 r-xp /usr/lib/libnetwork.so.0.0.0
b6792000 b679b000 r-xp /usr/lib/libvconf.so.0.2.45
b67a4000 b6874000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6875000 b68b6000 r-xp /usr/lib/libeina.so.1.7.99
b68bf000 b68c4000 r-xp /usr/lib/libappcore-common.so.1.1
b68cc000 b68d2000 r-xp /usr/lib/libappcore-efl.so.1.1
b68da000 b68dd000 r-xp /usr/lib/libbundle.so.0.1.22
b68e5000 b68eb000 r-xp /usr/lib/libappsvc.so.0.1.0
b68f3000 b6907000 r-xp /lib/libpthread-2.13.so
b6912000 b6935000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b693d000 b694a000 r-xp /usr/lib/libaul.so.0.1.0
b6954000 b6956000 r-xp /lib/libdl-2.13.so
b695f000 b696a000 r-xp /lib/libunwind.so.8.0.1
b6997000 b699f000 r-xp /lib/libgcc_s-4.6.so.1
b69a0000 b6ac4000 r-xp /lib/libc-2.13.so
b6ad2000 b6b47000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6b51000 b6b5d000 r-xp /usr/lib/libnotification.so.0.1.0
b6b66000 b6b75000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6b7e000 b6c4c000 r-xp /usr/lib/libevas.so.1.7.99
b6c72000 b6dae000 r-xp /usr/lib/libelementary.so.1.7.99
b6dc5000 b6de6000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6dee000 b6e05000 r-xp /usr/lib/libecore.so.1.7.99
b6e1c000 b6e1e000 r-xp /usr/lib/libdlog.so.0.0.0
b6e26000 b6e6a000 r-xp /usr/lib/libcurl.so.4.3.0
b6e73000 b6e78000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6e80000 b6e85000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6e8d000 b6e9d000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6ea5000 b6ead000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6eb5000 b6eb9000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6ec1000 b6ec5000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6ece000 b6ed0000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6edb000 b6ee6000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6ef0000 b6ef4000 r-xp /usr/lib/libsys-assert.so
b6efd000 b6f1a000 r-xp /lib/ld-2.13.so
b6f23000 b6f2d000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b8305000 b9e1e000 rw-p [heap]
bea74000 bea95000 rw-p [stack]
b6f23000 b6f2d000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b8305000 b9e1e000 rw-p [heap]
bea74000 bea95000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2143)
Call Stack Count: 20
 0: set_targeted_view + 0x87 (0xb6f2a544) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7544
 1: pop_from_stack_uib_vc + 0x38 (0xb6f2aa49) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a49
 2: uib_views_destroy_callback + 0x38 (0xb6f2aa05) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a05
 3: (0xb6b95af9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6bacc39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6b9f37b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6b9fab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb5f99cf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6d70953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6df93f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6df6e53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6dfa46b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6dfa7db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6d2d44d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x44c (0xb68cfc61) [/usr/lib/libappcore-efl.so.1] + 0x3c61
15: ui_app_main + 0xb0 (0xb6eb6ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16: uib_app_run + 0xea (0xb6f28437) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5437
17: main + 0x34 (0xb6f28b11) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5b11
18: __libc_start_main + 0x114 (0xb69b785c) [/lib/libc.so.6] + 0x1785c
19: (0xb6f26124) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x3124
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
IpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 08:45:09.990+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 08:45:10.000+0700 I/CAPI_WIDGET_APPLICATION( 1470): widget_app.c: __provider_resume_cb(316) > widget obj was resumed
05-03 08:45:10.000+0700 W/AUL     ( 1470): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.shealth.widget.pedometer) pid(1470) status(fg) type(widgetapp)
05-03 08:45:10.010+0700 I/CAPI_WIDGET_APPLICATION( 1470): widget_app.c: __check_status_for_cgroup(134) > enter foreground group
05-03 08:45:10.020+0700 I/watchface-app( 1306): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
05-03 08:45:10.030+0700 W/AUL_AMD (  927): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(start), value(uninstall)
05-03 08:45:10.030+0700 W/AUL_AMD (  927): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(997) > __amd_pkgmgrinfo_start_handler
05-03 08:45:10.040+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [UNINSTALL, STARTED]
05-03 08:45:10.040+0700 W/W_HOME  ( 1187): clock_event.c: _pkgmgr_event_cb(194) > Pkg:com.toyota.realtimefeedback is being uninstalled
05-03 08:45:10.040+0700 W/W_HOME  ( 1187): dbox.c: uninstall_cb(1434) > uninstalled:com.toyota.realtimefeedback
05-03 08:45:10.040+0700 W/SHealthCommon( 1752): SHealthMessagePortConnection.cpp: OnPortMessageReceived(135) > [1;40;33mfeatureType: 1, clientMsgType: 2, messageName: app_resumed[0;m
05-03 08:45:10.040+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.811530)
05-03 08:45:10.040+0700 E/ALARM_MANAGER(  930): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 08:45:10.040+0700 E/ALARM_MANAGER(  930): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 08:45:10.040+0700 E/ALARM_MANAGER(  930): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 01:49:33 (UTC).
05-03 08:45:10.040+0700 E/ALARM_MANAGER(  930): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 08:45:10.040+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.040+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.040+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(787), py(0)
05-03 08:45:10.040+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.811530)
05-03 08:45:10.040+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(787), py(0)
05-03 08:45:10.060+0700 E/ALARM_MANAGER(  930): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 08:45:10.060+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:45:10.070+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2143
05-03 08:45:10.070+0700 E/rpm-installer( 2258): installer-util.c: __check_running_app(1774) > app[com.toyota.realtimefeedback] is running, need to terminate it.
05-03 08:45:10.070+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 08:45:10.070+0700 W/AUL     ( 2258): launch.c: app_request_to_launchpad(284) > request cmd(5) to(2143)
05-03 08:45:10.070+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 5
05-03 08:45:10.070+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.894467)
05-03 08:45:10.070+0700 W/AUL     (  927): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2143) type(uiapp)
05-03 08:45:10.070+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.070+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.070+0700 W/AUL_AMD (  927): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(2143), cmd(4)
05-03 08:45:10.070+0700 W/AUL     ( 2258): launch.c: app_request_to_launchpad(298) > request cmd(5) result(0)
05-03 08:45:10.070+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(757), py(0)
05-03 08:45:10.070+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.894467)
05-03 08:45:10.070+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:45:10.070+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(757), py(0)
05-03 08:45:10.080+0700 I/APP_CORE( 2143): appcore-efl.c: __do_app(453) > [APP 2143] Event: TERMINATE State: PAUSED
05-03 08:45:10.080+0700 W/APP_CORE( 2143): appcore-efl.c: appcore_efl_main(1788) > power off : 0
05-03 08:45:10.080+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2143
05-03 08:45:10.080+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 22
05-03 08:45:10.080+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(999) > app status : 4
05-03 08:45:10.080+0700 W/APP_CORE( 2143): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3a00003] -> redirected win[600417] for com.toyota.realtimefeedback[2143]
05-03 08:45:10.110+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.965935)
05-03 08:45:10.110+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.110+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.110+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(732), py(0)
05-03 08:45:10.110+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.965935)
05-03 08:45:10.110+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(732), py(0)
05-03 08:45:10.130+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.991144)
05-03 08:45:10.130+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.130+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.150+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(723), py(0)
05-03 08:45:10.150+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.991144)
05-03 08:45:10.150+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(723), py(0)
05-03 08:45:10.170+0700 I/APP_CORE( 2143): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
05-03 08:45:10.170+0700 I/CAPI_APPFW_APPLICATION( 2143): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
05-03 08:45:10.180+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.994675)
05-03 08:45:10.180+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.180+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.180+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:45:10.190+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2559 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_CANCEL : px(720), py(0)
05-03 08:45:10.190+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.994675)
05-03 08:45:10.190+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] animation stop!!
05-03 08:45:10.190+0700 W/wnotib  ( 1187): w-notification-board-panel-manager.c: _wnb_pm_anim_stop_cb(96) > notiboard scroller anim stop [720][0][360][360]
05-03 08:45:10.190+0700 W/wnotib  ( 1187): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(981) > No postponed update with is_for_VI: 1.
05-03 08:45:10.190+0700 W/W_HOME  ( 1187): home_navigation.c: _anim_stop_cb(1319) > anim stop
05-03 08:45:10.190+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_del(822) > delete timer
05-03 08:45:10.190+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_add(850) > add timer:1
05-03 08:45:10.190+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
05-03 08:45:10.190+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(489) > [0xb7f98750 : elm_scroller] detent_count(-1)
05-03 08:45:10.190+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(490) > [0xb7f98750 : elm_scroller] pagenumber_v(0), pagenumber_h(2)
05-03 08:45:10.190+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(512) > [0xb7f98750 : elm_scroller] CurrentPage(2)
05-03 08:45:10.190+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_CANCEL : px(720), py(0)
05-03 08:45:10.190+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2143
05-03 08:45:10.190+0700 I/efl-extension( 2143): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:45:10.190+0700 I/efl-extension( 2143): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb85da118 is freed
05-03 08:45:10.190+0700 E/EFL     ( 2143): elementary<2143> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb85cd4c0 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 08:45:10.200+0700 I/efl-extension( 2143): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb85cdf08, obj: 0xb85cdf08
05-03 08:45:10.200+0700 I/efl-extension( 2143): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 08:45:10.200+0700 E/EFL     ( 2143): elementary<2143> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 08:45:10.210+0700 E/EFL     ( 2143): elementary<2143> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 08:45:10.210+0700 E/EFL     ( 2143): elementary<2143> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb85cdf08 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 08:45:10.210+0700 E/EFL     ( 2143): elementary<2143> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb85cdf08 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 08:45:10.210+0700 E/EFL     ( 2143): elementary<2143> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb85cdf08 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 08:45:10.210+0700 E/EFL     ( 2143): elementary<2143> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb85cdf08 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 08:45:10.210+0700 I/efl-extension( 2143): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:45:10.210+0700 I/efl-extension( 2143): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb85f51d8 is freed
05-03 08:45:10.210+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:45:10.210+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb85cdf08, elm_genlist, func : 0xb6dd2ea1
05-03 08:45:10.210+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:45:10.210+0700 W/SHealthCommon( 1470): SHealthMessagePortConnection.cpp: Send(509) > [1;40;33mSEND CLIENT MESSAGE [feature type: 1, msg type: 2, msg id: app_paused, name: app_paused][0;m
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.shealth-service:com.samsung.shealth.server]
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 08:45:10.220+0700 I/MESSAGE_PORT(  923): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 08:45:10.220+0700 I/CAPI_WIDGET_APPLICATION( 1470): widget_app.c: __provider_pause_cb(298) > widget obj was paused
05-03 08:45:10.220+0700 W/AUL     ( 1470): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.shealth.widget.pedometer) pid(1470) status(bg) type(widgetapp)
05-03 08:45:10.220+0700 I/CAPI_WIDGET_APPLICATION( 1470): widget_app.c: __check_status_for_cgroup(145) > enter background group
05-03 08:45:10.220+0700 W/SHealthCommon( 1752): SHealthMessagePortConnection.cpp: OnPortMessageReceived(135) > [1;40;33mfeatureType: 1, clientMsgType: 2, messageName: app_paused[0;m
05-03 08:45:10.220+0700 I/APP_CORE( 2143): appcore-efl.c: __after_loop(1243) > [APP 2143] After terminate() callback
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb040fd58 is freed
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb860b1a8, elm_genlist, func : 0xb6dd2ea1
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb04991a0
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb04a7828 is freed
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb040af08, elm_scroller, func : 0xb6dd6379
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb04991a0, elm_image, func : 0xb6dd6379
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 08:45:10.230+0700 I/efl-extension( 2143): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb040af08 : elm_scroller] rotary callabck is deleted
05-03 08:45:10.260+0700 I/efl-extension( 1122): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_rotary.c: _process_raw_event(438) > direction: Counter Clockwise
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_rotary.c: _rotary_rotate_handler_cb(539) > Deliver counter clockwise rotary event to object: 0xb7fbccf8, elm_box, time_stamp : 233064
05-03 08:45:10.270+0700 I/wnotib  ( 1187): w-notification-board-broker-main.c: _wnb_rotary_cb(564) > Pass rotary event to home.
05-03 08:45:10.270+0700 E/weather-widget( 1467): WidgetMain.cpp: ResumeWidgetInstance(672) > [0;40;31mResumeWidgetInstance[0;m
05-03 08:45:10.270+0700 W/W_HOME  ( 1187): home_navigation.c: _rotary_cb(1168) > Detent detected, obj[0xb7f98750], direction[1]
05-03 08:45:10.270+0700 E/W_HOME  ( 1187): home_navigation.c: _vi_init(954) > (scroller_info->vi_info.on == 1) -> _vi_init() return
05-03 08:45:10.270+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_del(822) > delete timer
05-03 08:45:10.270+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_add(850) > add timer:1
05-03 08:45:10.270+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
05-03 08:45:10.270+0700 W/W_HOME  ( 1187): home_navigation.c: _is_rightedge(187) > (720 360) not right edge: 0 0 0xb808da60 -> 360 0 0xb828e6c0
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_rotary_changed_cb(633) > [0xb7f98750 : elm_scroller] rotary callabck is called.
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_rotary_changed_cb(663) > [0xb7f98750 : elm_scroller] block(2)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_rotary_changed_cb(664) > [0xb7f98750 : elm_scroller] scroll_locked_x(0), scroll_locked_y(0)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_rotary_changed_cb(665) > [0xb7f98750 : elm_scroller] content size (w, h)(3600, 360)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_rotary_changed_cb(666) > [0xb7f98750 : elm_scroller] viewport size (w, h)(360, 360)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_rotary_changed_cb(730) > [0xb7f98750 : elm_scroller] CurrentPage(2) DetentValue(-1)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_rotary_changed_cb(731) > [0xb7f98750 : elm_scroller] DetentCount(0)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_can_scroll(407) > [0xb7f98750 : elm_scroller] mx(3240), my(0), minx(0), miny(0), px(720), py(0)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_can_scroll(407) > [0xb7f98750 : elm_scroller] mx(3240), my(0), minx(0), miny(0), px(720), py(0)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_can_scroll(407) > [0xb7f98750 : elm_scroller] mx(3240), my(0), minx(0), miny(0), px(720), py(0)
05-03 08:45:10.270+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_rotary_changed_cb(763) > [0xb7f98750 : elm_scroller] bring in 1 page
05-03 08:45:10.270+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:5560 _elm_scroll_page_bring_in() [0xb7f98750 : elm_scroller] pagenumber_h(1), pagenumber_v(0)
05-03 08:45:10.270+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7f98750 : elm_scroller] mx(3240), my(0), minx(0), miny(0), px(720), py(0)
05-03 08:45:10.270+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7f98750 : elm_scroller] cw(3600), ch(360), pw(360), ph(360)
05-03 08:45:10.270+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7f98750 : elm_scroller] x(360), y(0), nx(360), px(720), ny(0) py(0)
05-03 08:45:10.270+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb7f98750 : elm_scroller] x(360), y(0)
05-03 08:45:10.270+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:5578 _elm_scroll_page_bring_in() [0xb7f98750 : elm_scroller] _elm_scroll_content_region_show_internal() return TRUE!! x(360), y(0)
05-03 08:45:10.270+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb7f98750 : elm_scroller] t_in(0.270000), pos_x(360)
05-03 08:45:10.270+0700 W/W_HOME  ( 1187): home_navigation.c: _anim_start_cb(1293) > anim start
05-03 08:45:10.270+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_del(822) > delete timer
05-03 08:45:10.270+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb7f98750 : elm_scroller] t_in(0.270000), pos_y(0)
05-03 08:45:10.280+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.109122)
05-03 08:45:10.280+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.280+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.280+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(680), py(0)
05-03 08:45:10.280+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.109122)
05-03 08:45:10.280+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(680), py(0)
05-03 08:45:10.290+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:45:10.300+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2143
05-03 08:45:10.300+0700 E/weather-common( 1467): CPType.cpp: Renew(90) > [0;40;31mCPType is renewed : 5[0;m
05-03 08:45:10.300+0700 E/weather-common( 1467): DataManager.cpp: LoadData(326) > [0;40;31mnewCpTypeStr : TWC[0;m
05-03 08:45:10.300+0700 E/weather-common( 1467): DataManager.cpp: LoadData(329) > [0;40;31mweather data loaded[0;m
05-03 08:45:10.320+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.373493)
05-03 08:45:10.320+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.320+0700 W/WATCH_CORE( 1306): appcore-watch.c: __widget_resume(1124) > widget_resume
05-03 08:45:10.320+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.320+0700 W/AUL     ( 1306): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1306) status(fg) type(watchapp)
05-03 08:45:10.320+0700 E/watchface-app( 1306): watchface.cpp: OnAppResume(1136) > 
05-03 08:45:10.320+0700 I/CAPI_WATCH_APPLICATION( 1306): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 08:45:10.320+0700 E/watchface-app( 1306): watchface.cpp: OnAppTimeTick(1157) > 
05-03 08:45:10.330+0700 E/weather-widget( 1467): WidgetViewData.cpp: SetWidgetLifeCycleState(382) > [0;40;31mSetWidgetLifeCycleState, mWidgetLifeCycleState:2[0;m
05-03 08:45:10.330+0700 E/weather-widget( 1467): WidgetMainView.cpp: Resume(1236) > [0;40;31mWidgetMainView::Resume[0;m
05-03 08:45:10.340+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(585), py(0)
05-03 08:45:10.340+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.373493)
05-03 08:45:10.340+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(585), py(0)
05-03 08:45:10.350+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.531962)
05-03 08:45:10.350+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.350+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.350+0700 E/W_HOME  ( 1187): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
05-03 08:45:10.350+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(528), py(0)
05-03 08:45:10.350+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.531962)
05-03 08:45:10.350+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(528), py(0)
05-03 08:45:10.360+0700 E/weather-widget( 1467): WidgetMainView.cpp: AddTouchEventCallback(143) > [0;40;31mAddTouchEventCallback[0;m
05-03 08:45:10.360+0700 E/weather-widget( 1467): WidgetMainView.cpp: UpdateViewPage(267) > [0;40;31mUpdateViewPage[0;m
05-03 08:45:10.360+0700 E/weather-widget( 1467): WidgetMainView.cpp: HideFindingLocationView(595) > [0;40;31m[HideFindingLocationView(): 595] (mFindingLocationView == NULL) [return][0;m
05-03 08:45:10.360+0700 E/weather-widget( 1467): WidgetMainView.cpp: UpdateViewPage(276) > [0;40;31mUpdateViewPage locationId : e0ae79d8e25f1902b7edbb7c162fdd27b2def8c7cf049d7bd71824fa00491abe[0;m
05-03 08:45:10.360+0700 E/weather-widget( 1467): WidgetMainView.cpp: UpdateViewExceptLifeIndexProgress(377) > [0;40;31mUpdateViewExceptLifeIndexProgress[0;m
05-03 08:45:10.360+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.360+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.600955)
05-03 08:45:10.370+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.370+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.370+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(503), py(0)
05-03 08:45:10.370+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.600955)
05-03 08:45:10.370+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(503), py(0)
05-03 08:45:10.390+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.713645)
05-03 08:45:10.390+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.390+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.390+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(463), py(0)
05-03 08:45:10.390+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.713645)
05-03 08:45:10.390+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(463), py(0)
05-03 08:45:10.390+0700 E/weather-widget( 1467): WidgetViewData.cpp: GetWidgetLifeCycleState(389) > [0;40;31mGetWidgetLifeCycleState, mWidgetLifeCycleState:2[0;m
05-03 08:45:10.390+0700 E/weather-widget( 1467): WidgetMainView.cpp: LableSlidingStart(1127) > [0;40;31mcityName sliding start[0;m
05-03 08:45:10.390+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: CreateHighLowTemperature(249) > [0;40;31mLTR[0;m
05-03 08:45:10.400+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:45:10.410+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: CreateIconAndTemperature(355) > [0;40;31mLTR[0;m
05-03 08:45:10.410+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2143
05-03 08:45:10.410+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.420+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.818059)
05-03 08:45:10.420+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.420+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.420+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(425), py(0)
05-03 08:45:10.420+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.818059)
05-03 08:45:10.420+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(425), py(0)
05-03 08:45:10.440+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.450+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.450+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 08:45:10.450+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 08:45:10.450+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 08:45:10.450+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.450+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: CreateRightLifeIndexWeatherInfo(524) > [0;40;31mcolorCode : AO094[0;m
05-03 08:45:10.460+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 08:45:10.460+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 08:45:10.460+0700 E/EFL     ( 1467): evas_main<1467> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 08:45:10.460+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.915756)
05-03 08:45:10.460+0700 W/W_HOME  ( 1187): page.c: check_expired_widget_suggestion_page(1149) > check_expired_widget_suggestion_page
05-03 08:45:10.460+0700 E/W_HOME  ( 1187): page.c: check_expired_widget_suggestion_page(1157) > (!scroller_info->suggestion_page) -> check_expired_widget_suggestion_page() return
05-03 08:45:10.460+0700 W/W_HOME  ( 1187): event_manager.c: _clock_view_visible_cb(631) > state: 0 -> 1
05-03 08:45:10.460+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 3, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 08:45:10.460+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 1
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_register(581) > [windicator_battery_vconfkey_register:581] Set battery cb
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(77), length(2)
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 77%
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 77, isCharging: 0
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_80
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 80
05-03 08:45:10.460+0700 W/W_INDICATOR( 1122): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
05-03 08:45:10.460+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.460+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.470+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 3, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 08:45:10.470+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.470+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.470+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(390), py(0)
05-03 08:45:10.470+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.915756)
05-03 08:45:10.470+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(390), py(0)
05-03 08:45:10.480+0700 E/weather-common( 1467): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 08:45:10.480+0700 E/weather-widget( 1467): WidgetMainViewData.cpp: GetProgressAngleOnUVIndexWithCityIndex(142) > [0;40;31muv_index : 0, deltaAngle : 16.000000[0;m
05-03 08:45:10.480+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: DrawLifeIndexText(603) > [0;40;31mleftText : 0%, leftFontCode : AT094[0;m
05-03 08:45:10.480+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: DrawLifeIndexText(604) > [0;40;31mrightText : Low, rightFontCode : AT0941[0;m
05-03 08:45:10.480+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: SetFontCodeForCairo(682) > [0;40;31mfont : Tizen:style=normal:weight=regular[0;m
05-03 08:45:10.480+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.965505)
05-03 08:45:10.490+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.490+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.490+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(372), py(0)
05-03 08:45:10.490+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.965505)
05-03 08:45:10.490+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_RENEW : px(372), py(0)
05-03 08:45:10.490+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: DrawLifeIndexText(629) > [0;40;31m[Left] textAngle : 8.540986, endAngle : 154.540986, startAngle : 146.000000[0;m
05-03 08:45:10.490+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: SetFontCodeForCairo(682) > [0;40;31mfont : Tizen:style=normal:weight=regular[0;m
05-03 08:45:10.500+0700 E/weather-widget( 1467): WidgetMainViewElement.cpp: DrawLifeIndexText(660) > [0;40;31m[Right] textAngle : 11.032107, endAngle : 29.032107, startAngle : 18.000000[0;m
05-03 08:45:10.500+0700 E/weather-common( 1467): SamsungLogManager.cpp: OnWeatherWidgetResumed(248) > [0;40;31mOnWeatherWidgetResumed[0;m
05-03 08:45:10.500+0700 I/CAPI_WIDGET_APPLICATION( 1467): widget_app.c: __provider_resume_cb(316) > widget obj was resumed
05-03 08:45:10.500+0700 W/AUL     ( 1467): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.weather-widget) pid(1467) status(fg) type(widgetapp)
05-03 08:45:10.510+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:45:10.520+0700 I/CAPI_WIDGET_APPLICATION( 1467): widget_app.c: __check_status_for_cgroup(134) > enter foreground group
05-03 08:45:10.520+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2143
05-03 08:45:10.540+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] time(0.998818)
05-03 08:45:10.550+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3321) > tobj_item_01 is null
05-03 08:45:10.550+0700 I/ELM_RPANEL( 1187): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
05-03 08:45:10.550+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2559 _elm_scroll_scroll_to_x_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_CANCEL : px(360), py(0)
05-03 08:45:10.550+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] time(0.998818)
05-03 08:45:10.550+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] animation stop!!
05-03 08:45:10.550+0700 W/wnotib  ( 1187): w-notification-board-panel-manager.c: _wnb_pm_anim_stop_cb(96) > notiboard scroller anim stop [360][0][360][360]
05-03 08:45:10.550+0700 W/wnotib  ( 1187): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(981) > No postponed update with is_for_VI: 1.
05-03 08:45:10.550+0700 W/W_HOME  ( 1187): home_navigation.c: _anim_stop_cb(1319) > anim stop
05-03 08:45:10.550+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_del(822) > delete timer
05-03 08:45:10.550+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_add(850) > add timer:1
05-03 08:45:10.550+0700 W/W_HOME  ( 1187): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
05-03 08:45:10.550+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(489) > [0xb7f98750 : elm_scroller] detent_count(-1)
05-03 08:45:10.550+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(490) > [0xb7f98750 : elm_scroller] pagenumber_v(0), pagenumber_h(1)
05-03 08:45:10.550+0700 I/efl-extension( 1187): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(512) > [0xb7f98750 : elm_scroller] CurrentPage(1)
05-03 08:45:10.550+0700 E/EFL     ( 1187): elementary<1187> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb7f98750 : elm_scroller] ECORE_CALLBACK_CANCEL : px(360), py(0)
05-03 08:45:10.580+0700 E/weather-widget( 1467): WidgetMain.cpp: PauseWidgetInstance(627) > [0;40;31mPauseWidgetInstance[0;m
05-03 08:45:10.590+0700 E/weather-widget( 1467): WidgetViewData.cpp: SetWidgetLifeCycleState(382) > [0;40;31mSetWidgetLifeCycleState, mWidgetLifeCycleState:4[0;m
05-03 08:45:10.590+0700 E/weather-widget( 1467): WidgetMainView.cpp: Pause(1204) > [0;40;31mPause[0;m
05-03 08:45:10.590+0700 E/weather-widget( 1467): WidgetMainView.cpp: HideFindingLocationView(595) > [0;40;31m[HideFindingLocationView(): 595] (mFindingLocationView == NULL) [return][0;m
05-03 08:45:10.590+0700 E/weather-widget( 1467): WidgetMainView.cpp: LableSlidingStop(1113) > [0;40;31mcityName sliding stop[0;m
05-03 08:45:10.620+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:45:10.630+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2143
05-03 08:45:10.670+0700 E/W_HOME  ( 1187): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
05-03 08:45:10.690+0700 W/W_HOME  ( 1187): event_manager.c: _home_scroll_cb(579) > scroll,will,done
05-03 08:45:10.690+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 1, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 08:45:10.690+0700 W/W_HOME  ( 1187): event_manager.c: _home_scroll_cb(579) > scroll,done
05-03 08:45:10.690+0700 W/W_HOME  ( 1187): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 08:45:10.730+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 08:45:10.740+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
05-03 08:45:10.790+0700 E/PKGMGR_PARSER( 2258): pkgmgr_parser_signature.c: __ps_check_mdm_policy_by_pkgid(1056) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-CLIENT( 2258): SocketClient.cpp: SocketClient(37) > Client created
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-SERVER(  925): SocketService.cpp: mainloop(227) > Got incoming connection
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-CLIENT( 2258): SocketStream.h: SocketStream(31) > Created
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-CLIENT( 2258): SocketConnection.h: SocketConnection(44) > Created
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-CLIENT( 2258): SocketClient.cpp: connect(62) > Client connected
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-SERVER(  925): SocketService.cpp: connectionThread(253) > Starting connection thread
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-SERVER(  925): SocketStream.h: SocketStream(31) > Created
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-SERVER(  925): SocketConnection.h: SocketConnection(44) > Created
05-03 08:45:10.810+0700 I/PRIVACY-MANAGER-SERVER(  925): SocketService.cpp: connectionService(304) > Calling service
05-03 08:45:10.820+0700 I/PRIVACY-MANAGER-CLIENT( 2258): SocketClient.cpp: disconnect(72) > Client disconnected
05-03 08:45:10.830+0700 I/PRIVACY-MANAGER-SERVER(  925): SocketService.cpp: connectionService(307) > Removing client
05-03 08:45:10.830+0700 I/PRIVACY-MANAGER-SERVER(  925): SocketService.cpp: connectionService(311) > Call served
05-03 08:45:10.830+0700 I/PRIVACY-MANAGER-SERVER(  925): SocketService.cpp: connectionThread(262) > Client serviced
05-03 08:45:10.860+0700 E/weather-widget( 1467): WidgetMain.cpp: PauseWidgetInstance(661) > [0;40;31mlocationID : e0ae79d8e25f1902b7edbb7c162fdd27b2def8c7cf049d7bd71824fa00491abe[0;m
05-03 08:45:10.860+0700 I/CAPI_WIDGET_APPLICATION( 1467): widget_app.c: __provider_pause_cb(298) > widget obj was paused
05-03 08:45:10.860+0700 W/AUL     ( 1467): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.weather-widget) pid(1467) status(bg) type(widgetapp)
05-03 08:45:10.870+0700 I/CAPI_WIDGET_APPLICATION( 1467): widget_app.c: __check_status_for_cgroup(145) > enter background group
05-03 08:45:10.990+0700 I/CAPI_WATCH_APPLICATION( 1306): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 08:45:10.990+0700 E/watchface-app( 1306): watchface.cpp: OnAppTimeTick(1157) > 
05-03 08:45:11.040+0700 W/CRASH_MANAGER( 2261): worker.c: worker_job(1205) > 1102143726561152531191
