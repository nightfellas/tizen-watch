S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2363
Date: 2018-04-27 11:07:24+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2363, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb584f443, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb708ef58, r7   = 0xbe8a41d0
r8   = 0x000186a0, r9   = 0xb6f5de40
r10  = 0xb6c25b10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbe8a4150
lr   = 0xb584f443, pc   = 0xb69db6f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      3496 KB
Buffers:      8476 KB
Cached:     113720 KB
VmPeak:      89744 KB
VmSize:      89200 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23304 KB
VmRSS:       23304 KB
VmData:      29048 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24500 KB
VmPTE:          58 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2363 TID = 2363
2363 2391 2392 

Maps Information
b1e0c000 b1e10000 r-xp /usr/lib/libogg.so.0.7.1
b1e18000 b1e3a000 r-xp /usr/lib/libvorbis.so.0.4.3
b1e42000 b1e89000 r-xp /usr/lib/libsndfile.so.1.0.26
b1e95000 b1ede000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1ee7000 b1eec000 r-xp /usr/lib/libjson.so.0.0.1
b378d000 b3893000 r-xp /usr/lib/libicuuc.so.57.1
b38a9000 b3a31000 r-xp /usr/lib/libicui18n.so.57.1
b3a41000 b3a4e000 r-xp /usr/lib/libail.so.0.1.0
b3a57000 b3a5a000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3a62000 b3a9a000 r-xp /usr/lib/libpulse.so.0.16.2
b3a9b000 b3a9e000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3aa6000 b3b07000 r-xp /usr/lib/libasound.so.2.0.0
b3b11000 b3b2a000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3b33000 b3b37000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3b3f000 b3b4a000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3b57000 b3b5b000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3b64000 b3b7c000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3b8d000 b3b94000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3b9c000 b3ba7000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3baf000 b3bb1000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3bb9000 b3bba000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3bc2000 b3bca000 r-xp /usr/lib/libfeedback.so.0.1.4
b3be3000 b3be4000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3f77000 b3ffe000 rw-s anon_inode:dmabuf
b3fff000 b47fe000 rw-p [stack:2392]
b49a2000 b49a3000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4aaa000 b52a9000 rw-p [stack:2391]
b52a9000 b52ab000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52b3000 b52ca000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52d7000 b52d9000 r-xp /usr/lib/libdri2.so.0.0.0
b52e1000 b52ec000 r-xp /usr/lib/libtbm.so.1.0.0
b52f4000 b52fc000 r-xp /usr/lib/libdrm.so.2.4.0
b5304000 b5306000 r-xp /usr/lib/libgenlock.so
b530e000 b5313000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b531b000 b5326000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b552f000 b55f9000 r-xp /usr/lib/libCOREGL.so.4.0
b560a000 b561a000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5622000 b5628000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5630000 b5631000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b563a000 b563d000 r-xp /usr/lib/libEGL.so.1.4
b5645000 b5653000 r-xp /usr/lib/libGLESv2.so.2.0
b565c000 b56a5000 r-xp /usr/lib/libmdm.so.1.2.70
b56ae000 b56b4000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56bc000 b56c5000 r-xp /usr/lib/libcom-core.so.0.0.1
b56ce000 b5786000 r-xp /usr/lib/libcairo.so.2.11200.14
b5791000 b57aa000 r-xp /usr/lib/libnetwork.so.0.0.0
b57b2000 b57be000 r-xp /usr/lib/libnotification.so.0.1.0
b57c7000 b57d6000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b57df000 b5800000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5808000 b580d000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5815000 b581a000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b5822000 b5832000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b583a000 b5842000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b584a000 b5853000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b59f8000 b5a02000 r-xp /lib/libnss_files-2.13.so
b5a0b000 b5ada000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5af0000 b5b14000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b1d000 b5b23000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b2b000 b5b2f000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b3c000 b5b47000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b4f000 b5b51000 r-xp /usr/lib/libiniparser.so.0
b5b5a000 b5b5f000 r-xp /usr/lib/libappcore-common.so.1.1
b5b67000 b5b69000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b72000 b5b76000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5b83000 b5b85000 r-xp /usr/lib/libXau.so.6.0.0
b5b8d000 b5b94000 r-xp /lib/libcrypt-2.13.so
b5bc4000 b5bc6000 r-xp /usr/lib/libiri.so
b5bcf000 b5d61000 r-xp /usr/lib/libcrypto.so.1.0.0
b5d82000 b5dc9000 r-xp /usr/lib/libssl.so.1.0.0
b5dd5000 b5e03000 r-xp /usr/lib/libidn.so.11.5.44
b5e0b000 b5e14000 r-xp /usr/lib/libcares.so.2.1.0
b5e1e000 b5e31000 r-xp /usr/lib/libxcb.so.1.1.0
b5e3a000 b5e3d000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e45000 b5e47000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e50000 b5f1c000 r-xp /usr/lib/libxml2.so.2.7.8
b5f29000 b5f2b000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f34000 b5f39000 r-xp /usr/lib/libffi.so.5.0.10
b5f41000 b5f42000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f4a000 b5f4d000 r-xp /lib/libattr.so.1.1.0
b5f55000 b5fe9000 r-xp /usr/lib/libstdc++.so.6.0.16
b5ffc000 b6019000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6023000 b603b000 r-xp /usr/lib/libpng12.so.0.50.0
b6043000 b6059000 r-xp /lib/libexpat.so.1.6.0
b6063000 b60a7000 r-xp /usr/lib/libcurl.so.4.3.0
b60b0000 b60ba000 r-xp /usr/lib/libXext.so.6.4.0
b60c4000 b60c8000 r-xp /usr/lib/libXtst.so.6.1.0
b60d0000 b60d6000 r-xp /usr/lib/libXrender.so.1.3.0
b60de000 b60e4000 r-xp /usr/lib/libXrandr.so.2.2.0
b60ec000 b60ed000 r-xp /usr/lib/libXinerama.so.1.0.0
b60f6000 b60ff000 r-xp /usr/lib/libXi.so.6.1.0
b6107000 b610a000 r-xp /usr/lib/libXfixes.so.3.1.0
b6113000 b6115000 r-xp /usr/lib/libXgesture.so.7.0.0
b611d000 b611f000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6127000 b6129000 r-xp /usr/lib/libXdamage.so.1.1.0
b6131000 b6138000 r-xp /usr/lib/libXcursor.so.1.0.2
b6140000 b6143000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b614c000 b6150000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6159000 b615e000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6167000 b6248000 r-xp /usr/lib/libX11.so.6.3.0
b6253000 b6276000 r-xp /usr/lib/libjpeg.so.8.0.2
b628e000 b62a4000 r-xp /lib/libz.so.1.2.5
b62ad000 b62af000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62b7000 b632c000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6336000 b6350000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6358000 b638c000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6395000 b6468000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6474000 b6484000 r-xp /lib/libresolv-2.13.so
b6488000 b64a0000 r-xp /usr/lib/liblzma.so.5.0.3
b64a8000 b64ab000 r-xp /lib/libcap.so.2.21
b64b3000 b64e2000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b64ea000 b64eb000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b64f4000 b64fa000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6502000 b6519000 r-xp /usr/lib/liblua-5.1.so
b6522000 b6529000 r-xp /usr/lib/libembryo.so.1.7.99
b6531000 b6537000 r-xp /lib/librt-2.13.so
b6540000 b6596000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65a4000 b65fa000 r-xp /usr/lib/libfreetype.so.6.11.3
b6606000 b662e000 r-xp /usr/lib/libfontconfig.so.1.8.0
b662f000 b6674000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b667d000 b6690000 r-xp /usr/lib/libfribidi.so.0.3.1
b6698000 b66b2000 r-xp /usr/lib/libecore_con.so.1.7.99
b66bc000 b66c5000 r-xp /usr/lib/libedbus.so.1.7.99
b66cd000 b671d000 r-xp /usr/lib/libecore_x.so.1.7.99
b671f000 b6728000 r-xp /usr/lib/libvconf.so.0.2.45
b6730000 b6741000 r-xp /usr/lib/libecore_input.so.1.7.99
b6749000 b674e000 r-xp /usr/lib/libecore_file.so.1.7.99
b6756000 b6778000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6781000 b67c2000 r-xp /usr/lib/libeina.so.1.7.99
b67cb000 b67e4000 r-xp /usr/lib/libeet.so.1.7.99
b67f5000 b685e000 r-xp /lib/libm-2.13.so
b6867000 b686d000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6876000 b6877000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b687f000 b68a2000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68aa000 b68af000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68b7000 b68e1000 r-xp /usr/lib/libdbus-1.so.3.8.12
b68ea000 b6901000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6909000 b6914000 r-xp /lib/libunwind.so.8.0.1
b6941000 b695f000 r-xp /usr/lib/libsystemd.so.0.4.0
b6969000 b6a8d000 r-xp /lib/libc-2.13.so
b6a9b000 b6aa3000 r-xp /lib/libgcc_s-4.6.so.1
b6aa4000 b6aa8000 r-xp /usr/lib/libsmack.so.1.0.0
b6ab1000 b6ab7000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6abf000 b6b8f000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6b90000 b6bee000 r-xp /usr/lib/libedje.so.1.7.99
b6bf8000 b6c0f000 r-xp /usr/lib/libecore.so.1.7.99
b6c26000 b6cf4000 r-xp /usr/lib/libevas.so.1.7.99
b6d1a000 b6e56000 r-xp /usr/lib/libelementary.so.1.7.99
b6e6d000 b6e81000 r-xp /lib/libpthread-2.13.so
b6e8c000 b6e8e000 r-xp /usr/lib/libdlog.so.0.0.0
b6e96000 b6e99000 r-xp /usr/lib/libbundle.so.0.1.22
b6ea1000 b6ea3000 r-xp /lib/libdl-2.13.so
b6eac000 b6eb9000 r-xp /usr/lib/libaul.so.0.1.0
b6ecb000 b6ed1000 r-xp /usr/lib/libappcore-efl.so.1.1
b6eda000 b6ede000 r-xp /usr/lib/libsys-assert.so
b6ee7000 b6f04000 r-xp /lib/ld-2.13.so
b6f0d000 b6f12000 r-xp /usr/bin/launchpad-loader
b6f25000 b731f000 rw-p [heap]
be884000 be8a5000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2363)
Call Stack Count: 1
 0: realloc + 0x4c (0xb69db6f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
ORT(  925): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.windicator:org.tizen.idled.ReservedApp]
04-27 11:07:16.919+0700 I/MESSAGE_PORT(  925): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 11:07:16.919+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 11:07:16.919+0700 I/MESSAGE_PORT(  925): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:07:16.919+0700 W/STARTER ( 1141): clock-mgr.c: _on_lcd_signal_receive_cb(1271) > [_on_lcd_signal_receive_cb:1271] _on_lcd_signal_receive_cb, 1271, Post LCD on by [powerkey]
04-27 11:07:16.919+0700 W/STARTER ( 1141): clock-mgr.c: _post_lcd_on(1059) > [_post_lcd_on:1059] LCD ON as reserved app[(null)] alpm mode[0]
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: windicator_connection_resume(2158) > [windicator_connection_resume:2158] 
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: windicator_connection_resume(2223) > [windicator_connection_resume:2223] primary rssi level : (0), s_info.rssi_level : (8) / primary svc type : (1)
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_changed_cb(1924) > [_rssi_changed_cb:1924] roaming status : 0
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_changed_cb(1933) > [_rssi_changed_cb:1933] svc type : 1
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_changed_cb(1968) > [_rssi_changed_cb:1968] Rssi level has already been 0!, Don't need to show rssi down animation. Just Show No Signal icon
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_icon_set(1124) > [_rssi_icon_set:1124] RSSI level : 8/5, (0xb833a650)
04-27 11:07:16.929+0700 E/W_INDICATOR( 1142): windicator_connection.c: _rssi_icon_set(1147) > [_rssi_icon_set:1147] Set RSSI SHOW sw.icon_0 (rssi_level : 8) (rssi_hide : 0)(b833a650)
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_icon_set(1215) > [_rssi_icon_set:1215] NETWORK_ATT or NETWORK_TMB : there is no roaming icon
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_icon_set(1217) > [_rssi_icon_set:1217] rssi name : set_rssi_verizon_No_signal (0xb833a650)
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _tapi_changed_cb(2115) > [_tapi_changed_cb:2115] modem_power : 0
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_type_changed_cb(1324) > [_connection_type_changed_cb:1324] wifi state : 2
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_type_changed_cb(1327) > [_connection_type_changed_cb:1327] Show wifi icon!
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 3
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 15 / signal : type_wifi_connected_03
04-27 11:07:16.929+0700 E/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 15) / (hide : 0)
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 11:07:16.929+0700 E/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-27 11:07:16.929+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 11:07:16.929+0700 E/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 11:07:16.979+0700 W/W_HOME  ( 1251): gesture.c: _manual_render_disable_timer_cb(145) > timeout callback expired
04-27 11:07:16.979+0700 W/W_HOME  ( 1251): gesture.c: _manual_render_enable(138) > 0
04-27 11:07:16.979+0700 W/W_HOME  ( 1251): gesture.c: _manual_render_cancel_schedule(226) > cancel schedule, manual render
04-27 11:07:16.999+0700 W/W_INDICATOR( 1142): windicator_dbus.c: _msg_reserved_app_cb(341) > [_msg_reserved_app_cb:341] Moment view is already shown or call is enabled. moment view [0]
04-27 11:07:17.259+0700 I/CAPI_NETWORK_CONNECTION( 2363): connection.c: connection_create(453) > New handle created[0xb70ff488]
04-27 11:07:17.309+0700 I/CAPI_NETWORK_CONNECTION( 2363): connection.c: connection_destroy(471) > Destroy handle: 0xb70ff488
04-27 11:07:17.319+0700 E/EFL     ( 2363): <2363> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:17.339+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7053810 : elm_genlist] mx(0), my(272), minx(0), miny(0), px(0), py(0)
04-27 11:07:17.339+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7053810 : elm_genlist] cw(360), ch(632), pw(360), ph(360)
04-27 11:07:19.259+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-27 11:07:19.259+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-27 11:07:19.259+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
04-27 11:07:19.259+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
04-27 11:07:19.259+0700 E/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
04-27 11:07:19.259+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 11:07:19.259+0700 E/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 11:07:19.259+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-27 11:07:20.059+0700 E/EFL     ( 2363): ecore_x<2363> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=152812 button=1
04-27 11:07:20.109+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.109+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.139+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.139+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.149+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.149+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.179+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.179+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.189+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.189+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.199+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.199+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.209+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.209+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.229+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.229+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.249+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.249+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.259+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.259+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.279+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] mouse move
04-27 11:07:20.279+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7053810 : elm_genlist] hold(0), freeze(0)
04-27 11:07:20.289+0700 E/EFL     ( 2363): ecore_x<2363> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=153040 button=1
04-27 11:07:20.509+0700 E/GL SELECTED( 2363): ID GL : TRM1
04-27 11:07:20.889+0700 E/EFL     ( 2363): ecore_x<2363> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=153639 button=1
04-27 11:07:21.049+0700 E/EFL     ( 2363): ecore_x<2363> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=153803 button=1
04-27 11:07:21.049+0700 I/efl-extension( 2363): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:21.059+0700 I/efl-extension( 2363): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:21.059+0700 I/efl-extension( 2363): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb70f8578, elm_image, _activated_obj : 0xb70623c8, activated : 1
04-27 11:07:21.059+0700 I/efl-extension( 2363): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:07:21.059+0700 E/EFL     ( 2363): elementary<2363> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:07:21.069+0700 E/EFL     ( 2363): elementary<2363> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb70f89e0) will be pushed
04-27 11:07:21.069+0700 E/EFL     ( 2363): <2363> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:21.069+0700 E/EFL     ( 2363): <2363> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:21.069+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb712d148 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:21.069+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb712d148 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:21.089+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb712d148 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:21.089+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb712d148 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-27 11:07:21.109+0700 E/EFL     ( 2363): elementary<2363> elc_naviframe.c:2796 _push_transition_cb() item(0xb70f89e0) will transition
04-27 11:07:21.519+0700 E/EFL     ( 2363): elementary<2363> elc_naviframe.c:1193 _on_item_push_finished() item(0xb70b74f0) transition finished
04-27 11:07:21.519+0700 E/EFL     ( 2363): elementary<2363> elc_naviframe.c:1218 _on_item_show_finished() item(0xb70f89e0) transition finished
04-27 11:07:21.559+0700 E/EFL     ( 2363): <2363> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:22.049+0700 E/EFL     (  893): ecore_x<893> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3200002 time=153843
04-27 11:07:22.049+0700 E/EFL     ( 2363): ecore_x<2363> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=153843
04-27 11:07:22.049+0700 E/EFL     (  893): ecore_x<893> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=153843
04-27 11:07:23.199+0700 I/CAPI_NETWORK_CONNECTION( 2363): connection.c: connection_create(453) > New handle created[0xb3e01910]
04-27 11:07:23.229+0700 E/NOTI    ( 2363): 67026
04-27 11:07:23.229+0700 E/NOTIF ID( 2363): 40198
04-27 11:07:23.229+0700 E/problem ( 2363): Dirty
04-27 11:07:23.229+0700 E/problem_desc( 2363): 19A-Fender Lh Fender Dirty & Stain
04-27 11:07:23.239+0700 E/NOTIF ID( 2363): 40199
04-27 11:07:23.239+0700 E/problem ( 2363): Dirty
04-27 11:07:23.239+0700 E/problem_desc( 2363): 19A-Fender Rh Fender Dirty & Stain
04-27 11:07:23.249+0700 E/BODY NO ( 2363): 67026
04-27 11:07:23.249+0700 E/JSON PARSING( 2363): �Ʊ�7��
04-27 11:07:23.269+0700 I/AUL     ( 1146): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 11:07:23.279+0700 I/AUL     ( 1146): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 11:07:23.309+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:23.329+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:23.329+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1080, op_type : 1  //insert = 1, update = 2, delete = 3
04-27 11:07:23.349+0700 I/CAPI_NETWORK_CONNECTION( 2363): connection.c: connection_create(453) > New handle created[0xb708ab78]
04-27 11:07:23.349+0700 E/wnoti-service( 1443): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-27 11:07:23.349+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-27 11:07:23.349+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-27 11:07:23.359+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-27 11:07:23.359+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: (null), err: 0
04-27 11:07:23.369+0700 E/wnoti-service( 1443): wnoti-native-client.c: _insert_notification(1208) > fail to get metadata_value, ret : -2
04-27 11:07:23.369+0700 E/wnoti-service( 1443): wnoti-db-server.c: wnoti_update_panel(2712) > id : -1016, source : 1
04-27 11:07:23.369+0700 E/wnoti-service( 1443): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-27 11:07:23.369+0700 E/wnoti-service( 1443): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1080, application_id: -1016
04-27 11:07:23.389+0700 E/wnoti-service( 1443): wnoti-db-server.c: _wnoti_update_category(861) > Reuse category, application_id : -1016
04-27 11:07:23.389+0700 I/CAPI_NETWORK_CONNECTION( 2363): connection.c: connection_destroy(471) > Destroy handle: 0xb708ab78
04-27 11:07:23.389+0700 I/CAPI_NETWORK_CONNECTION( 2363): connection.c: connection_destroy(471) > Destroy handle: 0xb3e01910
04-27 11:07:23.389+0700 E/wnoti-service( 1443): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 0, count : 1
04-27 11:07:23.399+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:23.409+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:23.429+0700 E/APPS    ( 1251): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-27 11:07:23.439+0700 I/wnoti-service( 1443): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-27 11:07:23.439+0700 E/wnoti-service( 1443): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-27 11:07:23.439+0700 E/wnoti-service( 1443): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-27 11:07:23.439+0700 E/wnoti-proxy( 1877): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1877, caller_id : 0, listener_type : 0
04-27 11:07:23.439+0700 E/wnoti-proxy( 1251): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1251, caller_id : 0, listener_type : 0
04-27 11:07:23.439+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-27 11:07:23.449+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:23.449+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 1877
04-27 11:07:23.449+0700 E/wnoti-service( 1443): wnoti-sap-client.c: on_timer(275) > is_popup_running: 1, ret: 0
04-27 11:07:23.449+0700 E/wnoti-service( 1443): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524802043, g_use_aul_launch : 1524802043
04-27 11:07:23.589+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_service_changed_cb(243) > Handle this change type in idler.
04-27 11:07:23.589+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-27 11:07:23.589+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-27 11:07:23.589+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-27 11:07:23.599+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1140, is_duplicated : 0
04-27 11:07:23.599+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1311) > view_type : 1, turn_screen_on : 1, allow_gesture : 1, is_used_popup : 0, feedback : 2
04-27 11:07:23.609+0700 E/wnoti-proxy( 1877): wnoti-client.c: _wnoti_parse_extra_sub(373) > JSON_IS_NULL
04-27 11:07:23.609+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(226) > application_name: Real Feed Back, application_id: -1016, category_id: 114, time: 1524802043, launch_app_id: (null), bg_image: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, extracted_icon_color: -3139560, disble_block_app_action: 0, support_large_icon 0
04-27 11:07:23.609+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(236) > noti_type: 1
04-27 11:07:23.609+0700 I/wnotibp ( 1877): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1140, noti_type: 1
04-27 11:07:23.609+0700 I/wnotibp ( 1877): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 0, content_id: 0, notification_id: 0
04-27 11:07:23.609+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_convert_alert_data(67) > alert_type: 4, app_feedback_type: 2, popup_view_style: 0, feedback_pattern_app: -1
04-27 11:07:23.609+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 0, new_list count : 1
04-27 11:07:23.609+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_notification_change_cb(600) > [1,1140]
04-27 11:07:23.619+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:23.619+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-27 11:07:23.619+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: _common_app_id_from_win(152) > 2363
04-27 11:07:23.629+0700 E/WMS     ( 1008): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-27 11:07:23.639+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_check_env_condition(437) > focus app is com.toyota.realtimefeedback, 0
04-27 11:07:23.639+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_add_notification(991) > Add noti_queue [1140, 0]
04-27 11:07:23.639+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1139
04-27 11:07:23.639+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 1
04-27 11:07:23.639+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(73) > ::APP:: CHECK STATE : 8, 0, (null)
04-27 11:07:23.639+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:07:23.639+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(79) > ::APP:: CHECK DATA : 1 1 0000
04-27 11:07:23.639+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:07:23.659+0700 W/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-27 11:07:23.659+0700 I/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_active_app_id(1010) > [2363]
04-27 11:07:23.669+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:07:23.679+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1178) > [1140, 1, 0, 3, 0000]
04-27 11:07:23.679+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1179) > [0, 1, 0]
04-27 11:07:23.679+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1180) > [1, 0, 0, 0]
04-27 11:07:23.679+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-27 11:07:23.679+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_detail_layout(3707) > wnotiboard_popup_vi_type: 2
04-27 11:07:23.679+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_detail_layout(3712) > (1140, 1140)
04-27 11:07:23.679+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-27 11:07:23.699+0700 I/efl-extension( 1877): efl_extension_circle_surface.c: eext_circle_surface_layout_add(1290) > Put the surface[0xaf854878]'s widget[0xb0b4a228] to layout widget[0xb0b30850]
04-27 11:07:23.709+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_genlist(3639) > (1140, 1140)
04-27 11:07:23.709+0700 I/efl-extension( 1877): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:23.719+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_card_data(3073) > 0xb28f0bf8, 0xb28f0a80, 0xb28f0a80
04-27 11:07:23.729+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xaf831600 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:23.729+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xaf831600 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:23.749+0700 E/EFL     ( 1877): elementary<1877> elm_genlist.c:7236 elm_genlist_item_item_class_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-27 11:07:23.749+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:07:23.749+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:07:23.749+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:07:23.749+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:07:23.749+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: stack.separator
04-27 11:07:23.749+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:07:23.749+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.749+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.749+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.749+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.749+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.749+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.749+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.749+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.749+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.759+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 11:07:23.759+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.759+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.759+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.759+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.759+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.759+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.759+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.759+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.759+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.759+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:23.769+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:23.769+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:07:23.769+0700 E/EFL     ( 1877): elementary<1877> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 11:07:23.769+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(224) > can't get layout
04-27 11:07:23.769+0700 E/EFL     ( 1877): elementary<1877> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 11:07:23.769+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:23.769+0700 I/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1140, 114, -1016.
04-27 11:07:23.789+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:07:23.789+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:07:23.789+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:07:23.789+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:07:23.799+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:07:23.799+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:07:23.799+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:07:23.799+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:07:23.799+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.799+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.799+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.799+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.799+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.799+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.799+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.799+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.799+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.799+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 11:07:23.809+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.809+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.809+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.809+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.809+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.809+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.809+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:23.809+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:23.809+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:23.809+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:23.809+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:23.809+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:07:23.809+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 11:07:23.809+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:23.809+0700 I/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1140, 114, -1016.
04-27 11:07:23.819+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xaf831600 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:23.819+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xaf831600 : elm_genlist] cw(360), ch(360), pw(360), ph(360)
04-27 11:07:23.829+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xaf831600 : elm_genlist] mx(0), my(52), minx(0), miny(0), px(0), py(0)
04-27 11:07:23.829+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xaf831600 : elm_genlist] cw(360), ch(412), pw(360), ph(360)
04-27 11:07:23.839+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: wnbp_view_draw_small_view(4060) > ::UI:: window type is changed by unknown causes
04-27 11:07:23.859+0700 W/APP_CORE( 1877): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3000009
04-27 11:07:23.899+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(263) > fully_obscured: 0, 0
04-27 11:07:23.899+0700 I/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x3000009 0x300000d 0x3000009]
04-27 11:07:23.899+0700 I/APP_CORE( 1877): appcore-efl.c: __do_app(453) > [APP 1877] Event: RESUME State: PAUSED
04-27 11:07:23.899+0700 I/CAPI_APPFW_APPLICATION( 1877): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-27 11:07:23.899+0700 I/wnotibp ( 1877): wnotiboard-popup.c: _popup_app_resume(229) > 
04-27 11:07:23.899+0700 I/GATE    ( 1877): <GATE-M>APP_FULLY_LOADED_wnotiboard-popup</GATE-M>
04-27 11:07:23.899+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1877] goes to (3)
04-27 11:07:23.899+0700 W/AUL_AMD (  929): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 11:07:23.899+0700 W/AUL_AMD (  929): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-27 11:07:23.899+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.wnotiboard-popup) pid(1877) status(fg) type(uiapp)
04-27 11:07:23.909+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2809) > ::UI:: VI TYPE : 2
04-27 11:07:23.909+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:07:23.909+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2816) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 11:07:23.909+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:07:23.909+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(284) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 11:07:23.909+0700 W/TIZEN_N_RECORDER( 1877): recorder_product.c: recorder_is_in_recording(82) > pid =/proc/0 , state =0, alive=0
04-27 11:07:23.909+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(298) > ::APP:: application_id: -1016, is_disaster: 0, disaster_info: (null), app_feedback_type: 2, feedback_pattern_app: -1, is_recording: 0
04-27 11:07:23.909+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(307) > Gear side feedback setting is_sound_on: 0, is_vibration_on: 1, is_vibrate_when_noti_on: 0
04-27 11:07:23.909+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(341) > ::APP:: Determined feedback: sound 0, vibration: 1
04-27 11:07:23.919+0700 I/wnotib  ( 1877): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 13, ret: -3, request_id: 0
04-27 11:07:23.919+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_set_smart_relay(501) > Set the smart relay for 0, 114, -1016, Real Feed Back
04-27 11:07:23.919+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:07:23.919+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_turn_on_lcd(534) > [4, 0, 1, 0]
04-27 11:07:23.939+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(585) > ::UI:: [[[ ===> already [small popup] is LOCK, 0010 ]]]
04-27 11:07:23.939+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_sub_popup_show_animator_pre_cb(1781) > ::UI:: start showing animation
04-27 11:07:24.039+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1016) > _query_step failed(NO ROW)
04-27 11:07:24.049+0700 E/wnoti-service( 1443): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 2
04-27 11:07:24.049+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-27 11:07:24.059+0700 E/wnoti-proxy( 1877): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-27 11:07:24.059+0700 E/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(311) > ::DATA:: No categories available.
04-27 11:07:24.059+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 1, new_list count : 0
04-27 11:07:24.249+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_sub_popup_show_animator_cb(1684) > ::UI:: end show animation
04-27 11:07:24.249+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-27 11:07:24.649+0700 E/EFL     ( 2363): ecore_x<2363> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=157404 button=1
04-27 11:07:24.649+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.659+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.659+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] hold(0), freeze(0)
04-27 11:07:24.669+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.669+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] hold(0), freeze(0)
04-27 11:07:24.679+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.679+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] hold(0), freeze(0)
04-27 11:07:24.689+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.689+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] hold(0), freeze(0)
04-27 11:07:24.699+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.699+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] hold(0), freeze(0)
04-27 11:07:24.749+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.749+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] hold(0), freeze(0)
04-27 11:07:24.769+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.769+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] hold(0), freeze(0)
04-27 11:07:24.779+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] mouse move
04-27 11:07:24.779+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb712d148 : elm_genlist] hold(0), freeze(0)
04-27 11:07:24.779+0700 E/EFL     ( 2363): ecore_x<2363> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=157536 button=1
04-27 11:07:24.819+0700 I/efl-extension( 2363): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:24.819+0700 I/efl-extension( 2363): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:24.819+0700 I/efl-extension( 2363): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7119d88, elm_image, _activated_obj : 0xb70f8578, activated : 1
04-27 11:07:24.819+0700 I/efl-extension( 2363): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:07:24.829+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7118028 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-27 11:07:24.829+0700 E/EFL     ( 2363): elementary<2363> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7118028 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-27 11:07:25.429+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1251] goes to (3)
04-27 11:07:25.429+0700 E/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1251)'s state(3)
04-27 11:07:25.429+0700 W/AUL_AMD (  929): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 11:07:25.429+0700 W/AUL_AMD (  929): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-27 11:07:25.429+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1251) status(fg) type(uiapp)
04-27 11:07:25.519+0700 W/AUL_PAD ( 1876): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2363 pgid = 2363
04-27 11:07:25.519+0700 W/AUL_PAD ( 1876): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 11
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(0)
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): event_manager.c: _window_visibility_cb(470) > state: 0 -> 1
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:6, app_state:2 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(0)
04-27 11:07:25.539+0700 I/APP_CORE( 1251): appcore-efl.c: __do_app(453) > [APP 1251] Event: RESUME State: PAUSED
04-27 11:07:25.539+0700 I/CAPI_APPFW_APPLICATION( 1251): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): main.c: _appcore_resume_cb(480) > appcore resume
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): event_manager.c: _app_resume_cb(373) > state: 2 -> 1
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): main.c: home_resume(528) > journal_multimedia_screen_loaded_home called
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): main.c: home_resume(532) > clock/widget resumed
04-27 11:07:25.539+0700 E/W_HOME  ( 1251): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
04-27 11:07:25.539+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:07:25.549+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 11:07:25.549+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 11:07:25.549+0700 I/GATE    ( 1251): <GATE-M>APP_FULLY_LOADED_w-home</GATE-M>
04-27 11:07:25.549+0700 I/wnotib  ( 1251): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 0
04-27 11:07:25.549+0700 E/wnotib  ( 1251): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-27 11:07:25.549+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(958) > Do the postponed update job with is_for_VI: 0, postponed_for_VI: 0.
04-27 11:07:25.549+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(444) > idler for type: 0
04-27 11:07:25.589+0700 W/CRASH_MANAGER( 2399): worker.c: worker_job(1205) > 1102363726561152480204
