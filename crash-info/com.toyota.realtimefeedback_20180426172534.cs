S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2023
Date: 2018-04-26 17:25:34+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2023, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb5907445, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb84ee888, r7   = 0xbe91c1d0
r8   = 0x000186a0, r9   = 0xb80f17d8
r10  = 0xb6cdeb10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbe91c148
lr   = 0xb5907445, pc   = 0xb6a946f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      2996 KB
Buffers:      9796 KB
Cached:     113384 KB
VmPeak:     107948 KB
VmSize:     107404 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       28708 KB
VmRSS:       28708 KB
VmData:      47172 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          68 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2023 TID = 2023
2023 2049 2052 2053 

Maps Information
b1512000 b1515000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b1613000 b1e91000 rw-p [stack:2053]
b1e91000 b1e95000 r-xp /usr/lib/libogg.so.0.7.1
b1e9d000 b1ebf000 r-xp /usr/lib/libvorbis.so.0.4.3
b1ec7000 b1f0e000 r-xp /usr/lib/libsndfile.so.1.0.26
b1f1a000 b1f63000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1f6c000 b1f71000 r-xp /usr/lib/libjson.so.0.0.1
b3812000 b3918000 r-xp /usr/lib/libicuuc.so.57.1
b392e000 b3ab6000 r-xp /usr/lib/libicui18n.so.57.1
b3ac6000 b3ad3000 r-xp /usr/lib/libail.so.0.1.0
b3adc000 b3adf000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3ae7000 b3b1f000 r-xp /usr/lib/libpulse.so.0.16.2
b3b20000 b3b23000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3b2b000 b3b8c000 r-xp /usr/lib/libasound.so.2.0.0
b3b96000 b3baf000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3bb8000 b3bbc000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3bc4000 b3bcf000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3bdc000 b3be0000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3be9000 b3c01000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3c12000 b3c19000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3d94000 b3d9f000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3da7000 b3da9000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3db1000 b3db2000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3eee000 b3f75000 rw-s anon_inode:dmabuf
b3f76000 b4775000 rw-p [stack:2052]
b4a02000 b4a03000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4a0b000 b4a13000 r-xp /usr/lib/libfeedback.so.0.1.4
b4a23000 b4a24000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b4a5a000 b4a5b000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b62000 b5361000 rw-p [stack:2049]
b5361000 b5363000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b536b000 b5382000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b538f000 b5391000 r-xp /usr/lib/libdri2.so.0.0.0
b5399000 b53a4000 r-xp /usr/lib/libtbm.so.1.0.0
b53ac000 b53b4000 r-xp /usr/lib/libdrm.so.2.4.0
b53bc000 b53be000 r-xp /usr/lib/libgenlock.so
b53c6000 b53cb000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b53d3000 b53de000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b55e7000 b56b1000 r-xp /usr/lib/libCOREGL.so.4.0
b56c2000 b56d2000 r-xp /usr/lib/libmdm-common.so.1.1.25
b56da000 b56e0000 r-xp /usr/lib/libxcb-render.so.0.0.0
b56e8000 b56e9000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b56f2000 b56f5000 r-xp /usr/lib/libEGL.so.1.4
b56fd000 b570b000 r-xp /usr/lib/libGLESv2.so.2.0
b5714000 b575d000 r-xp /usr/lib/libmdm.so.1.2.70
b5766000 b576c000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5774000 b577d000 r-xp /usr/lib/libcom-core.so.0.0.1
b5786000 b583e000 r-xp /usr/lib/libcairo.so.2.11200.14
b5849000 b5862000 r-xp /usr/lib/libnetwork.so.0.0.0
b586a000 b5876000 r-xp /usr/lib/libnotification.so.0.1.0
b587f000 b588e000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b5897000 b58b8000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58c0000 b58c5000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58cd000 b58d2000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b58da000 b58ea000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b58f2000 b58fa000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5902000 b590c000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5ab1000 b5abb000 r-xp /lib/libnss_files-2.13.so
b5ac4000 b5b93000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5ba9000 b5bcd000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5bd6000 b5bdc000 r-xp /usr/lib/libappsvc.so.0.1.0
b5be4000 b5be8000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5bf5000 b5c00000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5c08000 b5c0a000 r-xp /usr/lib/libiniparser.so.0
b5c13000 b5c18000 r-xp /usr/lib/libappcore-common.so.1.1
b5c20000 b5c22000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c2b000 b5c2f000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c3c000 b5c3e000 r-xp /usr/lib/libXau.so.6.0.0
b5c46000 b5c4d000 r-xp /lib/libcrypt-2.13.so
b5c7d000 b5c7f000 r-xp /usr/lib/libiri.so
b5c88000 b5e1a000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e3b000 b5e82000 r-xp /usr/lib/libssl.so.1.0.0
b5e8e000 b5ebc000 r-xp /usr/lib/libidn.so.11.5.44
b5ec4000 b5ecd000 r-xp /usr/lib/libcares.so.2.1.0
b5ed7000 b5eea000 r-xp /usr/lib/libxcb.so.1.1.0
b5ef3000 b5ef6000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5efe000 b5f00000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5f09000 b5fd5000 r-xp /usr/lib/libxml2.so.2.7.8
b5fe2000 b5fe4000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5fed000 b5ff2000 r-xp /usr/lib/libffi.so.5.0.10
b5ffa000 b5ffb000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b6003000 b6006000 r-xp /lib/libattr.so.1.1.0
b600e000 b60a2000 r-xp /usr/lib/libstdc++.so.6.0.16
b60b5000 b60d2000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b60dc000 b60f4000 r-xp /usr/lib/libpng12.so.0.50.0
b60fc000 b6112000 r-xp /lib/libexpat.so.1.6.0
b611c000 b6160000 r-xp /usr/lib/libcurl.so.4.3.0
b6169000 b6173000 r-xp /usr/lib/libXext.so.6.4.0
b617d000 b6181000 r-xp /usr/lib/libXtst.so.6.1.0
b6189000 b618f000 r-xp /usr/lib/libXrender.so.1.3.0
b6197000 b619d000 r-xp /usr/lib/libXrandr.so.2.2.0
b61a5000 b61a6000 r-xp /usr/lib/libXinerama.so.1.0.0
b61af000 b61b8000 r-xp /usr/lib/libXi.so.6.1.0
b61c0000 b61c3000 r-xp /usr/lib/libXfixes.so.3.1.0
b61cc000 b61ce000 r-xp /usr/lib/libXgesture.so.7.0.0
b61d6000 b61d8000 r-xp /usr/lib/libXcomposite.so.1.0.0
b61e0000 b61e2000 r-xp /usr/lib/libXdamage.so.1.1.0
b61ea000 b61f1000 r-xp /usr/lib/libXcursor.so.1.0.2
b61f9000 b61fc000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6205000 b6209000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6212000 b6217000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6220000 b6301000 r-xp /usr/lib/libX11.so.6.3.0
b630c000 b632f000 r-xp /usr/lib/libjpeg.so.8.0.2
b6347000 b635d000 r-xp /lib/libz.so.1.2.5
b6366000 b6368000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6370000 b63e5000 r-xp /usr/lib/libsqlite3.so.0.8.6
b63ef000 b6409000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6411000 b6445000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b644e000 b6521000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b652d000 b653d000 r-xp /lib/libresolv-2.13.so
b6541000 b6559000 r-xp /usr/lib/liblzma.so.5.0.3
b6561000 b6564000 r-xp /lib/libcap.so.2.21
b656c000 b659b000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b65a3000 b65a4000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b65ad000 b65b3000 r-xp /usr/lib/libecore_imf.so.1.7.99
b65bb000 b65d2000 r-xp /usr/lib/liblua-5.1.so
b65db000 b65e2000 r-xp /usr/lib/libembryo.so.1.7.99
b65ea000 b65f0000 r-xp /lib/librt-2.13.so
b65f9000 b664f000 r-xp /usr/lib/libpixman-1.so.0.28.2
b665d000 b66b3000 r-xp /usr/lib/libfreetype.so.6.11.3
b66bf000 b66e7000 r-xp /usr/lib/libfontconfig.so.1.8.0
b66e8000 b672d000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6736000 b6749000 r-xp /usr/lib/libfribidi.so.0.3.1
b6751000 b676b000 r-xp /usr/lib/libecore_con.so.1.7.99
b6775000 b677e000 r-xp /usr/lib/libedbus.so.1.7.99
b6786000 b67d6000 r-xp /usr/lib/libecore_x.so.1.7.99
b67d8000 b67e1000 r-xp /usr/lib/libvconf.so.0.2.45
b67e9000 b67fa000 r-xp /usr/lib/libecore_input.so.1.7.99
b6802000 b6807000 r-xp /usr/lib/libecore_file.so.1.7.99
b680f000 b6831000 r-xp /usr/lib/libecore_evas.so.1.7.99
b683a000 b687b000 r-xp /usr/lib/libeina.so.1.7.99
b6884000 b689d000 r-xp /usr/lib/libeet.so.1.7.99
b68ae000 b6917000 r-xp /lib/libm-2.13.so
b6920000 b6926000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b692f000 b6930000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6938000 b695b000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6963000 b6968000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6970000 b699a000 r-xp /usr/lib/libdbus-1.so.3.8.12
b69a3000 b69ba000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69c2000 b69cd000 r-xp /lib/libunwind.so.8.0.1
b69fa000 b6a18000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a22000 b6b46000 r-xp /lib/libc-2.13.so
b6b54000 b6b5c000 r-xp /lib/libgcc_s-4.6.so.1
b6b5d000 b6b61000 r-xp /usr/lib/libsmack.so.1.0.0
b6b6a000 b6b70000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b78000 b6c48000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c49000 b6ca7000 r-xp /usr/lib/libedje.so.1.7.99
b6cb1000 b6cc8000 r-xp /usr/lib/libecore.so.1.7.99
b6cdf000 b6dad000 r-xp /usr/lib/libevas.so.1.7.99
b6dd3000 b6f0f000 r-xp /usr/lib/libelementary.so.1.7.99
b6f26000 b6f3a000 r-xp /lib/libpthread-2.13.so
b6f45000 b6f47000 r-xp /usr/lib/libdlog.so.0.0.0
b6f4f000 b6f52000 r-xp /usr/lib/libbundle.so.0.1.22
b6f5a000 b6f5c000 r-xp /lib/libdl-2.13.so
b6f65000 b6f72000 r-xp /usr/lib/libaul.so.0.1.0
b6f84000 b6f8a000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f93000 b6f97000 r-xp /usr/lib/libsys-assert.so
b6fa0000 b6fbd000 r-xp /lib/ld-2.13.so
b6fc6000 b6fcb000 r-xp /usr/bin/launchpad-loader
b80b9000 b8c67000 rw-p [heap]
be8fc000 be91d000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2023)
Call Stack Count: 1
 0: realloc + 0x4c (0xb6a946f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
04-26 17:25:31.319+0700 E/wnoti-service( 1447): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-26 17:25:31.319+0700 E/wnoti-service( 1447): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1071, application_id: -1016
04-26 17:25:31.329+0700 E/wnoti-service( 1447): wnoti-db-server.c: _wnoti_update_category(908) > re_table_id : 0
04-26 17:25:31.349+0700 E/wnoti-service( 1447): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 1, count : 2
04-26 17:25:31.349+0700 W/SHealthServiceCommon( 1815): NotificationServiceController.cpp: DeleteNotificationByPrivateId(928) > [1;40;33mdelete notification by private id = [1071][0;m
04-26 17:25:31.359+0700 I/wnoti-service( 1447): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-26 17:25:31.359+0700 E/wnoti-service( 1447): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-26 17:25:31.359+0700 E/wnoti-service( 1447): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-26 17:25:31.359+0700 E/wnoti-service( 1447): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-26 17:25:31.359+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1071, op_type : 3  //insert = 1, update = 2, delete = 3
04-26 17:25:31.359+0700 E/wnoti-service( 1447): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 1
04-26 17:25:31.359+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1722) > error_code from _get_native_application_info: 0
04-26 17:25:31.369+0700 E/wnoti-proxy( 1203): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1203, caller_id : 0, listener_type : 0
04-26 17:25:31.369+0700 I/AUL     ( 1125): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-26 17:25:31.379+0700 E/wnoti-service( 1447): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 1, count : 1
04-26 17:25:31.379+0700 E/wnoti-service( 1447): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-26 17:25:31.379+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 14
04-26 17:25:31.379+0700 I/AUL     ( 1125): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-26 17:25:31.389+0700 W/AUL_AMD (  969): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-26 17:25:31.389+0700 E/wnoti-service( 1447): wnoti-sap-client.c: on_timer(275) > is_popup_running: 0, ret: 0
04-26 17:25:31.389+0700 W/AUL     ( 1447): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.samsung.wnotiboard-popup)
04-26 17:25:31.389+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 0
04-26 17:25:31.399+0700 I/AUL_AMD (  969): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:25:31.409+0700 I/AUL_AMD (  969): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:25:31.409+0700 E/AUL_AMD (  969): amd_launch.c: _start_app(1772) > no caller appid info, ret: -1
04-26 17:25:31.409+0700 W/AUL_AMD (  969): amd_launch.c: _start_app(1782) > caller pid : 1447
04-26 17:25:31.419+0700 W/AUL_AMD (  969): amd_launch.c: _start_app(2218) > pad pid(-5)
04-26 17:25:31.419+0700 W/AUL_PAD ( 1909): launchpad.c: __launchpad_main_loop(611) > Launch on type-based process-pool
04-26 17:25:31.419+0700 W/AUL_PAD ( 1909): launchpad.c: __send_result_to_caller(272) > Check app launching
04-26 17:25:31.419+0700 W/AUL_PAD ( 2044): launchpad_loader.c: __candidate_process_prepare_exec(259) > [candidate] before __set_access
04-26 17:25:31.429+0700 W/AUL_PAD ( 2044): launchpad_loader.c: __candidate_process_prepare_exec(264) > [candidate] after __set_access
04-26 17:25:31.429+0700 W/AUL_PAD ( 2044): launchpad_loader.c: __candidate_process_launchpad_main_loop(414) > update argument
04-26 17:25:31.429+0700 W/AUL_PAD ( 2044): launchpad_loader.c: main(680) > [candidate] dlopen(com.samsung.wnotiboard-popup)
04-26 17:25:31.429+0700 I/CAPI_NETWORK_CONNECTION( 2023): connection.c: connection_create(453) > New handle created[0xb8608f90]
04-26 17:25:31.449+0700 I/CAPI_NETWORK_CONNECTION( 2023): connection.c: connection_destroy(471) > Destroy handle: 0xb8608f90
04-26 17:25:31.449+0700 I/CAPI_NETWORK_CONNECTION( 2023): connection.c: connection_destroy(471) > Destroy handle: 0xb8626198
04-26 17:25:31.449+0700 I/CAPI_NETWORK_CONNECTION( 2023): connection.c: connection_destroy(471) > Destroy handle: 0xb860e438
04-26 17:25:31.519+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-26 17:25:31.519+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-26 17:25:31.519+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-26 17:25:31.579+0700 I/efl-extension( 2044): efl_extension.c: eext_mod_init(40) > Init
04-26 17:25:31.579+0700 I/UXT     ( 2044): Uxt_ObjectManager.cpp: OnInitialized(753) > Initialized.
04-26 17:25:31.599+0700 W/AUL_PAD ( 2044): launchpad_loader.c: main(690) > [candidate] dlsym
04-26 17:25:31.599+0700 W/AUL_PAD ( 2044): launchpad_loader.c: main(694) > [candidate] dl_main(com.samsung.wnotiboard-popup)
04-26 17:25:31.599+0700 I/wnotibp ( 2044): wnotiboard-popup.c: main(293) > start main
04-26 17:25:31.599+0700 I/CAPI_APPFW_APPLICATION( 2044): app_main.c: app_efl_main(129) > app_efl_main
04-26 17:25:31.599+0700 I/CAPI_APPFW_APPLICATION( 2044): app_main.c: app_appcore_create(152) > app_appcore_create
04-26 17:25:31.619+0700 W/AUL     (  969): app_signal.c: aul_send_app_launch_request_signal(521) > aul_send_app_launch_request_signal app(com.samsung.wnotiboard-popup) pid(2044) type(uiapp) bg(0)
04-26 17:25:31.619+0700 W/AUL_AMD (  969): amd_status.c: __socket_monitor_cb(1277) > (2044) was created
04-26 17:25:31.619+0700 W/AUL     ( 1447): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2044)
04-26 17:25:31.619+0700 E/wnoti-service( 1447): wnoti-sap-client.c: on_timer(291) > Launching notification popup.
04-26 17:25:31.619+0700 E/wnoti-service( 1447): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524738331, g_use_aul_launch : 1524738331
04-26 17:25:31.629+0700 W/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-26 17:25:31.629+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: wnbp_ctrl_initialize(689) > initialize
04-26 17:25:31.629+0700 I/AUL     ( 1447): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:25:31.639+0700 W/STARTER ( 1122): pkg-monitor.c: _app_mgr_status_cb(400) > [_app_mgr_status_cb:400] Launch request [2044]
04-26 17:25:31.659+0700 I/AUL     ( 1447): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:25:31.659+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1072, op_type : 1  //insert = 1, update = 2, delete = 3
04-26 17:25:31.659+0700 E/wnoti-service( 1447): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 1
04-26 17:25:31.659+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-26 17:25:31.669+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-26 17:25:31.669+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-26 17:25:31.669+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: (null), err: 0
04-26 17:25:31.679+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_initialize(754) > Screen type: 2
04-26 17:25:31.689+0700 E/wnoti-service( 1447): wnoti-native-client.c: _insert_notification(1208) > fail to get metadata_value, ret : -2
04-26 17:25:31.699+0700 E/wnoti-service( 1447): wnoti-db-server.c: wnoti_update_panel(2712) > id : -1016, source : 1
04-26 17:25:31.699+0700 E/wnoti-service( 1447): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-26 17:25:31.699+0700 E/wnoti-service( 1447): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1072, application_id: -1016
04-26 17:25:31.699+0700 I/Adreno-EGL( 2044): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-26 17:25:31.699+0700 I/Adreno-EGL( 2044): OpenGL ES Shader Compiler Version: E031.24.00.16
04-26 17:25:31.699+0700 I/Adreno-EGL( 2044): Build Date: 09/02/15 Wed
04-26 17:25:31.699+0700 I/Adreno-EGL( 2044): Local Branch: 
04-26 17:25:31.699+0700 I/Adreno-EGL( 2044): Remote Branch: 
04-26 17:25:31.699+0700 I/Adreno-EGL( 2044): Local Patches: 
04-26 17:25:31.699+0700 I/Adreno-EGL( 2044): Reconstruct Branch: 
04-26 17:25:31.709+0700 E/wnoti-service( 1447): wnoti-db-server.c: _wnoti_update_category(861) > Reuse category, application_id : -1016
04-26 17:25:31.719+0700 E/wnoti-service( 1447): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 1, count : 2
04-26 17:25:31.729+0700 I/wnoti-service( 1447): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-26 17:25:31.729+0700 E/wnoti-service( 1447): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-26 17:25:31.729+0700 E/wnoti-service( 1447): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-26 17:25:31.729+0700 E/wnoti-proxy( 1203): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1203, caller_id : 0, listener_type : 0
04-26 17:25:31.729+0700 E/wnoti-service( 1447): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-26 17:25:31.729+0700 E/wnoti-service( 1447): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524738331, g_use_aul_launch : 1524738331
04-26 17:25:31.809+0700 I/CAPI_NETWORK_CONNECTION( 2023): connection.c: connection_create(453) > New handle created[0xb4938098]
04-26 17:25:31.829+0700 E/JSON PARSING( 2023): No data could be display
04-26 17:25:31.829+0700 E/JSON PARSING( 2023): �2�����;
04-26 17:25:31.859+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1072, op_type : 3  //insert = 1, update = 2, delete = 3
04-26 17:25:31.859+0700 E/wnoti-service( 1447): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 1
04-26 17:25:31.859+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1722) > error_code from _get_native_application_info: 0
04-26 17:25:31.869+0700 E/wnoti-service( 1447): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 1, count : 1
04-26 17:25:31.869+0700 E/wnoti-service( 1447): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-26 17:25:31.879+0700 W/SHealthServiceCommon( 1815): NotificationServiceController.cpp: DeleteNotificationByPrivateId(928) > [1;40;33mdelete notification by private id = [1072][0;m
04-26 17:25:31.879+0700 I/Adreno-EGL( 2044): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-26 17:25:31.879+0700 I/Adreno-EGL( 2044): OpenGL ES Shader Compiler Version: E031.24.00.16
04-26 17:25:31.879+0700 I/Adreno-EGL( 2044): Build Date: 09/02/15 Wed
04-26 17:25:31.879+0700 I/Adreno-EGL( 2044): Local Branch: 
04-26 17:25:31.879+0700 I/Adreno-EGL( 2044): Remote Branch: 
04-26 17:25:31.879+0700 I/Adreno-EGL( 2044): Local Patches: 
04-26 17:25:31.879+0700 I/Adreno-EGL( 2044): Reconstruct Branch: 
04-26 17:25:31.879+0700 I/AUL     ( 1125): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-26 17:25:31.879+0700 E/wnoti-service( 1447): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 0, 
04-26 17:25:31.879+0700 E/wnoti-proxy( 1203): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1203, caller_id : 0, listener_type : 0
04-26 17:25:31.879+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-26 17:25:31.879+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-26 17:25:31.879+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-26 17:25:31.889+0700 I/AUL     ( 1125): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-26 17:25:31.909+0700 I/Adreno-EGL( 2044): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-26 17:25:31.909+0700 I/Adreno-EGL( 2044): OpenGL ES Shader Compiler Version: E031.24.00.16
04-26 17:25:31.909+0700 I/Adreno-EGL( 2044): Build Date: 09/02/15 Wed
04-26 17:25:31.909+0700 I/Adreno-EGL( 2044): Local Branch: 
04-26 17:25:31.909+0700 I/Adreno-EGL( 2044): Remote Branch: 
04-26 17:25:31.909+0700 I/Adreno-EGL( 2044): Local Patches: 
04-26 17:25:31.909+0700 I/Adreno-EGL( 2044): Reconstruct Branch: 
04-26 17:25:31.929+0700 I/CAPI_NETWORK_CONNECTION( 2023): connection.c: connection_create(453) > New handle created[0xb49321b8]
04-26 17:25:31.949+0700 I/AUL     ( 1447): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:25:31.959+0700 I/CAPI_NETWORK_CONNECTION( 2023): connection.c: connection_destroy(471) > Destroy handle: 0xb49321b8
04-26 17:25:31.959+0700 I/CAPI_NETWORK_CONNECTION( 2023): connection.c: connection_destroy(471) > Destroy handle: 0xb4938098
04-26 17:25:31.979+0700 I/Adreno-EGL( 2044): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-26 17:25:31.979+0700 I/Adreno-EGL( 2044): OpenGL ES Shader Compiler Version: E031.24.00.16
04-26 17:25:31.979+0700 I/Adreno-EGL( 2044): Build Date: 09/02/15 Wed
04-26 17:25:31.979+0700 I/Adreno-EGL( 2044): Local Branch: 
04-26 17:25:31.979+0700 I/Adreno-EGL( 2044): Remote Branch: 
04-26 17:25:31.979+0700 I/Adreno-EGL( 2044): Local Patches: 
04-26 17:25:31.979+0700 I/Adreno-EGL( 2044): Reconstruct Branch: 
04-26 17:25:32.009+0700 I/AUL     ( 1447): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-26 17:25:32.009+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1073, op_type : 1  //insert = 1, update = 2, delete = 3
04-26 17:25:32.009+0700 E/wnoti-service( 1447): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 1
04-26 17:25:32.009+0700 E/wnoti-service( 1447): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-26 17:25:32.019+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-26 17:25:32.019+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-26 17:25:32.019+0700 E/wnoti-service( 1447): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/bg_red.png, err: 0
04-26 17:25:32.029+0700 E/wnoti-service( 1447): wnoti-native-client.c: _insert_notification(1208) > fail to get metadata_value, ret : -2
04-26 17:25:32.029+0700 E/wnoti-service( 1447): wnoti-db-server.c: wnoti_update_panel(2712) > id : -1016, source : 1
04-26 17:25:32.029+0700 E/wnoti-service( 1447): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-26 17:25:32.029+0700 E/wnoti-service( 1447): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1073, application_id: -1016
04-26 17:25:32.029+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 0
04-26 17:25:32.029+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-26 17:25:32.029+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-26 17:25:32.049+0700 E/wnoti-service( 1447): wnoti-db-server.c: _wnoti_update_category(861) > Reuse category, application_id : -1016
04-26 17:25:32.049+0700 E/wnoti-service( 1447): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 1, count : 2
04-26 17:25:32.059+0700 I/wnoti-service( 1447): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-26 17:25:32.059+0700 E/wnoti-service( 1447): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-26 17:25:32.059+0700 E/wnoti-service( 1447): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-26 17:25:32.059+0700 E/wnoti-service( 1447): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-26 17:25:32.059+0700 E/wnoti-service( 1447): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524738332, g_use_aul_launch : 1524738331
04-26 17:25:32.059+0700 E/wnoti-proxy( 1203): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1203, caller_id : 0, listener_type : 0
04-26 17:25:32.089+0700 E/EFL     ( 2044): elementary<2044> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8e7c830 in function: elm_win_resize_object_add, of type: 'elm_layout' when expecting type: 'elm_win'
04-26 17:25:32.119+0700 E/EFL     ( 2044): elementary<2044> elm_layout.c:1021 _elm_layout_smart_content_set() could not swallow 0xb8e7d9d0 into part 'elm.swallow.bg'
04-26 17:25:32.119+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_initialize(834) > Window width: 360.000000, height: 360.000000
04-26 17:25:32.119+0700 I/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_window_resize_cb(302) > geometry (0 0 360 360)
04-26 17:25:32.209+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-26 17:25:32.209+0700 I/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-26 17:25:32.209+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 0
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 3
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 4
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 5
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 6
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 7
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 8
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 11
04-26 17:25:32.269+0700 I/wnoti-proxy( 2044): wnoti.c: wnoti_register_listener(3343) > type : 10
04-26 17:25:32.269+0700 I/wnotibp ( 2044): wnotiboard-popup-dbus.c: wnbp_dbus_oom_priority_signal_send(569) > Sending OOM priority request signal
04-26 17:25:32.279+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: _common_app_id_from_win(152) > 2023
04-26 17:25:32.289+0700 W/AUL     ( 2044): launch.c: app_request_to_launchpad(284) > request cmd(7) to(com.samsung.wusvc)
04-26 17:25:32.289+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 7
04-26 17:25:32.289+0700 W/AUL_AMD (  969): amd_launch.c: _start_app(1782) > caller pid : 2044
04-26 17:25:32.289+0700 I/AUL_AMD (  969): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
04-26 17:25:32.299+0700 W/AUL_AMD (  969): amd_launch.c: __nofork_processing(1229) > __nofork_processing, cmd: 7, pid: 1720
04-26 17:25:32.299+0700 E/AUL_AMD (  969): amd_launch.c: __real_send(909) > send fail to client
04-26 17:25:32.299+0700 W/AUL_AMD (  969): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(1720), cmd(7)
04-26 17:25:32.299+0700 W/AUL     ( 2044): launch.c: app_request_to_launchpad(298) > request cmd(7) result(1720)
04-26 17:25:32.299+0700 I/APP_CORE( 2044): appcore-efl.c: __do_app(453) > [APP 2044] Event: RESET State: CREATED
04-26 17:25:32.299+0700 I/CAPI_APPFW_APPLICATION( 2044): app_main.c: app_appcore_reset(245) > app_appcore_reset
04-26 17:25:32.299+0700 I/wnotibp ( 2044): wnotiboard-popup.c: _popup_app_control(170) > popup launch-type: alert
04-26 17:25:32.309+0700 E/wnoti-service( 1447): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1034, is_duplicated : 0
04-26 17:25:32.319+0700 E/wnoti-service( 1447): wnoti-db-client.c: wnoti_get_alert_categories(1311) > view_type : 1, turn_screen_on : 1, allow_gesture : 1, is_used_popup : 0, feedback : 2
04-26 17:25:32.319+0700 E/wnoti-service( 1447): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1034, is_duplicated : 1
04-26 17:25:32.319+0700 E/wnoti-service( 1447): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1034, is_duplicated : 1
04-26 17:25:32.329+0700 E/wnoti-proxy( 2044): wnoti-client.c: _wnoti_parse_extra_sub(373) > JSON_IS_NULL
04-26 17:25:32.329+0700 I/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(226) > application_name: Real Feed Back, application_id: -1016, category_id: 81, time: 1524738331, launch_app_id: (null), bg_image: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, extracted_icon_color: -3139560, disble_block_app_action: 0, support_large_icon 0
04-26 17:25:32.329+0700 I/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(236) > noti_type: 1
04-26 17:25:32.329+0700 I/wnotibp ( 2044): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1034, noti_type: 1
04-26 17:25:32.329+0700 I/wnotibp ( 2044): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 0, content_id: 0, notification_id: 0
04-26 17:25:32.329+0700 W/wnotibp ( 2044): wnotiboard-popup-data.c: _data_convert_alert_data(67) > alert_type: 4, app_feedback_type: 2, popup_view_style: 0, feedback_pattern_app: -1
04-26 17:25:32.329+0700 W/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 0, new_list count : 1
04-26 17:25:32.329+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_notification_change_cb(600) > [1,1034]
04-26 17:25:32.339+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 14
04-26 17:25:32.339+0700 W/AUL_AMD (  969): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-26 17:25:32.339+0700 I/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_check_env_condition(437) > focus app is com.toyota.realtimefeedback, 1
04-26 17:25:32.339+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_add_notification(991) > Add noti_queue [1034, 0]
04-26 17:25:32.349+0700 W/WAKEUP-SERVICE( 1720): ContextualVoice.cpp: InsertContextApp(265) > [0;33mWARNING: [CONTEXTUAL] Test Print ==============================[0;m
04-26 17:25:32.349+0700 W/WAKEUP-SERVICE( 1720): ContextualVoice.cpp: InsertContextApp(270) > [0;33mWARNING: [CONTEXTUAL] Test Print ==============================[0;m
04-26 17:25:32.359+0700 I/APP_CORE( 2044): appcore-efl.c: __do_app(522) > Legacy lifecycle: 1
04-26 17:25:32.359+0700 E/APP_CORE( 2044): appcore-efl.c: __show_cb(856) >  This is child window. Skip!!! WIN:3800009
04-26 17:25:32.359+0700 E/APP_CORE( 2044): appcore-efl.c: __show_cb(856) >  This is child window. Skip!!! WIN:380000d
04-26 17:25:32.369+0700 W/APP_CORE( 2044): appcore-efl.c: __hide_cb(882) > [EVENT_TEST][EVENT] GET HIDE EVENT!!!. WIN:380000d
04-26 17:25:32.369+0700 W/APP_CORE( 2044): appcore-efl.c: __hide_cb(882) > [EVENT_TEST][EVENT] GET HIDE EVENT!!!. WIN:3800009
04-26 17:25:32.379+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(73) > ::APP:: CHECK STATE : 1, 0, (null)
04-26 17:25:32.379+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-26 17:25:32.379+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(79) > ::APP:: CHECK DATA : 1 1 0000
04-26 17:25:32.379+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-26 17:25:32.399+0700 W/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-26 17:25:32.399+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 6
04-26 17:25:32.399+0700 W/AUL     (  969): app_signal.c: aul_update_freezer_status(456) > aul_update_freezer_status pid(2044) type(wakeup)
04-26 17:25:32.399+0700 W/AUL_AMD (  969): amd_request.c: __foward_cmd(161) > __forward_cmd: 1720 1720
04-26 17:25:32.399+0700 I/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_get_active_app_id(1010) > [-1]
04-26 17:25:32.399+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1159) > can't get active win
04-26 17:25:32.409+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: _common_app_id_from_win(152) > 2023
04-26 17:25:32.419+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-26 17:25:32.419+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1178) > [1034, 1, 0, 1, 0000]
04-26 17:25:32.419+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1179) > [0, 1, 0]
04-26 17:25:32.419+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1180) > [1, 0, 0, 0]
04-26 17:25:32.419+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-26 17:25:32.419+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: _view_create_detail_layout(3707) > wnotiboard_popup_vi_type: 2
04-26 17:25:32.419+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: _view_create_detail_layout(3712) > (1034, 1034)
04-26 17:25:32.419+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-26 17:25:32.439+0700 I/efl-extension( 2044): efl_extension_circle_surface.c: eext_circle_surface_layout_add(1290) > Put the surface[0xb9020788]'s widget[0xb901dc90] to layout widget[0xb901d538]
04-26 17:25:32.459+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: _view_create_genlist(3639) > (1034, 1034)
04-26 17:25:32.509+0700 I/efl-extension( 2044): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-26 17:25:32.509+0700 I/efl-extension( 2044): efl_extension_rotary.c: eext_rotary_event_handler_add(77) > init_count: 0
04-26 17:25:32.509+0700 I/efl-extension( 2044): efl_extension_rotary.c: _init_Xi2_system(314) > In
04-26 17:25:32.509+0700 I/efl-extension( 2044): efl_extension_rotary.c: _init_Xi2_system(375) > Done
04-26 17:25:32.539+0700 I/wnotibp ( 2044): wnotiboard-popup-view.c: _view_create_card_data(3073) > 0xb8e7c6b8, 0xb8e7c540, 0xb8e7c540
04-26 17:25:32.549+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb901efa8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:32.549+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb901efa8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-26 17:25:32.589+0700 E/EFL     ( 2044): <2044> elm_main.c:1278 elm_object_style_set() safety check failed: obj == NULL
04-26 17:25:32.589+0700 W/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: stack.separator
04-26 17:25:32.589+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-26 17:25:32.589+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.589+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.589+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.589+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-26 17:25:32.589+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.589+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.589+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.589+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.589+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:25:32.609+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:25:32.609+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-26 17:25:32.609+0700 E/EFL     ( 2044): elementary<2044> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-26 17:25:32.609+0700 W/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(224) > can't get layout
04-26 17:25:32.609+0700 E/EFL     ( 2044): elementary<2044> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-26 17:25:32.609+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:25:32.609+0700 I/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1034, 81, -1016.
04-26 17:25:32.619+0700 W/AUL_AMD (  969): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-26 17:25:32.619+0700 W/AUL_AMD (  969): amd_launch.c: __grab_timeout_handler(1453) > back key ungrab error
04-26 17:25:32.649+0700 E/EFL     ( 2044): <2044> elm_main.c:1278 elm_object_style_set() safety check failed: obj == NULL
04-26 17:25:32.649+0700 E/EFL     ( 2044): evas_main<2044> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-26 17:25:32.649+0700 E/EFL     ( 2044): evas_main<2044> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-26 17:25:32.649+0700 E/EFL     ( 2044): evas_main<2044> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-26 17:25:32.649+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-26 17:25:32.649+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.649+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.649+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.649+0700 I/wnotibp ( 2044): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-26 17:25:32.649+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.649+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.649+0700 F/EFL     ( 2044): evas_main<2044> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Expected: 747ad76c - Evas_Object (Image)
04-26 17:25:32.649+0700 F/EFL     ( 2044):     Supplied: 78c7c73f - Evas_Object (Smart)
04-26 17:25:32.649+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:25:32.649+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:25:32.649+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-26 17:25:32.649+0700 W/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-26 17:25:32.649+0700 I/wnotibp ( 2044): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-26 17:25:32.649+0700 I/wnotibp ( 2044): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1034, 81, -1016.
04-26 17:25:32.659+0700 W/SHealthCommon( 1815): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
04-26 17:25:32.669+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb901efa8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-26 17:25:32.669+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb901efa8 : elm_genlist] cw(360), ch(360), pw(360), ph(360)
04-26 17:25:32.699+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb901efa8 : elm_genlist] mx(0), my(50), minx(0), miny(0), px(0), py(0)
04-26 17:25:32.699+0700 E/EFL     ( 2044): elementary<2044> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb901efa8 : elm_genlist] cw(360), ch(410), pw(360), ph(360)
04-26 17:25:32.699+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 14
04-26 17:25:32.709+0700 W/AUL_AMD (  969): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2044
04-26 17:25:32.709+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 12
04-26 17:25:32.739+0700 W/APP_CORE( 2044): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3800009
04-26 17:25:32.799+0700 I/CAPI_APPFW_APP_CONTROL( 2044): app_control.c: app_control_request_result_broker(187) > app_control_request_result_broker, result: 0
04-26 17:25:32.799+0700 W/CAPI_APPFW_APP_CONTROL( 2044): app_control.c: app_control_error(136) > [app_control_get_extra_data] KEY_NOT_FOUND(0xffffff82)
04-26 17:25:32.799+0700 I/wnotib  ( 2044): w-notification-board-common-context.c: _wnotib_common_context_register_response(145) > APP_CONTROL_RESULT_SUCCEEDED
04-26 17:25:32.799+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnConnectionRequest(411) > _MessagePortIpcServer::OnConnectionRequest
04-26 17:25:32.799+0700 W/STARTER ( 1122): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2044] goes to (3)
04-26 17:25:32.799+0700 W/AUL_AMD (  969): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-26 17:25:32.799+0700 W/AUL_AMD (  969): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-26 17:25:32.799+0700 W/AUL     (  969): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.wnotiboard-popup) pid(2044) status(fg) type(uiapp)
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcClientConnected(172) > MessagePort Ipc connected
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnConnectionRequest(411) > _MessagePortIpcServer::OnConnectionRequest
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnRegisterMessagePort(91) > _MessagePortStub::OnRegisterMessagePort.
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: RegisterMessagePort(83) > _MessagePortService::RegisterMessagePort
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: RegisterMessagePort(95) > Register a message port: [com.samsung.wnotiboard-popup:Voice Wakeup Notification], client = 2044
04-26 17:25:32.819+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-26 17:25:32.829+0700 I/wnotib  ( 2044): w-notification-board-common-context.c: _wnotib_common_register_context_message_port(136) > message_port_id: 1
04-26 17:25:32.829+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(263) > fully_obscured: 0, 0
04-26 17:25:32.829+0700 I/wnotibp ( 2044): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x3800009 0x380000d 0x3800009]
04-26 17:25:32.829+0700 I/APP_CORE( 2044): appcore-efl.c: __do_app(453) > [APP 2044] Event: RESUME State: RUNNING
04-26 17:25:32.829+0700 I/GATE    ( 2044): <GATE-M>APP_FULLY_LOADED_wnotiboard-popup</GATE-M>
04-26 17:25:32.839+0700 E/wnoti-service( 1447): wnoti-db-client.c: wnoti_get_alert_categories(1016) > _query_step failed(NO ROW)
04-26 17:25:32.849+0700 E/wnoti-service( 1447): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 2
04-26 17:25:32.849+0700 E/wnoti-service( 1447): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-26 17:25:32.849+0700 E/wnoti-proxy( 2044): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-26 17:25:32.849+0700 E/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(311) > ::DATA:: No categories available.
04-26 17:25:32.849+0700 W/wnotibp ( 2044): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 1, new_list count : 0
04-26 17:25:32.849+0700 W/wnotibp ( 2044): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2809) > ::UI:: VI TYPE : 2
04-26 17:25:32.849+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-26 17:25:32.849+0700 W/wnotibp ( 2044): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2816) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-26 17:25:32.849+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-26 17:25:32.849+0700 I/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_play_feedback(284) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-26 17:25:32.859+0700 W/TIZEN_N_RECORDER( 2044): recorder_product.c: recorder_is_in_recording(82) > pid =/proc/0 , state =0, alive=0
04-26 17:25:32.859+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_play_feedback(298) > ::APP:: application_id: -1016, is_disaster: 0, disaster_info: (null), app_feedback_type: 2, feedback_pattern_app: -1, is_recording: 0
04-26 17:25:32.859+0700 I/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_play_feedback(307) > Gear side feedback setting is_sound_on: 0, is_vibration_on: 1, is_vibrate_when_noti_on: 0
04-26 17:25:32.859+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_play_feedback(341) > ::APP:: Determined feedback: sound 0, vibration: 1
04-26 17:25:32.859+0700 I/wnotib  ( 2044): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 13, ret: -3, request_id: 0
04-26 17:25:32.859+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_set_smart_relay(501) > Set the smart relay for 0, 81, -1016, Real Feed Back
04-26 17:25:32.859+0700 I/wnotibp ( 2044): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-26 17:25:32.859+0700 W/wnotibp ( 2044): wnotiboard-popup-control.c: _ctrl_turn_on_lcd(534) > [4, 0, 1, 0]
04-26 17:25:32.879+0700 W/SHealthCommon( 1815): TimelineSessionStorage.cpp: OnTriggered(1290) > [1;40;33mlocalStartTime: 1524700800000.000000[0;m
04-26 17:25:32.929+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(585) > ::UI:: [[[ ===> already [small popup] is LOCK, 0010 ]]]
04-26 17:25:32.929+0700 W/wnotibp ( 2044): wnotiboard-popup-view.c: _view_sub_popup_show_animator_pre_cb(1781) > ::UI:: start showing animation
04-26 17:25:32.939+0700 W/SHealthCommon( 1815): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_session_updated, pendingClientInfoList.size(): 0[0;m
04-26 17:25:32.979+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 14
04-26 17:25:32.989+0700 W/AUL_AMD (  969): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2044
04-26 17:25:32.989+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 12
04-26 17:25:32.989+0700 W/SHealthCommon( 1815): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_summary_updated, pendingClientInfoList.size(): 0[0;m
04-26 17:25:32.989+0700 W/SHealthServiceCommon( 1815): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1524675600000.000000, end 1524738333006.000000, calories 1173.367156[0;m
04-26 17:25:32.989+0700 W/SHealthCommon( 1815): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
04-26 17:25:33.239+0700 W/wnotibp ( 2044): wnotiboard-popup-view.c: _view_sub_popup_show_animator_cb(1684) > ::UI:: end show animation
04-26 17:25:33.239+0700 W/wnotibp ( 2044): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-26 17:25:33.349+0700 I/AUL_PAD ( 2073): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-26 17:25:34.469+0700 E/EFL     ( 2023): ecore_x<2023> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=103729 button=1
04-26 17:25:34.469+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] mouse move
04-26 17:25:34.489+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] mouse move
04-26 17:25:34.489+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] hold(0), freeze(0)
04-26 17:25:34.519+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] mouse move
04-26 17:25:34.519+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] hold(0), freeze(0)
04-26 17:25:34.539+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] mouse move
04-26 17:25:34.539+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] hold(0), freeze(0)
04-26 17:25:34.549+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] mouse move
04-26 17:25:34.549+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb824f510 : elm_genlist] hold(0), freeze(0)
04-26 17:25:34.559+0700 E/EFL     ( 2023): ecore_x<2023> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=103827 button=1
04-26 17:25:34.599+0700 I/efl-extension( 2023): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-26 17:25:34.599+0700 I/efl-extension( 2023): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-26 17:25:34.599+0700 I/efl-extension( 2023): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8601038, elm_image, _activated_obj : 0xb83ffa70, activated : 1
04-26 17:25:34.599+0700 I/efl-extension( 2023): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-26 17:25:34.599+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb86056a0 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-26 17:25:34.599+0700 E/EFL     ( 2023): elementary<2023> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb86056a0 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-26 17:25:35.099+0700 W/CRASH_MANAGER( 2079): worker.c: worker_job(1205) > 1102023726561152473833
