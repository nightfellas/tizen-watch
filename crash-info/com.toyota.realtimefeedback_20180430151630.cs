S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2089
Date: 2018-04-30 15:16:30+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2089, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb58b08f0
r2   = 0xb58b0ef8, r3   = 0x00000000
r4   = 0xb831d4a8, r5   = 0xb8512c88
r6   = 0x00000013, r7   = 0xbed93208
r8   = 0x0000199c, r9   = 0xb831d4a8
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb58b0b44, sp   = 0xbed931e8
lr   = 0xb589e1e9, pc   = 0xb589e21e
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:     15088 KB
Buffers:      9844 KB
Cached:     114760 KB
VmPeak:     107448 KB
VmSize:     105284 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23636 KB
VmRSS:       22564 KB
VmData:      45052 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          72 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2089 TID = 2089
2089 2141 2147 

Maps Information
b1d2e000 b1d31000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b1ead000 b1eb1000 r-xp /usr/lib/libogg.so.0.7.1
b1eb9000 b1edb000 r-xp /usr/lib/libvorbis.so.0.4.3
b1ee3000 b1f2a000 r-xp /usr/lib/libsndfile.so.1.0.26
b1f36000 b1f7f000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1f88000 b1f8d000 r-xp /usr/lib/libjson.so.0.0.1
b382e000 b3934000 r-xp /usr/lib/libicuuc.so.57.1
b394a000 b3ad2000 r-xp /usr/lib/libicui18n.so.57.1
b3ae2000 b3aef000 r-xp /usr/lib/libail.so.0.1.0
b3af8000 b3afb000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3b03000 b3b3b000 r-xp /usr/lib/libpulse.so.0.16.2
b3b3c000 b3b3f000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3b47000 b3ba8000 r-xp /usr/lib/libasound.so.2.0.0
b3bb2000 b3bcb000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3bd4000 b3bd8000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3be0000 b3beb000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3bf8000 b3bfc000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3c05000 b3c8c000 rw-s anon_inode:dmabuf
b3c8c000 b3d13000 rw-s anon_inode:dmabuf
b3d1b000 b3d33000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3d44000 b3d4b000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3d53000 b3d5e000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3d66000 b3d67000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3dee000 b3e75000 rw-s anon_inode:dmabuf
b3e75000 b3efc000 rw-s anon_inode:dmabuf
b3efd000 b46fc000 rw-p [stack:2147]
b4906000 b4908000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b4910000 b4911000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4919000 b4921000 r-xp /usr/lib/libfeedback.so.0.1.4
b4931000 b4932000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b49ef000 b49f0000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4af7000 b52f6000 rw-p [stack:2141]
b52f6000 b52f8000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b5300000 b5317000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b5324000 b5326000 r-xp /usr/lib/libdri2.so.0.0.0
b532e000 b5339000 r-xp /usr/lib/libtbm.so.1.0.0
b5341000 b5349000 r-xp /usr/lib/libdrm.so.2.4.0
b5351000 b5353000 r-xp /usr/lib/libgenlock.so
b535b000 b5360000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5368000 b5373000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b557c000 b5646000 r-xp /usr/lib/libCOREGL.so.4.0
b5657000 b5667000 r-xp /usr/lib/libmdm-common.so.1.1.25
b566f000 b5675000 r-xp /usr/lib/libxcb-render.so.0.0.0
b567d000 b567e000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5687000 b568a000 r-xp /usr/lib/libEGL.so.1.4
b5692000 b56a0000 r-xp /usr/lib/libGLESv2.so.2.0
b56a9000 b56f2000 r-xp /usr/lib/libmdm.so.1.2.70
b56fb000 b5701000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5709000 b5712000 r-xp /usr/lib/libcom-core.so.0.0.1
b571b000 b57d3000 r-xp /usr/lib/libcairo.so.2.11200.14
b57de000 b57f7000 r-xp /usr/lib/libnetwork.so.0.0.0
b57ff000 b580b000 r-xp /usr/lib/libnotification.so.0.1.0
b5814000 b5823000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b582c000 b584d000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5855000 b585a000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5862000 b5867000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b586f000 b587f000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5887000 b588f000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5897000 b58a1000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a46000 b5a50000 r-xp /lib/libnss_files-2.13.so
b5a59000 b5b28000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b3e000 b5b62000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b6b000 b5b71000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b79000 b5b7d000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b8a000 b5b95000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b9d000 b5b9f000 r-xp /usr/lib/libiniparser.so.0
b5ba8000 b5bad000 r-xp /usr/lib/libappcore-common.so.1.1
b5bb5000 b5bb7000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5bc0000 b5bc4000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5bd1000 b5bd3000 r-xp /usr/lib/libXau.so.6.0.0
b5bdb000 b5be2000 r-xp /lib/libcrypt-2.13.so
b5c12000 b5c14000 r-xp /usr/lib/libiri.so
b5c1d000 b5daf000 r-xp /usr/lib/libcrypto.so.1.0.0
b5dd0000 b5e17000 r-xp /usr/lib/libssl.so.1.0.0
b5e23000 b5e51000 r-xp /usr/lib/libidn.so.11.5.44
b5e59000 b5e62000 r-xp /usr/lib/libcares.so.2.1.0
b5e6c000 b5e7f000 r-xp /usr/lib/libxcb.so.1.1.0
b5e88000 b5e8b000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e93000 b5e95000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e9e000 b5f6a000 r-xp /usr/lib/libxml2.so.2.7.8
b5f77000 b5f79000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f82000 b5f87000 r-xp /usr/lib/libffi.so.5.0.10
b5f8f000 b5f90000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f98000 b5f9b000 r-xp /lib/libattr.so.1.1.0
b5fa3000 b6037000 r-xp /usr/lib/libstdc++.so.6.0.16
b604a000 b6067000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6071000 b6089000 r-xp /usr/lib/libpng12.so.0.50.0
b6091000 b60a7000 r-xp /lib/libexpat.so.1.6.0
b60b1000 b60f5000 r-xp /usr/lib/libcurl.so.4.3.0
b60fe000 b6108000 r-xp /usr/lib/libXext.so.6.4.0
b6112000 b6116000 r-xp /usr/lib/libXtst.so.6.1.0
b611e000 b6124000 r-xp /usr/lib/libXrender.so.1.3.0
b612c000 b6132000 r-xp /usr/lib/libXrandr.so.2.2.0
b613a000 b613b000 r-xp /usr/lib/libXinerama.so.1.0.0
b6144000 b614d000 r-xp /usr/lib/libXi.so.6.1.0
b6155000 b6158000 r-xp /usr/lib/libXfixes.so.3.1.0
b6161000 b6163000 r-xp /usr/lib/libXgesture.so.7.0.0
b616b000 b616d000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6175000 b6177000 r-xp /usr/lib/libXdamage.so.1.1.0
b617f000 b6186000 r-xp /usr/lib/libXcursor.so.1.0.2
b618e000 b6191000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b619a000 b619e000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b61a7000 b61ac000 r-xp /usr/lib/libecore_fb.so.1.7.99
b61b5000 b6296000 r-xp /usr/lib/libX11.so.6.3.0
b62a1000 b62c4000 r-xp /usr/lib/libjpeg.so.8.0.2
b62dc000 b62f2000 r-xp /lib/libz.so.1.2.5
b62fb000 b62fd000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6305000 b637a000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6384000 b639e000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b63a6000 b63da000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63e3000 b64b6000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b64c2000 b64d2000 r-xp /lib/libresolv-2.13.so
b64d6000 b64ee000 r-xp /usr/lib/liblzma.so.5.0.3
b64f6000 b64f9000 r-xp /lib/libcap.so.2.21
b6501000 b6530000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6538000 b6539000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6542000 b6548000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6550000 b6567000 r-xp /usr/lib/liblua-5.1.so
b6570000 b6577000 r-xp /usr/lib/libembryo.so.1.7.99
b657f000 b6585000 r-xp /lib/librt-2.13.so
b658e000 b65e4000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65f2000 b6648000 r-xp /usr/lib/libfreetype.so.6.11.3
b6654000 b667c000 r-xp /usr/lib/libfontconfig.so.1.8.0
b667d000 b66c2000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b66cb000 b66de000 r-xp /usr/lib/libfribidi.so.0.3.1
b66e6000 b6700000 r-xp /usr/lib/libecore_con.so.1.7.99
b670a000 b6713000 r-xp /usr/lib/libedbus.so.1.7.99
b671b000 b676b000 r-xp /usr/lib/libecore_x.so.1.7.99
b676d000 b6776000 r-xp /usr/lib/libvconf.so.0.2.45
b677e000 b678f000 r-xp /usr/lib/libecore_input.so.1.7.99
b6797000 b679c000 r-xp /usr/lib/libecore_file.so.1.7.99
b67a4000 b67c6000 r-xp /usr/lib/libecore_evas.so.1.7.99
b67cf000 b6810000 r-xp /usr/lib/libeina.so.1.7.99
b6819000 b6832000 r-xp /usr/lib/libeet.so.1.7.99
b6843000 b68ac000 r-xp /lib/libm-2.13.so
b68b5000 b68bb000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b68c4000 b68c5000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b68cd000 b68f0000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68f8000 b68fd000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6905000 b692f000 r-xp /usr/lib/libdbus-1.so.3.8.12
b6938000 b694f000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6957000 b6962000 r-xp /lib/libunwind.so.8.0.1
b698f000 b69ad000 r-xp /usr/lib/libsystemd.so.0.4.0
b69b7000 b6adb000 r-xp /lib/libc-2.13.so
b6ae9000 b6af1000 r-xp /lib/libgcc_s-4.6.so.1
b6af2000 b6af6000 r-xp /usr/lib/libsmack.so.1.0.0
b6aff000 b6b05000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b0d000 b6bdd000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6bde000 b6c3c000 r-xp /usr/lib/libedje.so.1.7.99
b6c46000 b6c5d000 r-xp /usr/lib/libecore.so.1.7.99
b6c74000 b6d42000 r-xp /usr/lib/libevas.so.1.7.99
b6d68000 b6ea4000 r-xp /usr/lib/libelementary.so.1.7.99
b6ebb000 b6ecf000 r-xp /lib/libpthread-2.13.so
b6eda000 b6edc000 r-xp /usr/lib/libdlog.so.0.0.0
b6ee4000 b6ee7000 r-xp /usr/lib/libbundle.so.0.1.22
b6eef000 b6ef1000 r-xp /lib/libdl-2.13.so
b6efa000 b6f07000 r-xp /usr/lib/libaul.so.0.1.0
b6f19000 b6f1f000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f28000 b6f2c000 r-xp /usr/lib/libsys-assert.so
b6f35000 b6f52000 r-xp /lib/ld-2.13.so
b6f5b000 b6f60000 r-xp /usr/bin/launchpad-loader
b820e000 b89bf000 rw-p [heap]
bed73000 bed94000 rw-p [stack]
b820e000 b89bf000 rw-p [heap]
bed73000 bed94000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2089)
Call Stack Count: 21
 0: set_targeted_view + 0xd5 (0xb589e21e) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x721e
 1: pop_from_stack_uib_vc + 0x38 (0xb589e6d5) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x76d5
 2: uib_views_destroy_callback + 0x38 (0xb589e691) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7691
 3: (0xb6c8baf9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6ca2c39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6c9537b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6c95ab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb67b3cf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6e66953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6c513f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6c4ee53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6c5246b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6c527db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6e2344d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x454 (0xb6f1cc69) [/usr/lib/libappcore-efl.so.1] + 0x3c69
15: ui_app_main + 0xb0 (0xb5bc1ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16: uib_app_run + 0xea (0xb589c3fb) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53fb
17: main + 0x34 (0xb589c8e5) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x58e5
18:  + 0x0 (0xb6f5ca53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
19: __libc_start_main + 0x114 (0xb69ce85c) [/lib/libc.so.6] + 0x1785c
20: (0xb6f5ce0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:28.169+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:28.169+0700 W/W_HOME  ( 1204): win.c: win_back_gesture_disable_set(170) > disable back gesture
04-30 15:16:28.169+0700 W/W_HOME  ( 1204): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 0
04-30 15:16:28.169+0700 W/W_INDICATOR( 1113): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 15:16:28.169+0700 W/W_INDICATOR( 1113): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 15:16:28.199+0700 W/APP_CORE( 2103): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3200003
04-30 15:16:28.289+0700 E/AUL     (  965): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-30 15:16:28.289+0700 W/W_HOME  ( 1204): event_manager.c: _window_visibility_cb(460) > Window [0x2600003] is now visible(1)
04-30 15:16:28.289+0700 W/W_HOME  ( 1204): event_manager.c: _window_visibility_cb(470) > state: 1 -> 0
04-30 15:16:28.289+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:28.289+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:6, app_state:1 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:28.289+0700 W/W_HOME  ( 1204): main.c: _window_visibility_cb(996) > Window [0x2600003] is now visible(1)
04-30 15:16:28.299+0700 I/APP_CORE( 1204): appcore-efl.c: __do_app(453) > [APP 1204] Event: PAUSE State: RUNNING
04-30 15:16:28.299+0700 I/CAPI_APPFW_APPLICATION( 1204): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-30 15:16:28.299+0700 W/W_HOME  ( 1204): main.c: _appcore_pause_cb(489) > appcore pause
04-30 15:16:28.299+0700 W/W_HOME  ( 1204): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
04-30 15:16:28.299+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:28.299+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:28.299+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 0, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:28.299+0700 W/W_HOME  ( 1204): rotary.c: rotary_deattach(156) > rotary_deattach:0xafb47080
04-30 15:16:28.299+0700 I/efl-extension( 1204): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:16:28.299+0700 I/efl-extension( 1204): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xafb47080, elm_layout, func : 0xb6eff455
04-30 15:16:28.299+0700 I/efl-extension( 1204): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-30 15:16:28.299+0700 I/efl-extension( 1204): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-30 15:16:28.299+0700 I/efl-extension( 1204): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:16:28.299+0700 I/efl-extension( 1204): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7b3cc90, elm_box, _activated_obj : 0xafb47080, activated : 1
04-30 15:16:28.299+0700 I/efl-extension( 1204): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 E/APPS    ( 1204): AppsItem.cpp: onItemBadgeHide(830) >  (!__pVeBadge) -> onItemBadgeHide() return
04-30 15:16:28.299+0700 W/W_HOME  ( 1204): win.c: win_back_gesture_disable_set(170) > disable back gesture
04-30 15:16:28.299+0700 W/AUL     (  965): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1204) status(bg) type(uiapp)
04-30 15:16:28.299+0700 W/W_INDICATOR( 1113): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 15:16:28.299+0700 W/W_INDICATOR( 1113): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 15:16:28.299+0700 W/STARTER ( 1112): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1204] goes to (4)
04-30 15:16:28.299+0700 E/STARTER ( 1112): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1204)'s state(4)
04-30 15:16:28.299+0700 W/STARTER ( 1112): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2103] goes to (3)
04-30 15:16:28.299+0700 W/AUL     (  965): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-taskmanager) pid(2103) status(fg) type(uiapp)
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-30 15:16:28.309+0700 I/MESSAGE_PORT(  961): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 15:16:28.309+0700 I/wnotib  ( 1204): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 1
04-30 15:16:28.309+0700 E/wnotib  ( 1204): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-30 15:16:28.309+0700 W/wnotib  ( 1204): w-notification-board-noti-manager.c: wnb_nm_postpone_updating_job(997) > Set is_notiboard_update_postponed to true with is_for_VI 0, notiboard panel creation count [1], notiboard card appending count [2].
04-30 15:16:28.309+0700 W/MUSIC_CONTROL_SERVICE( 1768): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1768]   [com.samsung.w-home]register msg port [false][0m
04-30 15:16:28.329+0700 I/APP_CORE( 2103): appcore-efl.c: __do_app(453) > [APP 2103] Event: RESUME State: RUNNING
04-30 15:16:28.329+0700 I/GATE    ( 2103): <GATE-M>APP_FULLY_LOADED_w-taskmanager</GATE-M>
04-30 15:16:28.349+0700 W/APPS    ( 1204): AppsViewNecklace.cpp: onPausedIdlerCb(5178) >  elm_cache_all_flush
04-30 15:16:28.779+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:16:28.789+0700 W/AUL_AMD (  965): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2103
04-30 15:16:28.789+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 15:16:28.799+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:16:28.799+0700 W/AUL_AMD (  965): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2103
04-30 15:16:28.799+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 15:16:28.809+0700 I/APP_CORE( 1204): appcore-efl.c: __do_app(453) > [APP 1204] Event: MEM_FLUSH State: PAUSED
04-30 15:16:28.949+0700 E/EFL     ( 2103): ecore_x<2103> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=219426 button=1
04-30 15:16:28.969+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:28.969+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:28.989+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:28.989+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:28.989+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:28.999+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:29.019+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:29.019+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:29.029+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:29.029+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:29.039+0700 E/EFL     ( 2103): ecore_x<2103> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=219512 button=1
04-30 15:16:29.039+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:16:29.049+0700 W/AUL_AMD (  965): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2136
04-30 15:16:29.049+0700 W/AUL     ( 2103): launch.c: app_request_to_launchpad(284) > request cmd(27) to(2136)
04-30 15:16:29.049+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 27
04-30 15:16:29.049+0700 W/AUL     (  965): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(org.example.uibuildernavigationview) pid(2136) type(uiapp)
04-30 15:16:29.049+0700 I/APP_CORE( 2136): appcore-efl.c: __do_app(453) > [APP 2136] Event: TERMINATE State: PAUSED
04-30 15:16:29.059+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 22
04-30 15:16:29.059+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(999) > app status : 4
04-30 15:16:29.059+0700 W/AUL     ( 2103): launch.c: app_request_to_launchpad(298) > request cmd(27) result(0)
04-30 15:16:29.059+0700 W/APP_CORE( 2136): appcore-efl.c: appcore_efl_main(1788) > power off : 0
04-30 15:16:29.059+0700 W/APP_CORE( 2136): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3800002] -> redirected win[60062f] for org.example.uibuildernavigationview[2136]
04-30 15:16:29.109+0700 I/APP_CORE( 2136): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-30 15:16:29.109+0700 I/CAPI_APPFW_APPLICATION( 2136): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
04-30 15:16:29.139+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.149+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.149+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.159+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.159+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.169+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.179+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.179+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.189+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.199+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.199+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.209+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.209+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.219+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.219+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.229+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.239+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.239+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.249+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.259+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.259+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.269+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.279+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.279+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8cad868 in function: elm_win_alpha_set, of type: 'elm_box' when expecting type: 'elm_win'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9059fb8 in function: elm_grid_clear, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9059fb8 in function: elm_grid_size_set, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_win_alpha_set, of type: '(unknown)' when expecting type: 'elm_win'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_box_homogeneous_set, of type: '(unknown)' when expecting type: 'elm_box'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_box_horizontal_set, of type: '(unknown)' when expecting type: 'elm_box'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_box_padding_set, of type: '(unknown)' when expecting type: 'elm_box'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027898 in function: elm_label_line_wrap_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027898 in function: elm_label_wrap_width_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027898 in function: elm_label_ellipsis_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9027430 in function: elm_box_pack_end, of type: '(unknown)' when expecting type: 'elm_box'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9059fb8 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9059fb8 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:29.289+0700 E/EFL     ( 2136): elementary<2136> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb9059fb8 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:29.289+0700 I/APP_CORE( 2136): appcore-efl.c: __after_loop(1243) > [APP 2136] After terminate() callback
04-30 15:16:29.569+0700 E/EFL     ( 2103): elementary<2103> elm_layout.c:1021 _elm_layout_smart_content_set() could not swallow 0xb73a86a8 into part 'elm.swallow.event.0'
04-30 15:16:29.569+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7379f30 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:16:29.569+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7379f30 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
04-30 15:16:29.579+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7379f30 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-30 15:16:29.629+0700 W/CRASH_MANAGER( 2207): worker.c: worker_job(1205) > 11021367569621525076189
04-30 15:16:29.879+0700 I/efl-extension( 2202): efl_extension.c: eext_mod_init(40) > Init
04-30 15:16:29.919+0700 W/AUL_PAD ( 1898): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2136 pgid = 2136
04-30 15:16:29.919+0700 W/AUL_PAD ( 1898): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 11
04-30 15:16:29.939+0700 E/EFL     ( 2103): ecore_x<2103> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=220416 button=1
04-30 15:16:29.939+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:29.949+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:29.949+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:29.959+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:29.959+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:29.969+0700 W/AUL_PAD ( 1898): sigchild.h: __launchpad_process_sigchld(197) > after __sigchild_action
04-30 15:16:29.969+0700 I/AUL_AMD (  965): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2136
04-30 15:16:29.969+0700 W/AUL     (  965): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2136)
04-30 15:16:29.969+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:29.969+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:29.989+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:29.989+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:29.999+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:29.999+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:29.999+0700 I/UXT     ( 2202): Uxt_ObjectManager.cpp: OnInitialized(753) > Initialized.
04-30 15:16:30.009+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:30.009+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:30.029+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] mouse move
04-30 15:16:30.029+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7379f30 : elm_scroller] hold(0), freeze(0)
04-30 15:16:30.039+0700 E/EFL     (  928): ecore_x<928> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3200003 time=219769
04-30 15:16:30.039+0700 E/EFL     ( 2103): ecore_x<2103> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=219769
04-30 15:16:30.039+0700 E/EFL     (  928): ecore_x<928> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=219769
04-30 15:16:30.059+0700 E/EFL     ( 2103): ecore_x<2103> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=220525 button=1
04-30 15:16:30.059+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 15:16:30.069+0700 W/AUL_AMD (  965): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2089
04-30 15:16:30.069+0700 W/AUL     ( 2103): launch.c: app_request_to_launchpad(284) > request cmd(27) to(2089)
04-30 15:16:30.069+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 27
04-30 15:16:30.069+0700 W/AUL     (  965): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2089) type(uiapp)
04-30 15:16:30.069+0700 W/AUL     ( 2103): launch.c: app_request_to_launchpad(298) > request cmd(27) result(0)
04-30 15:16:30.069+0700 I/APP_CORE( 2089): appcore-efl.c: __do_app(453) > [APP 2089] Event: TERMINATE State: PAUSED
04-30 15:16:30.069+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 22
04-30 15:16:30.069+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(999) > app status : 4
04-30 15:16:30.069+0700 W/APP_CORE( 2089): appcore-efl.c: appcore_efl_main(1788) > power off : 0
04-30 15:16:30.069+0700 W/APP_CORE( 2089): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3400002] -> redirected win[60057d] for com.toyota.realtimefeedback[2089]
04-30 15:16:30.129+0700 I/AUL_PAD ( 2202): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-30 15:16:30.159+0700 I/APP_CORE( 2089): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-30 15:16:30.159+0700 I/CAPI_APPFW_APPLICATION( 2089): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
04-30 15:16:30.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2089): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
04-30 15:16:30.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2089): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
04-30 15:16:30.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2089): preference.c: preference_get_string(1258) > preference_get_string(2089) : URL error
04-30 15:16:30.179+0700 I/efl-extension( 2089): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:16:30.179+0700 I/efl-extension( 2089): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb83427a8 is freed
04-30 15:16:30.179+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb833c190 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 15:16:30.179+0700 I/efl-extension( 2089): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb833cbd8, obj: 0xb833cbd8
04-30 15:16:30.179+0700 I/efl-extension( 2089): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 15:16:30.189+0700 E/EFL     ( 2089): elementary<2089> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 15:16:30.189+0700 E/EFL     ( 2089): elementary<2089> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 15:16:30.189+0700 E/EFL     ( 2089): elementary<2089> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb833cbd8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:16:30.189+0700 E/EFL     ( 2089): elementary<2089> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb833cbd8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:16:30.189+0700 E/EFL     ( 2089): elementary<2089> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb833cbd8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:16:30.189+0700 E/EFL     ( 2089): elementary<2089> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb833cbd8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:16:30.189+0700 I/efl-extension( 2089): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:16:30.189+0700 I/efl-extension( 2089): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb4826a70 is freed
04-30 15:16:30.189+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:16:30.189+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb833cbd8, elm_genlist, func : 0xb5839ea1
04-30 15:16:30.189+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb833c190 in function: elm_grid_clear, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb833c190 in function: elm_grid_size_set, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:30.199+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb833c5f8, , _activated_obj : 0xb87673b0, activated : 1
04-30 15:16:30.199+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8350830 in function: elm_label_line_wrap_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8350830 in function: elm_label_wrap_width_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb8350830 in function: elm_label_ellipsis_set, of type: '(unknown)' when expecting type: 'elm_label'
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb833c190 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb833c190 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb833c190 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb833c190 in function: elm_grid_pack, of type: '(unknown)' when expecting type: 'elm_grid'
04-30 15:16:30.199+0700 I/efl-extension( 2089): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb83caec0, obj: 0xb83caec0
04-30 15:16:30.199+0700 I/efl-extension( 2089): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb83caec0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb83caec0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb83caec0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:16:30.199+0700 E/EFL     ( 2089): elementary<2089> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb83caec0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 15:16:30.199+0700 I/efl-extension( 2089): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:16:30.209+0700 I/efl-extension( 2089): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb84384e0 is freed
04-30 15:16:30.209+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:16:30.209+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb83caec0, elm_genlist, func : 0xb5839ea1
04-30 15:16:30.209+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:16:30.209+0700 I/CAPI_NETWORK_CONNECTION( 2089): connection.c: connection_create(453) > New handle created[0xb4802cb0]
04-30 15:16:30.219+0700 I/APP_CORE( 2089): appcore-efl.c: __after_loop(1243) > [APP 2089] After terminate() callback
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb850bf50 is freed
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb8505388, elm_scroller, func : 0xb583d379
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb87673b0, elm_image, func : 0xb583d379
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:16:30.219+0700 I/efl-extension( 2089): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb8505388 : elm_scroller] rotary callabck is deleted
04-30 15:16:30.239+0700 E/JSON PARSING( 2089): No data could be display
04-30 15:16:30.239+0700 E/JSON PARSING( 2089): 0��X��;
04-30 15:16:30.239+0700 I/CAPI_NETWORK_CONNECTION( 2089): connection.c: connection_destroy(471) > Destroy handle: 0xb4802cb0
04-30 15:16:30.279+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(669) > __request_handler: 22
04-30 15:16:30.279+0700 W/AUL_AMD (  965): amd_request.c: __request_handler(999) > app status : 4
04-30 15:16:30.279+0700 E/APP_CORE( 2103): appcore.c: __del_vconf(453) > [FAILED]vconfkey_ignore_key_changed
04-30 15:16:30.279+0700 I/APP_CORE( 2103): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-30 15:16:30.279+0700 I/APP_CORE( 2103): appcore-efl.c: __after_loop(1234) > [APP 2103] PAUSE before termination
04-30 15:16:30.279+0700 I/CAPI_APPFW_APPLICATION( 2103): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-30 15:16:30.279+0700 I/CAPI_APPFW_APPLICATION( 2103): app_main.c: app_appcore_terminate(177) > app_appcore_terminate
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb7379f30, obj: 0xb7379f30
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-30 15:16:30.289+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7379f30 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 15:16:30.289+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7379f30 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
04-30 15:16:30.289+0700 E/EFL     ( 2103): elementary<2103> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7379f30 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb7397ec0
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb73bbdb0 is freed
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7379f30, elm_scroller, func : 0xb3724379
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7397ec0, elm_image, func : 0xb3724379
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-30 15:16:30.289+0700 I/efl-extension( 2103): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb7379f30 : elm_scroller] rotary callabck is deleted
04-30 15:16:30.309+0700 W/AUL_AMD (  965): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-30 15:16:30.309+0700 W/AUL_AMD (  965): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-30 15:16:30.309+0700 W/AUL     (  965): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1204) status(fg) type(uiapp)
04-30 15:16:30.309+0700 W/STARTER ( 1112): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1204] goes to (3)
04-30 15:16:30.309+0700 E/STARTER ( 1112): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1204)'s state(3)
04-30 15:16:30.309+0700 E/W_TASKMANAGER( 2103): task.c: taskmanager_remove_alltask(647) > [taskmanager_remove_alltask:647] App list is empty!
04-30 15:16:30.309+0700 I/APP_CORE( 2103): appcore-efl.c: __after_loop(1243) > [APP 2103] After terminate() callback
04-30 15:16:30.439+0700 I/UXT     ( 2103): uxt_theme_private.c: uxt_theme_delete_file_monitor(1655) > deleted color config file monitor
04-30 15:16:30.439+0700 I/UXT     ( 2103): uxt_theme_private.c: uxt_theme_delete_file_monitor(1662) > deleted font config file monitor
04-30 15:16:30.439+0700 I/UXT     ( 2103): Uxt_ObjectManager.cpp: OnTerminating(774) > Terminating.
04-30 15:16:30.609+0700 W/PROCESSMGR(  928): e_mod_processmgr.c: _e_mod_processmgr_send_update_watch_action(663) > [PROCESSMGR] =====================> send UpdateClock
04-30 15:16:30.609+0700 W/W_HOME  ( 1204): event_manager.c: _ecore_x_message_cb(421) > state: 1 -> 0
04-30 15:16:30.609+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:30.609+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:0(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:30.609+0700 W/W_INDICATOR( 1113): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 15:16:30.609+0700 W/W_INDICATOR( 1113): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 15:16:30.609+0700 W/W_HOME  ( 1204): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:0(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
04-30 15:16:30.609+0700 W/W_HOME  ( 1204): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 1
04-30 15:16:30.609+0700 W/WATCH_CORE( 1301): appcore-watch.c: __signal_process_manager_handler(1269) > process_manager_signal
04-30 15:16:30.609+0700 I/WATCH_CORE( 1301): appcore-watch.c: __signal_process_manager_handler(1285) > Call the time_tick_cb
04-30 15:16:30.609+0700 I/CAPI_WATCH_APPLICATION( 1301): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 15:16:30.609+0700 E/watchface-app( 1301): watchface.cpp: OnAppTimeTick(1157) > 
04-30 15:16:30.609+0700 I/watchface-app( 1301): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-30 15:16:30.619+0700 W/CRASH_MANAGER( 2207): worker.c: worker_job(1205) > 1102089726561152507619
