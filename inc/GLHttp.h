/*
 * HttpReqGL.h
 *
 *  Created on: Feb 12, 2018
 *      Author: Hartaji
 */

#ifndef HTTPREQGL_H_
#define HTTPREQGL_H_

#include <curl/curl.h>
#include <curl/easy.h>
#include <ecore-1/Ecore.h>
#include <net_connection.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <dlog.h>
#include "app_main.h"
#include "uib_GLChoose_view.h"


void generateGenListItem();
void endCurlGLConnection(connection_h connection,CURL *curl);
void setList(char *data, int *resultCount);
void initGLList(uib_GLChoose_view_context *vc,char *URL);

Ecore_Thread_Cb getAndSetGLList(Ecore_Thread *thread);
static void _selectedGL(char *data, Evas_Object *obj, void *event_info);

#endif /* HTTPREQGL_H_ */
