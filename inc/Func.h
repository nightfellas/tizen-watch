/*
 * Func.h
 *
 *  Created on: Jan 22, 2018
 *      Author: Hartaji
 */

#ifndef FUNC_H_
#define FUNC_H_

#include <tizen.h>
#include <device/haptic.h>

#include "app_main.h"
#include "system_info.h"
#include <dlog.h>
#include <ecore/Ecore.h>
#include <notification.h>


void get_system_info_int(char* key, int *value);
haptic_device_h device_vibrate(int duration, int feedback);
int ShowNotif(char idVehicle[30],char description[255],bool silence);

int ResetNotif();
int stopVibrate();
void deleteNotification();
char* get_write_filepath(char *filename);
int UpdateNotif(notification_h notification);
char splitChar(char *splitChar,const char *delimeter,int limit);

void write_file(
		char *filename,
		const char* buf
		);

void read_file(
		char *filename,
		char* result
		);

void setNotifListItem(
		char jam[200],
		char idVehicle[200],
		char *jobCodes[200],
		char *shopCodes[200],
		char *listDefects[200],
		char *result[504],
		Ecore_Thread *thread
		);

void deleteNotification();
void Toast();
char mallocCharArray(char *string, int length);

#endif /* FUNC_H_ */
