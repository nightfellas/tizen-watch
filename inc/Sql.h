/*
 * Sql.h
 *
 *  Created on: Feb 11, 2018
 *      Author: Hartaji
 */

#ifndef SQL_H_
#define SQL_H_

#include <string.h>
#include <sqlite3.h>
#include <stdint.h>
#include <stdlib.h>
#include <storage.h>
#include <app_common.h>
#include <stdio.h>

typedef struct
{
    int *count;
    char *BODY_NO;

} BodyData;

typedef struct
{
    int  *ID;
    char *BODY_NO;
    char *NOTIF_ID;
    char *PROBLEM;
    char *PROBLEM_DESCRIPTION;
} NotifData;


int opendb();
int initdb();
int Insert(char * bodyno,char * notifid,char * problem,char * problem_description);
//static int selectAllItem(void *data, int argc, char **argv, char **azColName);
static int selectAllItemCount(void *data, int argc, char **argv, char **azColName);
int getAllNotifCount(int* num_of_rows);
int getAllNotif(NotifData **msg_data, int* num_of_rows, char *body_no);
int getAllBodyDefect(BodyData **msg_data, int* num_of_rows);
int deleteALL(char *body_no);
char *getReturnDatas();

#endif /* SQL_H_ */
